/*global exports,require*/
var _ = require('/utils/Underscore');

var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15,
};

exports.ExchangeRate = {
	exchangeView : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF'
	},
	mainView : {
		top:0,
		bottom:0,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgBack : {
		left:0,
		image:'/images/back.png',
	    width:35,
	    height:40
	},
	tabView : {
		top:40,
		height:40,
		left:0,
		right:0
	},
	tabSelView : {
		top:0,
		left:0,
		height:2,
		width:'50%',
		zIndex:10,
		backgroundColor:'#FF243A',
	},
	lblGuaranteed : {
		left:0,
		height:40,
		width:'50%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('whiteFont'),
		backgroundColor:'#6F6F6F',
		sel:'guaranteed'
	},
	lblIndicative : {
		right:0,
		height:40,
		width:'50%',
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('blackFont'),
		backgroundColor:'#E9E9E9',
		sel:'indicative'
	},
	tabSelIconView : {
		top:80,
		left:0,
		height:7,
		width:'50%',
		zIndex:10
	},
	imgTabSel : {
		image:'/images/tab_sel.png',
		top:0,
		height:7,
		width:15
	},
	scrollableView : {
		left:0,
		right:0,
		top:80,
		bottom:0,
		disableBounce:true,
		showPagingControl:false,
		backgroundColor:'#FFF'
	},
	guaranteedView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
		scrollType:'vertical',
		showVerticalScrollIndicator:true
	},
	indicativeView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
		scrollType:'vertical',
		showVerticalScrollIndicator:true
	},
	rateView : {
		top:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		backgroundColor:'#202020'
	},
	lblRate : {
		top:10,
		left:0,
		height:22,
		left:0,
		right:0,
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold20'),
		color:TiFonts.FontStyle('whiteFont')
	},
	lblPaymode : {
		top:35,
		height:16,
		bottom:10,
		left:0,
		right:0,
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('redFont')
	},
	txtRateView : {
		top:10,
		height:30,
		left:0,
		right:0
	}, 
	txtRate : _.defaults({top:0,right:60}, textField),
	lblRateCountry : _.defaults({top:0,right:20,width:40,textAlign:'right',font:TiFonts.FontStyle('lblSwissNormal12'),color:TiFonts.FontStyle('greyFont'),}, label),
	txtValueView : {
		top:10,
		height:30,
		left:0,
		right:0
	}, 
	txtValue : _.defaults({top:0,right:60}, textField),
	lblValueCountry : _.defaults({top:0,right:20,width:40,textAlign:'right',font:TiFonts.FontStyle('lblSwissNormal12'),color:TiFonts.FontStyle('greyFont'),}, label),
	
	lblTransferTxt : {
		top:10,
		height:Ti.UI.SIZE,
		left:20,
		right:20,
		textAlign:'left',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('greyFont')
	},
	
	lblRateInfo : {
		top:10,
		height:Ti.UI.SIZE,
		left:20,
		right:20,
		textAlign:'left',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('greyFont')
	},
	
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	
	btnSendMoney : _.defaults({}, button),
	imgTransferFeesView : {
		top:10,
		width:250,
		height:40,
		image:'/images/transfer_fees.png'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};