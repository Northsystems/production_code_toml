/*global exports,require*/
var _ = require('/utils/Underscore');

var imgTab = {
	width:160,
	height:146
};

var menuTabView = {
	width:'50%',
	height:146
};

exports.Menu = {
	menuView : {
		top:TiGlobals.osname === 'android' ? TiGlobals.platformHeight : parseInt(TiGlobals.platformHeight-20),
		height:TiGlobals.osname === 'android' ? TiGlobals.platformHeight : parseInt(TiGlobals.platformHeight-20),
		left:0,
		right:0,
		backgroundColor:'#fff',
		zIndex:100
	},
	scrollableView : {
		top:40,
		left:0,
		right:0,
		bottom:100,
		disableBounce:true,
		scrollingEnabled:false,
		showPagingControl:false
	},
	mainView : {
		top:0,
		left:0,
		right:0,
		bottom:0,
		layout:'vertical',
		scrollType:'vertical',
		showVerticalScrollIndicator:false,
		disableBounce:true
	},
	
	view1 : {top:0,left:0,right:0,height:146},
	view2 : {top:1,left:0,right:0,height:146},
	view3 : {top:1,left:0,right:0,height:146},
	view4 : {top:1,left:0,right:0,height:146},
	view5 : {top:1,left:0,right:0,height:146},
	exchangeView : _.defaults({top:0,left:0,backgroundColor:'#e8e8e8'}, menuTabView),
	imgExchangeView : _.defaults({image:'/images/exchange_calculator.png'}, imgTab),
	
	transferView : _.defaults({top:0,right:0,backgroundColor:'#dcdcdc'}, menuTabView),
	imgTransferView : _.defaults({image:'/images/transfer_money.png'}, imgTab),
	
	sendingOptionsView : _.defaults({left:0,backgroundColor:'#e8e8e8'}, menuTabView),
	imgSendingOptionsView : _.defaults({image:'/images/sending_options.png'}, imgTab),
	
	trackTransactionView : _.defaults({right:0,backgroundColor:'#dcdcdc'}, menuTabView),
	imgTrackTransactionView : _.defaults({image:'/images/track_transaction.png'}, imgTab),
	
	myAccountView : _.defaults({left:0,backgroundColor:'#e8e8e8'}, menuTabView),
	imgMyAccountView : _.defaults({image:'/images/my_account.png'}, imgTab),
	
	earnView : _.defaults({right:0,backgroundColor:'#dcdcdc'}, menuTabView),
	imgEarnView : _.defaults({image:'/images/earn_free_money.png'}, imgTab),
	
	faqView : _.defaults({left:0,backgroundColor:'#e8e8e8'}, menuTabView),
	imgFAQView : _.defaults({image:'/images/faq.png',}, imgTab),
	
	aboutView : _.defaults({right:0,backgroundColor:'#dcdcdc'}, menuTabView),
	imgAboutView : _.defaults({image:'/images/aboutr2i.png'}, imgTab),
	
	registerView : _.defaults({left:0,backgroundColor:'#e8e8e8'}, menuTabView),
	imgRegisterView : _.defaults({image:'/images/register_menu.png'}, imgTab),
	
	loginView : _.defaults({right:0,backgroundColor:'#dcdcdc'}, menuTabView),
	imgLoginView : _.defaults({image:'/images/login_menu.png'}, imgTab),
	imgLock : {
		image:'/images/lock.png',
		top:10,
		right:10,
		width:15,
		height:15,
	},
	topView : {
		top:0,
		left:0,
		right:0,
		height:40,
	},
	imgCall : {
		left:0,
		height:40,
		width:75,
		image:'/images/call.jpg'
	},
	imgMail : {
		left:85,
		height:40,
		width:73,
		image:'/images/mail.jpg'
	},
	imgLocation : {
		left:163,
		height:40,
		width:35,
		image:'/images/country.jpg'
	},
	locationView : {
		top:15,
		left:198,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		layout:'vertical'
	},
	lblLocation : {
		left:0,
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal10'),
		color:TiFonts.FontStyle('blackFont')
	},
	borderView : {
		top:2,
		left:0,
		width:Ti.UI.SIZE,
		height:1,
		backgroundColor:'#000'
	},
	lblLogout : {
		bottom:59,
		left:0,
		right:0,
		height:41,
		textAlign:'center',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('greyFont')
	},
	
	//////// Sending Options SubView ////////
	
	subView : {
		top:0,
		left:0,
		right:0,
		bottom:0,
		backgroundColor:'#f7f7f7'
	},
	subImage :_.defaults({top:0}, imgTab),
	subMainView : {
		top:146,
		left:0,
		right:0,
		bottom:80,
		backgroundColor:'#fff'
	},
	subTxt : {
		left:25,
		right:20,
		height:20,
		textAlign:'left',
		font:TiFonts.FontStyle('lblSwissNormal16'),
		color:TiFonts.FontStyle('blackFont')
	},
	subBackImage : {
		image:'/images/menu_back.png',
		bottom:0,
		height:80,
		width:80
	},
};