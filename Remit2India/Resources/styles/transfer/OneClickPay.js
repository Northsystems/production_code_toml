/*global exports,require*/
var _ = require('/utils/Underscore');
var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var optionsView = {
	top:20,
	height:35,
	left:20,
	right:20,
	backgroundColor:'transparent',
	borderColor:'transparent'
};

var imgArrow = {
	image:'/images/arrow_dd.png',
    width:40,
    height:40
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

exports.OneClickPay = {
	winOneClickPay : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	mainView : {
		top:40,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		layout:'vertical',
		scrollType:'vertical',
		showVerticalScrollIndicator:true
	},
	
	receiverView : _.defaults({top:0,height:60,left:0,right:0,backgroundColor:'#6f6f6f'}, optionsView),
	receiverMainView : {height:Ti.UI.SIZE,left:20,right:20,layout:'vertical'},
	lblReceiverName : _.defaults({top:0,left:0,height:Ti.UI.SIZE,width:Ti.UI.SIZE,color:TiFonts.FontStyle('whiteFont'),font:TiFonts.FontStyle('lblNormal16'),}, label),
	lblReceiverBankName : _.defaults({top:2,left:0,height:Ti.UI.SIZE,width:Ti.UI.SIZE,color:TiFonts.FontStyle('greyFont'),font:TiFonts.FontStyle('lblNormal12'),}, label),
	
	lblPaymentDetails : _.defaults({top:15,height:Ti.UI.SIZE,left:20,right:20,color:'#ed1b24'}, label),
	lblPaymode : _.defaults({top:8,height:Ti.UI.SIZE,left:20,right:20}, label),
	lblBankDetails : _.defaults({top:0,height:Ti.UI.SIZE,left:20,right:20,font:TiFonts.FontStyle('lblNormal12'),color:TiFonts.FontStyle('greyFont')}, label),
	
	ddView : _.defaults({top:5}, optionsView),
	lblDD : _.defaults({top:0,left:0,right:20}, textField),
	imgDD : _.defaults({right:0}, imgArrow),
	
	txtAmount : _.defaults({top:5,right:20}, textField), 
	lblExchangeRate : {
		top:15,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('redFont'),
	},
	
	tableView : {
		top:0,
		left:0,
		right:0,
		height:Ti.UI.SIZE,
		scrollable:false,
		showVerticalScrollIndicator:false,
		tableSeparatorInsets:{left:0,right:0},
		separatorColor:'#eee',
		backgroundColor:'transparent'
	},
	
	
	btnSendMoney : _.defaults({}, button),
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	borderMainView : {
		top:0,
		left:0,
		right:0,
		height:2,
		backgroundColor:'#eee'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};