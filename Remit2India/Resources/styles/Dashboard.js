/*global exports,require*/
var _ = require('/utils/Underscore');

var textField = {
	height:35,
	left:TiGlobals.osname === 'android' ? 15 : 10,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
	backgroundColor:'transparent',
	borderColor:'transparent',
	borderWidth:1,
	paddingLeft:10,
	paddingRight:20,
	autoComplete:false,
	autocorrect:false,
	autocapitalization:Ti.UI.TEXT_AUTOCAPITALIZATION_NONE
};

var label = {
	height:35,
	textAlign:'left',
	font:TiFonts.FontStyle('lblNormal14'),
	color:TiFonts.FontStyle('blackFont'),
};

var button = {
	top:20,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblSwissBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15,
};

exports.Dashboard = {
	dashboardView : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF'
	},
	mainView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		layout:'vertical'
	},
	scrollableView : {
		left:0,
		right:0,
		top:0,
		height:295,
		disableBounce:true,
		showPagingControl:false,
		backgroundColor:'#fff'
	},
	guaranteedView : {
		left:0,
		right:0,
		top:0,
		height:Ti.UI.SIZE,
		disableBounce:true,
		showPagingControl:false,
		layout:'vertical'
	},
	indicativeView : {
		left:0,
		right:0,
		top:0,
		height:Ti.UI.SIZE,
		disableBounce:true,
		showPagingControl:false,
		layout:'vertical'
	},
	rateView : {
		top:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0,
		backgroundColor:'#202020'
	},
	lblRate : {
		top:20,
		left:0,
		height:22,
		left:0,
		right:0,
		textAlign:'center',
		font:TiFonts.FontStyle('lblSwissBold20'),
		color:TiFonts.FontStyle('whiteFont')
	},
	lblPaymode : {
		top:47,
		height:16,
		bottom:20,
		left:0,
		right:0,
		textAlign:'center',
		font:TiFonts.FontStyle('lblBold12'),
		color:TiFonts.FontStyle('redFont')
	},
	txtRateView : {
		top:0,
		height:30,
		left:0,
		right:0
	}, 
	txtRate : _.defaults({top:0,right:60}, textField),
	lblRateCountry : _.defaults({top:0,right:20,width:40,textAlign:'right',font:TiFonts.FontStyle('lblSwissNormal14'),color:TiFonts.FontStyle('greyFont'),}, label),
	txtValueView : {
		top:5,
		height:30,
		left:0,
		right:0
	},
	// Captions on textfields
	lblAmountTxt : {
		top:15,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont')
	},
	txtValue : _.defaults({top:0,right:60}, textField),
	lblValueCountry : _.defaults({top:0,right:20,width:40,textAlign:'right',font:TiFonts.FontStyle('lblSwissNormal14'),color:TiFonts.FontStyle('greyFont'),}, label),
	
	borderView : {
		top:0,
		left:20,
		right:20,
		height:1,
		backgroundColor:'#000'
	},
	
	btnSendMoney : _.defaults({}, button),
	done : {
		title:'Done',
		height:35,
		width:50
	},
	iconView : {
		top:295,
		left:0,
		right:0,
		height:Ti.UI.FILL
	},
	tansferView : {
		top:0,
		left:0,
		width:'50%',
		bottom:0,
		backgroundColor:'#e8e8e8'
	},
	imgTransfer : {
		image:'/images/transfer_money.png',
		width:160,
		height:146,
	},
	imgLock : {
		image:'/images/lock.png',
		top:10,
		right:10,
		width:15,
		height:15,
	},
	iconView : {
		top:0,
		left:0,
		right:0,
		height:Ti.UI.FILL
	},
	aboutView : {
		top:0,
		right:0,
		width:'50%',
		bottom:0,
		backgroundColor:'#dcdcdc'
	},
	imgAbout : {
		image:'/images/aboutr2i.png',
		width:160,
		height:146,
	}
};