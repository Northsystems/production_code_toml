/*global exports,require*/
var _ = require('/utils/Underscore');

var button = {
	width: 90,
	height: 90,
	borderRadius: TiGlobals.osname === 'android' ? 100 : 45
};

var lblTxt = {
	touchEnabled: false,
	color: "#fff",
	height:Ti.UI.SIZE,
	textAlign: 'center',
	font:TiFonts.FontStyle('lblThinNormal45'),
	color:TiFonts.FontStyle('whiteFont'),
};

var lblTxtChar = {
	touchEnabled: false,
	color: "#fff",
	bottom: 20,
	height:Ti.UI.SIZE,
	textAlign: 'center',
	font:TiFonts.FontStyle('lblNormal10'),
	color:TiFonts.FontStyle('whiteFont'),
};

exports.Passcode = {
	winPasscode : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'transparent',
		fullscreen:false,
		navBarHidden:true
	},
	maskView : {
		width: TiGlobals.platformWidth,
		height: TiGlobals.platformHeight,
		backgroundColor: '#E6000000'
	},
	insetView : {
		left: 20,
		right: 20,
		height: Ti.UI.SIZE,
		key:''
	},
	lblCode : {
		top: 20,
		height:Ti.UI.SIZE,
		textAlign: 'center',
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('whiteFont'),
		key:''
	},
	okView : {
		top: 40,
		height: 40,
		width:Ti.UI.SIZE,
		layout:'horizontal',
		horizontalWrap:false,
		key:''
	},
	btnView1 : _.defaults({top:80, left:0, key:1}, button),
	btnView2 : _.defaults({top:80, key:2}, button),
	btnView3 : _.defaults({top:80, right:0, key:3}, button),
	btnView4 : _.defaults({top:160, left:0, key:4}, button),
	btnView5 : _.defaults({top:160, key:5}, button),
	btnView6 : _.defaults({top:160, right:0, key:6}, button),
	btnView7 : _.defaults({top:250, left:0, key:7}, button),
	btnView8 : _.defaults({top:250, key:8}, button),
	btnView9 : _.defaults({top:250, right:0, key:9}, button),
	btnView10 : _.defaults({top:340, key:0}, button),
	btnDelete : {
		right: 0,
		width: 90,
		height: 90,
		top: 340,
		font:TiFonts.FontStyle('lblNormal14'),
		color:TiFonts.FontStyle('whiteFont'),
		borderRadius: TiGlobals.osname === 'android' ? 100 : 45,
		key:'del',
		backgroundColor:'transparent',
	},
	
	lblTxt1 : _.defaults({}, lblTxt),
	lblTxt2 : _.defaults({}, lblTxt),
	lblTxt3 : _.defaults({}, lblTxt),
	lblTxt4 : _.defaults({}, lblTxt),
	lblTxt5 : _.defaults({}, lblTxt),
	lblTxt6 : _.defaults({}, lblTxt),
	lblTxt7 : _.defaults({}, lblTxt),
	lblTxt8 : _.defaults({}, lblTxt),
	lblTxt9 : _.defaults({}, lblTxt),
	lblTxt10 : _.defaults({}, lblTxt),
	
	lblForgotMPIN : _.defaults({width:Ti.UI.SIZE, height:40, bottom:20, zIndex:100, font:TiFonts.FontStyle('lblNormal14'), touchEnabled: true}, lblTxt),
	
	croutonView : {
		bottom:-parseInt(TiGlobals.platformHeight+60),
		left:0,
		right:0,
		height: 40,
		backgroundColor:'#FD483F'
	},
	lblCrouton : {
		height:Ti.UI.SIZE,
		textAlign: 'center',
		font:TiFonts.FontStyle('lblBold14'),
		color:TiFonts.FontStyle('whiteFont'),
	}

};