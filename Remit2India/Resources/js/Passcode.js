exports.Passcode = function()
{
	var _obj = {
		style : require('/styles/Passcode').Passcode,
		winPasscode : null,
		maskView : null,
		insetView : null,
		lblCode : null,
		okView : null,
		btnView1 : null,
		lblTxt1 : null,
		btnView2 : null,
		lblTxt2 : null,
		btnView3 : null,
		lblTxt3 : null,
		btnView4 : null,
		lblTxt4 : null,
		btnView5 : null,
		lblTxt5 : null,
		btnView6 : null,
		lblTxt6 : null,
		btnView7 : null,
		lblTxt7 : null,
		btnView8 : null,
		lblTxt8 : null,
		btnView9 : null,
		lblTxt9 : null,
		btnView10 : null,
		lblTxt10 : null,
		btnDelete : null,
		croutonView : null,
		maskView : null,
		code : null,
		codeLength : null,
		promptMode : null,
		errtimes : null,
		attempts : null,
		cc : null,
		ERR_WIDTH : null,
		ERR_TIME : null,
	};
	
	//////////////////////////// Create UI ////////////////////////////
    function createUI ()
	{
		//////////////// Global Modal //////////////
		
		_obj.winPasscode = Ti.UI.createWindow(_obj.style.winPasscode);
		
		// Activity Indicator Assign
		var ActivityIndicator = require('/utils/ActivityIndicator');
		var activityIndicator = new ActivityIndicator(_obj.winPasscode);
		
		_obj.maskView = Ti.UI.createView(_obj.style.maskView);
		
		_obj.insetView = Ti.UI.createView(_obj.style.insetView);
		
		_obj.lblCode = Ti.UI.createLabel(_obj.style.lblCode);
		_obj.lblCode.text = 'Insert mPIN';
		
		_obj.okView = Ti.UI.createView(_obj.style.okView);
		
		_obj.btnView1 = Ti.UI.createView(_obj.style.btnView1);
		
		_obj.lblTxt1 = Ti.UI.createLabel(_obj.style.lblTxt1);
		_obj.lblTxt1.text = '1';
		
		_obj.btnView2 = Ti.UI.createView(_obj.style.btnView2);
		
		_obj.lblTxt2 = Ti.UI.createLabel(_obj.style.lblTxt2);
		_obj.lblTxt2.text = '2';
		
		_obj.btnView3 = Ti.UI.createView(_obj.style.btnView3);
		
		_obj.lblTxt3 = Ti.UI.createLabel(_obj.style.lblTxt3);
		_obj.lblTxt3.text = '3';
		
		_obj.btnView4 = Ti.UI.createView(_obj.style.btnView4);
		
		_obj.lblTxt4 = Ti.UI.createLabel(_obj.style.lblTxt4);
		_obj.lblTxt4.text = '4';
		
		_obj.btnView5 = Ti.UI.createView(_obj.style.btnView5);
		
		_obj.lblTxt5 = Ti.UI.createLabel(_obj.style.lblTxt5);
		_obj.lblTxt5.text = '5';
		
		_obj.btnView6 = Ti.UI.createView(_obj.style.btnView6);
		
		_obj.lblTxt6 = Ti.UI.createLabel(_obj.style.lblTxt6);
		_obj.lblTxt6.text = '6';
		
		_obj.btnView7 = Ti.UI.createView(_obj.style.btnView7);
		
		_obj.lblTxt7 = Ti.UI.createLabel(_obj.style.lblTxt7);
		_obj.lblTxt7.text = '7';
		
		_obj.btnView8 = Ti.UI.createView(_obj.style.btnView8);
		
		_obj.lblTxt8 = Ti.UI.createLabel(_obj.style.lblTxt8);
		_obj.lblTxt8.text = '8';
		
		_obj.btnView9 = Ti.UI.createView(_obj.style.btnView9);
		
		_obj.lblTxt9 = Ti.UI.createLabel(_obj.style.lblTxt9);
		_obj.lblTxt9.text = '9';
		
		_obj.btnView10 = Ti.UI.createView(_obj.style.btnView10);
		
		_obj.lblTxt10 = Ti.UI.createLabel(_obj.style.lblTxt10);
		_obj.lblTxt10.text = '0';
		
		_obj.btnDelete = Ti.UI.createButton(_obj.style.btnDelete);
		_obj.btnDelete.title = 'Delete';
		
		_obj.lblForgotMPIN = Ti.UI.createLabel(_obj.style.lblForgotMPIN);
		_obj.lblForgotMPIN.text = 'Forgot MPIN?';
		
		_obj.maskView.add(_obj.insetView);
		_obj.insetView.add(_obj.lblCode);
		_obj.insetView.add(_obj.okView);
		
		_obj.btnView1.add(_obj.lblTxt1);
		_obj.insetView.add(_obj.btnView1);
		
		_obj.btnView2.add(_obj.lblTxt2);
		_obj.insetView.add(_obj.btnView2);
		
		_obj.btnView3.add(_obj.lblTxt3);
		_obj.insetView.add(_obj.btnView3);
		
		_obj.btnView4.add(_obj.lblTxt4);
		_obj.insetView.add(_obj.btnView4);
		
		_obj.btnView5.add(_obj.lblTxt5);
		_obj.insetView.add(_obj.btnView5);
		
		_obj.btnView6.add(_obj.lblTxt6);
		_obj.insetView.add(_obj.btnView6);
		
		_obj.btnView7.add(_obj.lblTxt7);
		_obj.insetView.add(_obj.btnView7);
		
		_obj.btnView8.add(_obj.lblTxt8);
		_obj.insetView.add(_obj.btnView8);
		
		_obj.btnView9.add(_obj.lblTxt9);
		_obj.insetView.add(_obj.btnView9);
		
		_obj.btnView10.add(_obj.lblTxt10);
		_obj.insetView.add(_obj.btnView10);
		
		_obj.insetView.add(_obj.btnDelete);
		
		_obj.winPasscode.add(_obj.maskView);
		_obj.winPasscode.add(_obj.lblForgotMPIN);
		
		_obj.winPasscode.open();
		
		_obj.winPasscode.addEventListener('androidback', function(){});
		
		_obj.lblForgotMPIN.addEventListener('click',function(e){
			require('/js/ForgotMPINModal').ForgotMPINModal();
		});
		
		/////////// Logic start ////////////
		
		_obj.code = '';
		_obj.codeLength = 6;
		_obj.promptMode = false;
		_obj.errtimes = 0;
		_obj.attempts = 5;
		_obj.cc = [];
		_obj.ERR_WIDTH = 50;
		_obj.ERR_TIME = 90;
		
		var _ = require('/utils/Underscore');
		
		function setPromptMode(len) {
			_obj.promptMode = true;
			_obj.code = null;
			_obj.codeLength = len;
		}
		
		_obj.attempts = 1;
		Ti.App.Properties.setString('attempts',_obj.attempts);
		
		function validate() {
			var code = '';
			
			for (var i = 0; i < _obj.cc.length; i++) {
				code = code + _obj.cc[i]; 
			}
			
			var cipher = 'rijndael-128';
			var mode = 'ecb';
			var iv = null;
			var base64 = require('/lib/Crypt/Base64');
			
			require('/lib/Crypt/AES').Crypt(null,null,null, key, cipher, mode);
			var ciphertext = require('/lib/Crypt/AES').Encrypt(true, code, iv, key, cipher, mode);
			ciphertext = base64.encode(ciphertext);
			
			activityIndicator.showIndicator();
			
			var xhr = require('/utils/XHR');
			var base64 = require('/lib/Crypt/Base64');
			
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"LOGINWITHMPIN",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+
					'"noofAttempts":"'+Ti.App.Properties.getString('attempts')+'",'+
					'"mPin":"'+ciphertext+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});

			function xhrSuccess(e) {
				activityIndicator.hideIndicator();
				if(e.result.responseFlag === "S")
				{
					// if success - login
					Ti.App.Properties.setString('ownerId',e.result.ownerId);
					Ti.App.Properties.setString('loginId',e.result.loginId);
					Ti.App.Properties.setString('customerType',e.result.customerType);
					Ti.App.Properties.setString('firstName',e.result.firstName);
					Ti.App.Properties.setString('lastName',e.result.lastName);
					Ti.App.Properties.setString('forceChangeFlag',e.result.forceChangeFlag);
					Ti.App.Properties.setString('lastLoginTime',e.result.lastLoginTime);
					Ti.App.Properties.setString('sessionId',e.result.sessionId);
					//Ti.API.info("******",e.result.sessionId);
					Ti.App.Properties.setString('loginStatus','1');
					
					Ti.App.fireEvent('loginData');
					
					var xmlHttpACS = require('/utils/XHR_BCM');
					xmlHttpACS.call({
						url : TiGlobals.pushURL,
						get : '',
						post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"PUSHLOGIN",'+
						'"source":"mobile",'+
						'"platform":"'+TiGlobals.osname+'",'+
						'"deviceid":"'+Ti.App.Properties.getString('deviceToken')+'",'+
						'"uId":"'+Ti.App.Properties.getString('ownerId')+'",'+
						'"channelId":"'+TiGlobals.pushChannelId+'"'+
						'}',
						success : xmlHttpACSSuccess,
						error : xmlHttpACSError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
					
					function xmlHttpACSSuccess(eACS) {
						if (eACS.result.status === 'S') {
							activityIndicator.hideIndicator();
							require('/utils/RemoveViews').removeAllScrollableViews();
							destroyPasscode();
							Ti.App.Properties.removeProperty('attempts');
						}
						xmlHttpACS = null;
					}
					
					function xmlHttpACSError(eACS) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xmlHttpACS = null;
					}
				}
				else
				{
					activityIndicator.hideIndicator();
					
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast(e.result.message);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast(e.result.message);
					}
					
					_obj.attempts = _obj.attempts + 1;
					Ti.App.Properties.setString('attempts',_obj.attempts);
					resetUI(true);
				}
			}

			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xmlhttp = null;
			}
		}
		
		function setCodeLengthUI() {
			_.each(_obj.okView.children || [], function($c) { _obj.okView.remove($c); });
		
			for (var i = 0; i < _obj.codeLength; i++) {
				_obj.okView.add(Ti.UI.createView({
					left: i>0 ? 10 : 0,
					width: 10,
					height: 10,
					borderColor: '#AFFF',
					borderWidth: 1,
					borderRadius: 5
				}));
			}
		}
		
		function resetUI(animate) {
			_obj.cc = [];
			_.each(_obj.okView.children, function($c){ $c.backgroundColor = 'transparent'; });
			
			if (animate) {
				var m = Ti.UI.create2DMatrix();
				var ms = [
					m.translate(-_obj.ERR_WIDTH, 0),
					m.translate(_obj.ERR_WIDTH, 0),
					m.translate(-_obj.ERR_WIDTH/2, 0),
					m.translate(_obj.ERR_WIDTH/2, 0),
					m
				];
				
				for (var i=0; i<ms.length; i++) {
					setTimeout(_.bind(function(j){
						_obj.okView.animate({ transform: ms[j], duration: _obj.ERR_TIME });
					}, {}, i), i * _obj.ERR_TIME);
				}
			}
		}
		
		// Initialize
		
		setCodeLengthUI();
		
		
		function touchStart(e)
		{
			if(e.source.key === '')
			{return;}
			
			if(e.source.key !== 'del')
			{
				e.source.backgroundColor = '#33EEEEEE';
				
				if (_obj.cc.length >= _obj.codeLength) return;
				_obj.cc.push(parseInt(e.source.key, 10));
			
				_obj.okView.children[_obj.cc.length-1].backgroundColor = '#AFFF';
				if (_obj.cc.length !== _obj.codeLength) return;
				validate();
			}
			else
			{
				e.source.backgroundColor = '#33EEEEEE';
				if (_obj.cc.length > 0) {
					_obj.okView.children[_obj.cc.length-1].backgroundColor = 'transparent';
					_obj.cc.pop();
				}
			}
		}
		
		function touchEnd(e)
		{
			if(e.source.key !== '')
			{
				e.source.backgroundColor = 'transparent';
			}
		}
		
		_obj.insetView.addEventListener('touchstart', touchStart);
		_obj.insetView.addEventListener('touchend', touchEnd);
	
		function destroyPasscode()
		{
			try{
				require('/utils/Console').info('Lock Screen Delete Start');
				
				_obj.winPasscode.close();
				
				setTimeout(function(e){
					_obj.insetView.removeEventListener('touchstart', touchStart);
					_obj.insetView.removeEventListener('touchend', touchEnd);
					
					require('/utils/RemoveViews').removeViews(_obj.winPasscode,_obj.maskView);
					_obj = null;	
				},300);
				require('/utils/Console').info('Lock Screen Delete End');
			}
			catch(e)
			{require('/utils/Console').info(e);}
		}
	}
	
	createUI();
	
}; // Passcode()