exports.ScheduleTransactionConfirmationModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Schedule Payments Pre-Confirmation');
	
	var _obj = {
		style : require('/styles/schedule_payment/ScheduleTransactionConfirmation').ScheduleTransactionConfirmation,
		winScheduleTransactionConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblAddressHeader : null,
		lblAddress : null,
		lblTxt : null,
		lblEmail : null,
		emailView : null,
		btnPaymentList : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winScheduleTransactionConfirmation = Ti.UI.createWindow(_obj.style.winScheduleTransactionConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winScheduleTransactionConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Easy EMI Payments';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.lblAddressHeader = Ti.UI.createLabel(_obj.style.lblAddressHeader);
	_obj.lblAddressHeader.text = 'Your transaction is confirmed!';
	
	_obj.lblAddress = Ti.UI.createLabel(_obj.style.lblAddress);
	_obj.lblAddress.text = 'You have successfully set up your Easy EMI Payment schedule. Kindly ensure that you have the requisite funds in your account for us to process your transactions.';
	
	_obj.emailView = Ti.UI.createView(_obj.style.emailView);
	
	_obj.lblTxt = Ti.UI.createLabel(_obj.style.lblTxt);
	_obj.lblTxt.text = 'Should you have any clarifications, please write to us at ';
	
	_obj.lblEmail = Ti.UI.createLabel(_obj.style.lblEmail);
	_obj.lblEmail.text = Ti.App.Properties.getString('sourceCountryInfoEmail');
	
	_obj.lblEmail.addEventListener('click',function(e){
		var emailDialog = Titanium.UI.createEmailDialog();
		emailDialog.subject = '';
		emailDialog.messageBody = '';
		emailDialog.toRecipients = [Ti.App.Properties.getString('sourceCountryInfoEmail')];
		emailDialog.open();
	});
	
	_obj.btnPaymentList = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnPaymentList.title = 'PAYMENT LIST';
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	
	_obj.mainView.add(_obj.lblAddressHeader);
	_obj.mainView.add(_obj.lblAddress);
	
	_obj.emailView.add(_obj.lblTxt);
	_obj.emailView.add(_obj.lblEmail);
	_obj.mainView.add(_obj.emailView);
	
	_obj.mainView.add(_obj.btnPaymentList);
	_obj.globalView.add(_obj.mainView);
	_obj.winScheduleTransactionConfirmation.add(_obj.globalView);
	_obj.winScheduleTransactionConfirmation.open();
	
	_obj.btnPaymentList.addEventListener('click',function(e){
		destroy_scheduletxnconf();
		require('/js/schedule_payment/ScheduleTransactionModal').ScheduleTransactionModal();
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_scheduletxnconf();
	});
	
	_obj.winScheduleTransactionConfirmation.addEventListener('androidback',function(e){
		destroy_scheduletxnconf();
	});
	
	function destroy_scheduletxnconf()
	{
		try{
			
			require('/utils/Console').info('############## Remove ScheduleTransactionConfirmation start ##############');
			
			_obj.winScheduleTransactionConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winScheduleTransactionConfirmation);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_scheduletxnconf',destroy_scheduletxnconf);
			require('/utils/Console').info('############## Remove ScheduleTransactionConfirmation end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_scheduletxnconf', destroy_scheduletxnconf);
}; // ScheduleTransactionConfirmationModal()