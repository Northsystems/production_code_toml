exports.ScheduleTxnDetailsModal = function(params)
{
	require('/lib/analytics').GATrackScreen('Schedule Payment Details');
	
	var _obj = {
		style : require('/styles/schedule_payment/ScheduleTxnDetails').ScheduleTxnDetails,
		winScheduleTxnDetails : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		scrollableView : null,
		stepView1 : null,
		tblPaymentDetails : null,
		stepView2 : null,
		tblEasyEMI : null,
		
		tab : null,
		start : 0,
		last : 0,
		page : 1,
		limit : 10,
		totalPages : 1,
		totalRecords : 0,
		lastDistance : 0,
		updating : false
	};
	
	_obj.tab = 'payment';
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var destCountryName = Ti.App.Properties.getString('destinationCountryCurName');
	
	_obj.winScheduleTxnDetails = Titanium.UI.createWindow(_obj.style.winScheduleTxnDetails);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winScheduleTxnDetails);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Schedule Payment Details';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.lblPaymentDetails = Ti.UI.createLabel(_obj.style.lblPaymentDetails);
	_obj.lblPaymentDetails.text = 'Payment Details';
	_obj.lblEasyEMI = Ti.UI.createLabel(_obj.style.lblEasyEMI);
	_obj.lblEasyEMI.text = 'Easy EMI Details';
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
	
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.tabView.add(_obj.lblPaymentDetails);
	_obj.tabView.add(_obj.lblEasyEMI);
	_obj.tabView.add(_obj.tabSelView);
	_obj.mainView.add(_obj.tabView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.mainView.add(_obj.tabSelIconView);
	
	_obj.tabView.addEventListener('click',function(e){
		if(e.source.sel === 'payment' && _obj.tab !== 'payment')
		{
			_obj.scrollableView.movePrevious();
		}
		
		if(e.source.sel === 'emi' && _obj.tab !== 'emi')
		{
			_obj.scrollableView.moveNext();
		}
	});
	
	_obj.scrollableView.addEventListener('scroll',function(e){
		if(e.currentPage === 0)
		{
			changeTabs('payment');
			_obj.tab = 'payment';
		}
		
		if(e.currentPage === 1)
		{
			changeTabs('emi');
			_obj.tab = 'emi';
		}
	});
	
	function changeTabs(selected)
	{
		if(selected === 'payment')
		{
			_obj.tabSelView.left = 0;
			_obj.tabSelView.right = null;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			_obj.tabSelIconView.width = '50%';
			_obj.lblPaymentDetails.color = TiFonts.FontStyle('whiteFont');
			_obj.lblPaymentDetails.backgroundColor = '#6F6F6F';
			_obj.lblEasyEMI.color = TiFonts.FontStyle('blackFont');
			_obj.lblEasyEMI.backgroundColor = '#E9E9E9';
		}
		else
		{
			_obj.tabSelView.left = null;
			_obj.tabSelView.right = 0;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = null;
			_obj.tabSelIconView.right = 0;
			_obj.tabSelIconView.width = '50%';
			_obj.lblEasyEMI.color = TiFonts.FontStyle('whiteFont');
			_obj.lblEasyEMI.backgroundColor = '#6F6F6F';
			_obj.lblPaymentDetails.color = TiFonts.FontStyle('blackFont');
			_obj.lblPaymentDetails.backgroundColor = '#E9E9E9';
		}
	}
	
	/////////////////// Payment Details ///////////////////
	
	_obj.stepView1 = Ti.UI.createView(_obj.style.stepView1);
	
	_obj.tblPaymentDetails = Ti.UI.createTableView(_obj.style.tableView);
	_obj.tblPaymentDetails.top = 7;
	_obj.stepView1.add(_obj.tblPaymentDetails);
	
	_obj.tblPaymentDetails.addEventListener('scroll',function(e){
		
		require('/utils/Console').info(_obj.page +' < '+ _obj.totalPages);
		if(_obj.page < _obj.totalPages)
		{
			if (TiGlobals.osname === 'android')
			{
		        if((e.firstVisibleItem + e.visibleItemCount) === (_obj.page * _obj.limit))
				{
					_obj.page = _obj.page + 1;
		            populatePaymentDetailsPaginate();
			    }
		    }
		    else
		    {
		        var offset = e.contentOffset.y;
		        var height = e.size.height;
		        var total = offset + height;
		        var theEnd = e.contentSize.height;
		        var distance = theEnd - total;
		         
		        // going down is the only time we dynamically load,
		        // going up we can safely ignore -- note here that
		        // the values will be negative so we do the opposite
		        if (distance < _obj.lastDistance)
		        {
		            // adjust the % of rows scrolled before we decide to start fetching
		            var nearEnd = theEnd;
		 
		            if (!_obj.updating && (total >= nearEnd))
		            {
		               _obj.page = _obj.page + 1;
		               populatePaymentDetailsPaginate();
		            }
		        }
		        _obj.lastDistance = distance;
		    }    
		}
	});
	
	function populatePaymentDetails()
	{
		_obj.start = 1;
		_obj.last = _obj.limit;
		
		_obj.tblPaymentDetails.setData([]);
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETSIBOOKEDTXNDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"rangeFrom":"'+_obj.start+'",'+
				'"rangeTo":"'+_obj.last+'",'+
				'"operatingmodeId":"INTL"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			_obj.totalRecords = parseInt(e.result.totalCount);	
			
			_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
			
			if(e.result.responseFlag === "S")
			{
				if(_obj.totalRecords === 0)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'payment_details',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : '',
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblPaymentDetails.appendRow(row);
				}
				else
				{
					for(var i=0; i<e.result.siDetailsData.length; i++)
					{
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 163,
							backgroundColor : 'transparent',
							className : 'payment_details',
							rw : 1
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var topView = Ti.UI.createView({
							top : 0,
							left : 0,
							right : 0,
							height : 50,
							backgroundColor : '#6f6f6f',
							layout:'horizontal',
							horizontalWrap:false
						});
						
						var detailTxnView = Ti.UI.createView({
							top : 0,
							left : 0,
							width : '50%',
							height : 50
						});
						
						var txnView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var dt = e.result.siDetailsData[i].transactionDate.split('-');
						
						var lblDate = Ti.UI.createLabel({
							text : 'Date ' + dt[0]+'/'+require('/utils/Date').monthNo(dt[1])+'/'+dt[2],
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblBank = Ti.UI.createLabel({
							text : e.result.siDetailsData[i].bank,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						var borderView = Ti.UI.createView({
							height : 50,
							top : 0,
							left : 0,
							width:1,
							backgroundColor:'#898989'
						});
						
						var detailAmountView = Ti.UI.createView({
							left : 0,
							right : 0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblAmountTxt = Ti.UI.createLabel({
							text:'Amount', 
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblAmount = Ti.UI.createLabel({
							text:e.result.siDetailsData[i].amount, 
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						var bottomView = Ti.UI.createView({
							top : 50,
							left : 0,
							right : 0,
							height : 56,
							backgroundColor : '#fff',
							layout:'horizontal',
							horizontalWrap:false
						});
						
						var detailAccountView = Ti.UI.createView({
							top : 0,
							left : 0,
							width : '50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var accountView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblAccountHead = Ti.UI.createLabel({
							text : 'Account No.',
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						var lblAccountNo = Ti.UI.createLabel({
							text : e.result.siDetailsData[i].accountNumber,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var detailRTRNView = Ti.UI.createView({
							top : 0,
							left : 0,
							width:'50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var rtrnView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblRTRNHead = Ti.UI.createLabel({
							text : 'RTRN*',
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						var lblRTRN = Ti.UI.createLabel({
							text : e.result.siDetailsData[i].rtrn,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var borderView1 = Ti.UI.createView({
							top : 106,
							left : 0,
							right : 0,
							height : 1,
							backgroundColor : '#eee'
						});
						
						var bottomView1 = Ti.UI.createView({
							top : 107,
							left : 0,
							right : 0,
							height : 56,
							backgroundColor : '#fff',
							layout:'horizontal',
							horizontalWrap:false
						});
						
						var detailRoutingView = Ti.UI.createView({
							top : 0,
							left : 0,
							width : '50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var routingView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblRoutingHead = Ti.UI.createLabel({
							text : 'Routing No.',
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						var lblRoutingNo = Ti.UI.createLabel({
							text : e.result.siDetailsData[i].routingNumber,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var detailStatusView = Ti.UI.createView({
							top : 0,
							left : 0,
							width:'50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var statusView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblStatusHead = Ti.UI.createLabel({
							text : 'Status',
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						var lblStatus = Ti.UI.createLabel({
							text : e.result.siDetailsData[i].status,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						txnView.add(lblDate);
						txnView.add(lblBank);
						detailTxnView.add(txnView);
						topView.add(detailTxnView);
						topView.add(borderView);
						detailAmountView.add(lblAmountTxt);
						detailAmountView.add(lblAmount);
						topView.add(detailAmountView);
						accountView.add(lblAccountHead);
						accountView.add(lblAccountNo);
						detailAccountView.add(accountView);
						bottomView.add(detailAccountView);
						rtrnView.add(lblRTRNHead);
						rtrnView.add(lblRTRN);
						detailRTRNView.add(rtrnView);
						bottomView.add(detailRTRNView);
						routingView.add(lblRoutingHead);
						routingView.add(lblRoutingNo);
						detailRoutingView.add(routingView);
						bottomView1.add(detailRoutingView);
						statusView.add(lblStatusHead);
						statusView.add(lblStatus);
						detailStatusView.add(statusView);
						bottomView1.add(detailStatusView);
						row.add(topView);
						row.add(bottomView);
						row.add(borderView1);
						row.add(bottomView1);
						_obj.tblPaymentDetails.appendRow(row);
					}
				}
				activityIndicator.hideIndicator();
			}
			else
			{
				activityIndicator.hideIndicator();
				
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session' || e.result.message === 'Invalid Session.')
				{
					require('/lib/session').session();
					destroy_scheduletxndetails();
				}
				else
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : 'transparent',
						className : 'payment_details',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblPaymentDetails.appendRow(row);
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
		}
	}
	
	populatePaymentDetails();
	
	function populatePaymentDetailsPaginate()
	{
		_obj.updating = true;
		
		_obj.start = (((_obj.page * _obj.limit) + 1) - _obj.limit); 
		_obj.last = (_obj.page * _obj.limit);
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETSIBOOKEDTXNDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"rangeFrom":"'+_obj.start+'",'+
				'"rangeTo":"'+_obj.last+'",'+
				'"operatingmodeId":"INTL"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			_obj.totalRecords = parseInt(e.result.totalCount);	
			
			_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
			
			if(e.result.responseFlag === "S")
			{
				for(var i=0; i<e.result.siDetailsData.length; i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 106,
						backgroundColor : 'transparent',
						className : 'payment_details',
						rw : 1
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var topView = Ti.UI.createView({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : '#6f6f6f',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailTxnView = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '50%',
						height : 50
					});
					
					var txnView = Ti.UI.createView({
						left : 0,
						right:0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var dt = e.result.siDetailsData[i].startDate.split('-');
					
					var lblDate = Ti.UI.createLabel({
						text : 'Date ' + dt[0]+'/'+require('/utils/Date').monthNo(dt[1])+'/'+dt[2],
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblAmount = Ti.UI.createLabel({
						text : e.result.siDetailsData[i].amount,
						top : 2,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var borderView = Ti.UI.createView({
						height : 50,
						top : 0,
						left : 0,
						width:1,
						backgroundColor:'#898989'
					});
					
					var detailSendView = Ti.UI.createView({
						left : 0,
						right : 0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblSendTxt = Ti.UI.createLabel({
						text:'No. of Payments', 
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblNoPayment = Ti.UI.createLabel({
						text:e.result.siDetailsData[i].noOfPayments, 
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var bottomView = Ti.UI.createView({
						top : 50,
						left : 0,
						right : 0,
						height : 56,
						backgroundColor : '#fff',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailBenView = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var statusCheckView = Ti.UI.createView({
						left : 0,
						right:0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblStatusHead = Ti.UI.createLabel({
						text : 'Status',
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var lblStatus = Ti.UI.createLabel({
						text : e.result.siDetailsData[i].status,
						top : 2,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var statusView = Ti.UI.createView({
						top : 0,
						left : 20,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var lblAccountStatus = Ti.UI.createLabel({
						text : 'Action:',
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont'),
						sel:e.result.siDetailsData[i].accountStatus
					});
					
					var actionView = Ti.UI.createView({
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						layout:'horizontal',
						horizontalWrap:true
					});
					
					var lblView = Ti.UI.createLabel({
						text : 'View Details ',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						sel:'view'
						
					});
					
					var lblCancel = Ti.UI.createLabel({
						text : '| Cancel EMI',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						sel:'cancel'
					});
					
					txnView.add(lblDate);
					txnView.add(lblAmount);
					detailTxnView.add(txnView);
					topView.add(detailTxnView);
					topView.add(borderView);
					detailSendView.add(lblSendTxt);
					detailSendView.add(lblNoPayment);
					topView.add(detailSendView);
					statusCheckView.add(lblStatusHead);
					statusCheckView.add(lblStatus);
					detailBenView.add(statusCheckView);
					bottomView.add(detailBenView);
					statusView.add(lblAccountStatus);
					if((e.result.siDetailsData[i].action.indexOf('DETAILS,CANCEL')) > -1)
					{
						lblView.text = 'View Details ';
						lblCancel.text = '| Cancel EMI';
						actionView.add(lblView);
						actionView.add(lblCancel);	
					}
					else if((e.result.siDetailsData[i].action.indexOf('DETAILS')) > -1)
					{
						lblView.text = 'View Details ';
						actionView.add(lblView);
					}
					else if((e.result.siDetailsData[i].action.indexOf('CANCEL')) > -1)
					{
						lblCancel.text = 'Cancel EMI';
						actionView.add(lblCancel);
					}
					else
					{
						
					}
					statusView.add(actionView);
					bottomView.add(statusView);
					row.add(topView);
					row.add(bottomView);
					_obj.tblPaymentDetails.appendRow(row);
					_obj.updating = false;
				}
				activityIndicator.hideIndicator();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session' || e.result.message === 'Invalid Session.')
				{
					require('/lib/session').session();
					destroy_scheduletxndetails();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	
	/////////////////// Easy EMI Details ///////////////////
	
	_obj.stepView2 = Ti.UI.createScrollView(_obj.style.stepView2);
	
	_obj.tblEasyEMI = Ti.UI.createTableView(_obj.style.tableView);
	_obj.stepView2.add(_obj.tblEasyEMI);
	
	function populateEasyEMI()
	{
		for(var i=0; i<10; i++)
		{
			var row = Ti.UI.createTableViewRow({
				top : 0,
				left : 0,
				right : 0,
				height : 60,
				backgroundColor : 'transparent',
				className : 'easy_emi'
			});
			
			if(TiGlobals.osname !== 'android')
			{
				row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
			
			var lblKey = Ti.UI.createLabel({
				top : 10,
				left : 20,
				height : 15,
				width:Ti.UI.SIZE,
				textAlign: 'left',
				font : TiFonts.FontStyle('lblNormal12'),
				color : TiFonts.FontStyle('greyFont')
			});
			
			var lblValue = Ti.UI.createLabel({
				top : 30,
				height : 20,
				left : 20,
				width:Ti.UI.SIZE,
				textAlign: 'left',
				font : TiFonts.FontStyle('lblNormal14'),
				color : TiFonts.FontStyle('blackFont')
			});
			
			switch(i)
			{
				case 0:
					var dt = params.startDate.split('-');
					
					lblKey.text = 'Start Date';
					lblValue.text = dt[0]+'/'+require('/utils/Date').monthNo(dt[1])+'/'+dt[2];
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
				
				case 1:
					lblKey.text = 'Sending Country';
					lblValue.text = countryName[0];
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
				
				case 2:
					lblKey.text = 'Destination Country';
					lblValue.text = params.destCountry;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
				
				case 3:
					lblKey.text = 'Amount';
					lblValue.text = params.amount;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
				
				case 4:
					lblKey.text = 'Beneficiary Name';
					lblValue.text = params.beneficiaryName;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
				
				case 5:
					lblKey.text = 'Bank Name';
					lblValue.text = params.bankName;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
				
				case 6:
					lblKey.text = 'Periodicity';
					lblValue.text = params.periodicity;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
				
				case 7:
					lblKey.text = 'Number of Payments';
					lblValue.text = params.noOfPayments;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
				
				case 8:
					lblKey.text = 'Status';
					lblValue.text = params.status;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
				
				case 9:
					lblKey.text = 'Type';
					lblValue.text = params.siType;
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblEasyEMI.appendRow(row);
					_obj.tblEasyEMI.height = parseInt(_obj.tblEasyEMI.height + 60);
				break;
			}
		}
	}
	
	populateEasyEMI();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_scheduletxndetails();
	});
	
	_obj.scrollableView.views = [_obj.stepView1,_obj.stepView2];
	_obj.mainView.add(_obj.scrollableView);
	_obj.globalView.add(_obj.mainView);
	_obj.winScheduleTxnDetails.add(_obj.globalView);
	_obj.winScheduleTxnDetails.open();
	
	_obj.winScheduleTxnDetails.addEventListener('androidback', function(){
		destroy_scheduletxndetails();
	});
	
	function destroy_scheduletxndetails()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove Schedule Txn Details start ##############');
			
			_obj.winScheduleTxnDetails.close();
			require('/utils/RemoveViews').removeViews(_obj.winScheduleTxnDetails);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_scheduletxndetails',destroy_scheduletxndetails);
			require('/utils/Console').info('############## Remove Schedule Txn Details end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_scheduletxndetails', destroy_scheduletxndetails);
}; // ScheduleTxnDetailsModal()