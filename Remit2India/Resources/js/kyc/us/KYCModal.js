exports.KYCModal = function(logId,sesId,ownId)
{
	require('/lib/analytics').GATrackScreen('KYC Form US');
	
	var _obj = {
	    sessionId : null,
	    loginId:null,
	    ownId : null,
		style : require('/styles/kyc/us/KYC').KYC,
		winKYC : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		
		kycView : null,
		txtAptNo : null,
		borderViewS21 : null,
		txtBldgNo : null,
		borderViewS22 : null,
		txtStreet1 : null,
		borderViewS23 : null,
		txtStreet2 : null,
		borderViewS24 : null,
		txtLocality : null,
		borderViewS25 : null,
		txtSubLocality : null,
		borderViewS26 : null,
		cityView : null,
		txtCity : null,
		imgCity : null,
		borderViewS27 : null,
		stateView : null,
		lblState : null,
		imgState : null,
		borderViewS28 : null,
		txtZip : null,
		borderViewS29 : null,
		
		//SSN
		ssnView : null,
		ssnBorder1View : null,
		ssnBorder2View : null,
		lblSSN : null,
		lblSSNTxt : null,
		ssnTxtView : null,
		txtSSN1 : null,
		borderSSN1 : null,
		txtSSN2 : null,
		borderSSN2 : null,
		txtSSN3 : null,
		borderSSN3 : null,
		lblSSNReTxt : null,
		ssnTxtReView : null,
		txtSSNRe1 : null,
		borderSSNRe1 : null,
		txtSSNRe2 : null,
		borderSSNRe2 : null,
		txtSSNRe3 : null,
		borderSSNRe3 : null,
		btnSubmit : null,
		
		flat : null,
		bldgNo : null,
		street1 : null,
		street2 : null,
		locality : null,
		sub_locality : null,
		city : null,
		state : null,
	};
	
	
	        _obj.loginId =  logId;
			_obj.sessionId  = sesId;
			_obj.ownId = ownId;
		    
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winKYC = Titanium.UI.createWindow(_obj.style.winKYC);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winKYC);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Help us know you better';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	/////////////////// KYC ///////////////////
	
	_obj.kycView = Ti.UI.createScrollView(_obj.style.kycView);
	
	_obj.txtAptNo = Ti.UI.createTextField(_obj.style.txtAptNo);
	
	if(countryName[0] === 'Germany' || countryName[0] === 'Singapore' || countryName[0] === 'United Arab Emirates' || countryName[0] === 'Australia' || countryName[0] === 'United States')
	{
		_obj.txtAptNo.hintText = "Flat/House No.*";
	}
	else if(countryName[0] === 'United Kingdom')
	{
		_obj.txtAptNo.hintText = 'House/Apartment No.*';
	}
	else
	{
		_obj.txtAptNo.hintText = 'Flat/Unit No.*';
	}
	_obj.txtAptNo.maxLength = 25;
	_obj.borderViewS21 = Ti.UI.createView(_obj.style.borderView);
	
	
	if(countryName[0] !== 'Australia')
	{
		_obj.txtBldgNo = Ti.UI.createTextField(_obj.style.txtBldgNo);
		_obj.txtBldgNo.hintText = 'Building No./Name';
		_obj.txtBldgNo.maxLength = 25;
		_obj.borderViewS22 = Ti.UI.createView(_obj.style.borderView);
	}
	
	_obj.txtStreet1 = Ti.UI.createTextField(_obj.style.txtStreet1);
	
	if(countryName[0] === 'Australia' || countryName[0] === 'United Arab Emirates')
	{
		_obj.txtStreet1.hintText = "Street*";
		_obj.txtStreet1.maxLength = 25;
		_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
 	}
	else 
	{
		_obj.txtStreet1.hintText = "Street 1*";
		_obj.txtStreet1.maxLength = 25;
		_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtStreet2 = Ti.UI.createTextField(_obj.style.txtStreet2);
		_obj.txtStreet2.hintText = 'Street 2';
		_obj.txtStreet2.maxLength = 25;
		_obj.borderViewS24 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtLocality = Ti.UI.createTextField(_obj.style.txtLocality);
		_obj.txtLocality.hintText = 'Locality';
		_obj.txtLocality.maxLength = 25;
		_obj.borderViewS25 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtSubLocality = Ti.UI.createTextField(_obj.style.txtSubLocality);
		_obj.txtSubLocality.hintText = 'Sub-Locality';
		_obj.txtSubLocality.maxLength = 25;
		_obj.borderViewS26 = Ti.UI.createView(_obj.style.borderView);
	}
	
	if(countryName[0] === 'United Arab Emirates')
	{
		_obj.cityView = Ti.UI.createView(_obj.style.cityView);
		_obj.txtCity = Ti.UI.createLabel(_obj.style.txtCity);
		_obj.txtCity.top = 0;
		_obj.txtCity.left = 0;
		_obj.txtCity.text = 'Select City*';
		_obj.imgCity = Ti.UI.createImageView(_obj.style.imgCity);
		
		_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.cityView.addEventListener('click',function(){
			activityIndicator.showIndicator();
			var cityOptions = [];
			
			var xhr = require('/utils/XHR_BCM');
	
			xhr.call({
				url : TiGlobals.appURLBCM,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"'+TiGlobals.bcmConfig+'",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"source":"'+TiGlobals.bcmSource+'",'+
					'"type":"citylisting",'+
					'"platform":"'+TiGlobals.osname+'",'+
					'"corridor":"'+countryName[0]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				activityIndicator.hideIndicator();
				if(e.result.status === "S")
				{
					for(var i=0;i<e.result.response[0].citylisting.UAE.length;i++)
					{
						cityOptions.push(e.result.response[0].citylisting.UAE[i]);
					}
					
					var optionDialogCity = require('/utils/OptionDialog').showOptionDialog('City',cityOptions,-1);
					optionDialogCity.show();
					
					optionDialogCity.addEventListener('click',function(evt){
						if(optionDialogCity.options[evt.index] !== L('btn_cancel'))
						{
							_obj.txtCity.text = optionDialogCity.options[evt.index];
							optionDialogCity = null;
						}
						else
						{
							optionDialogCity = null;
							_obj.txtCity.text = 'Select City*';
						}
					});	
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_kyc();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		});
	}
	else
	{
		_obj.txtCity = Ti.UI.createTextField(_obj.style.txtCity);
		_obj.txtCity.hintText = 'City*';
		_obj.txtCity.maxLength = 25;
		_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
	}
	
	if(countryName[0] !== 'Singapore')
	{
		if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
		{
			
			_obj.stateView = Ti.UI.createView(_obj.style.stateView);
			_obj.lblState = Ti.UI.createLabel(_obj.style.lblState);
			_obj.imgState = Ti.UI.createImageView(_obj.style.imgState);
			_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
			
			if(countryName[0] === 'United Arab Emirates')
			{
				_obj.lblState.text = 'Select Emirates*';	
			}
			else
			{
				_obj.lblState.text = 'Select State*';	
			}
			
			_obj.stateView.addEventListener('click',function(){
				activityIndicator.showIndicator();
				var stateOptions = [];
				
				var xhr = require('/utils/XHR');
		
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"country":"'+countryName[0]+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
		
				function xhrSuccess(e) {
					activityIndicator.hideIndicator();
					if(e.result.responseFlag === "S")
					{
						var splitArr = e.result.countryRelatedStates.split('~');
				
						for(var i=0;i<splitArr.length;i++)
						{
							stateOptions.push(splitArr[i]);
						}
						
						var optionDialogState = require('/utils/OptionDialog').showOptionDialog('State',stateOptions,-1);
						optionDialogState.show();
						
						optionDialogState.addEventListener('click',function(evt){
							if(optionDialogState.options[evt.index] !== L('btn_cancel'))
							{
								_obj.lblState.text = optionDialogState.options[evt.index];
								optionDialogState = null;
							}
							else
							{
								optionDialogState = null;
								
								if(countryName[0] === 'United Arab Emirates')
								{
									_obj.lblState.text = 'Select Emirates*';	
								}
								else
								{
									_obj.lblState.text = 'Select State*';	
								}
							}
						});	
					}
					else
					{
						if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_kyc();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
						}
					}
					xhr = null;
				}
		
				function xhrError(e) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}
			});

        }
        else
        {
			_obj.lblState = Ti.UI.createTextField(_obj.style.txtCity);
			_obj.lblState.hintText = 'State*';
			_obj.lblState.maxLength = 25;
			_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
        }
	}
	
	_obj.txtZip = Ti.UI.createTextField(_obj.style.txtZip);
	_obj.txtZip.hintText = 'Zip Code*';
	
	_obj.doneZip = Ti.UI.createButton(_obj.style.done);
	_obj.doneZip.addEventListener('click',function(){
		_obj.txtZip.blur();
	});
	
	////// Pin code hintText ///////
	
	switch(countryName[0]){
		case 'United States' :
		{
			_obj.txtZip.hintText = 'Zip Code (5 digits)*';
			_obj.txtZip.maxLength = 5;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Canada' :
		{
			_obj.txtZip.hintText = 'Zip Code (7 alpha-numeric)*';
			_obj.txtZip.maxLength = 7;
			break;
		}
		case 'Australia' :
		{
			_obj.txtZip.hintText = 'Zip Code (4 digits)*';
			_obj.txtZip.maxLength = 4;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Hong Kong' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Japan' :
		{
			_obj.txtZip.hintText = 'Zip Code (7 digits)*';
			_obj.txtZip.maxLength = 7;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'New Zealand' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Singapore' :
		{
			_obj.txtZip.hintText = 'Postal Code (6 digits)*';
			_obj.txtZip.maxLength = 6;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Ireland' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'United Kingdom' :
		{
			_obj.txtZip.hintText = 'Postcode (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'United Arab Emirates' :
		{
			_obj.txtZip.hintText = 'P. O. Box (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Austria' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Belgium' :
		{
			_obj.txtZip.hintText = 'Zip Code (4 digits)*';
			_obj.txtZip.maxLength = 4;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Cyprus' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Finland' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'France' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Germany' :
		{
			_obj.txtZip.hintText = 'Postal Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}		 
		case 'Greece' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Italy' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Luxembourg' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Malta' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}		
		case 'Netherlands' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Portugal' :
		{
			_obj.txtZip.hintText = 'Zip Code (25 alpha-numeric)*';
			_obj.txtZip.maxLength = 25;
			break;
		}
		case 'Slovakia' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Slovenia' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Spain' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		default:
		{
			break;
		}
	}
	_obj.borderViewS29 = Ti.UI.createView(_obj.style.borderView);
	
	// SSN
	_obj.ssnBorder1View = Ti.UI.createView(_obj.style.sectionHeaderBorder1);
	_obj.ssnView = Ti.UI.createView(_obj.style.ssnView);
	_obj.lblSSN = Ti.UI.createLabel(_obj.style.lblSSN);
	_obj.lblSSN.text = 'Social Security Number';
	_obj.ssnBorder2View = Ti.UI.createView(_obj.style.sectionHeaderBorder2);
	
	_obj.lblSSNTxt = Ti.UI.createLabel(_obj.style.lblSSNTxt);
	_obj.lblSSNTxt.text = 'Your Social Security Number';
	
	_obj.ssnTxtView = Ti.UI.createView(_obj.style.ssnTxtView);
	
	_obj.txtSSN1 = Ti.UI.createTextField(_obj.style.txtSSN1);
	_obj.txtSSN1.hintText = 'xxx';
	_obj.txtSSN1.maxLength = 3;
	_obj.txtSSN1.passwordMask = true;
	_obj.txtSSN1.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.borderSSN1 = Ti.UI.createView(_obj.style.borderSSN1);
	
	_obj.txtSSN2 = Ti.UI.createTextField(_obj.style.txtSSN2);
	_obj.txtSSN2.hintText = 'xx';
	_obj.txtSSN2.maxLength = 2;
	_obj.txtSSN2.passwordMask = true;
	_obj.txtSSN2.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.borderSSN2 = Ti.UI.createView(_obj.style.borderSSN2);
	
	_obj.txtSSN3 = Ti.UI.createTextField(_obj.style.txtSSN3);
	_obj.txtSSN3.hintText = 'xxxx';
	_obj.txtSSN3.maxLength = 4;
	_obj.txtSSN3.passwordMask = true;
	_obj.txtSSN3.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.borderSSN3 = Ti.UI.createView(_obj.style.borderSSN3);
	
	_obj.lblSSNReTxt = Ti.UI.createLabel(_obj.style.lblSSNReTxt);
	_obj.lblSSNReTxt.text = 'Re-type Social Security Number';
	
	_obj.ssnTxtReView = Ti.UI.createView(_obj.style.ssnTxtReView);
	
	_obj.txtSSNRe1 = Ti.UI.createTextField(_obj.style.txtSSNRe1);
	_obj.txtSSNRe1.hintText = 'xxx';
	_obj.txtSSNRe1.maxLength = 3;
	_obj.txtSSNRe1.passwordMask = true;
	_obj.txtSSNRe1.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.borderSSNRe1 = Ti.UI.createView(_obj.style.borderSSNRe1);
	
	_obj.txtSSNRe2 = Ti.UI.createTextField(_obj.style.txtSSNRe2);
	_obj.txtSSNRe2.hintText = 'xx';
	_obj.txtSSNRe2.maxLength = 2;
	_obj.txtSSNRe2.passwordMask = true;
	_obj.txtSSNRe2.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.borderSSNRe2 = Ti.UI.createView(_obj.style.borderSSNRe2);
	
	_obj.txtSSNRe3 = Ti.UI.createTextField(_obj.style.txtSSNRe3);
	_obj.txtSSNRe3.hintText = 'xxxx';
	_obj.txtSSNRe3.maxLength = 4;
	_obj.txtSSNRe3.passwordMask = true;
	_obj.txtSSNRe3.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.borderSSNRe3 = Ti.UI.createView(_obj.style.borderSSNRe3);
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.kycView.add(_obj.txtAptNo);
	_obj.kycView.add(_obj.borderViewS21);
	
	if(countryName[0] !== 'Australia')
	{
		_obj.kycView.add(_obj.txtBldgNo);
		_obj.kycView.add(_obj.borderViewS22);
	}
	_obj.kycView.add(_obj.txtStreet1);
	_obj.kycView.add(_obj.borderViewS23);
	
	if(countryName[0] !== 'Australia' || countryName[0] !== 'United Arab Emirates')
	{
		try{
			_obj.kycView.add(_obj.txtStreet2);
			_obj.kycView.add(_obj.borderViewS24);
	
			_obj.kycView.add(_obj.txtLocality);
			_obj.kycView.add(_obj.borderViewS25);
			
			_obj.kycView.add(_obj.txtSubLocality);
			_obj.kycView.add(_obj.borderViewS26);
		}catch(e){}
	}
	
	if(countryName[0] === 'United Arab Emirates')
	{
		_obj.cityView.add(_obj.txtCity);
		_obj.cityView.add(_obj.imgCity);
		_obj.kycView.add(_obj.cityView);
		_obj.kycView.add(_obj.borderViewS27);
	}	
	else
	{
		_obj.kycView.add(_obj.txtCity);
		_obj.kycView.add(_obj.borderViewS27);
	}
	
	if(countryName[0] !== 'Singapore')
	{
		if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
		{
			_obj.stateView.add(_obj.lblState);
			_obj.stateView.add(_obj.imgState);
			_obj.kycView.add(_obj.stateView);
			_obj.kycView.add(_obj.borderViewS28);
        }
        else
        {
			_obj.kycView.add(_obj.lblState);
			_obj.kycView.add(_obj.borderViewS28);
        }
	}
	
	_obj.kycView.add(_obj.txtZip);
	_obj.kycView.add(_obj.borderViewS29);
	
	_obj.ssnView.add(_obj.ssnBorder1View);
	_obj.ssnView.add(_obj.lblSSN);
	_obj.ssnView.add(_obj.ssnBorder2View);
	_obj.kycView.add(_obj.ssnView);
	
	_obj.kycView.add(_obj.lblSSNTxt);
	_obj.ssnTxtView.add(_obj.txtSSN1);
	_obj.ssnTxtView.add(_obj.borderSSN1);
	_obj.ssnTxtView.add(_obj.txtSSN2);
	_obj.ssnTxtView.add(_obj.borderSSN2);
	_obj.ssnTxtView.add(_obj.txtSSN3);
	_obj.ssnTxtView.add(_obj.borderSSN3);
	_obj.kycView.add(_obj.ssnTxtView);
	
	_obj.kycView.add(_obj.lblSSNReTxt);
	_obj.ssnTxtReView.add(_obj.txtSSNRe1);
	_obj.ssnTxtReView.add(_obj.borderSSNRe1);
	_obj.ssnTxtReView.add(_obj.txtSSNRe2);
	_obj.ssnTxtReView.add(_obj.borderSSNRe2);
	_obj.ssnTxtReView.add(_obj.txtSSNRe3);
	_obj.ssnTxtReView.add(_obj.borderSSNRe3);
	_obj.kycView.add(_obj.ssnTxtReView);
	
	_obj.kycView.add(_obj.btnSubmit);
	_obj.mainView.add(_obj.kycView);
	_obj.globalView.add(_obj.mainView);
	_obj.winKYC.add(_obj.globalView);
	_obj.winKYC.open();
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		// Apartment/House No.
		_obj.flat = _obj.txtAptNo.value; 
		if(_obj.txtAptNo.value == '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your ' + _obj.txtAptNo.hintText,[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtAptNo.hintText + ' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('',_obj.txtAptNo.hintText + ' cannot contain  |  or  \'  or  ` or  \" or #',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtAptNo.value.charAt(i)+' entered for '+_obj.txtAptNo.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
			return;
		}
		else
		{
			
		}
		// Building No.
		if(countryName[0] !== 'Australia')
		{
			_obj.bldgNo = _obj.txtBldgNo.value;
			if(_obj.txtBldgNo.value.length > 0)
			{
				if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtBldgNo.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtBldgNo.hintText + ' field',[L('btn_ok')]).show();
		    		_obj.txtBldgNo.value = '';
		    		_obj.txtBldgNo.focus();
					return;
				}
				else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtBldgNo.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtBldgNo.value.charAt(i)+' entered for '+_obj.txtBldgNo.hintText+' field',[L('btn_ok')]).show();
		    		_obj.txtBldgNo.value = '';
		    		_obj.txtBldgNo.focus();
					return;
				}
			}
			else
			{
				require('/utils/AlertDialog').showAlert('','Please provide a valid ' + _obj.txtBldgNo.hintText,[L('btn_ok')]).show();
	    		_obj.txtBldgNo.value = '';
	    		_obj.txtBldgNo.focus();
				return;
			}
		}
		else
		{
			_obj.bldgNo = '';
		}
		
		//Street1
		
		_obj.street1 = _obj.txtStreet1.value;
		if(_obj.txtStreet1.value === "")
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Street Name',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street Name',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Street Name cannot contain | or \' or ` or \" or #',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else if(_obj.txtStreet1.value.search("[^a-zA-Z ]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Street Name field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtStreet1.value.charAt(i)+' entered for '+_obj.txtStreet1.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
			return;
		}
		else
		{
			
		}
		
		if(countryName[0] !== 'Australia' || countryName[0] !== 'United Arab Emirates')
		{
			try{
				// Street 2
				_obj.street2 = _obj.txtStreet2.value;
				
				if(_obj.txtStreet2.value.length > 0)
				{
					if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreet2.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street 2 field',[L('btn_ok')]).show();
			    		_obj.txtStreet2.value = '';
			    		_obj.txtStreet2.focus();
						return;
					}
					else if(_obj.txtStreet2.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Street 2 field',[L('btn_ok')]).show();
			    		_obj.txtStreet2.value = '';
			    		_obj.txtStreet2.focus();
						return;
					}
					else
					{
						
					}
				}
				
				// Locality
				_obj.locality = _obj.txtLocality.value;
			
				if(_obj.txtLocality.value.length > 0)
				{
					if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLocality.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of locality',[L('btn_ok')]).show();
			    		_obj.txtLocality.value = '';
			    		_obj.txtLocality.focus();
						return;
					}
					else if(_obj.txtLocality.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in locality field',[L('btn_ok')]).show();
			    		_obj.txtLocality.value = '';
			    		_obj.txtLocality.focus();
						return;
					}
					else
					{
					}
				}
			
				// Sub Locality
				_obj.sub_locality = _obj.txtSubLocality.value;
				
				if(_obj.txtSubLocality.value.length > 0)
				{
					if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtSubLocality.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Sub Locality',[L('btn_ok')]).show();
						_obj.txtSubLocality.value = '';
			    		_obj.txtSubLocality.focus();
						return;
					}
					else if(_obj.txtSubLocality.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Sub Locality field',[L('btn_ok')]).show();
			    		_obj.txtSubLocality.value = '';
			    		_obj.txtSubLocality.focus();
						return;
					}
					else
					{
						
					}
				}
			}catch(e){}
		}
		
		// City
		
		if(countryName[0] === 'United Arab Emirates')
		{
			_obj.city = _obj.txtCity.text;
			if(_obj.txtCity.text === 'Select City*')
			{
				require('/utils/AlertDialog').showAlert('','Please select your city',[L('btn_ok')]).show();
	    		_obj.txtCity.text = 'Select City*';
				return;
			}
			else
			{
			}
		}
		else
		{
			_obj.city = _obj.txtCity.value;
			
			if(_obj.txtCity.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter your city',[L('btn_ok')]).show();
	    		_obj.txtCity.value = '';
	    		_obj.txtCity.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtCity.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of city',[L('btn_ok')]).show();
	    		_obj.txtCity.value = '';
	    		_obj.txtCity.focus();
				return;
			}
			else if(_obj.txtCity.value.search("[^a-zA-Z ]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in city',[L('btn_ok')]).show();
	    		_obj.txtCity.value = '';
	    		_obj.txtCity.focus();
				return;
			}
			else
			{
			}	
		}
		
		//State
		if(countryName[0] !== 'Singapore')
		{
			if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
			{
				
				_obj.state = _obj.lblState.text;
				
				if(countryName[0] === 'United Arab Emirates')
				{
					if(_obj.lblState.text === 'Select Emirates*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your Emirate',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select Emirates*';
						return;
					}
					else
					{
					}	
				}
				else
				{
					if(_obj.lblState.text === 'Select State*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your state',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select State*';
						return;
					}
					else
					{
					}
				}
	
	        }
	        else
	        {
				_obj.state = _obj.lblState.value;
				
				if(_obj.lblState.value === '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your state',[L('btn_ok')]).show();
		    		_obj.lblState.value = '';
		    		_obj.lblState.focus();
					return;
				}
				else if(require('/lib/toml_validations').checkBeginningSpace(_obj.lblState.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of state',[L('btn_ok')]).show();
		    		_obj.lblState.value = '';
		    		_obj.lblState.focus();
					return;
				}
				else if(_obj.lblState.value.search("[^a-zA-Z ]") >= 0)
				{
					require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in state',[L('btn_ok')]).show();
		    		_obj.lblState.value = '';
		    		_obj.lblState.focus();
					return;
				}
				else
				{
				}
				
	        }
		}
		
		// Zipcode
		
		if(_obj.txtZip.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your '+_obj.txtZip.hintText,[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtZip.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of '+_obj.txtZip.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
			return;
		}
		
		if(_obj.txtZip.value.length > 0)
		{
	 		var len = _obj.txtZip.value.length;
			var validchars = '';	
			validchars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
			var currChar, prevChar, nextChar;
			var validAlph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var validNum = "0123456789";
			var validAlphFlag = false;
			var validNumFlag = false;
	
			for(i = 0; i <= len - 1; i++)
			{
				currChar = _obj.txtZip.value.charAt(i);
				prevChar = _obj.txtZip.value.charAt(i-1);
				nextChar = _obj.txtZip.value.charAt(i+1);
	
				if(!(validAlph.indexOf(currChar) == -1) && validAlphFlag == false)
				{
					validAlphFlag = true;
				}
	
				if(!(validNum.indexOf(currChar) == -1) && validNumFlag == false)
				{
					validNumFlag = true;
				}
							
				if(validchars.indexOf(currChar) == -1)
				{
					if(countryName[0] != 'Canada')
					{
						require('/utils/AlertDialog').showAlert('','Invalid character "' + currChar + '" entered in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
						return;
					}
				}
	
				if(currChar == ' ' && prevChar == ' ' || prevChar == ' ' && currChar == ' ' && nextChar == ' ' || prevChar == ' ' && currChar == ' ' && i == len-1 || currChar == ' ' && i == len-1)
				{
					if(countryName[0] == 'United Kingdom')
					{
						if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText + '. Only one space is allowed in middle',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
					else
					{
						if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
					break;
				}
			}
						
			if((countryName[0] != 'United Arab Emirates') && (countryName[0] != 'UAE') && (countryName[0] != 'Hong Kong')) //Country check for HK & UAE
			{		
				if((countryName[0] == 'Singapore') || (countryName[0] == 'Australia') || (countryName[0] == 'United States') || (countryName[0] == 'Portugal'))
				{
					if(countryName[0] != 'Portugal')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
	
					if((countryName[0] == 'Singapore') && (len != 6))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 6 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
						return;
					}
					else if((countryName[0] == 'Australia') && (len != 4))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
						return;
					}
					else if((countryName[0] == 'United States') && (len != 5))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
						return;
					}
					else if((countryName[0] == 'Portugal'))
					{			
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
						else if( len < 3 || len > 25)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 25 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
				}
				else
				{
					if(countryName[0] == 'Canada')
					{
						var pcode = _obj.txtZip.value;
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
						else if(pcode.indexOf(" ") != 3)
						{
							require('/utils/AlertDialog').showAlert('','There must be a space after 3rd character in '+_obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'United Kingdom')
					{			
						if( len < 5 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 5 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
						
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
					else
					{
						if( len < 3 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
	
					if((countryName[0] == 'France') || (countryName[0] == 'Germany') || (countryName[0] == 'Italy') || (countryName[0] == 'Euroland') || (countryName[0] == 'New Zealand'))
					{
						var pncode = _obj.txtZip.value;
						if(pncode.split(" ").length > 2)
						{
							require('/utils/AlertDialog').showAlert('','Only one single space is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
							
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'Belgium')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
	
						if(len != 4)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'Spain')
					{
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'United States')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
						if(len != 5)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] == 'Japan')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
					else if(countryName[0] != 'Spain')
					{ 
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
							return;
						}
					}
				}
			} 
		 }
		 
		// SSN Validation
		var ssn1 = _obj.txtSSN1.value;
		var ssn2 = _obj.txtSSN2.value;
		var ssn3 = _obj.txtSSN3.value;
		var ssn = ssn1+ssn2+ssn3;
		
		if(ssn1.trim() == '' || ssn2.trim() == '' || ssn3.trim() == '')
		{
			require('/utils/AlertDialog').showAlert('','Please provide your valid Social Security Number',[L('btn_ok')]).show();
    		_obj.txtSSN1.value = '';
    		_obj.txtSSN2.value = '';
    		_obj.txtSSN3.value = '';
			return;
		}
		else if(ssn.trim() == '')
		{
			require('/utils/AlertDialog').showAlert('','Please provide your valid Social Security Number',[L('btn_ok')]).show();
    		_obj.txtSSN1.value = '';
    		_obj.txtSSN2.value = '';
    		_obj.txtSSN3.value = '';
			return;
		}
		else if(ssn1.length != 3)
		{
			require('/utils/AlertDialog').showAlert('','The first section of Social Security Number should be of 3 digits',[L('btn_ok')]).show();
    		_obj.txtSSN1.value = '';
			return;				
		}
		else if(ssn2.length != 2)
		{
			require('/utils/AlertDialog').showAlert('','The second section of Social Security Number should be of 2 digits',[L('btn_ok')]).show();
    		_obj.txtSSN2.value = '';
			return;
		}
		else if(ssn3.length != 4)
		{
			require('/utils/AlertDialog').showAlert('','The third section of Social Security Number should be of 4 digits',[L('btn_ok')]).show();
    		_obj.txtSSN3.value = '';
			return;
		}
		else if(isNaN(ssn1) || isNaN(ssn2) || isNaN(ssn3))
		{
			require('/utils/AlertDialog').showAlert('','Please provide your valid Social Security Number',[L('btn_ok')]).show();
    		_obj.txtSSN1.value = '';
    		_obj.txtSSN2.value = '';
    		_obj.txtSSN3.value = '';
			return;
		}
		else
		{
			
		}
		
		// Re SSN Validation Starts
		var ssn11 = _obj.txtSSNRe1.value;
		var ssn22 = _obj.txtSSNRe2.value;
		var ssn33 = _obj.txtSSNRe3.value;
		var re_ssn = ssn11+ssn22+ssn33;
		
		if(ssn11.trim() == '' || ssn22.trim() == '' || ssn33.trim() == '')
		{
			require('/utils/AlertDialog').showAlert('','Please provide your valid Social Security Number',[L('btn_ok')]).show();
    		_obj.txtSSNRe1.value = '';
    		_obj.txtSSNRe2.value = '';
    		_obj.txtSSNRe3.value = '';
			return;
		}
		else if(re_ssn.trim() == '')
		{
			require('/utils/AlertDialog').showAlert('','Please provide your valid Social Security Number',[L('btn_ok')]).show();
    		_obj.txtSSNRe1.value = '';
    		_obj.txtSSNRe2.value = '';
    		_obj.txtSSNRe3.value = '';
			return;
		}
		else if(ssn11.length != 3)
		{
			require('/utils/AlertDialog').showAlert('','The first section of Social Security Number should be of 3 digits',[L('btn_ok')]).show();
    		_obj.txtSSNRe1.value = '';
			return;				
		}
		else if(ssn22.length != 2)
		{
			require('/utils/AlertDialog').showAlert('','The second section of Social Security Number should be of 2 digits',[L('btn_ok')]).show();
    		_obj.txtSSNRe2.value = '';
			return;
		}
		else if(ssn33.length != 4)
		{
			require('/utils/AlertDialog').showAlert('','The third section of Social Security Number should be of 4 digits',[L('btn_ok')]).show();
    		_obj.txtSSNRe3.value = '';
			return;
		}
		else if(isNaN(ssn11) || isNaN(ssn22) || isNaN(ssn33))
		{
			require('/utils/AlertDialog').showAlert('','Please provide your valid Social Security Number',[L('btn_ok')]).show();
    		_obj.txtSSNRe1.value = '';
    		_obj.txtSSNRe2.value = '';
    		_obj.txtSSNRe3.value = '';
			return;
		}
		else if(ssn !== re_ssn)
		{
			require('/utils/AlertDialog').showAlert('','The two SSN does not match. Please type the correct SSN',[L('btn_ok')]).show();
    		_obj.txtSSN1.value = '';
    		_obj.txtSSN2.value = '';
    		_obj.txtSSN3.value = '';
    		_obj.txtSSNRe1.value = '';
    		_obj.txtSSNRe2.value = '';
    		_obj.txtSSNRe3.value = '';
			return;
		}
		else
		{
			
		}
		
		// xhr call
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		 
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"CUSTKYCVERIFICATIONREGISTRATION",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				//'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"loginId":"'+_obj.loginId+'",'+ 
				'"sessionId":"'+_obj.sessionId+'",'+
				'"ownerId":"'+_obj.ownId+'",'+
				'"operatingmodeId":"INTL",'+
				'"roleId":"SEND",'+
				'"flatNo":"'+_obj.flat+'",'+
				'"buildingNo":"'+_obj.bldgNo+'",'+
				'"street1":"'+_obj.street1+'",'+
				'"street2":"'+_obj.street2+'",'+
				'"locality":"'+_obj.locality+'",'+
				'"subLocality":"'+_obj.sub_locality+'",'+
				'"vendorId":"F",'+
				'"zipCode":"'+_obj.txtZip.value+'",'+
				'"city":"'+_obj.city+'",'+
				'"state":"'+_obj.state+'",'+
				'"country":"'+countryName[0]+'",'+
				'"ssn":"'+ssn+'",'+
				'"mrzLine":"",'+
				'"drivingLicenseNo":"",'+
				'"passportNo":"",'+
				'"passportIssuePlace":"",'+
				'"passNationality":"",'+
				'"passportPersonalNo":"",'+
				'"passportCountry":"",'+
				'"passportBirthPlace":"",'+
				'"medicareNo":"",'+
				'"medicareRefNo":"",'+
				'"surnameAtCitizenShip":"",'+
				'"surnameAtBirth":"",'+
				'"countryOfBirth":"",'+
				'"passExpiryDate":"",'+
				'"countryCode":"'+countryCode[0]+'",'+
				'"dayTimeNumber":"",'+
				'"mobileNo":"",'+
				'"privacyPolicyFlag":"Y"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
		

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				
				destroy_kyc();
				
				/*if(arg === 'bank')
				{
					setTimeout(function(e){
						require('/js/ach/AddBankModal').AddBankModal();
					},200);	
				}
				else
				{
					Ti.App.fireEvent('changekycFlag');	
				}*/
				Ti.App.fireEvent('changekycFlag');	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
		
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		//alert("Please fill KYC")
		destroy_kyc();
	});
	
	_obj.winKYC.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_kyc();
				alertDialog = null;
			}
		});
	});
	
	function destroy_kyc()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove registration start ##############');
			
			_obj.winKYC.close();
			require('/utils/RemoveViews').removeViews(_obj.winKYC,_obj.globalView);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_kyc',destroy_kyc);
			require('/utils/Console').info('############## Remove registration end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_kyc', destroy_kyc);
}; // KYCModal()