exports.KYCModal = function()
{
	require('/lib/analytics').GATrackScreen('KYC Form UK');
	
	var _obj = {
		style : require('/styles/kyc/uk/KYC').KYC,
		winKYC : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		
		kycView : null,
		txtAptNo : null,
		borderViewS21 : null,
		txtBldgNo : null,
		borderViewS22 : null,
		txtStreet1 : null,
		borderViewS23 : null,
		txtStreet2 : null,
		borderViewS24 : null,
		txtLocality : null,
		borderViewS25 : null,
		txtSubLocality : null,
		borderViewS26 : null,
		cityView : null,
		txtCity : null,
		imgCity : null,
		borderViewS27 : null,
		stateView : null,
		lblState : null,
		imgState : null,
		borderViewS28 : null,
		txtZip : null,
		borderViewS29 : null,
		
		doneDay : null,
		dayView : null,
		lblDay : null,
		txtDay : null,
		borderViewS30 : null,
		
		doneMobile : null,
		mobileView : null,
		lblMobile : null,
		txtMobile : null,
		borderViewS31 : null,
		
		tabView : null,
		tabSelView : null,
		lblPassport : null,
		lblDriving : null,
		tabSelIconView : null,
		imgTabSel : null,
		scrollableView : null,
		
		btnSubmitPassport : null,
		btnSubmitDriving : null,
		
		flat : null,
		bldgNo : null,
		street1 : null,
		street2 : null,
		locality : null,
		sub_locality : null,
		city : null,
		state : null,
		tab : null, 
	};
	
	_obj.tab = 'passport';
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winKYC = Titanium.UI.createWindow(_obj.style.winKYC);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winKYC);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Help us know you better';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	/////////////////// KYC ///////////////////
	
	_obj.kycView = Ti.UI.createScrollView(_obj.style.kycView);
	
	_obj.txtAptNo = Ti.UI.createTextField(_obj.style.txtAptNo);
	
	if(countryName[0] === 'Germany' || countryName[0] === 'Singapore' || countryName[0] === 'United Arab Emirates' || countryName[0] === 'Australia' || countryName[0] === 'United States')
	{
		_obj.txtAptNo.hintText = "Flat/House No.*";
	}
	else if(countryName[0] === 'United Kingdom')
	{
		_obj.txtAptNo.hintText = 'House/Apartment No.*';
	}
	else
	{
		_obj.txtAptNo.hintText = 'Flat/Unit No.*';
	}
	
	_obj.txtAptNo.maxLength = 25;
	_obj.borderViewS21 = Ti.UI.createView(_obj.style.borderView);
	
	
	if(countryName[0] !== 'Australia')
	{
		_obj.txtBldgNo = Ti.UI.createTextField(_obj.style.txtBldgNo);
		_obj.txtBldgNo.hintText = 'Building No./Name';
		_obj.txtBldgNo.maxLength = 25;
		_obj.borderViewS22 = Ti.UI.createView(_obj.style.borderView);
	}
	
	_obj.txtStreet1 = Ti.UI.createTextField(_obj.style.txtStreet1);
	
	if(countryName[0] === 'Australia' || countryName[0] === 'United Arab Emirates')
	{
		_obj.txtStreet1.hintText = "Street*";
		_obj.txtStreet1.maxLength = 25;
		_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
 	}
	else 
	{
		_obj.txtStreet1.hintText = "Street 1*";
		_obj.txtStreet1.maxLength = 25;
		_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtStreet2 = Ti.UI.createTextField(_obj.style.txtStreet2);
		_obj.txtStreet2.hintText = 'Street 2';
		_obj.txtStreet2.maxLength = 25;
		_obj.borderViewS24 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtLocality = Ti.UI.createTextField(_obj.style.txtLocality);
		_obj.txtLocality.hintText = 'Locality';
		_obj.txtLocality.maxLength = 25;
		_obj.borderViewS25 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtSubLocality = Ti.UI.createTextField(_obj.style.txtSubLocality);
		_obj.txtSubLocality.hintText = 'Sub-Locality';
		_obj.txtSubLocality.maxLength = 25;
		_obj.borderViewS26 = Ti.UI.createView(_obj.style.borderView);
	}
	
	if(countryName[0] === 'United Arab Emirates')
	{
		_obj.cityView = Ti.UI.createView(_obj.style.cityView);
		_obj.txtCity = Ti.UI.createLabel(_obj.style.txtCity);
		_obj.txtCity.top = 0;
		_obj.txtCity.left = 0;
		_obj.txtCity.text = 'Select City*';
		_obj.imgCity = Ti.UI.createImageView(_obj.style.imgCity);
		
		_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.cityView.addEventListener('click',function(){
			activityIndicator.showIndicator();
			var cityOptions = [];
			
			var xhr = require('/utils/XHR_BCM');
	
			xhr.call({
				url : TiGlobals.appURLBCM,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"'+TiGlobals.bcmConfig+'",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"source":"'+TiGlobals.bcmSource+'",'+
					'"type":"citylisting",'+
					'"platform":"'+TiGlobals.osname+'",'+
					'"corridor":"'+countryName[0]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				activityIndicator.hideIndicator();
				if(e.result.status === "S")
				{
					for(var i=0;i<e.result.response[0].citylisting.UAE.length;i++)
					{
						cityOptions.push(e.result.response[0].citylisting.UAE[i]);
					}
					
					var optionDialogCity = require('/utils/OptionDialog').showOptionDialog('City',cityOptions,-1);
					optionDialogCity.show();
					
					optionDialogCity.addEventListener('click',function(evt){
						if(optionDialogCity.options[evt.index] !== L('btn_cancel'))
						{
							_obj.txtCity.text = optionDialogCity.options[evt.index];
							optionDialogCity = null;
						}
						else
						{
							optionDialogCity = null;
							_obj.txtCity.text = 'Select City*';
						}
					});	
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_kyc();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		});
	}
	else
	{
		_obj.txtCity = Ti.UI.createTextField(_obj.style.txtCity);
		_obj.txtCity.hintText = 'City*';
		_obj.txtCity.maxLength = 25;
		_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
	}
	
	if(countryName[0] !== 'Singapore')
	{
		if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
		{
			
			_obj.stateView = Ti.UI.createView(_obj.style.stateView);
			_obj.lblState = Ti.UI.createLabel(_obj.style.lblState);
			_obj.imgState = Ti.UI.createImageView(_obj.style.imgState);
			_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
			
			if(countryName[0] === 'United Arab Emirates')
			{
				_obj.lblState.text = 'Select Emirates*';	
			}
			else
			{
				_obj.lblState.text = 'Select State*';	
			}
			
			_obj.stateView.addEventListener('click',function(){
				activityIndicator.showIndicator();
				var stateOptions = [];
				
				var xhr = require('/utils/XHR');
		
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"country":"'+countryName[0]+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
		
				function xhrSuccess(e) {
					activityIndicator.hideIndicator();
					if(e.result.responseFlag === "S")
					{
						var splitArr = e.result.countryRelatedStates.split('~');
				
						for(var i=0;i<splitArr.length;i++)
						{
							stateOptions.push(splitArr[i]);
						}
						
						var optionDialogState = require('/utils/OptionDialog').showOptionDialog('State',stateOptions,-1);
						optionDialogState.show();
						
						optionDialogState.addEventListener('click',function(evt){
							if(optionDialogState.options[evt.index] !== L('btn_cancel'))
							{
								_obj.lblState.text = optionDialogState.options[evt.index];
								optionDialogState = null;
							}
							else
							{
								optionDialogState = null;
								
								if(countryName[0] === 'United Arab Emirates')
								{
									_obj.lblState.text = 'Select Emirates*';	
								}
								else
								{
									_obj.lblState.text = 'Select State*';	
								}
							}
						});	
					}
					else
					{
						if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_kyc();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
						}
					}
					xhr = null;
				}
		
				function xhrError(e) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}
			});

        }
        else
        {
			_obj.lblState = Ti.UI.createTextField(_obj.style.txtCity);
			_obj.lblState.hintText = 'State*';
			_obj.lblState.maxLength = 25;
			_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
        }
	}
	
	_obj.txtZip = Ti.UI.createTextField(_obj.style.txtZip);
	_obj.txtZip.hintText = 'Zip Code*';
	
	_obj.doneZip = Ti.UI.createButton(_obj.style.done);
	_obj.doneZip.addEventListener('click',function(){
		_obj.txtZip.blur();
	});
	
	////// Pin code hintText ///////
	
	switch(countryName[0]){
		case 'United States' :
		{
			_obj.txtZip.hintText = 'Zip Code (5 digits)*';
			_obj.txtZip.maxLength = 5;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Canada' :
		{
			_obj.txtZip.hintText = 'Zip Code (7 alpha-numeric)*';
			_obj.txtZip.maxLength = 7;
			break;
		}
		case 'Australia' :
		{
			_obj.txtZip.hintText = 'Zip Code (4 digits)*';
			_obj.txtZip.maxLength = 4;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Hong Kong' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Japan' :
		{
			_obj.txtZip.hintText = 'Zip Code (7 digits)*';
			_obj.txtZip.maxLength = 7;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'New Zealand' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Singapore' :
		{
			_obj.txtZip.hintText = 'Postal Code (6 digits)*';
			_obj.txtZip.maxLength = 6;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Ireland' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'United Kingdom' :
		{
			_obj.txtZip.hintText = 'Postcode (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'United Arab Emirates' :
		{
			_obj.txtZip.hintText = 'P. O. Box (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Austria' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Belgium' :
		{
			_obj.txtZip.hintText = 'Zip Code (4 digits)*';
			_obj.txtZip.maxLength = 4;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Cyprus' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Finland' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'France' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Germany' :
		{
			_obj.txtZip.hintText = 'Postal Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}		 
		case 'Greece' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Italy' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Luxembourg' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Malta' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}		
		case 'Netherlands' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Portugal' :
		{
			_obj.txtZip.hintText = 'Zip Code (25 alpha-numeric)*';
			_obj.txtZip.maxLength = 25;
			break;
		}
		case 'Slovakia' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Slovenia' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Spain' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		default:
		{
			break;
		}
	}
	
	_obj.borderViewS29 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneDay = Ti.UI.createButton(_obj.style.done);
	_obj.doneDay.addEventListener('click',function(){
		_obj.txtDay.blur();
	});
	
	_obj.dayView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblDay = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblDay.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtDay = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtDay.hintText = 'Day Time Number*';
	_obj.txtDay.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDay.keyboardToolbar = [_obj.doneDay];
	_obj.borderViewS30 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneMobile = Ti.UI.createButton(_obj.style.done);
	_obj.doneMobile.addEventListener('click',function(){
		_obj.txtMobile.blur();
	});
	
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblMobile = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblMobile.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtMobile = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtMobile.hintText = 'Mobile Number*';
	_obj.txtMobile.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtMobile.keyboardToolbar = [_obj.doneMobile];
	_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderView);
	
	// Set HintText for mobile and day nos
	
	switch(countryName[0]){
		case 'United States' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'United Kingdom' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;			break;
		}
		case 'Australia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		 }
		case 'Singapore' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			_obj.txtDay.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDay.maxLength = 8;
			break;
		 }
		 case 'Germany' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			_obj.txtDay.hintText = 'Day Time Number*';
			_obj.txtDay.maxLength = 14;
			break;
		 }			 
		 case 'Canada' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Belgium' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDay.maxLength = 8;
			break;
		}		
		case 'Cyprus' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}			
		case 'Finland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}			
		case 'France' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Greece' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Ireland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Italy' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Japan' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Luxembourg' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Malta' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}		
		case 'Netherlands' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		}
		case 'Portugal' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		}
		case 'Slovakia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Slovenia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Hong Kong' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		
		case 'Austria' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		
		case 'New Zealand' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		}
		
		case 'Spain' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		
		case 'United Arab Emirates' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		}
		
		default:
		{
			break;
		}
	}
	
	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.lblPassport = Ti.UI.createLabel(_obj.style.lblPassport);
	_obj.lblPassport.text = 'Passport Details';
	_obj.lblDriving = Ti.UI.createLabel(_obj.style.lblDriving);
	_obj.lblDriving.text = 'Driving Licence Details';
	
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
		
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	_obj.scrollableView.height = 750;
	_obj.scrollableView.scrollingEnabled = false;
	
	// Passport 
	_obj.passportView = Ti.UI.createView(_obj.style.stepView);
	_obj.lblNote = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNote.text = 'Note';
	
	_obj.lblNoteTxt = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNoteTxt.text = '\u00B7 If you are entering your passport details, please ensure that the name (First Name, First Middle Name, Last Name) mentioned for registration is as per the details mentioned on your passport';
	_obj.lblNoteTxt.top = 5; 
	
	_obj.txtPassportNo = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtPassportNo.maxLength = 9;
	_obj.txtPassportNo.hintText = 'Passport No*';
	_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.countryView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblCountry = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblCountry.text = 'Passport Issue Place*';
	_obj.imgCountry = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewS33 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.countryView.addEventListener('click',function(){
		country();
	});
	
	_obj.expiryView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblExpiry = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblExpiry.text = 'Passport Expiry Date*';
	_obj.imgExpiry = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS34 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.expiryView.addEventListener('click',function(e){
		require('/utils/DatePicker').DatePicker(_obj.lblExpiry,'normal');
		
	});
	
	_obj.nationalityView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblNationality = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblNationality.text = 'Passport Nationality*';
	_obj.imgNationality = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewS35 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.nationalityView.addEventListener('click',function(){
		nationality();
	});
	
	_obj.txtPassportPersonalNo = Ti.UI.createTextField(_obj.style.txtPassportPersonalNo);
	_obj.txtPassportPersonalNo.maxLength = 16;
	_obj.txtPassportPersonalNo.hintText = 'Passport Personal No.*';
	_obj.borderViewS36 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.lblMRZ = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblMRZ.text = 'MRZ No.*';
	_obj.lblMRZ.top = 15;
	_obj.mrzView = Ti.UI.createView(_obj.style.mrzView);
	
	_obj.txtMRZ1 = Ti.UI.createTextField(_obj.style.txtMRZ1);
	_obj.txtMRZ1.maxLength = 8;
	_obj.txtMRZ1.hintText = 'xxxxxxxx';
	_obj.borderMRZ1 = Ti.UI.createView(_obj.style.borderMRZ1);
	
	_obj.txtMRZ2 = Ti.UI.createTextField(_obj.style.txtMRZ2);
	_obj.borderMRZ2 = Ti.UI.createView(_obj.style.borderMRZ2);
	_obj.txtMRZ2.value = '<';
	_obj.txtMRZ2.editable = false;
	
	_obj.txtMRZ3 = Ti.UI.createTextField(_obj.style.txtMRZ3);
	_obj.txtMRZ3.maxLength = 19;
	_obj.txtMRZ3.hintText = 'xxxxxxxxxxxxxxxxxxx';
	_obj.borderMRZ3 = Ti.UI.createView(_obj.style.borderMRZ3);
	
	_obj.txtMRZ4 = Ti.UI.createTextField(_obj.style.txtMRZ4);
	_obj.borderMRZ4 = Ti.UI.createView(_obj.style.borderMRZ4);
	_obj.txtMRZ4.value = '<<<<<<<<<<<<<';
	_obj.txtMRZ4.editable = false;
	
	_obj.txtMRZ5 = Ti.UI.createTextField(_obj.style.txtMRZ5);
	_obj.borderMRZ5 = Ti.UI.createView(_obj.style.borderMRZ5);
	_obj.txtMRZ5.maxLength = 2;
	_obj.txtMRZ5.hintText = 'xx';
	
	_obj.imgPassport = Ti.UI.createImageView(_obj.style.imgPassport);
	
	_obj.btnSubmitPassport = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmitPassport.title = 'SUBMIT';
	
	// Driving
	_obj.drivingView = Ti.UI.createView(_obj.style.stepView);
	_obj.lblNote1 = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNote1.text = 'Note';
	
	_obj.lblNoteTxt1 = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNoteTxt1.text = '\u00B7 If you are entering your driving license number, please ensure that the name (First Name, First Middle Name, Last Name) mentioned for registration is as per the details mentioned on your driving license';
	_obj.lblNoteTxt1.top = 5; 
	
	_obj.txtLicense = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtLicense.maxLength = 16;                  //Sanjivani code
	_obj.txtLicense.hintText = 'Driving License No*';
	_obj.borderViewS40 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.btnSubmitDriving = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmitDriving.title = 'SUBMIT';
	
	_obj.passportView.add(_obj.lblNote);
	_obj.passportView.add(_obj.lblNoteTxt);
	_obj.passportView.add(_obj.txtPassportNo);
	_obj.passportView.add(_obj.borderViewS32);
	_obj.countryView.add(_obj.lblCountry);
	_obj.countryView.add(_obj.imgCountry);
	_obj.passportView.add(_obj.countryView);
	_obj.passportView.add(_obj.borderViewS33);
	_obj.expiryView.add(_obj.lblExpiry);
	_obj.expiryView.add(_obj.imgExpiry);
	_obj.passportView.add(_obj.expiryView);
	_obj.passportView.add(_obj.borderViewS34);
	_obj.nationalityView.add(_obj.lblNationality);
	_obj.nationalityView.add(_obj.imgNationality);
	_obj.passportView.add(_obj.nationalityView);
	_obj.passportView.add(_obj.borderViewS35);
	_obj.passportView.add(_obj.txtPassportPersonalNo);
	_obj.passportView.add(_obj.borderViewS36);
	_obj.passportView.add(_obj.lblMRZ);
	_obj.mrzView.add(_obj.txtMRZ1);
	_obj.mrzView.add(_obj.borderMRZ1);
	_obj.mrzView.add(_obj.txtMRZ2);
	_obj.mrzView.add(_obj.borderMRZ2);
	_obj.mrzView.add(_obj.txtMRZ3);
	_obj.mrzView.add(_obj.borderMRZ3);
	_obj.mrzView.add(_obj.txtMRZ4);
	_obj.mrzView.add(_obj.borderMRZ4);
	_obj.mrzView.add(_obj.txtMRZ5);
	_obj.mrzView.add(_obj.borderMRZ5);
	_obj.passportView.add(_obj.mrzView);
	_obj.passportView.add(_obj.imgPassport);
	_obj.passportView.add(_obj.btnSubmitPassport);
	
	_obj.drivingView.add(_obj.lblNote1);
	_obj.drivingView.add(_obj.lblNoteTxt1);
	_obj.drivingView.add(_obj.txtLicense);
	_obj.drivingView.add(_obj.borderViewS40);
	_obj.drivingView.add(_obj.btnSubmitDriving);
	
	_obj.scrollableView.views = [_obj.passportView,_obj.drivingView];
	
	_obj.kycView.add(_obj.txtAptNo);
	_obj.kycView.add(_obj.borderViewS21);
	
	if(countryName[0] !== 'Australia')
	{
		_obj.kycView.add(_obj.txtBldgNo);
		_obj.kycView.add(_obj.borderViewS22);
	}
	_obj.kycView.add(_obj.txtStreet1);
	_obj.kycView.add(_obj.borderViewS23);
	
	if(countryName[0] !== 'Australia' || countryName[0] !== 'United Arab Emirates')
	{
		try{
			_obj.kycView.add(_obj.txtStreet2);
			_obj.kycView.add(_obj.borderViewS24);
	
			_obj.kycView.add(_obj.txtLocality);
			_obj.kycView.add(_obj.borderViewS25);
			
			_obj.kycView.add(_obj.txtSubLocality);
			_obj.kycView.add(_obj.borderViewS26);
		}catch(e){}
	}
	
	if(countryName[0] === 'United Arab Emirates')
	{
		_obj.cityView.add(_obj.txtCity);
		_obj.cityView.add(_obj.imgCity);
		_obj.kycView.add(_obj.cityView);
		_obj.kycView.add(_obj.borderViewS27);
	}	
	else
	{
		_obj.kycView.add(_obj.txtCity);
		_obj.kycView.add(_obj.borderViewS27);
	}
	
	if(countryName[0] !== 'Singapore')
	{
		if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
		{
			_obj.stateView.add(_obj.lblState);
			_obj.stateView.add(_obj.imgState);
			_obj.kycView.add(_obj.stateView);
			_obj.kycView.add(_obj.borderViewS28);
        }
        else
        {
			_obj.kycView.add(_obj.lblState);
			_obj.kycView.add(_obj.borderViewS28);
        }
	}
	
	_obj.kycView.add(_obj.txtZip);
	_obj.kycView.add(_obj.borderViewS29);
	
	_obj.dayView.add(_obj.lblDay);
	_obj.dayView.add(_obj.txtDay);
	_obj.kycView.add(_obj.dayView);
	_obj.kycView.add(_obj.borderViewS30);
	
	_obj.mobileView.add(_obj.lblMobile);
	_obj.mobileView.add(_obj.txtMobile);
	_obj.kycView.add(_obj.mobileView);
	_obj.kycView.add(_obj.borderViewS31);
	
	_obj.kycView.add(_obj.tabView);
	_obj.tabView.add(_obj.lblPassport);
	_obj.tabView.add(_obj.lblDriving);
	_obj.tabView.add(_obj.tabSelView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.tabView.add(_obj.tabSelIconView);
	_obj.kycView.add(_obj.scrollableView);
	
	_obj.mainView.add(_obj.kycView);
	_obj.globalView.add(_obj.mainView);
	_obj.winKYC.add(_obj.globalView);
	_obj.winKYC.open();
	
	function changeTabs(selected)
	{
		if(selected === 'passport')
		{
			_obj.tabSelView.left = 0;
			_obj.tabSelView.right = null;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			_obj.tabSelIconView.width = '50%';
			_obj.lblPassport.color = TiFonts.FontStyle('whiteFont');
			_obj.lblPassport.backgroundColor = '#6F6F6F';
			_obj.lblDriving.color = TiFonts.FontStyle('blackFont');
			_obj.lblDriving.backgroundColor = '#E9E9E9';
		}
		else
		{
			_obj.tabSelView.left = null;
			_obj.tabSelView.right = 0;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = null;
			_obj.tabSelIconView.right = 0;
			_obj.tabSelIconView.width = '50%';
			_obj.lblDriving.color = TiFonts.FontStyle('whiteFont');
			_obj.lblDriving.backgroundColor = '#6F6F6F';
			_obj.lblPassport.color = TiFonts.FontStyle('blackFont');
			_obj.lblPassport.backgroundColor = '#E9E9E9';
		}
	}
	
	_obj.tabView.addEventListener('click',function(e){
		if(e.source.sel === 'passport' && _obj.tab !== 'passport')
		{
			_obj.scrollableView.movePrevious();
			changeTabs('passport');
			_obj.tab = 'passport';
		}
		
		if(e.source.sel === 'driving' && _obj.tab !== 'driving')
		{
			_obj.scrollableView.moveNext();
			changeTabs('driving');
			_obj.tab = 'driving';
		}
	});
	
	function submitKYC()
	{
		// Apartment/House No.
		_obj.flat = _obj.txtAptNo.value; 
		if(_obj.txtAptNo.value == '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your ' + _obj.txtAptNo.hintText,[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtAptNo.hintText + ' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('',_obj.txtAptNo.hintText + ' cannot contain  |  or  \'  or  ` or  \" or #',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtAptNo.value.charAt(i)+' entered for '+_obj.txtAptNo.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		// Building No.
		if(countryName[0] !== 'Australia')
		{
			_obj.bldgNo = _obj.txtBldgNo.value;
			if(_obj.txtBldgNo.value.length > 0)
			{
				if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtBldgNo.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtBldgNo.hintText + ' field',[L('btn_ok')]).show();
		    		_obj.txtBldgNo.value = '';
		    		_obj.txtBldgNo.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
				else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtBldgNo.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtBldgNo.value.charAt(i)+' entered for '+_obj.txtBldgNo.hintText+' field',[L('btn_ok')]).show();
		    		_obj.txtBldgNo.value = '';
		    		_obj.txtBldgNo.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
			}
			else
			{
				require('/utils/AlertDialog').showAlert('','Please provide a valid ' + _obj.txtBldgNo.hintText,[L('btn_ok')]).show();
	    		_obj.txtBldgNo.value = '';
	    		_obj.txtBldgNo.focus();
	    		_obj.kycView.scrollTo(0,0);
				return;
			}
		}
		else
		{
			_obj.bldgNo = '';
		}
		
		//Street1
		
		_obj.street1 = _obj.txtStreet1.value;
		if(_obj.txtStreet1.value === "")
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Street Name',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street Name',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Street Name cannot contain | or \' or ` or \" or #',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(_obj.txtStreet1.value.search("[^a-zA-Z ]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Street Name field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtStreet1.value.charAt(i)+' entered for '+_obj.txtStreet1.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		
		if(countryName[0] !== 'Australia' || countryName[0] !== 'United Arab Emirates')
		{
			try{
				// Street 2
				_obj.street2 = _obj.txtStreet2.value;
				
				if(_obj.txtStreet2.value.length > 0)
				{
					if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreet2.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street 2 field',[L('btn_ok')]).show();
			    		_obj.txtStreet2.value = '';
			    		_obj.txtStreet2.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if(_obj.txtStreet2.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Street 2 field',[L('btn_ok')]).show();
			    		_obj.txtStreet2.value = '';
			    		_obj.txtStreet2.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else
					{
						
					}
				}
				
				// Locality
				_obj.locality = _obj.txtLocality.value;
			
				if(_obj.txtLocality.value.length > 0)
				{
					if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLocality.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of locality',[L('btn_ok')]).show();
			    		_obj.txtLocality.value = '';
			    		_obj.txtLocality.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if(_obj.txtLocality.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in locality field',[L('btn_ok')]).show();
			    		_obj.txtLocality.value = '';
			    		_obj.txtLocality.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else
					{
					}
				}
			
				// Sub Locality
				_obj.sub_locality = _obj.txtSubLocality.value;
				
				if(_obj.txtSubLocality.value.length > 0)
				{
					if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtSubLocality.value) === false)
					{
						require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Sub Locality',[L('btn_ok')]).show();
						_obj.txtSubLocality.value = '';
			    		_obj.txtSubLocality.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if(_obj.txtSubLocality.value.search("[^a-zA-Z0-9 ]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Sub Locality field',[L('btn_ok')]).show();
			    		_obj.txtSubLocality.value = '';
			    		_obj.txtSubLocality.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else
					{
						
					}
				}
			}catch(e){}
		}
		
		// City
		
		if(countryName[0] === 'United Arab Emirates')
		{
			_obj.city = _obj.txtCity.text;
			if(_obj.txtCity.text === 'Select City*')
			{
				require('/utils/AlertDialog').showAlert('','Please select your city',[L('btn_ok')]).show();
	    		_obj.txtCity.text = 'Select City*';
	    		_obj.kycView.scrollTo(0,0);
				return;
			}
			else
			{
			}
		}
		else
		{
			_obj.city = _obj.txtCity.value;
			
			if(_obj.txtCity.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter your city',[L('btn_ok')]).show();
	    		_obj.txtCity.value = '';
	    		_obj.txtCity.focus();
	    		_obj.kycView.scrollTo(0,0);
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtCity.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of city',[L('btn_ok')]).show();
	    		_obj.txtCity.value = '';
	    		_obj.txtCity.focus();
	    		_obj.kycView.scrollTo(0,0);
				return;
			}
			else if(_obj.txtCity.value.search("[^a-zA-Z ]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in city',[L('btn_ok')]).show();
	    		_obj.txtCity.value = '';
	    		_obj.txtCity.focus();
	    		_obj.kycView.scrollTo(0,0);
				return;
			}
			else
			{
			}	
		}
		
		//State
		if(countryName[0] !== 'Singapore')
		{
			if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
			{
				
				_obj.state = _obj.lblState.text;
				
				if(countryName[0] === 'United Arab Emirates')
				{
					if(_obj.lblState.text === 'Select Emirates*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your Emirate',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select Emirates*';
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else
					{
					}	
				}
				else
				{
					if(_obj.lblState.text === 'Select State*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your state',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select State*';
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else
					{
					}
				}
	
	        }
	        else
	        {
				_obj.state = _obj.lblState.value;
				
				if(_obj.lblState.value === '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your state',[L('btn_ok')]).show();
		    		_obj.lblState.value = '';
		    		_obj.lblState.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
				else if(require('/lib/toml_validations').checkBeginningSpace(_obj.lblState.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of state',[L('btn_ok')]).show();
		    		_obj.lblState.value = '';
		    		_obj.lblState.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
				else if(_obj.lblState.value.search("[^a-zA-Z ]") >= 0)
				{
					require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in state',[L('btn_ok')]).show();
		    		_obj.lblState.value = '';
		    		_obj.lblState.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
				else
				{
				}
				
	        }
		}
		
		// Zipcode
		
		if(_obj.txtZip.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your '+_obj.txtZip.hintText,[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtZip.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of '+_obj.txtZip.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		
		if(_obj.txtZip.value.length > 0)
		{
	 		var len = _obj.txtZip.value.length;
			var validchars = '';	
			validchars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
			var currChar, prevChar, nextChar;
			var validAlph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var validNum = "0123456789";
			var validAlphFlag = false;
			var validNumFlag = false;
	
			for(i = 0; i <= len - 1; i++)
			{
				currChar = _obj.txtZip.value.charAt(i);
				prevChar = _obj.txtZip.value.charAt(i-1);
				nextChar = _obj.txtZip.value.charAt(i+1);
	
				if(!(validAlph.indexOf(currChar) == -1) && validAlphFlag == false)
				{
					validAlphFlag = true;
				}
	
				if(!(validNum.indexOf(currChar) == -1) && validNumFlag == false)
				{
					validNumFlag = true;
				}
							
				if(validchars.indexOf(currChar) == -1)
				{
					if(countryName[0] != 'Canada')
					{
						require('/utils/AlertDialog').showAlert('','Invalid character "' + currChar + '" entered in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
				}
	
				if(currChar == ' ' && prevChar == ' ' || prevChar == ' ' && currChar == ' ' && nextChar == ' ' || prevChar == ' ' && currChar == ' ' && i == len-1 || currChar == ' ' && i == len-1)
				{
					if(countryName[0] == 'United Kingdom')
					{
						if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText + '. Only one space is allowed in middle',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else
					{
						if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					break;
				}
			}
						
			if((countryName[0] != 'United Arab Emirates') && (countryName[0] != 'UAE') && (countryName[0] != 'Hong Kong')) //Country check for HK & UAE
			{		
				if((countryName[0] == 'Singapore') || (countryName[0] == 'Australia') || (countryName[0] == 'United States') || (countryName[0] == 'Portugal'))
				{
					if(countryName[0] != 'Portugal')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
	
					if((countryName[0] == 'Singapore') && (len != 6))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 6 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if((countryName[0] == 'Australia') && (len != 4))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if((countryName[0] == 'United States') && (len != 5))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if((countryName[0] == 'Portugal'))
					{			
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else if( len < 3 || len > 25)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 25 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
				}
				else
				{
					if(countryName[0] == 'Canada')
					{
						var pcode = _obj.txtZip.value;
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else if(pcode.indexOf(" ") != 3)
						{
							require('/utils/AlertDialog').showAlert('','There must be a space after 3rd character in '+_obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'United Kingdom')
					{			
						if( len < 5 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 5 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else
					{
						if( len < 3 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
	
					if((countryName[0] == 'France') || (countryName[0] == 'Germany') || (countryName[0] == 'Italy') || (countryName[0] == 'Euroland') || (countryName[0] == 'New Zealand'))
					{
						var pncode = _obj.txtZip.value;
						if(pncode.split(" ").length > 2)
						{
							require('/utils/AlertDialog').showAlert('','Only one single space is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
							
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'Belgium')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
	
						if(len != 4)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'Spain')
					{
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'United States')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						if(len != 5)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'Japan')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
							
						}
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] != 'Spain')
					{ 
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
				}
			} 
		 }
		 
		////////////////////// Mobile No ////////////////////
		if(_obj.txtMobile.value === '' || _obj.txtMobile.value === 'Mobile Number*')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(countryName[0] === 'United Kingdom')
		{ 						
			if(_obj.txtMobile.value.charAt(0) === 0 && _obj.txtMobile.value != '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Mobile Number without 0 in the beginning',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
		|| countryName[0] === 'Finland' || countryName[0] === 'France' 
		|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
		|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
		|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
		 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal' || countryName[0] === 'Belgium' || countryName[0] === 'Netherlands')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'Germany')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 11))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Cyprus')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 11) && countryName[0] === 'New Zealand')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if((_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Spain')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if((_obj.txtMobile.value.length < 10 || _obj.txtMobile.value.length > 11) && countryName[0] === 'Austria')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if((_obj.txtMobile.value.length < 7 || _obj.txtMobile.value.length > 9) && ((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')))
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else
		{
			
		}
		
		////////////////////// Daytime No ////////////////////
		//if UAE than it is not cumpusory
		if(countryName[0] != 'United Arab Emirates' && countryName[0] != 'UAE') 
		{		
			if(_obj.txtDay.value === '' || _obj.txtDay.value === 'Day Time Number*')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDay.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDay.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if((_obj.txtDay.value.length < 9 || _obj.txtDay.value.length > 10) && countryName[0] === 'Spain')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(_obj.txtDay.value.length != 0 && _obj.txtDay.value.length < 7 && countryName[0] === 'New Zealand')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if((_obj.txtDay.value.length < 3 || _obj.txtDay.value.length > 9) && countryName[0] === 'Germany')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(countryName[0] === 'United Kingdom')
			{
				if(_obj.txtDay.value.charAt(0) === 0 && _obj.txtDay.value != '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your Day Time Number without 0 in the beginning',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
				else if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
			else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
			|| countryName[0] === 'Finland' || countryName[0] === 'France' 
			|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
			|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
			|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
			 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
			{ 									
				if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
			else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal')
			{ 									
				if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 9))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
			else if(countryName[0] === 'Belgium' || countryName[0] === 'Netherlands' || countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
			{ 									
				if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 8))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
		}
		if(((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')) && _obj.txtDay.value.length > 0)
		{
			if(_obj.txtDay.value.length < 7 || _obj.txtDay.value.length > 9)
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		
		if(_obj.txtMobile.value === _obj.txtDay.value)
		{
			require('/utils/AlertDialog').showAlert('','Day Time No. and Mobile No. cannot be the same',[L('btn_ok')]).show();
    		_obj.txtDay.value = '';
    		_obj.txtDay.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		
		// If Passport
		
		if(_obj.tab === 'passport')
		{
			// Passport No
			if(_obj.txtPassportNo.value.trim() === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a passport number',[L('btn_ok')]).show();
	    		_obj.txtPassportNo.value = '';
	    		_obj.txtPassportNo.focus();
				return;		
			}
			else
			{
				
			}
			
			// Passport Issue Place
			if(_obj.lblCountry.text === 'Passport Issue Place*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a passport country',[L('btn_ok')]).show();
	    		return;		
			}
			else
			{
				
			}
				
			// Passport Expiry Date
			if(_obj.lblExpiry.text === 'Passport Expiry Date*')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a passport expiry date',[L('btn_ok')]).show();
	    		return;		
			}
			else
			{
				
			}
			
			// Passport Nationality
			if(_obj.lblNationality.text === 'Passport Nationality*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a passport nationality',[L('btn_ok')]).show();
	    		return;		
			}
			else
			{
				
			}
			
			// Passport Personal No
			if(_obj.txtPassportPersonalNo.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a passport personal number',[L('btn_ok')]).show();
	    		_obj.txtPassportPersonalNo.value = '';
	    		_obj.txtPassportPersonalNo.focus();
	    		return;		
			}
			else
			{
				
			}
			
			// MRZ Validation Starts
			var mrz1 = _obj.txtMRZ1.value;
			var mrz2 = _obj.txtMRZ3.value;
			var mrz3 = _obj.txtMRZ5.value;
			if(mrz1.trim() === '' || mrz2.trim() === '' || mrz3.trim() === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide your valid MRZ Number',[L('btn_ok')]).show();
	    		return;
			}
			else if(mrz1.length != 8)
			{
				require('/utils/AlertDialog').showAlert('','The first section of MRZ Number should be of 8 digits',[L('btn_ok')]).show();
	    		_obj.txtMRZ1.value = '';
	    		_obj.txtMRZ1.focus();
	    		return;				
			}
			else if(mrz2.length != 19)
			{
				require('/utils/AlertDialog').showAlert('','The second section of MRZ Number should be of 19 digits',[L('btn_ok')]).show();
	    		_obj.txtMRZ2.value = '';
	    		_obj.txtMRZ2.focus();
	    		return;
			}
			else if(mrz3.length != 2)
			{
				require('/utils/AlertDialog').showAlert('','The third section of MRZ Number should be of 2 digits',[L('btn_ok')]).show();
	    		_obj.txtMRZ3.value = '';
	    		_obj.txtMRZ3.focus();
	    		return;
			}
			else
			{
				
			}
		}
		
		if(_obj.tab === 'driving')
	     {
			//Driving Licence No
			if(_obj.txtLicense.value.trim() === '') //||_obj.txtLicense.value.search("[^a-zA-Z0-9]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Please provide 16 digit alpha-numeric driving licence number',[L('btn_ok')]).show();
	    		_obj.txtLicense.value = '';
	    		_obj.txtLicense.focus();
	    		return;		
			}
	
			else if(_obj.txtLicense.value.length > 0)
		{
			if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLocality.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning Driving Licence No',[L('btn_ok')]).show();
				_obj.txtLicense.value= '';
	    		_obj.txtLicense.focus();
	    		//_obj.kycView.scrollTo(0,0);
				return;
			}
			else{
				if(_obj.txtLicense.value.search("[^a-zA-Z0-9 ]") >= 0)
			    {
				require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Driving Licence  field',[L('btn_ok')]).show();
				_obj.txtLicense.value = '';
				_obj.txtLicense.focus();
	    		//_obj.kycView.scrollTo(0,0);
				return;
			  }
				
				}
		}
			else
			{
				
			}
	   }
		
		/*function validateAlpaNumeric()
		{
			var amtinv = _obj.txtLicense;
			var validStr = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890- ";
			  
			for(i=0;i<=_obj.txtLicense.value.length-1;i++)
			{
				if(validStr.indexOf(amtinv.charAt(i))==-1)
				{
					require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in '+msg,[L('btn_ok')]).show();
					return false;
					break;
				}
			}
		};
		
		validateAlpaNumeric();*/
		
		
		
		var dateSplit = _obj.lblExpiry.text.split('/');
		var dt = dateSplit[0]+'-'+require('/utils/Date').monthShort(dateSplit[1])+'-'+dateSplit[2];
		
		// xhr call
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"CUSTKYCVERIFICATIONREGISTRATION",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"operatingmodeId":"INTL",'+
				'"roleId":"SEND",'+
				'"flatNo":"'+_obj.flat+'",'+
				'"buildingNo":"'+_obj.bldgNo+'",'+
				'"street1":"'+_obj.street1+'",'+
				'"street2":"'+_obj.street2+'",'+
				'"locality":"'+_obj.locality+'",'+
				'"subLocality":"'+_obj.sub_locality+'",'+
				'"vendorId":"F",'+
				'"zipCode":"'+_obj.txtZip.value+'",'+
				'"city":"'+_obj.city+'",'+
				'"state":"'+_obj.state+'",'+
				'"country":"'+countryName[0]+'",'+
				'"ssn":"",'+
				'"mrzLine":"'+mrz1+mrz2+mrz3+'",'+
				'"drivingLicenseNo":"'+_obj.txtLicense.value+'",'+
				'"passportNo":"'+_obj.txtPassportNo.value+'",'+
				'"passportIssuePlace":"'+_obj.lblCountry.text+'",'+
				'"passNationality":"'+_obj.lblNationality.text+'",'+
				'"passportPersonalNo":"'+_obj.txtPassportPersonalNo.value+'",'+
				'"passportCountry":"",'+
				'"passportBirthPlace":"",'+
				'"medicareNo":"",'+
				'"medicareRefNo":"",'+
				'"surnameAtCitizenShip":"",'+
				'"surnameAtBirth":"",'+
				'"countryOfBirth":"",'+
				'"passExpiryDate":"'+dt+'",'+
				'"countryCode":"'+countryCode[0]+'",'+
				'"dayTimeNumber":"'+_obj.txtDay.value+'",'+
				'"mobileNo":"'+_obj.txtMobile.value+'",'+
				'"privacyPolicyFlag":"Y"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
		
		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				
				try{
					Ti.App.fireEvent('changekycFlag');
				}catch(e){}
				
				destroy_kyc();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.btnSubmitDriving.addEventListener('click',function(e){
		submitKYC();
	});
	
	_obj.btnSubmitPassport.addEventListener('click',function(e){
		submitKYC();
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_kyc();
	});
	
	_obj.winKYC.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_kyc();
				alertDialog = null;
			}
		});
	});
	
	function country()
	{
		activityIndicator.showIndicator();
		var countryOptions = [];
		
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
		
				for(var i=0;i<splitArr.length;i++)
				{
					countryOptions.push(splitArr[i]);
				}
				
				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Country',countryOptions,-1);
				optionDialogCountry.show();
				
				optionDialogCountry.addEventListener('click',function(evt){
					if(optionDialogCountry.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblCountry.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					}
					else
					{
						optionDialogCountry = null;
						_obj.lblCountry.text = 'Passport Issue Place*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
		    require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function nationality()
	{
		activityIndicator.showIndicator();
		var countryOptions = [];
		
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
		
				for(var i=0;i<splitArr.length;i++)
				{
					countryOptions.push(splitArr[i]);
				}
				
				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Nationality',countryOptions,-1);
				optionDialogCountry.show();
				
				optionDialogCountry.addEventListener('click',function(evt){
					if(optionDialogCountry.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblNationality.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					}
					else
					{
						optionDialogCountry = null;
						_obj.lblNationality.text = 'Passport Nationality*';
					}
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
		    require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function destroy_kyc()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove registration start ##############');
			
			_obj.winKYC.close();
			require('/utils/RemoveViews').removeViews(_obj.winKYC,_obj.globalView);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_kyc',destroy_kyc);
			require('/utils/Console').info('############## Remove registration end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_kyc', destroy_kyc);
}; // KYCModal()