function MOR(_self) {
	Ti.App.Properties.setString('pg', 'money_on_referral');
	
	pushView(Ti.App.Properties.getString('pg'));
	require('/lib/analytics').GATrackScreen(Ti.App.Properties.getString('pg'));

	var _obj = {
		style : require('/styles/MOR').MOR, // style
		globalView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		headerBorderView : null,
		tblRefer : null,
		btnRefer : null,
		btnStatement : null,
		
		result : null
	};
	
	Ti.App.fireEvent('showLabel', {
		data : L('dashboard')
	});
	Ti.App.fireEvent('menuColor', {
		data : 'dashboard'
	});
	// Menu select
	Ti.App.fireEvent('showHeader');
	Ti.App.fireEvent('showMenu');
	Ti.App.fireEvent('showSearch');
	
	if(TiGlobals.osname !== 'android')
	{
		Ti.App.fireEvent('hideBack');
	}
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	//////////////////////////// Create UI ////////////////////////////
		
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);

	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.headerBorderView = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.lblHeader.text = 'Money on Referral';
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack);
	
	_obj.imgBack.addEventListener('click',function(){
		popView();
	});
	
	_obj.tblRefer = Ti.UI.createTableView(_obj.style.tableView);
	_obj.tblRefer.height = 250;
	
	_obj.btnRefer = Ti.UI.createButton(_obj.style.btn);
	_obj.btnRefer.title = 'REFER FRIENDS';
	
	_obj.btnStatement = Ti.UI.createButton(_obj.style.btn);
	_obj.btnStatement.title = 'STATEMENT';
	
	function mor()
	{
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETREFERRALPROGRAMDETAILSFORLOGINID",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			//Ti.API.info("INSIDE S FLAG*****in MOR:--------->");
			activityIndicator.hideIndicator();
			_obj.result = e.result;
			if((e.result.responseFlag === "S" && e.result.hasOwnProperty('morDtls')) || e.result.message === "Referal program data for given ownerid not present") {       //change for 1232
			//if(e.result.responseFlag === "S")	{
				//Ti.API.info("INSIDE S FLAG1*******IN MOR:--------->");	
				for(var i=0; i<5; i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : '#ffffff',
						className : 'refer',
						rw:i
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKey = Ti.UI.createLabel({
						top:0,
						left:20,
						height:50,
				    	width:'70%',
				    	textAlign: 'left',
				    	font:TiFonts.FontStyle('lblNormal14'),
						color:TiFonts.FontStyle('greyFont')
					});
					
					var lblValue = Ti.UI.createLabel({
						top:0,
						right:20,
						height:50,
				    	width:'30%',
				    	textAlign: 'left',
				    	font:TiFonts.FontStyle('lblNormal14'),
						color:TiFonts.FontStyle('blackFont')
					});
					
					switch (i)
					{
						case 0:
							lblKey.text = 'Transacted';
							lblValue.text = e.result.transacted !== ''  ? destSplit[1]+' '+e.result.transacted : destSplit[1]+' 0.0';
						break;
						
						case 1:
							lblKey.text = 'Registered';
							lblValue.text = e.result.registered !== '' ? destSplit[1]+' '+e.result.registered : destSplit[1]+' 0.0';
						break;
						
						case 2:
							lblKey.text = 'Redeemed till date';
							lblValue.text = e.result.redeemed !== '' ? destSplit[1]+' '+e.result.redeemed : destSplit[1]+' 0.0';
						break;
						
						case 3:
							lblKey.text = 'Total earned';
							lblValue.text = e.result.totalEarned !== '' ? destSplit[1]+' '+e.result.totalEarned : destSplit[1]+' 0.0';
						break;
						
						case 4:
							row.backgroundColor = '#6f6f6f';
							lblKey.text = 'Current balance';
							lblKey.color = TiFonts.FontStyle('greyFont');
							lblValue.text = e.result.currentBalance !== '' ? destSplit[1]+' '+e.result.currentBalance : destSplit[1]+' 0.0';
							lblValue.color = TiFonts.FontStyle('whiteFont');
						break;
						
					}
					
					row.add(lblKey);
					row.add(lblValue);
					_obj.tblRefer.appendRow(row);
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	mor();
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.headerBorderView);
	_obj.mainView.add(_obj.tblRefer);
	_obj.mainView.add(_obj.btnRefer);
	_obj.mainView.add(_obj.btnStatement);
	_obj.globalView.add(_obj.mainView);
	
	_obj.btnRefer.addEventListener('click', function(e){
		require('/js/mor/ReferMORModal').ReferMORModal();
	});
	
	_obj.btnStatement.addEventListener('click', function(e){
		require('/js/mor/StatementModal').StatementModal(_obj.result);
	});
	
	function destroy_mor() {
		try {
			if (_obj.globalView === null) {
				return;
			}

			require('/utils/Console').info('############## Remove MOR111 start ##############');

			require('/utils/RemoveViews').removeViews(_obj.globalView);
			_obj = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_mor', destroy_mor);
			require('/utils/Console').info('############## Remove MOR111 end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}


	Ti.App.addEventListener('destroy_mor', destroy_mor);

	return _obj.globalView;

};// MOR()

module.exports = MOR;