exports.StaticPagesModal = function(page,section,url_slug)
{
	require('/lib/analytics').GATrackScreen(page);

	var _obj = {
		style : require('/styles/StaticPages').StaticPages, // style
		globalView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		webView : null,
		currCode : null
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	// CreateUI
	
	_obj.winModal = Ti.UI.createWindow(_obj.style.winModal);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = page;
	//Ti.API.info("Hi in static page" + _obj.lblHeader.text);
	
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack); //Event added by Sanjivani on 16Nov16 for bug.490
    _obj.imgBack.addEventListener('click',function(){
	_obj.winModal.close();
	});
	
	 
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.enableZoomControls = true;
	_obj.webView.scalesPageToFit = false;
	//_obj.webView.scalesPageToFit = true;
	//Ti.API.info("PAGES:---",page);
	if(page == 'Privacy Policy'){
	_obj.webView.url = TiGlobals.staticPagesURL + 'pages.php?url_slug=privacy-policy&corridor='+_obj.currCode[0];
	}else if(page == 'Terms & Conditions')
	{
	if(_obj.currCode[0] == 'US'){
		_obj.webView.url='https://www.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry=AUS&orgCurrency=USD&orgCountryName=United%20States';
	    }
	else if(_obj.currCode[0] == 'AUS'){
		_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry=AUS&orgCurrency=AUD&orgCountryName=United%20States';
	}
	else{
		_obj.webView.url = 'https://www.remit2india.com/sendmoneytoindia/terms_and_conditions.jsp?orgCountry=AUS&orgCurrency=AUD&orgCountryName=United%20States';
		
	}
	}
	//_obj.webView.url = TiGlobals.staticPagesURL + 'master-static.php?section='+section+'&url_slug='+url_slug+'&corridor='+_obj.currCode[0];
	//_obj.webView.url = TiGlobals.staticPagesURL + 'terms-condition.php?section=about&url_slug=terms-and-conditions&corridor='+_obj.currCode[0];
	
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	/*var scrollView = Ti.UI.createScrollView({
		  showVerticalScrollIndicator: true,
		  showHorizontalScrollIndicator: false,
		  //height: '80%',
		  //width: '80%'
		});*/  //created and commented by sanjivani
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.webView);
	//scrollView.add(_obj.mainView);
	_obj.globalView.add(_obj.mainView);
	//_obj.globalView.add(scrollView);
	_obj.winModal.add(_obj.globalView);
	_obj.winModal.open();
	
	function destroy_receiving_options() {
		try {
			require('/utils/Console').info('############## Remove win modal start ##############');
			_obj.winModal.close();
			require('/utils/RemoveViews').removeViews(_obj.winModal,_obj.globalView);
			_obj = null;

			require('/utils/Console').info('############## Remove win modal end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}
}; // StaticPagesModal()
