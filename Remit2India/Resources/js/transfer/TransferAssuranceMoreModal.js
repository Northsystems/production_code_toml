exports.TransferAssuranceMoreModal = function()
{
	require('/lib/analytics').GATrackScreen('Transfer Assurance more Details');
	
	var _obj = {
		style : require('/styles/transfer/TransferAssuranceMore').TransferAssuranceMore,
		winTransferAssuranceMore : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblSectionHead : null,
		sectionHeaderBorder : null,
		webView : null,
		currCode : null,
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winTransferAssuranceMore = Ti.UI.createWindow(_obj.style.winTransferAssuranceMore);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winTransferAssuranceMore);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Assurance Program';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
	_obj.lblSectionHead.text = 'Remit2India Transaction\nAssurance Program';
	_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder); 
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.url = TiGlobals.staticPagesURL + 'more-details.php?corridor='+_obj.currCode[0];
	
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblSectionHead);
	_obj.mainView.add(_obj.sectionHeaderBorder);
	_obj.mainView.add(_obj.webView);
	_obj.globalView.add(_obj.mainView);
	_obj.winTransferAssuranceMore.add(_obj.globalView);
	_obj.winTransferAssuranceMore.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_transfermore();
	});
	
	_obj.winTransferAssuranceMore.addEventListener('androidback', function(){
		destroy_transfermore();
	});
	
	function destroy_transfermore()
	{
		try{
			
			require('/utils/Console').info('############## Remove transfer more start ##############');
			
			_obj.winTransferAssuranceMore.close();
			require('/utils/RemoveViews').removeViews(_obj.winTransferAssuranceMore);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_transfermore',destroy_transfermore);
			require('/utils/Console').info('############## Remove transfer more end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_transfermore', destroy_transfermore);
}; // TransferAssuranceMoreModal()