exports.SavedTxnModal = function(args)
{
	require('/lib/analytics').GATrackScreen('Saved Transactions');
	
	var _obj1 ={
		
		contactDate : '',
		timeFrom : '',
		timeTo : '',
		timeFrom1 : '',
		timeTo1 : '',
		//isFirstTransaction : null,
		};
	
	var _obj = {
		style : require('/styles/transfer/OneClickPay').OneClickPay, // style
		winSavedTxn : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblReceiverName : null,
		lblReceiverBankName : null,
		receiverMainView : null,
		receiverView : null,
		lblPaymentDetails : null,
		lblPaymode : null,
		lblBankDetails : null,
		borderViewM1 : null,
		lblPurpose : null,
		imgPurpose : null,
		purposeView : null,
		borderView1 : null,
		borderViewM2 : null,
		txtAmount : null,
		borderView3 : null,
		lblExchangeRate : null,
		borderViewM3 : null,
		btnSendMoney : null,
		
		purposeOptions : [],
		receiverOptions : [],
		receiverDetails : [],
		receiverSel : null,
		
		
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winSavedTxn = Ti.UI.createWindow(_obj.style.winOneClickPay);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winSavedTxn);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Saved Transactions';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.receiverView = Ti.UI.createView(_obj.style.receiverView);
	_obj.receiverMainView = Ti.UI.createView(_obj.style.receiverMainView);
	_obj.lblReceiverName = Ti.UI.createLabel(_obj.style.lblReceiverName);
	_obj.lblReceiverName.text = args.recName;
	_obj.lblReceiverBankName = Ti.UI.createLabel(_obj.style.lblReceiverBankName);
	_obj.lblReceiverBankName.text = args.recBankName;
	
	_obj.lblPaymentDetails = Ti.UI.createLabel(_obj.style.lblPaymentDetails);
	_obj.lblPaymentDetails.text = 'Payment Details';
	_obj.lblPaymode = Ti.UI.createLabel(_obj.style.lblPaymode);
	_obj.lblPaymode.text = args.paymodeName;
	_obj.lblBankDetails = Ti.UI.createLabel(_obj.style.lblBankDetails);
	_obj.lblBankDetails.text = args.bankName;
	
	_obj.borderViewM1 = Ti.UI.createView(_obj.style.borderMainView);
	_obj.borderViewM1.top = 15;
	
	_obj.purposeView = Ti.UI.createView(_obj.style.ddView);
	_obj.lblPurpose = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblPurpose.text = args.recPurpose;
	_obj.imgPurpose = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderView1 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.borderViewM2 = Ti.UI.createView(_obj.style.borderMainView);
	_obj.borderViewM2.top = 15;
	
	_obj.doneAmount = Ti.UI.createButton(_obj.style.done);
	_obj.doneAmount.addEventListener('click',function(){
		_obj.txtAmount.blur();
	});
	
	_obj.txtAmount = Ti.UI.createTextField(_obj.style.txtAmount);
	_obj.txtAmount.hintText = 'Send Amount ('+origSplit[1]+')';
	_obj.txtAmount.value = args.recAmount;
	_obj.txtAmount.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtAmount.keyboardToolbar = [_obj.doneAmount];
	_obj.borderView3 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.borderViewM3 = Ti.UI.createView(_obj.style.borderMainView);
	_obj.borderViewM3.top = 15;
	
	_obj.lblExchangeRate = Ti.UI.createLabel(_obj.style.lblExchangeRate); 
	_obj.lblExchangeRate.text = '1 '+args.origCurrency+' = --.-- ' + args.destCurrency;
	
	_obj.btnSendMoney = Ti.UI.createButton(_obj.style.btnSendMoney);
	_obj.btnSendMoney.title = 'SEND MONEY NOW';
	
	_obj.txtAmount.addEventListener('change',function(e){
		if(_obj.txtAmount.value === '')
		{
			_obj.lblExchangeRate.text = '1 '+origSplit[1]+' = --.-- ' + destSplit[1];
		}
		else if((parseInt(_obj.txtAmount.value) < TiGlobals.minExchangeValue))
		{
			if(_obj.txtAmount.value.length > 1)
			{
				_obj.lblExchangeRate.text = 'Value cannot be less than '+TiGlobals.minExchangeValue;
			}
		}
		else if((parseInt(_obj.txtAmount.value) > TiGlobals.maxExchangeValue))
		{
			_obj.lblExchangeRate.text = 'Value cannot be greater than '+TiGlobals.maxExchangeValue;
		}
		else
		{
			getExchangeRate(args.paymode,_obj.txtAmount.value);	
		}
	});
	
	function getExchangeRate(paymode,amt)
	{
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETEXCHANGERATE",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'",'+
				'"paymodeCode":"'+paymode+'",'+
				'"amount":"'+amt+'",'+
				'"deliveryMode":"'+Ti.App.Properties.getString('deliveryMode')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(e.result.responseFlag === "S")
			{
				require('/utils/Console').info(e.result);
				_obj.lblExchangeRate.text = '1 '+origSplit[1]+' = ' + e.result.exchangeRate + ' ' + destSplit[1];
			}
			xhr = null;
		}

		function xhrError(e) {
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.purposeView.addEventListener('click',function(e){	
		var optionDialogPurpose = require('/utils/OptionDialog').showOptionDialog('Purpose of Remittance',_obj.purposeOptions,-1);
		optionDialogPurpose.show();
		
		setTimeout(function(){
			activityIndicator.hideIndicator();
		},200);
		
		optionDialogPurpose.addEventListener('click',function(evt){
			if(optionDialogPurpose.options[evt.index] !== L('btn_cancel'))
			{
				_obj.lblPurpose.text = optionDialogPurpose.options[evt.index];
				optionDialogPurpose = null;
				purposeOptions = null;
				pdataSplit = null;
			}
			else
			{
				_obj.lblPurpose.text = 'Purpose of Remittance*';
				optionDialogPurpose = null;
				purposeOptions = null;
				pdataSplit = null;
			}
		});
	});	
	
	
	
		
	
	_obj.btnSendMoney.addEventListener('click',function(e){
		
		var conf = [];
		
		
		
		
		// Receiver
		/*conf['receiverNickName'] = receiverSplit[0];
		conf['receiverParentChildflag'] = receiverSplit[1];
		conf['receiverOwnerId'] = receiverSplit[2];
		conf['receiverLoginid'] = receiverSplit[3];
		conf['receiverFName'] = receiverSplit[4];
		conf['receiverLName'] = receiverSplit[5];*/
		
		// Purpose
		if(_obj.lblPurpose.text === 'Purpose of Remittance*')
		{
			require('/utils/AlertDialog').showAlert('','Please select purpose of remittance',[L('btn_ok')]).show();
			return;		
		}
		else
		{
			conf['purpose'] = _obj.lblPurpose.text;
		}
		
		// Amount
		if(_obj.txtAmount.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please add an amount to be transferred',[L('btn_ok')]).show();
			_obj.txtAmount.value = '';
			_obj.txtAmount.focus();
    		return;		
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAmount.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Amount',[L('btn_ok')]).show();
    		_obj.txtAmount.value = '';
    		_obj.txtAmount.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtAmount.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Amount',[L('btn_ok')]).show();
    		_obj.txtAmount.value = '';
    		_obj.txtAmount.focus();
			return;
		}
		else if(_obj.txtAmount.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Amount must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtAmount.value = '';
    		_obj.txtAmount.focus();
			return;
		}
		else if(parseInt(_obj.txtAmount.value) < parseInt(_obj.minExchange))
		{
			require('/utils/AlertDialog').showAlert('','Please add an amount greater than '+_obj.minExchange,[L('btn_ok')]).show();
    		_obj.txtAmount.focus();
			return;
		}
		else if(parseInt(_obj.txtAmount.value) > parseInt(_obj.maxExchange))
		{
			require('/utils/AlertDialog').showAlert('','Please add an amount less than '+_obj.maxExchange,[L('btn_ok')]).show();
    		_obj.txtAmount.focus();
			return;
		}
		else if((_obj.fxfwVoucherSel === 1 && origSplit[0] === 'UK') && (parseInt(_obj.txtAmount.value) < parseInt(_obj.freewayMinAmt)))
		{
			require('/utils/AlertDialog').showAlert('','Freeway program is applicable on transactions above ' + _obj.freewayMinAmt,[L('btn_ok')]).show();
    		_obj.txtAmount.value = '';
    		_obj.txtAmount.focus();
			return;
		}
		else
		{
			conf['amount'] = _obj.txtAmount.value;
			conf['exchangeRate'] = _obj.lblExchangeRate.text;
		}
		
	
	
		//Ti.API.info("CONF:----",_obj1.contactDate);
		conf['contactDate'] = _obj1.contactDate;
		//Ti.API.info("CONF1:----",conf['contactDate']);
		conf['timeFrom'] = _obj1.timeFrom;
		conf['timeTo'] = _obj1.timeTo;
		conf['timeFrom1'] =_obj1.timeFrom1;
		conf['timeTo1'] = _obj1.timeTo1;
		
		// Call Preconfirmation
		
		conf['paymode'] = args.paymode;
		conf['accountId'] = args.accountId;
		conf['bankName'] = args.bankName;
		conf['bankBranch'] = args.bankBranch;
		conf['accountNo'] = args.accountNo;
		conf['receiverNickName'] = args.recName;
		conf['receiverOwnerId'] = args.recOwnerId;
		conf['receiverLoginid'] = args.recLoginId;
		conf['receiverFName'] = '';
		conf['receiverLName'] = '';
		conf['receiverParentChildflag'] = args.recParentChildFlag;
		conf['promoCode'] = '';
		conf['agentSpecialCode'] = '';
		conf['morRefferalAmnt'] = args.morRefferalAmnt;
		conf['mobileAlerts'] = 'No';
		conf['transactionInsurance'] = args.transactionInsurance;
		conf['fxVoucherRedeem'] = args.fxVoucherRedeem;
		conf['programType'] = args.programType;
		conf['voucherCode'] = '';
		conf['voucherQty'] = '';
		conf['txnRefId'] = args.rtrn;
		conf['contactDate'] = '';
		conf['timeFrom'] = '';
		conf['timeTo'] = '';
		conf['timeFrom1'] = '';
		conf['timeTo1'] = '';
		/*conf['contactDate'] = _obj1.contactDate;
		conf['timeFrom'] = _obj1.timeFrom;
		conf['timeTo'] = _obj1.timeTo;
		conf['timeFrom1'] = _obj1.timeFrom1;
		conf['timeTo1'] = _obj1.timeTo1;
		*/
		//sanju code
		//conf['purpose']=args.purpose;
		
			function changefirstTxnFlag(evt)
	{
		var fx = evt.data;
		//Ti.API.info("EVT CONTENT:----",fx);
		var fTxnSplit = evt.data.split('||');
		
		//Ti.API.info("contact date:----",fTxnSplit[0]);
		 _obj1.contactDate = fTxnSplit[0];
		//Ti.API.info("contact date:----",_obj1.contactDate);
		 _obj1.timeFrom = fTxnSplit[1];
		_obj1.timeTo = fTxnSplit[2];
		_obj1.timeFrom1 = fTxnSplit[3];
		_obj1.timeTo1 = fTxnSplit[4];
		conf['contactDate'] = _obj1.contactDate;
		conf['timeFrom'] = _obj1.timeFrom;
		conf['timeTo'] = _obj1.timeTo;
		conf['timeFrom1'] = _obj1.timeFrom1;
		conf['timeTo1'] = _obj1.timeTo1;
		
		//_obj1.isFirstTransaction = 'No';
		
		require('/utils/Console').info( 
				'Sanjivani 111'+
		'paymode = ' + conf['paymode'] + 
		'\naccountId = ' + conf['accountId'] + 
		'\nbankName = ' + conf['bankName'] +
		'\nbankBranch = ' + conf['bankBranch'] +
		'\naccountNo = ' + conf['accountNo'] + 
		'\nreceiverNickName = ' + conf['receiverNickName'] +
		'\nreceiverOwnerId = ' + conf['receiverOwnerId'] +
		'\nreceiverLoginid = ' + conf['receiverLoginid'] +
		'\nreceiverFName = ' + conf['receiverFName'] +
		'\nreceiverLName = ' + conf['receiverLName'] +
		'\nreceiverParentChildflag = ' + conf['receiverParentChildflag'] +
		'\npurpose = ' + conf['purpose'] + 
		'\namount = ' + conf['amount'] + 
		'\nexchangeRate = ' + conf['exchangeRate'] +
		'\npromoCode = ' + conf['promoCode'] + 
		'\nagentSpecialCode = ' + conf['agentSpecialCode'] + 
		'\nmorRefferalAmnt = ' + conf['morRefferalAmnt'] + 
		'\nmobileAlerts = ' + conf['mobileAlerts'] + 
		'\ntransactionInsurance = ' + conf['transactionInsurance'] + 
		'\nfxVoucherRedeem = ' + conf['fxVoucherRedeem'] + 
		'\nprogramType = ' + conf['programType'] + 
		'\nvoucherCode = ' + conf['voucherCode'] + 
		'\nvoucherQty = ' + conf['voucherQty'] +
		'\ntxnRefId = ' + conf['txnRefId'] +
		'\ncontactDate = ' + conf['contactDate'] + 
		'\ntimeFrom = ' + conf['timeFrom'] + 
		'\ntimeTo = ' + conf['timeTo'] + 
		'\ntimeFrom1 = ' + conf['timeFrom1'] + 
		'\ntimeTo1 = ' + conf['timeTo1']);
		require('/js/transfer/PreConfirmationModal1').PreConfirmationModal1(conf);
		destroy_savedtxn();
		}
	Ti.App.addEventListener('changefirstTxnFlag',changefirstTxnFlag);
		
		require('/js/transfer/PreConfirmationModal1').PreConfirmationModal1(conf);
		destroy_savedtxn();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	
	_obj.receiverMainView.add(_obj.lblReceiverName);
	_obj.receiverMainView.add(_obj.lblReceiverBankName);
	_obj.receiverView.add(_obj.receiverMainView);
	_obj.mainView.add(_obj.receiverView);
	_obj.mainView.add(_obj.lblPaymentDetails);
	_obj.mainView.add(_obj.lblPaymode);
	_obj.mainView.add(_obj.lblBankDetails);
	_obj.mainView.add(_obj.borderViewM1);
	_obj.purposeView.add(_obj.lblPurpose);
	_obj.purposeView.add(_obj.imgPurpose);
	_obj.mainView.add(_obj.purposeView);
	_obj.mainView.add(_obj.borderView1);
	_obj.mainView.add(_obj.borderViewM2);
	_obj.mainView.add(_obj.txtAmount);
	_obj.mainView.add(_obj.borderView3);
	_obj.mainView.add(_obj.lblExchangeRate);
	_obj.mainView.add(_obj.borderViewM3);
	_obj.mainView.add(_obj.btnSendMoney);
	_obj.globalView.add(_obj.mainView);
	_obj.winSavedTxn.add(_obj.globalView);
	_obj.winSavedTxn.open();
	
	function getTransactionInfo()
	{
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETTRANSACTIONINFO",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"payModeCode":"",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(e.result.responseFlag === "S")
			{
				_obj.purposeOptions = [];
			
				for(var i=0;i<e.result.purposeData.length;i++)
				{
					_obj.purposeOptions.push(e.result.purposeData[i].purposeName);
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_savedtxn();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			activityIndicator.hideIndicator();
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	getTransactionInfo();
	getExchangeRate(args.paymode,args.recAmount);
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('cancel_transaction')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_savedtxn();
				alertDialog = null;
			}
		});
	});
	
	_obj.winSavedTxn.addEventListener('androidback',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('cancel_transaction')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_savedtxn();
				alertDialog = null;
			}
		});
	});
	
	function destroy_savedtxn()
	{
		try{
			
			require('/utils/Console').info('############## Remove savedtxn start ##############');
			
			_obj.winSavedTxn.close();
			require('/utils/RemoveViews').removeViews(_obj.winSavedTxn);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_savedtxn',destroy_savedtxn);
				Ti.App.removeEventListener('changefirstTxnFlag',changefirstTxnFlag);
			require('/utils/Console').info('############## Remove savedtxn end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_savedtxn', destroy_savedtxn);
}; // SavedTxnModal()
