   //original
exports.LoginModal = function()
{
	require('/lib/analytics').GATrackScreen('Login');
	
	var _obj = {
		style : require('/styles/Login').Login,
		winLogin : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		txtUserName : null,
		borderView1 : null,
		txtPassword : null,
		borderView2 : null,
		btnLogin : null,
		lblNewUser : null,
		registerView : null,
		borderRegistrationView : null,
		lblOR : null,
		socialView : null,
		imgFB : null,
		imgGP : null,
		imgLI : null,
		btnRegister : null,
		forgotView : null,
		lblForgotPassword : null,
		lblForgotSlash : null,
		lblForgotUsername : null,
		
		attempts : 0,
	};
	
	_obj.winLogin = Ti.UI.createWindow(_obj.style.winLogin);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winLogin);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Login';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.txtUserName = Ti.UI.createTextField(_obj.style.txtUsername);
	_obj.txtUserName.hintText = 'Username';
	_obj.borderView1 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtPassword = Ti.UI.createTextField(_obj.style.txtPassword);
	_obj.txtPassword.hintText = 'Password';
	_obj.borderView2 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.forgotView = Ti.UI.createView(_obj.style.forgotView);
	
	_obj.lblForgotPassword = Ti.UI.createLabel(_obj.style.lblForgotPassword);
	_obj.lblForgotPassword.text = 'Forgot Password';
	
	_obj.lblForgotSlash = Ti.UI.createLabel(_obj.style.lblForgotSlash);
	_obj.lblForgotSlash.text = '/';
	
	_obj.lblForgotUsername = Ti.UI.createLabel(_obj.style.lblForgotPassword);
	_obj.lblForgotUsername.text = 'Forgot Username';
	
	_obj.btnLogin = Ti.UI.createButton(_obj.style.btnLogin);
	_obj.btnLogin.title = 'SIGN IN';
	
	_obj.registerView = Ti.UI.createView(_obj.style.registerView);
	
	_obj.lblNewUser = Ti.UI.createLabel(_obj.style.lblNewUser);
	_obj.lblNewUser.text = 'New User';
	
	_obj.btnRegister = Ti.UI.createButton(_obj.style.btnRegister);
	_obj.btnRegister.title = 'REGISTER NOW';
	
	_obj.borderRegistrationView = Ti.UI.createView(_obj.style.borderRegistrationView);
	
	_obj.lblOR = Ti.UI.createLabel(_obj.style.lblOR);
	_obj.lblOR.text = 'Or Register With';
	
	_obj.socialView = Ti.UI.createView(_obj.style.socialView);
	
	_obj.imgFB = Ti.UI.createImageView(_obj.style.imgFB);
	
	_obj.imgLI = Ti.UI.createImageView(_obj.style.imgLI);
	
	_obj.imgGP = Ti.UI.createImageView(_obj.style.imgGP);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.txtUserName);
	_obj.mainView.add(_obj.borderView1);
	_obj.mainView.add(_obj.txtPassword);
	_obj.mainView.add(_obj.borderView2);
	_obj.forgotView.add(_obj.lblForgotPassword);
	_obj.forgotView.add(_obj.lblForgotSlash);
	_obj.forgotView.add(_obj.lblForgotUsername);
	_obj.mainView.add(_obj.forgotView);
	_obj.mainView.add(_obj.btnLogin);
	_obj.registerView.add(_obj.lblNewUser);
	_obj.registerView.add(_obj.btnRegister);
	_obj.registerView.add(_obj.borderRegistrationView);
	_obj.registerView.add(_obj.lblOR);
	_obj.socialView.add(_obj.imgFB);
	_obj.socialView.add(_obj.imgLI);
	_obj.socialView.add(_obj.imgGP);
	_obj.registerView.add(_obj.socialView);
	_obj.mainView.add(_obj.registerView);
	_obj.globalView.add(_obj.mainView);
	_obj.winLogin.add(_obj.globalView);
	_obj.winLogin.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_login();
	});
	
	_obj.btnLogin.addEventListener('click',function(e){
		
		if(_obj.txtUserName.value === '')
  		{
  			require('/utils/AlertDialog').showAlert('',L('requiredLoginId'),[L('btn_ok')]).show();
			return;
  		}
  		else if(_obj.txtPassword.value === '')
  		{
  			require('/utils/AlertDialog').showAlert('',L('requiredPassword'),[L('btn_ok')]).show();
			return;
  		}
  		else
		{
			_obj.attempts = _obj.attempts + 1;
			Ti.App.Properties.setString('attempts',_obj.attempts);
			
			activityIndicator.showIndicator();
			
				
			var xhr = require('/utils/XHR');
			
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"LOGIN",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+_obj.txtUserName.value+'",'+ 
					'"password":"'+Ti.Utils.md5HexDigest(_obj.txtPassword.value)+'",'+
					'"noofAttempts":"'+Ti.App.Properties.getString('attempts')+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});

			function xhrSuccess(e) {
				
				if(e.result.responseFlag === "S")
				{
					// if success - login
					Ti.App.Properties.setString('ownerId',e.result.ownerId);
					Ti.App.Properties.setString('loginId',e.result.loginId);
					Ti.App.Properties.setString('customerType',e.result.customerType);
					Ti.App.Properties.setString('firstName',e.result.firstName);
					Ti.App.Properties.setString('lastName',e.result.lastName);
					Ti.App.Properties.setString('forceChangeFlag',e.result.forceChangeFlag);
					Ti.App.Properties.setString('isMPINCreated',e.result.isMPINCreated);
					Ti.App.Properties.setString('lastLoginTime',e.result.lastLoginTime);
					Ti.App.Properties.setString('sessionId',e.result.sessionId);
					//Ti.API.info("******",e.result.sessionId);
					Ti.App.Properties.setString('loginStatus','1');
					
					Ti.App.fireEvent('loginData');
					
					var xmlHttpACS = require('/utils/XHR_BCM');
					xmlHttpACS.call({
						url : TiGlobals.pushURL,
						get : '',
						post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"PUSHLOGIN",'+
						'"source":"mobile",'+
						'"platform":"'+TiGlobals.osname+'",'+
						'"deviceid":"'+Ti.App.Properties.getString('deviceToken')+'",'+
						'"uId":"'+Ti.App.Properties.getString('ownerId')+'",'+
						'"channelId":"'+TiGlobals.pushChannelId+'"'+
						'}',
						success : xmlHttpACSSuccess,
						error : xmlHttpACSError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
					
					function xmlHttpACSSuccess(eACS) {
						if (eACS.result.status === 'S') {
							
							activityIndicator.hideIndicator();
							
							if(e.result.isMPINCreated === "N")
							{
								if(Ti.App.Properties.getString('mPIN') === '')
								{
									var alertDialog = require('/utils/AlertDialog').showAlert('', L('create_mpin'), [L('btn_ok')]);
									alertDialog.show();
							
									alertDialog.addEventListener('click', function(e) {
										alertDialog.hide();
										if (e.index === 0) {
											require('/js/CreateMPINModal').CreateMPINModal();
											alertDialog = null;
										}
										else
										{
											Ti.App.Properties.setString('mPIN','0');
										}
										
										require('/utils/RemoveViews').removeAllScrollableViews();
							
										Ti.App.Properties.removeProperty('attempts');
										destroy_login();
									});
								}
							}
							else
							{
								// Refresh page behind
								
								require('/utils/RemoveViews').removeAllScrollableViews();
							
								Ti.App.Properties.setString('mPIN','1');
								Ti.App.Properties.removeProperty('attempts');
								destroy_login();
							}		
						
						}
						xmlHttpACS = null;
					}
					
					function xmlHttpACSError(eACS) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xmlHttpACS = null;
					}
				}
				else
				{
					activityIndicator.hideIndicator();
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
				xhr = null;
			}

			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		}
	});
	
	_obj.socialView.addEventListener('click',function(e){
		if(e.source.type === 'fb')
		{
			require('/lib/facebook').facebookAuthentication(_obj.globalView,'function','destroy_login');
		}
		else if(e.source.type === 'li')
		{
			if(linkedIn.isAuthorized())
			{
				linkedIn.getProfileLinkedin({
					success: function(e) {
						var result = JSON.parse(e);
			            var params = {
				    		social:'LinkedIn',
				    		fname:result.firstName,
				    		lname:result.lastName,
				    		email:result.emailAddress
				    	};
				    	
				    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
			        },
			        error: function(error) {
			        	
			        }
			    });
			}
			else
			{
				linkedIn.authorize(function(e){
					linkedIn.getProfileLinkedin({
						success: function(e) {
				            var result = JSON.parse(e);
				            var params = {
					    		social:'LinkedIn',
					    		fname:result.firstName,
					    		lname:result.lastName,
					    		email:result.emailAddress
					    	};
					    	
					    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
				        },
				        error: function(error) {
				        	
				        }
				    });
				});
			}
		}
		else{
			googleAuth.isAuthorized(function(e) {
				require('/utils/Console').info('Access Token: ' + googleAuth.getAccessToken());
				
				var xhr = Ti.Network.createHTTPClient({
					// function called when the response data is available
					onload : function(e) {
						var json = JSON.parse(this.responseText);
		                require('/utils/Console').info(json.given_name + ' --- ' + json.family_name + ' --- ' + json.email);
						
						var params = {
				    		social:'Google+',
				    		fname:json.given_name,
				    		lname:json.family_name,
				    		email:json.email
				    	};
				    	
				    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
						
					},
					onerror : function(e) {
						Ti.API.error('HTTP: '+JSON.stringify(e));
					}
				});
				
				xhr.open("GET", 'https://www.googleapis.com/oauth2/v1/userinfo?access_token='+googleAuth.getAccessToken()+'&alt=json&v=2');
				xhr.send();	
				
			}, function() {
				//authorize first
				googleAuth.authorize(function(){
					require('/utils/Console').info('Access Token: ' + googleAuth.getAccessToken());
					
					var xhr = Ti.Network.createHTTPClient({
						// function called when the response data is available
						onload : function(e) {
							var json = JSON.parse(this.responseText);
			                require('/utils/Console').info(json.given_name + ' --- ' + json.family_name + ' --- ' + json.email);
							
							var params = {
					    		social:'Google+',
					    		fname:json.given_name,
					    		lname:json.family_name,
					    		email:json.email
					    	};
					    	
					    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
							
						},
						onerror : function(e) {
							Ti.API.error('HTTP: '+JSON.stringify(e));
						}
					});
					
					xhr.open("GET", 'https://www.googleapis.com/oauth2/v1/userinfo?access_token='+googleAuth.getAccessToken()+'&alt=json&v=2');
					xhr.send();	
					
				});
			});
		}
	});
	
	_obj.btnRegister.addEventListener('click',function(e){
		require('/js/RegisterModal').RegisterModal();
	});
	
	_obj.lblForgotPassword.addEventListener('click',function(e){
		require('/js/ForgotPasswordModal').ForgotPasswordModal();
	});
	
	_obj.lblForgotUsername.addEventListener('click',function(e){
		require('/js/ForgotUsernameModal').ForgotUsernameModal();
	});
	
	_obj.winLogin.addEventListener('androidback', function(){
		destroy_login();
	});
	
	function destroy_login()
	{
		try{
			
			require('/utils/Console').info('############## Remove login start ##############');
			
			_obj.winLogin.close();
			require('/utils/RemoveViews').removeViews(_obj.winLogin);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_login',destroy_login);
			require('/utils/Console').info('############## Remove login end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_login', destroy_login);
}; // Login()
