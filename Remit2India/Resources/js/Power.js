function Power(_self) {
	Ti.App.Properties.setString('pg', 'power');
	
	pushView(Ti.App.Properties.getString('pg'));
	require('/lib/analytics').GATrackScreen('Remit2India Power 30');

	var _obj = {
		style : require('/styles/StaticPages').StaticPages, // style
		globalView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		webView : null,
		currCode : null
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	// CreateUI
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Remit2India Power 30';
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack);
	
	_obj.imgBack.addEventListener('click',function(){
		popView();
	});
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.url = TiGlobals.staticPagesURL + 'pages.php?url_slug=power-30&corridor='+_obj.currCode[0];
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.webView);
	_obj.globalView.add(_obj.mainView);
	
	function destroy_power() {
		try {
			require('/utils/Console').info('############## Remove powerr2i start ##############');

			require('/utils/RemoveViews').removeViews(_obj.globalView);
			_obj = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_power', destroy_power);
			require('/utils/Console').info('############## Remove powerr2i end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}


	Ti.App.addEventListener('destroy_power', destroy_power);

	return _obj.globalView;

};// Power()

module.exports = Power;