exports.AddRecipientModal = function()
{
	require('/lib/analytics').GATrackScreen('Add Recipient');
	
	var _obj = {
		style : require('/styles/recipients/AddRecipient').AddRecipient,
		winAddRecipient : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		tabView : null,
		tabSelView : null,
		lblStep1 : null,
		lblStep2 : null,
		lblStep3 : null,
		lblStep4 : null,
		tabSelIconView : null,
		imgTabSel : null,
		lblSectionHead : null,
		scrollableView : null,
		
		// Step 1
		step1 : null,
		txtFirstName : null,
		borderViewS11 : null,
		txtLastName : null,
		borderViewS12 : null,
		txtNickName : null,
		borderViewS13 : null,
		dobView : null,
		dobView : null,
		dobView : null,
		borderViewS14 : null,
		btnStep1 : null,
		
		// Step 2
		step2 : null,
		txtAddress : null,
		borderViewS21 : null,
		txtCity : null,
		borderViewS22 : null,
		lblState : null,
		imgState : null,
		stateView : null,
		borderViewS23 : null,
	    txtZip : null,
		borderViewS24 : null,
		lblCountry : null,
		txtEmail : null,
		borderViewS25 : null,
		txtDayNo : null,
		borderViewS26 : null,
		lblMobileNo : null,
		imgMobileNo : null,
		txtMobile : null,
		mobileView : null,
		borderViewS27 : null,
		btnStep2 : null,
		
		// Step 3
		step3 : null,
		lblDelivery : null,
		imgDelivery : null,
		deliveryView : null,
		borderViewS31 : null,
		btnStep3 : null,
		
		// Step 4
		step4 : null,
		lblACBank : null,
		imgACBank : null,
		bankACView : null,
		borderViewSAC41 : null,
		txtACIFSCCode : null,
		borderViewSAC42 : null,
		txtACBankBranch : null,
		borderViewSAC43 : null,
		txtACCity : null,
		borderViewSAC44 : null,
		txtACAccNo : null,
		borderViewSAC45 : null,
		txtACConfirmAccNo : null,
		borderViewSAC46 : null,
		lblACAccType : null,
		imgACAccType : null,
		accACTypeView : null,
		borderViewSAC47 : null,
		btnStep4 : null,
		ifscACView : null,
		lblDD : null,
		imgDD : null,
		ddView : null,
		borderViewSDD41 : null,
		txtOtherLocation : null,
		borderViewSDD42 : null,
		txtDDAccountNo : null,
		borderViewSDD43 : null,
		lblDDAccType : null,
		imgDDAccType : null,
		accDDTypeView : null,
		borderViewSDD44 : null,
		txtDDBankName : null,
		borderViewSDD45 : null,
		txtDDBankBranch : null,
		borderViewSDD46 : null,
		txtDDBankCity : null,
		borderViewSDD47 : null,
		lblAC : null,
		imgYes : null,
		imgNo : null,
		ifscView : null,
		mainIFSCView : null,
		imgInfo : null,
		lblInfo : null,
		infoTextView : null,
		
		dob: null,
		deliverOptionSel : null,
		accType : null,
		bankId : null,
		isPartner : 'N',
	};
	
	_obj.dob = require('/utils/Date').today('/','dmy').split('/');
	
	var countryName = Ti.App.Properties.getString('destinationCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	//alert(countryName[0]);
	
	_obj.winAddRecipient = Titanium.UI.createWindow(_obj.style.winAddRecipient);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winAddRecipient);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Add Recipients';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.lblStep1 = Ti.UI.createLabel(_obj.style.lblStep);
	_obj.lblStep1.text = 'Step 1';
	_obj.lblStep2 = Ti.UI.createLabel(_obj.style.lblStep);
	_obj.lblStep2.text = 'Step 2';
	_obj.lblStep2.left = 1;
	_obj.lblStep3 = Ti.UI.createLabel(_obj.style.lblStep);
	_obj.lblStep3.text = 'Step 3';
	_obj.lblStep3.left = 1;
	_obj.lblStep4 = Ti.UI.createLabel(_obj.style.lblStep);
	_obj.lblStep4.text = 'Step 4';
	_obj.lblStep4.left = 1;
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
	_obj.lblStep1.color = TiFonts.FontStyle('whiteFont');
	_obj.lblStep1.backgroundColor = '#6F6F6F';
	_obj.lblStep2.color = TiFonts.FontStyle('blackFont');
	_obj.lblStep2.backgroundColor = '#E9E9E9';
	_obj.lblStep3.color = TiFonts.FontStyle('blackFont');
	_obj.lblStep3.backgroundColor = '#E9E9E9';
	_obj.lblStep4.color = TiFonts.FontStyle('blackFont');
	_obj.lblStep4.backgroundColor = '#E9E9E9';
	
	_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
	_obj.lblSectionHead.text = 'Personal Details';
	_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder); 
	
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.tabView.add(_obj.lblStep1);
	_obj.tabView.add(_obj.lblStep2);
	_obj.tabView.add(_obj.lblStep3);
	_obj.tabView.add(_obj.lblStep4);
	_obj.mainView.add(_obj.tabView);
	_obj.mainView.add(_obj.tabSelView);
	_obj.mainView.add(_obj.tabSelIconView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.mainView.add(_obj.lblSectionHead);
	_obj.mainView.add(_obj.sectionHeaderBorder);

	/////////////////// Step 1 ///////////////////
	
	_obj.step1 = Ti.UI.createScrollView(_obj.style.step1);
	_obj.txtFirstName = Ti.UI.createTextField(_obj.style.txtFirstName);
	_obj.txtFirstName.hintText = 'First Name*';
	_obj.txtFirstName.maxLength = 25;
	_obj.borderViewS11 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtLastName = Ti.UI.createTextField(_obj.style.txtLastName);
	_obj.txtLastName.hintText = 'Last Name*';
	_obj.txtLastName.maxLength = 25;
	_obj.borderViewS12 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtNickName = Ti.UI.createTextField(_obj.style.txtNickName);
	_obj.txtNickName.hintText = 'Nick Name*';
	_obj.txtNickName.maxLength = 25;
	_obj.borderViewS13 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.dobView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblDOB = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblDOB.text = 'Date of Birth*';
	_obj.imgDOB = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS14 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.btnStep1 = Ti.UI.createButton(_obj.style.btnStep1);
	_obj.btnStep1.title = 'CONTINUE';
	
	_obj.step1.add(_obj.txtFirstName);
	_obj.step1.add(_obj.borderViewS11);
	_obj.step1.add(_obj.txtLastName);
	_obj.step1.add(_obj.borderViewS12);
	_obj.step1.add(_obj.txtNickName);
	_obj.step1.add(_obj.borderViewS13);
	_obj.dobView.add(_obj.lblDOB);
	_obj.dobView.add(_obj.imgDOB);
	_obj.step1.add(_obj.dobView);
	_obj.step1.add(_obj.borderViewS14);
	_obj.step1.add(_obj.btnStep1);
	
   function validatedate()
   {
   	  var dateofbirth = _obj.lblDOB.text;
   	  var currentdate = require('/utils/Date').today('/', 'dmy');
	  var opera1 = dateofbirth.split('/');
		
	  lopera1 = opera1.length;
		
		//Extract the string into month, date and year
	  if(lopera1 > 1)
	  {
		 var pdate = dateofbirth.split('/');
		 var dd = parseInt(pdate[0]);
		 var mm = parseInt(pdate[1]);
		 var yy = parseInt(pdate[2]);
	  }
		  
	  var inputText = dd+"-"+mm+"-"+yy;
	  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
   		  
  	  //Match the date format through regular expression
  	  if(inputText.match(dateformat))
	  {
		  // Create list of days of a month [assume there is no leap year by default]
		  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
		  if (mm == 1 || mm > 2)
		  {
			  if (dd > ListofDays[mm-1])
			  {
			  	require('/utils/AlertDialog').showAlert('','Please enter a valid recipient Date of Birth',[L('btn_ok')]).show();
	    		_obj.lblDOB.text = 'Date of Birth*';
				return false;
			  }
		  }
		  
		  if (mm == 2)
		  {
		  		var lyear = false;
		  		if((!(yy % 4) && yy % 100) || !(yy % 400)) 
		  		{
		  			lyear = true;
		  		}
		  		if((lyear == false) && (dd >= 29))
		  		{
		  			require('/utils/AlertDialog').showAlert('','Please enter a valid recipient Date of Birth',[L('btn_ok')]).show();
		    		_obj.lblDOB.text = 'Date of Birth*';
					return false;
		  		}
			  	if((lyear == true) && (dd > 29))
			  	{
			  		require('/utils/AlertDialog').showAlert('','Please enter a valid recipient Date of Birth',[L('btn_ok')]).show();
		    		_obj.lblDOB.text = 'Date of Birth*';
					return false;
			  	}
		  }
	  }
	  else
	  {
	  		require('/utils/AlertDialog').showAlert('','Please enter a valid recipient Date of Birth',[L('btn_ok')]).show();
    		_obj.lblDOB.text = 'Date of Birth*';
			return false;
	  }
	  
	  var servdate_val = currentdate;
	  var date  = servdate_val.substring(0,2);
	  var month  = servdate_val.substring(3,5); 
	  var year  = servdate_val.substring(6,10);
	  
	  var frmdate  = dd;			 
	  var frmmonth = mm;			 
	  var frmyear  = yy;
	  
	  if(parseFloat(frmyear) < 1920)
	  {
			require('/utils/AlertDialog').showAlert('','Please enter a valid recipient Date of Birth',[L('btn_ok')]).show();
    		_obj.lblDOB.text = 'Date of Birth*';
			return false;
	  }
	  else if(parseFloat(frmyear) > parseFloat(year))
	  {
			require('/utils/AlertDialog').showAlert('','Please enter a valid recipient Date of Birth',[L('btn_ok')]).show();
    		_obj.lblDOB.text = 'Date of Birth*';
			return false;
			
	  }// end -if frmyear > server year
	  else if(parseFloat(frmyear) == parseFloat(year))
	  { 			 
			if(parseFloat(frmmonth) > parseFloat(month))
			{
				require('/utils/AlertDialog').showAlert('','Date of Birth cannot be a future date',[L('btn_ok')]).show();
	    		_obj.lblDOB.text = 'Date of Birth*';
				return false;
			
			}//end-if frm month>server month
			else if(parseFloat(frmmonth) == parseFloat(month))
			{
				if(parseFloat(frmdate) > parseFloat(date))
				{
					require('/utils/AlertDialog').showAlert('','Date of Birth cannot be a future date',[L('btn_ok')]).show();
		    		_obj.lblDOB.text = 'Date of Birth*';
					return false;
				}//end if frm day== server day
			}//end if month == server
		}//end if year == server year
		
		return true;
	}
	
	function calculateAge(evt) { // birthday is a date
	    var ageDifMs = Date.now() - evt.data.getTime();
	    var ageDate = new Date(ageDifMs); // miliseconds from epoch
	    
	    if((Math.abs(ageDate.getUTCFullYear() - 1970))<18)
	    {
	    	require('/utils/AlertDialog').showAlert('','Your age should be 18 years or above',[L('btn_ok')]).show();
    		_obj.lblDOB.text = 'Date of Birth*';
			return false;
	    }
	}
	
	Ti.App.addEventListener('calculateAge',calculateAge);
	
	_obj.dobView.addEventListener('click',function(e){
		require('/utils/DatePicker').DatePicker(_obj.lblDOB,'dob18');
	});
	
	_obj.btnStep1.addEventListener('click',function(e){
		var fName = require('/lib/toml_validations').trim(_obj.txtFirstName.value);
		var lName = require('/lib/toml_validations').trim(_obj.txtLastName.value);
		
		////////////////////// First Name ////////////////////
    	if(_obj.txtFirstName.value === '' || _obj.txtFirstName.value === 'First Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter recipient First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter recipient First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
	    	return;
		}
		else if(fName.length <= 1 && fName.length > 0)
		{	
			require('/utils/AlertDialog').showAlert('','First Name cannot be a single alphabet. Please enter a complete recipient First Name',[L('btn_ok')]).show();
    		_obj.txtFirstName.value = '';
    		_obj.txtFirstName.focus();
    		return;
		}
		else
		{
			
		}
		////////////////////// Last Name ////////////////////
		if(_obj.txtLastName.value === '' || _obj.txtLastName.value === 'Last Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter recipient Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
    		return;
		}
		else if(_obj.txtLastName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Last Name field',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter recipient Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
			return;
		}
		else if(lName.length <= 1 && lName.length > 0) // Last NAME SHOULD BE GREATER THEN 1
		{	
			require('/utils/AlertDialog').showAlert('','Last Name cannot be a single alphabet. Please enter a complete recipient Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
			return;
		}
		else
		{
			
		}
		
		////////////////////// Nick Name ////////////////////
		if(_obj.txtNickName.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a Nick Name for your Receiver',[L('btn_ok')]).show();
    		_obj.txtNickName.value = '';
    		_obj.txtNickName.focus();
    		return;
		}
		else if(_obj.txtNickName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Nick Name field',[L('btn_ok')]).show();
    		_obj.txtNickName.value = '';
    		_obj.txtNickName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtNickName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Nick Name',[L('btn_ok')]).show();
    		_obj.txtNickName.value = '';
    		_obj.txtNickName.focus();
			return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtNickName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter a Nick Name for your Receiver',[L('btn_ok')]).show();
    		_obj.txtNickName.value = '';
    		_obj.txtNickName.focus();
			return;
		}
		else
		{
			
		}
		
		////////////////////// DOB ////////////////////
		if(_obj.lblDOB.text === '' || _obj.lblDOB.text === 'Date of Birth*')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid recipient Date of Birth',[L('btn_ok')]).show();
    		_obj.lblDOB.text = 'Date of Birth*';
			return;
		}
		else
		{
			
		}
		
		_obj.scrollableView.moveNext();
		_obj.lblSectionHead.text = 'Contact Information';
		changeTabs('step2');
	});
	
	/////////////////// Step 2 ///////////////////
	
	_obj.step2 = Ti.UI.createScrollView(_obj.style.step2);
	
	_obj.txtAddress = Ti.UI.createTextArea(_obj.style.txtAddress);
	_obj.txtAddress.left = 15;
	_obj.txtAddress.value = "Address*";
	_obj.txtAddress.color = "#dddde0";
	_obj.borderViewS21 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtAddress.addEventListener('focus',function(e){
		try{
			if(e.source.value === 'Address*'){
		        e.source.value = '';
		        _obj.txtAddress.color = "#000";
		    }
		}catch(e){}
	});
	
	_obj.txtAddress.addEventListener('blur',function(e){
		try{
			if(e.source.value === ''){
		        e.source.value = 'Address*';
		        _obj.txtAddress.color = "#dddde0";
		    }
	    }catch(e){}
	});
	
	_obj.txtCity = Ti.UI.createTextField(_obj.style.txtCity);
	_obj.txtCity.hintText = 'City*';
	_obj.txtCity.maxLength = 25;
	_obj.borderViewS22 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.stateView = Ti.UI.createView(_obj.style.stateView);
	_obj.lblState = Ti.UI.createLabel(_obj.style.lblState);
	_obj.lblState.text = 'State*';
	_obj.imgState = Ti.UI.createImageView(_obj.style.imgState);
	_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);

	_obj.stateView.addEventListener('click',function(){
		activityIndicator.showIndicator();
		var stateOptions = [];
		
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"statelisting",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryCode[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.status === "S")
			{
				for(var i=0;i<e.result.response[0].statelisting.length;i++)
				{
					stateOptions.push(e.result.response[0].statelisting[i].state_name);
				}
				
				var optionDialogState = require('/utils/OptionDialog').showOptionDialog('States',stateOptions,-1);
				optionDialogState.show();
				
				optionDialogState.addEventListener('click',function(evt){
					if(optionDialogState.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblState.text = optionDialogState.options[evt.index];
						optionDialogState = null;
					}
					else
					{
						optionDialogState = null;
						_obj.lblState.text = 'State*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addrecipient();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.doneZip = Ti.UI.createButton(_obj.style.done);
	_obj.doneZip.addEventListener('click',function(){
		_obj.txtZip.blur();
	});
		
	_obj.txtZip = Ti.UI.createTextField(_obj.style.txtZip);
	_obj.txtZip.hintText = 'Pincode*';
	_obj.txtZip.maxLength = 6;
	_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtZip.keyboardToolbar = [_obj.doneZip];
	_obj.borderViewS24 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.lblCountry = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblCountry.text = 'Country - ' + countryName[0];
	
	_obj.txtEmail = Ti.UI.createTextField(_obj.style.txtEmail);
	_obj.txtEmail.hintText = 'Email Address*';
	_obj.txtEmail.keyboardType = Ti.UI.KEYBOARD_TYPE_EMAIL;
	_obj.borderViewS25 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneDay = Ti.UI.createButton(_obj.style.done);
	_obj.doneDay.addEventListener('click',function(){
		_obj.txtDayNo.blur();
	});
	
	_obj.txtDayNo = Ti.UI.createTextField(_obj.style.txtDayNo);
	_obj.txtDayNo.hintText = 'Phone Number*';
	_obj.txtDayNo.maxLength = 10;
	_obj.txtDayNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDayNo.keyboardToolbar = [_obj.doneDay];
	_obj.borderViewS26 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneMobileNo = Ti.UI.createButton(_obj.style.done);
	_obj.doneMobileNo.addEventListener('click',function(){
		_obj.txtMobile.blur();
	});
	
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblMobileNo = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblMobileNo.text = '+91';
	_obj.imgMobileNo = Ti.UI.createImageView(_obj.style.imgMobileNo);
	_obj.txtMobile = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtMobile.hintText = 'Mobile Number';
	_obj.txtMobile.maxLength = 10;
	_obj.txtMobile.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtMobile.keyboardToolbar = [_obj.doneMobileNo];
	_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.btnStep2 = Ti.UI.createButton(_obj.style.btnStep2);
	_obj.btnStep2.title = 'CONTINUE';
	
	_obj.step2.add(_obj.txtAddress);
	_obj.step2.add(_obj.borderViewS21);
	
	_obj.step2.add(_obj.txtCity);
	_obj.step2.add(_obj.borderViewS22);
	
	_obj.stateView.add(_obj.lblState);
	_obj.stateView.add(_obj.imgState);
	_obj.step2.add(_obj.stateView);
	_obj.step2.add(_obj.borderViewS23);
    
    _obj.step2.add(_obj.txtZip);
	_obj.step2.add(_obj.borderViewS24);
	
	_obj.step2.add(_obj.lblCountry);
	
	_obj.step2.add(_obj.txtEmail);
	_obj.step2.add(_obj.borderViewS25);
	
	_obj.step2.add(_obj.txtDayNo);
	_obj.step2.add(_obj.borderViewS26);
	
	_obj.mobileView.add(_obj.lblMobileNo);
	_obj.mobileView.add(_obj.imgMobileNo);
	_obj.mobileView.add(_obj.txtMobile);
	_obj.step2.add(_obj.mobileView);
	_obj.step2.add(_obj.borderViewS27);
	
	_obj.step2.add(_obj.btnStep2);
	
	_obj.lblMobileNo.addEventListener('click',function(e){
		isdListing();
	});
	
	_obj.imgMobileNo.addEventListener('click',function(e){
		isdListing();
	});
	
	_obj.btnStep2.addEventListener('click',function(e){
		
		// Address
		if(_obj.txtAddress.value == '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Recipient\'s Address',[L('btn_ok')]).show();
    		_obj.txtAddress.value = '';
    		_obj.txtAddress.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAddress.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Address field',[L('btn_ok')]).show();
    		_obj.txtAddress.value = '';
    		_obj.txtAddress.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtAddress.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Address field',[L('btn_ok')]).show();
    		_obj.txtAddress.value = '';
    		_obj.txtAddress.focus();
			return;
		}
		else if(_obj.txtAddress.value.search("[^a-zA-Z0-9 ,]") === false)
		{
			require('/utils/AlertDialog').showAlert('','Address cannot contain special characters other than comma',[L('btn_ok')]).show();
    		_obj.txtAddress.value = '';
    		_obj.txtAddress.focus();
			return;
		}
		else
		{
			
		}
		
		// City
			
		if(_obj.txtCity.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Receiver\'s City',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtCity.value) == false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of city',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtCity.value) == false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of City field',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
			return;
		}
		else if(_obj.txtCity.value.search("[^a-zA-Z0-9 ,]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','City cannot contain special characters other than comma',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
			return;
		}
		else
		{
		}	
		
		//State
		
		if(_obj.lblState.text === 'State*')
		{
			require('/utils/AlertDialog').showAlert('','Please select your Receiver\'s state',[L('btn_ok')]).show();
    		_obj.lblState.text = 'State*';
			return;
		}
		else
		{
		}
		// Zipcode
		
		if(_obj.txtZip.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Receiver\'s Pincode',[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtZip.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Pincode field',[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
			return;
		}
		else if(_obj.txtZip.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Pincode must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
			return;
		}
		else if(_obj.txtZip.value.length < 6)
		{
			require('/utils/AlertDialog').showAlert('','Pincode should be a 6 digit numeric value',[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
			return;
		}
		else
		{
			
		}
		
		////////////////////// Email ////////////////////
		if(_obj.txtEmail.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter Recipient\'s Email Address',[L('btn_ok')]).show();
    		_obj.txtEmail.value = '';
    		_obj.txtEmail.focus();
			return;
		}
		else if((require('/lib/toml_validations').isEmail(_obj.txtEmail.value)) === false)
		{
			//Ti.API.info('EMAIL FAIL');
			require('/utils/AlertDialog').showAlert('','Please enter a valid Email Address',[L('btn_ok')]).show();
    		_obj.txtEmail.value = '';
    		_obj.txtEmail.focus();
			return;
		}
		else
		{
			
		}
		
		//Phone
		if(_obj.txtDayNo.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter valid Receiver\'s Phone Number',[L('btn_ok')]).show();
    		_obj.txtDayNo.value = '';
    		_obj.txtDayNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDayNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Receiver\'s Phone Number',[L('btn_ok')]).show();
    		_obj.txtDayNo.value = '';
    		_obj.txtDayNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDayNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Receiver\'s Phone Number',[L('btn_ok')]).show();
    		_obj.txtDayNo.value = '';
    		_obj.txtDayNo.focus();
			return;
		}
		else if(_obj.txtDayNo.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Receiver\'s Phone Number must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtDayNo.value = '';
    		_obj.txtDayNo.focus();
			return;
		}
		else if(_obj.txtDayNo.value.length < 8)
		{
			require('/utils/AlertDialog').showAlert('','Receiver\'s Phone Number cannot be less than 8 digits',[L('btn_ok')]).show();
    		_obj.txtDayNo.value = '';
    		_obj.txtDayNo.focus();
			return;
		}
		else
		{
			
		}
		
		//Mobile Number
		if(_obj.txtMobile.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Receiver\'s Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Receiver\'s Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Receiver\'s Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(_obj.txtMobile.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Receiver\'s Mobile Number must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(_obj.txtMobile.value.length < 10)
		{
			require('/utils/AlertDialog').showAlert('','Receiver\'s Mobile Number cannot be less than 10 digits',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else
		{
			
		}
		
		////////// Go to Step 3 //////////
		_obj.scrollableView.moveNext();
		_obj.lblSectionHead.text = 'Delivery Options';
		changeTabs('step3');
	});
	
	function isdListing()
	{
		activityIndicator.showIndicator();
		var isdOptions = [];
		
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"isdListing",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			if(e.result.status === "S")
			{
				for(var i=0;i<e.result.response[0].isdListing.length;i++)
				{
					isdOptions.push('+'+e.result.response[0].isdListing[i]);
				}
				
				var optionDialogISD = require('/utils/OptionDialog').showOptionDialog('ISD',isdOptions,-1);
				optionDialogISD.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogISD.addEventListener('click',function(evt){
					if(optionDialogISD.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblMobileNo.text = optionDialogISD.options[evt.index];
						optionDialogISD = null;
					}
					else
					{
						optionDialogISD = null;
						_obj.lblMobileNo.text = '+91';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addrecipient();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	/////////////////// Step 3 ///////////////////
	
	_obj.step3 = Ti.UI.createScrollView(_obj.style.step3);
	
	_obj.deliveryView = Ti.UI.createView(_obj.style.stateView);
	_obj.lblDelivery = Ti.UI.createLabel(_obj.style.lblState);
	_obj.lblDelivery.text = 'Delivery Options*';
	_obj.imgDelivery = Ti.UI.createImageView(_obj.style.imgState);
	_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderView);
	_obj.btnStep3 = Ti.UI.createButton(_obj.style.btnStep3);
	_obj.btnStep3.title = 'CONTINUE';
	
	_obj.deliveryView.add(_obj.lblDelivery);
	_obj.deliveryView.add(_obj.imgDelivery);
	_obj.step3.add(_obj.deliveryView);
	_obj.step3.add(_obj.borderViewS31);
	_obj.step3.add(_obj.btnStep3);
	
	_obj.deliveryView.addEventListener('click',function(e){
		
		/*if(countryName[0] === 'Netherlands')
		{*/
			var optionDialogDelivery = require('/utils/OptionDialog').showOptionDialog('Delivery Options',['Account Credit'],-1);	
		/*}
		else
		{
			var optionDialogDelivery = require('/utils/OptionDialog').showOptionDialog('Delivery Options',['Account Credit','Demand Draft'],-1);	
		}*/
		
		optionDialogDelivery.show();
		
		setTimeout(function(){
			activityIndicator.hideIndicator();
		},200);
		
		optionDialogDelivery.addEventListener('click',function(evt){
			if(optionDialogDelivery.options[evt.index] !== L('btn_cancel'))
			{
				_obj.lblDelivery.text = optionDialogDelivery.options[evt.index];
				
				if(optionDialogDelivery.options[evt.index] === 'Account Credit')
				{
					_obj.deliverOptionSel = 'DC';
				}
				else
				{
					_obj.deliverOptionSel = 'DD';
				}
				
				optionDialogDelivery = null;
			}
			else
			{
				optionDialogDelivery = null;
				_obj.lblDelivery.text = 'Delivery Options*';
			}
		});
	});
	
	_obj.btnStep3.addEventListener('click',function(e){
		// Delivery Options
		if(_obj.lblDelivery.text === 'Delivery Options*')
		{
			require('/utils/AlertDialog').showAlert('','Please select a Delivery Option',[L('btn_ok')]).show();
    		_obj.lblDelivery.text = 'Delivery Options*';
			return;
		}
		else
		{
		}
		
		////////// Go to Step 4 //////////
		_obj.scrollableView.moveNext();
		_obj.lblSectionHead.text = _obj.lblDelivery.text;
		
		if(_obj.lblDelivery.text === 'Account Credit')
		{
			ac();
		}
		else
		{
			dd();
		}
		changeTabs('step4');
	});
	
	/////////////////// Step 4 ///////////////////
	
	_obj.step4 = Ti.UI.createScrollView(_obj.style.step4);
	
	// Demand Draft View
	
	_obj.ddView = Ti.UI.createView(_obj.style.ddView);
	_obj.lblDD = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblDD.text = 'DD Payable at*';
	_obj.imgDD = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderViewSDD41 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtOtherLocation = Ti.UI.createTextField(_obj.style.txtOtherLocation);
	_obj.txtOtherLocation.hintText = 'Other Locations';
	_obj.txtOtherLocation.maxLength = 25;
	_obj.borderViewSDD42 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneAccountNo = Ti.UI.createButton(_obj.style.done);
	_obj.doneAccountNo.addEventListener('click',function(){
		_obj.txtDDAccountNo.blur();
	});
	
	_obj.txtDDAccountNo = Ti.UI.createTextField(_obj.style.txtOtherLocation);
	_obj.txtDDAccountNo.hintText = 'Account No*';
	_obj.txtDDAccountNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDDAccountNo.keyboardToolbar = [_obj.doneAccountNo];
	_obj.txtDDAccountNo.maxLength = 25;
	_obj.borderViewSDD43 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.accDDTypeView = Ti.UI.createView(_obj.style.accTypeView);
	_obj.lblDDAccType = Ti.UI.createLabel(_obj.style.lblAccType);
	_obj.lblDDAccType.text = 'Select Account Type*';
	_obj.imgDDAccType = Ti.UI.createImageView(_obj.style.imgAccType);
	_obj.borderViewSDD44 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtDDBankName = Ti.UI.createTextField(_obj.style.txtOtherLocation);
	_obj.txtDDBankName.hintText = 'Bank Name*';
	_obj.txtDDBankName.maxLength = 25;
	_obj.borderViewSDD45 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtDDBankBranch = Ti.UI.createTextField(_obj.style.txtOtherLocation);
	_obj.txtDDBankBranch.hintText = 'Bank Branch*';
	_obj.txtDDBankBranch.maxLength = 25;
	_obj.borderViewSDD46 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtDDBankCity = Ti.UI.createTextField(_obj.style.txtOtherLocation);
	_obj.txtDDBankCity.hintText = 'Bank City*';
	_obj.txtDDBankCity.maxLength = 25;
	_obj.borderViewSDD47 = Ti.UI.createView(_obj.style.borderView);
	
	// Account Credit View
	
	_obj.lblAC = Ti.UI.createLabel(_obj.style.lblAC);
	_obj.lblAC.text = 'Do you know the IFSC code of Beneficiary Bank?';
	
	_obj.ifscView = Ti.UI.createView(_obj.style.ifscView);
	_obj.imgYes = Ti.UI.createImageView(_obj.style.imgYes);
	_obj.imgNo = Ti.UI.createImageView(_obj.style.imgNo);
	
	_obj.mainIFSCView = Ti.UI.createView(_obj.style.mainIFSCView);
	
	_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
	_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
	_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
	_obj.lblInfo.text = 'You are advised that we shall effect credit based solely on the beneficiary details so indicated by you.\n\nPlease note that we shall not ascertain and/or verify the beneficiary name and/or account number prior to effecting the remittance. Remittance instructions so provided by you shall be based solely on the beneficiary account number information provided by you to us.\n\nWe will not be able to retrieve the funds transferred to any beneficiary.';
	
	// Yes
	
	_obj.ifscCodeView = Ti.UI.createView(_obj.style.ifscCodeView);
	
	_obj.txtIFSC = Ti.UI.createTextField(_obj.style.txtIFSC);
	_obj.txtIFSC.value = 'IFSC Code*';
	_obj.txtIFSC.color = '#fff';
	_obj.txtIFSC.maxLength = 11;
	_obj.borderViewIFSC = Ti.UI.createView(_obj.style.borderViewIFSC);
	_obj.btnGo = Ti.UI.createButton(_obj.style.btnGo);
	_obj.btnGo.title = 'GO';
	
	_obj.btnGo.addEventListener('click',function(e){
		
		//IFSC Code Validation Code Start Here
		if(_obj.txtIFSC.value === '' || _obj.txtIFSC.value === 'IFSC Code*')
		{
			require('/utils/AlertDialog').showAlert('','Please enter 11 digits of IFSC code',[L('btn_ok')]).show();
    		_obj.txtIFSC.value = '';
    		_obj.txtIFSC.focus();
			return;
		}
		else if(_obj.txtIFSC.value.length < 11)
		{
			require('/utils/AlertDialog').showAlert('','Please enter 11 digits of IFSC code',[L('btn_ok')]).show();
    		_obj.txtIFSC.value = '';
    		_obj.txtIFSC.focus();
			return;
		}
		else
		{
			
		}
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETIFSCBANKDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"operatingModeId":"INTL",'+
				'"destinationCountry":"'+countryCode[0]+'",'+
				'"destinationCurrency":"'+countryCode[1]+'",'+
				'"bankName":"",'+ 
				'"bankCity":"",'+
				'"bankPatternSearch":"N",'+
				'"cityPatternSearch":"N",'+
				'"micr":"'+_obj.txtIFSC.value+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				_obj.lblACBank.text = e.result.bankName;
				_obj.txtACIFSCCode.value = e.result.ifscCode;
				_obj.txtACBankBranch.value = e.result.branchLocation;
				_obj.txtACCity.value = e.result.cityName;
				_obj.bankId = e.result.bankId;
				_obj.isPartner = e.result.isPartnerBank;
				noIFSC(0);
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addrecipient();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	
	_obj.ifscCodeView.add(_obj.txtIFSC);
	_obj.ifscCodeView.add(_obj.borderViewIFSC);
	_obj.ifscCodeView.add(_obj.btnGo);
	_obj.mainIFSCView.add(_obj.ifscCodeView);
	
	_obj.txtIFSC.addEventListener('focus',function(e){
		try{
			if(e.source.value === 'IFSC Code*'){
		        e.source.value = '';
		    }
	    }catch(e){}
	});
	
	_obj.txtIFSC.addEventListener('blur',function(e){
		try{
			if(e.source.value === ''){
		        e.source.value = 'IFSC Code*';
		    }
	    }catch(e){}
	});
	
	
	// No
	
	_obj.ifscACView = Ti.UI.createView(_obj.style.ifscACView);
	
	_obj.bankACView = Ti.UI.createView(_obj.style.bankView);
	_obj.lblACBank = Ti.UI.createLabel(_obj.style.lblBank);
	_obj.lblACBank.text = 'Select a Bank*';
	_obj.imgACBank = Ti.UI.createImageView(_obj.style.imgBank);
	_obj.borderViewSAC41 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtACIFSCCode = Ti.UI.createTextField(_obj.style.txtIFSC);
	_obj.txtACIFSCCode.hintText = 'IFSC Code*';
	_obj.txtACIFSCCode.editable = false;
	_obj.borderViewSAC42 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtACBankBranch = Ti.UI.createTextField(_obj.style.txtIFSC);
	_obj.txtACBankBranch.hintText = 'Bank Branch*';
	_obj.txtACBankBranch.maxLength = 25;
	_obj.borderViewSAC43 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtACCity = Ti.UI.createTextField(_obj.style.txtIFSC);
	_obj.txtACCity.hintText = 'City*';
	_obj.txtACCity.maxLength = 25;
	_obj.borderViewSAC44 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneAccNo = Ti.UI.createButton(_obj.style.done);
	_obj.doneAccNo.addEventListener('click',function(){
		_obj.txtACAccNo.blur();
	});
	
	_obj.txtACAccNo = Ti.UI.createTextField(_obj.style.txtIFSC);
	_obj.txtACAccNo.hintText = 'Account No*';
	_obj.txtACAccNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtACAccNo.keyboardToolbar = [_obj.doneAccNo];
	_obj.txtACAccNo.maxLength = 25;
	_obj.borderViewSAC45 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneConfirmAccNo = Ti.UI.createButton(_obj.style.done);
	_obj.doneConfirmAccNo.addEventListener('click',function(){
		_obj.txtACConfirmAccNo.blur();
	});
	
	_obj.txtACConfirmAccNo = Ti.UI.createTextField(_obj.style.txtIFSC);
	_obj.txtACConfirmAccNo.hintText = 'Confirm Account No*';
	_obj.txtACConfirmAccNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtACConfirmAccNo.keyboardToolbar = [_obj.doneConfirmAccNo];
	_obj.txtACConfirmAccNo.maxLength = 25;
	_obj.borderViewSAC46 = Ti.UI.createView(_obj.style.borderView);
	
	
	_obj.accACTypeView = Ti.UI.createView(_obj.style.accTypeView);
	_obj.lblACAccType = Ti.UI.createLabel(_obj.style.lblAccType);
	_obj.lblACAccType.text = 'Select Account Type*';
	_obj.imgACAccType = Ti.UI.createImageView(_obj.style.imgAccType);
	_obj.borderViewSAC47 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.btnStep4 = Ti.UI.createButton(_obj.style.btnStep4);
	_obj.btnStep4.title = 'SUBMIT';
	
	_obj.bankACView.add(_obj.lblACBank);
	_obj.bankACView.add(_obj.imgACBank);
	_obj.ifscACView.add(_obj.bankACView);
	_obj.ifscACView.add(_obj.borderViewSAC41);
	
	_obj.ifscACView.add(_obj.txtACIFSCCode);
	_obj.ifscACView.add(_obj.borderViewSAC42);
	
	_obj.ifscACView.add(_obj.txtACBankBranch);
	_obj.ifscACView.add(_obj.borderViewSAC43);
	
	_obj.ifscACView.add(_obj.txtACCity);
	_obj.ifscACView.add(_obj.borderViewSAC44);
	
	_obj.ifscACView.add(_obj.txtACAccNo);
	_obj.ifscACView.add(_obj.borderViewSAC45);
	
	_obj.ifscACView.add(_obj.txtACConfirmAccNo);
	_obj.ifscACView.add(_obj.borderViewSAC46);
	
	_obj.accACTypeView.add(_obj.lblACAccType);
	_obj.accACTypeView.add(_obj.imgACAccType);
	_obj.ifscACView.add(_obj.accACTypeView);
	_obj.ifscACView.add(_obj.borderViewSAC47);
	
	_obj.ifscACView.add(_obj.btnStep4);
	
	_obj.mainIFSCView.add(_obj.ifscACView);
	
	_obj.ifscACView.hide();
	
	/////////// Select Beneficiary Bank Branch //////////
	
	function beneficiaryBranchIFSC(bankName,bankCity)
	{
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETIFSCBANKDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"operatingModeId":"INTL",'+
				'"destinationCountry":"'+countryCode[0]+'",'+
				'"destinationCurrency":"'+countryCode[1]+'",'+
				'"bankName":"'+bankName+'",'+ 
				'"bankCity":"'+bankCity+'",'+
				'"bankPatternSearch":"N",'+
				'"cityPatternSearch":"N"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				// Bank Branch-IFSC OptioDialog
				
				var bankBranchOptions = [];
				var bankBranchOptions1 = [];
				
				for(var i=0;i<e.result.bankCityBranchDetails.length;i++)
				{
					bankBranchOptions.push(e.result.bankCityBranchDetails[i].branchName+'-'+e.result.bankCityBranchDetails[i].ifscCode);
					bankBranchOptions1.push(e.result.bankCityBranchDetails[i].branchName+'$*$'+e.result.bankCityBranchDetails[i].ifscCode);
				}
				
				var optionDialogBankBranch = require('/utils/OptionDialog').showOptionDialog('MICR/NEFT Code List',bankBranchOptions,-1);
				optionDialogBankBranch.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogBankBranch.addEventListener('click',function(evt){
					if(optionDialogBankBranch.options[evt.index] !== L('btn_cancel'))
					{
						var srtSplit = bankBranchOptions1[evt.index].split('$*$');
						
						_obj.txtACBankBranch.value = srtSplit[0];
						_obj.txtACIFSCCode.value = srtSplit[1];
						
						optionDialogBankBranch = null;
					}
					else
					{
						optionDialogBankBranch = null;
						_obj.txtACBankBranch.value = '';
						_obj.txtACIFSCCode.value = '';
					}
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addrecipient();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	/////////// Select Beneficiary Bank Location //////////
	
	function beneficiaryCity(bankName)
	{
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETIFSCBANKDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"operatingModeId":"INTL",'+
				'"destinationCountry":"'+countryCode[0]+'",'+
				'"destinationCurrency":"'+countryCode[1]+'",'+
				'"bankName":"'+bankName+'",'+ 
				'"bankCity":"",'+
				'"bankPatternSearch":"N",'+
				'"cityPatternSearch":"N"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				// Bank City OptioDialog
				
				var bankCityOptions = [];
				
				for(var i=0;i<e.result.bankCityDetails.length;i++)
				{
					bankCityOptions.push(e.result.bankCityDetails[i].cityName);
				}
				
				var optionDialogBankCity = require('/utils/OptionDialog').showOptionDialog('Beneficiary Bank City',bankCityOptions,-1);
				optionDialogBankCity.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogBankCity.addEventListener('click',function(evt){
					if(optionDialogBankCity.options[evt.index] !== L('btn_cancel'))
					{
						_obj.txtACCity.value = optionDialogBankCity.options[evt.index];
						
						/////////// Select Beneficiary Bank Branch //////////
						beneficiaryBranchIFSC(bankName,_obj.txtACCity.value);
						optionDialogBankCity = null;
					}
					else
					{
						optionDialogBankCity = null;
						_obj.txtACCity.value = '';
					}
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addrecipient();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.bankACView.addEventListener('click',function(e){
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETIFSCBANKDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"operatingModeId":"INTL",'+
				'"destinationCountry":"'+countryCode[0]+'",'+
				'"destinationCurrency":"'+countryCode[1]+'",'+
				'"bankName":"",'+ 
				'"bankCity":"",'+
				'"bankPatternSearch":"N",'+
				'"micr":""'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				// Bank OptioDialog
				
				var bankOptions = [];
				var partnerOptions = [];
				
				for(var i=0;i<e.result.bankDetails.length;i++)
				{
					bankOptions.push(e.result.bankDetails[i].bankName);
					partnerOptions.push(e.result.bankDetails[i].isPartnerBank);
				}
				
				var optionDialogBankAccount = require('/utils/OptionDialog').showOptionDialog('Select Bank Name',bankOptions,-1);
				optionDialogBankAccount.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogBankAccount.addEventListener('click',function(evt){
					if(optionDialogBankAccount.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblACBank.text = optionDialogBankAccount.options[evt.index];
						_obj.isPartner = partnerOptions[evt.index];
						
						if(partnerOptions[evt.index] === 'Y')
						{
							require('/utils/AlertDialog').showAlert('','You need not select MICR/NEFT Codes as we have special arrangement with your bank and we ensure that your amount will be credited to the specified account number.',[L('btn_ok')]).show();
							
							_obj.txtACIFSCCode.hide();
							_obj.txtACBankBranch.hide();
							_obj.txtACCity.hide();
							_obj.borderViewSAC42.hide();
							_obj.borderViewSAC43.hide();
							_obj.borderViewSAC44.hide();
							_obj.txtACIFSCCode.setHeight(0);
							_obj.txtACBankBranch.setHeight(0);
							_obj.txtACCity.setHeight(0);
							_obj.txtACIFSCCode.setTop(0);
							_obj.txtACBankBranch.setTop(0);
							_obj.txtACCity.setTop(0);
						}
						else
						{
							_obj.txtACIFSCCode.show();
							_obj.txtACBankBranch.show();
							_obj.txtACCity.show();
							_obj.borderViewSAC42.show();
							_obj.borderViewSAC43.show();
							_obj.borderViewSAC44.show();
							_obj.txtACIFSCCode.setHeight(35);
							_obj.txtACBankBranch.setHeight(35);
							_obj.txtACCity.setHeight(35);
							_obj.txtACIFSCCode.setTop(15);
							_obj.txtACBankBranch.setTop(15);
							_obj.txtACCity.setTop(15);
							
							/////////// Select Beneficiary Bank Location //////////
							beneficiaryCity(_obj.lblACBank.text);
						}
						
						optionDialogBankAccount = null;
					}
					else
					{
						optionDialogBankAccount = null;
						
						_obj.lblACBank.text = 'Select a Bank*';
						_obj.txtACIFSCCode.value = '';
						_obj.txtACIFSCCode.editable = true;
						_obj.txtACBankBranch.value = '';
						_obj.txtACCity.value = '';
						_obj.txtACAccNo.value = '';
						_obj.txtACConfirmAccNo.value = '';
						_obj.lblACAccType.text = 'Select Account Type*';
					}
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addrecipient();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	function dd()
	{
		_obj.ddView.add(_obj.lblDD);
		_obj.ddView.add(_obj.imgDD);
		_obj.step4.add(_obj.ddView);
		_obj.step4.add(_obj.borderViewSDD41);
		_obj.step4.add(_obj.txtOtherLocation);
		_obj.step4.add(_obj.borderViewSDD42);
		_obj.step4.add(_obj.txtDDAccountNo);
		_obj.step4.add(_obj.borderViewSDD43);
		_obj.accDDTypeView.add(_obj.lblDDAccType);
		_obj.accDDTypeView.add(_obj.imgDDAccType);
		_obj.step4.add(_obj.accDDTypeView);
		_obj.step4.add(_obj.borderViewSDD44);
		_obj.step4.add(_obj.txtDDBankName);
		_obj.step4.add(_obj.borderViewSDD45);
		_obj.step4.add(_obj.txtDDBankBranch);
		_obj.step4.add(_obj.borderViewSDD46);
		_obj.step4.add(_obj.txtDDBankCity);
		_obj.step4.add(_obj.borderViewSDD47);
		_obj.step4.add(_obj.btnStep4);
		
		_obj.btnStep4.addEventListener('click',function(e){
			/////////////// Demand Draft /////////////// 
			
			if(_obj.txtOtherLocation.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please select DD Payable Location',[L('btn_ok')]).show();
	    		_obj.txtOtherLocation.value = '';
	    		_obj.txtOtherLocation.focus();
				return;
			}
			else
			{
				
			}
			
			var dobSplit = _obj.lblDOB.text.split('/');
			var dob = dobSplit[0] + '-' + dobSplit[1] + '-' + dobSplit[2];
			//Ti.API.info(dob);
			
			var post = '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"BENEFICIARYADDMODIFYDEL",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"actionVar":"ADD",'+ 
				'"nickName":"'+_obj.txtNickName.value+'",'+
			    '"firstName":"'+_obj.txtFirstName.value+'",'+
			    '"lastName":"'+_obj.txtLastName.value+'",'+
			    '"address":"'+_obj.txtAddress.value+'",'+
			    '"country":"'+countryName[0]+'",'+
			    '"state":"'+_obj.lblState.text+'",'+
			    '"city":"'+_obj.txtCity.value+'",'+
			    '"area":"",'+
			    '"pincode":"'+_obj.txtZip.value+'",'+
			    '"email":"'+_obj.txtEmail.value+'",'+
			    '"resPhone":"'+_obj.txtDayNo.value+'",'+
			    '"mobileNo":"'+_obj.lblMobileNo.text+"-"+_obj.txtMobile.value+'",'+
			    '"destCountry":"'+countryCode[0]+'",'+
			    '"destCurrency":"'+countryCode[1]+'",'+
			    '"nearestLogisticCity":"'+_obj.txtOtherLocation.value+'",'+
			    '"deliveryMode":"DD",'+
			    '"accNumber":"'+_obj.txtDDAccountNo.value+'",'+
			    '"accType":"'+_obj.accType+'",'+
			    '"bankName":"'+_obj.txtDDBankName.value+'",'+
			    '"bankBranch":"'+_obj.txtDDBankBranch.value+'",'+
			    '"bankCity":"'+_obj.txtDDBankCity.value+'",'+
			    '"bankState":"",'+
			    '"isPartnerBank":"N",'+
			    '"recvBankSameFlag":"",'+
			    '"accHolderName":"",'+
			    '"corresBankName":"",'+
			    '"corresBankSwift":"",'+
			    '"otherDetails":"",'+
			    '"tin":"",'+
			    '"dob":"'+dob+'"'+
				'}';
			
			var params = {
				nickName:_obj.txtNickName.value,
			    firstName:_obj.txtFirstName.value,
			    lastName:_obj.txtLastName.value,
			    address:_obj.txtAddress.value,
			    country:countryName[0],
			    state:_obj.lblState.text,
			    city:_obj.txtCity.value,
			    pincode:_obj.txtZip.value,
			    email:_obj.txtEmail.value,
			    resPhone:_obj.txtDayNo.value,
			    mobileNo:_obj.lblMobileNo.text+"-"+_obj.txtMobile.value,
			    dob:dob
				};
				
				require('/js/recipients/AddRecipientConfirmationModal').AddRecipientConfirmationModal(params,post);
				setTimeout(function(){
					destroy_addrecipient();
				},500);
		});
	}
	
	function ac()
	{
		_obj.step4.add(_obj.lblAC);
		_obj.ifscView.add(_obj.imgYes);
		_obj.ifscView.add(_obj.imgNo);
		_obj.step4.add(_obj.ifscView);
		yesIFSC();
		_obj.step4.add(_obj.mainIFSCView);
		_obj.infoTextView.add(_obj.imgInfo);
		_obj.infoTextView.add(_obj.lblInfo);
		_obj.step4.add(_obj.infoTextView);
		
		_obj.btnStep4.addEventListener('click',function(e){
			
			/////////////// Account Credit ///////////////
			var varBankName = '';
			
			//Bank Name
			if(_obj.lblACBank.text === 'Select a Bank*')
			{
				require('/utils/AlertDialog').showAlert('','Please select your Receiver\'s Bank Name',[L('btn_ok')]).show();
				return;
			}
			else
			{
				varBankName = _obj.lblACBank.text.toUpperCase();
			}
			
			if(_obj.isPartner !== 'Y') // Not a bank partner with TOML
			{
				//IFSC
				if(_obj.txtACIFSCCode.value == "")
				{
					require('/utils/AlertDialog').showAlert('','Please enter your Receiver\'s Bank IFSC code',[L('btn_ok')]).show();
		    		_obj.txtACIFSCCode.value = '';
		    		_obj.txtACIFSCCode.focus();
					return;
				}
				else if(_obj.txtACIFSCCode.value.length != 9 && _obj.txtACIFSCCode.value.length != 11 && _obj.lblACBank.text !== 'Select a Bank*')
				{
					require('/utils/AlertDialog').showAlert('','Please enter the 9 or 11 digit MICR/NEFT code for the branch of the bank chosen',[L('btn_ok')]).show();
		    		_obj.txtACIFSCCode.value = '';
		    		_obj.txtACIFSCCode.focus();
					return;
				}
				else
				{
					
				}
				
				//Bank Branch
				if(_obj.txtACBankBranch.value == "")
				{
					require('/utils/AlertDialog').showAlert('','Please enter your Receiver\'s Bank Branch',[L('btn_ok')]).show();
		    		_obj.txtACBankBranch.value = '';
		    		_obj.txtACBankBranch.focus();
					return;
				}
				else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtACBankBranch.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Bank Branch',[L('btn_ok')]).show();
		    		_obj.txtACBankBranch.value = '';
		    		_obj.txtACBankBranch.focus();
					return;
				}
				else if(require('/lib/toml_validations').validateAlpaNumericSpecialChars(_obj.txtACBankBranch.value,'Bank Branch Name') === false)
				{
					_obj.txtACBankBranch.value = '';
		    		_obj.txtACBankBranch.focus();
					return;
				}
				else
				{
					
				}
				
				//Bank City
				if(_obj.txtACCity.value === '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your Receiver\'s Bank City',[L('btn_ok')]).show();
		    		_obj.txtACCity.value = '';
		    		_obj.txtACCity.focus();
					return;
				}
				else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtACCity.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Bank City',[L('btn_ok')]).show();
		    		_obj.txtACCity.value = '';
		    		_obj.txtACCity.focus();
					return;
				}
				else if(require('/lib/toml_validations').validateAlpaNumericSpecialChars(_obj.txtACCity.value,'Bank City') === false)
				{
					_obj.txtACCity.value = '';
		    		_obj.txtACCity.focus();
					return;
				}
				else
				{
					
				}
			}
			
			// Account Number
			if(_obj.txtACAccNo.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Account Number',[L('btn_ok')]).show();
	    		_obj.txtACAccNo.value = '';
	    		_obj.txtACAccNo.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtACAccNo.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Account Number',[L('btn_ok')]).show();
	    		_obj.txtACAccNo.value = '';
	    		_obj.txtACAccNo.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtACAccNo.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Account Number',[L('btn_ok')]).show();
	    		_obj.txtACAccNo.value = '';
	    		_obj.txtACAccNo.focus();
				return;
			}
			else
			{
				
			}
			
			if(_obj.lblACBank.text !== 'Select a Bank*')
			{
				if(!(varBankName === "INDUSIND BANK"))
				{
					/*if(varBankName === "PUNJAB NATIONAL BANK")
					{
						if(!isNaN(_obj.txtACAccNo.value))
						{
							 if(require('/lib/toml_validations').validateAlpaNumeric(_obj.txtACAccNo.value,'Account Number') === false)
							 {
							 	_obj.txtACAccNo.value = '';
					    		_obj.txtACAccNo.focus();
								return;
							 }
						}
					}*/
				  if(varBankName != "CORPORATION BANK")
					{ 
						if(_obj.txtACAccNo.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('','Account Number must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtACAccNo.value = '';
				    		_obj.txtACAccNo.focus();
							return;
						}
					}
				}
				
				var addFlag = false;
				var addStr = _obj.txtACAccNo.value;
				var re = /[0-9]/g; // Regular Expressions to match Globally.
				
				if(addStr.match(re)){
					addFlag = true;
					accountFlag = true;
				}else{			
					addFlag = false;
					accountFlag = false;
				}
		
				if(varBankName !== "CORPORATION BANK")
				{
					if(!addFlag)
					{
						require('/utils/AlertDialog').showAlert('','Please enter a valid Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				
				if(varBankName == "DEUTSCHE BANK")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "PUNJAB NATIONAL BANK")
				{
					if(_obj.txtACAccNo.value.length != 16)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 16 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "ABN AMRO BANK")
				{
					if(_obj.txtACAccNo.value.length > 7 || _obj.txtACAccNo.value.length<5)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 5 to 7 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if((varBankName.indexOf("STATE BANK OF") !== -1) || (varBankName.indexOf("STATE BANK OF") !== -1))
				{
					if(_obj.txtACAccNo.value.length != 11)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 11 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "Allahabad Bank")
				{
					if(_obj.txtACAccNo.value.length != 11)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 11 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "ANDHRA BANK")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "AXIS BANK LTD")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "BANK OF BARODA")
				{
					if(_obj.txtACAccNo.value.length != 14)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "BANK OF INDIA")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "BANK OF MAHARASHTRA")
				{
					if(_obj.txtACAccNo.value.length != 11)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 11 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "BANK OF RAJASTHAN LTD.")
				{
					if(_obj.txtACAccNo.value.length != 13)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 13 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "CANARA BANK")
				{
					if(_obj.txtACAccNo.value.length < 13)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 13 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
					else if(_obj.txtACAccNo.value.length > 14)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "CENTRAL BANK OF INDIA")
				{
					if(_obj.txtACAccNo.value.length != 10)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 10 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "CITY UNION BANK LIMITED")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "CITIBANK")
				{
					if(_obj.txtACAccNo.value.length != 10)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 10 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "CORPORATION BANK")
				{
					if((_obj.txtACAccNo.value.length < 10) || (_obj.txtACAccNo.value.length) > 15 )	
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 10 to 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}			
				}
				else if(varBankName == "COSMOS CO-OPERATIVE BANK LTD.")
				{
					if((_obj.txtACAccNo.value.length < 10) || (_obj.txtACAccNo.value.length) > 15 )	
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 10 to 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "DENA BANK")
				{
					if(_obj.txtACAccNo.value.length != 12)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 12 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "DEUTSCHE BANK")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "DHANLAXMI BANK LTD.")
				{
					if(_obj.txtACAccNo.value.length != 16 && _obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 to 16 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}		
				}
				else if(varBankName == "HDFC BANK")
				{
					if(_obj.txtACAccNo.value.length != 14)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "HSBC")
				{
					if(_obj.txtACAccNo.value.length != 12)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 12 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "ICICI BANKING CORPORATION LTD.")
				{
					if(_obj.txtACAccNo.value.length != 12)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 12 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "IDBI BANK")
				{
					if(_obj.txtACAccNo.value.length < 1 || _obj.txtACAccNo.value.length > 20)
					{
						require('/utils/AlertDialog').showAlert('','The Account Number has to be between 1 and 20 digits',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
					else if(_obj.txtACAccNo.value.search("[^0-9]") >= 0)
					{
						require('/utils/AlertDialog').showAlert('','Account Number must be a Numeric Value',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "INDIAN BANK")
				{
					if(_obj.txtACAccNo.value.length != 9 && _obj.txtACAccNo.value.length !=10)			
					{
						require('/utils/AlertDialog').showAlert('','The Account Number has to be between 9 to 10 digits',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "INDIAN OVERSEAS BANK")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}		
				}
				else if(varBankName == "INDUSIND BANK")
				{
					if(require('/lib/toml_validations').validateAlpaNumeric(_obj.txtACAccNo.value,'Account Number') === false)
					 {
					 	_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					 }
					else if(_obj.txtACAccNo.value.length != 12 && _obj.txtACAccNo.value.length !=13)			
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 12 or 13 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "ING VYSYA BANK")
				{
					if(_obj.txtACAccNo.value.length != 12)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 12 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "JAMMU AND KASHMIR BANK LTD.")
				{
					if(_obj.txtACAccNo.value.length != 16)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 16 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "KARNATAKA BANK LTD.")
				{
					if(_obj.txtACAccNo.value.length != 16)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 16 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "KARUR VYSYA BANK LTD.")
				{
					if(_obj.txtACAccNo.value.length !== 16)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 16 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "KOTAK MAHINDRA BANK LTD") 
				{
					if(_obj.txtACAccNo.value.length < 10 || _obj.txtACAccNo.value.length > 14) 
					{ 
						require('/utils/AlertDialog').showAlert('','Please enter the 10 to 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
		 		else if(varBankName == "ORIENTAL BANK OF COMMERCE")
				{
					if(_obj.txtACAccNo.value.length != 14)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "PUNJAB NATIONAL BANK")
				{ 
					if(_obj.txtACAccNo.value.length != 16)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 16 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "SOUTH INDIAN BANK LTD.")
				{
					if(_obj.txtACAccNo.value.length != 16)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 16 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "STANDARD CHARTERED BANK")
				{
					if(_obj.txtACAccNo.value.length != 11)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 11 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}		
				else if(varBankName == "SYNDICATE BANK")
				{
					if(_obj.txtACAccNo.value.length != 14)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "SARASWAT CO-OPERATIVE BANK LTD.")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "UCO BANK")
				{
					if(_obj.txtACAccNo.value.length != 14)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "UNION BANK OF INDIA")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "UNITED BANK OF INDIA")
				{
					if(_obj.txtACAccNo.value.length != 13)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 13 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "VIJAYA BANK")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "YES BANK LTD.")
				{
					if(_obj.txtACAccNo.value.length != 15)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 15 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "FEDERAL BANK LTD.")
				{
					if(_obj.txtACAccNo.value.length != 14)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "CENTURION BANK OF PUNJAB")
				{
					if(_obj.txtACAccNo.value.length != 14)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else if(varBankName == "ORIENTAL BANK OF COMMERCE")
				{
					if(_obj.txtACAccNo.value.length != 14)
					{
						require('/utils/AlertDialog').showAlert('','Please enter the 14 digit Account Number',[L('btn_ok')]).show();
			    		_obj.txtACAccNo.value = '';
			    		_obj.txtACAccNo.focus();
						return;
					}
				}
				else
				{
					
				}
			}
			
			// Confirm Account Number
			if(_obj.txtACConfirmAccNo.value == "")
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Confirm Account Number',[L('btn_ok')]).show();
	    		_obj.txtACConfirmAccNo.value = '';
	    		_obj.txtACConfirmAccNo.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtACConfirmAccNo.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Confirm Account Number',[L('btn_ok')]).show();
	    		_obj.txtACConfirmAccNo.value = '';
	    		_obj.txtACConfirmAccNo.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtACConfirmAccNo.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Confirm Account Number',[L('btn_ok')]).show();
	    		_obj.txtACConfirmAccNo.value = '';
	    		_obj.txtACConfirmAccNo.focus();
				return;
			}
			
			// Check if both account number and confirm account will be same
			else if(_obj.txtACAccNo.value !== _obj.txtACConfirmAccNo.value)
			{
				require('/utils/AlertDialog').showAlert('','Please enter correct Account Number',[L('btn_ok')]).show();
	    		_obj.txtACConfirmAccNo.value = '';
	    		_obj.txtACConfirmAccNo.focus();
				return;
			}
			else
			{
				
			}
			
			//Account Type
			if(_obj.lblACAccType.text === 'Select Account Type*')
			{
				require('/utils/AlertDialog').showAlert('','Please select your Receiver\'s Account Type',[L('btn_ok')]).show();
	    		return;
			}
			else
			{
				
			}
			
			var dobSplit = _obj.lblDOB.text.split('/');
			var dob = dobSplit[0] + '-' + dobSplit[1] + '-' + dobSplit[2];
		
			var post = '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"BENEFICIARYADDMODIFYDEL",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"actionVar":"ADD",'+ 
				'"nickName":"'+_obj.txtNickName.value+'",'+
			    '"firstName":"'+_obj.txtFirstName.value+'",'+
			    '"lastName":"'+_obj.txtLastName.value+'",'+
			    '"address":"'+_obj.txtAddress.value+'",'+
			    '"country":"'+countryName[0]+'",'+
			    '"state":"'+_obj.lblState.text+'",'+
			    '"city":"'+_obj.txtCity.value+'",'+
			    '"area":"",'+
			    '"pincode":"'+_obj.txtZip.value+'",'+
			    '"email":"'+_obj.txtEmail.value+'",'+
			    '"resPhone":"'+_obj.txtDayNo.value+'",'+
			    '"mobileNo":"'+_obj.lblMobileNo.text+"-"+_obj.txtMobile.value+'",'+
			    '"destCountry":"'+countryCode[0]+'",'+
			    '"destCurrency":"'+countryCode[1]+'",'+
			    '"deliveryMode":"DC",'+
			    '"micr":"'+_obj.txtACIFSCCode.value+'",'+
			    '"accNumber":"'+_obj.txtACAccNo.value+'",'+
			    '"accType":"'+_obj.accType+'",'+
			    '"bankName":"'+_obj.lblACBank.text+'",'+
			    '"bankBranch":"'+_obj.txtACBankBranch.value+'",'+
			    '"bankCity":"'+_obj.txtACCity.value+'",'+
			    '"bankState":"",'+
			    '"isPartnerBank":"'+_obj.isPartner+'",'+
			    '"recvBankSameFlag":"",'+
			    '"accHolderName":"",'+
			    '"corresBankName":"",'+
			    '"corresBankSwift":"",'+
			    '"otherDetails":"",'+
			    '"tin":"",'+
			    '"dob":"'+dob+'"'+
				'}';
	
			var params = {
				nickName:_obj.txtNickName.value,
			    firstName:_obj.txtFirstName.value,
			    lastName:_obj.txtLastName.value,
			    address:_obj.txtAddress.value,
			    country:countryName[0],
			    state:_obj.lblState.text,
			    city:_obj.txtCity.value,
			    pincode:_obj.txtZip.value,
			    email:_obj.txtEmail.value,
			    resPhone:_obj.txtDayNo.value,
			    mobileNo:_obj.lblMobileNo.text+"-"+_obj.txtMobile.value,
			    dob:dob
				};
	
			require('/js/recipients/AddRecipientConfirmationModal').AddRecipientConfirmationModal(params,post);
			setTimeout(function(){
				destroy_addrecipient();
			},500);
		});
	}
	
	function yesIFSC()
	{
		_obj.txtIFSC.value = 'IFSC Code*';
		_obj.ifscCodeView.show();
		_obj.ifscACView.hide();
	}
	
	function noIFSC(editIFSC)
	{
		if(editIFSC === 1)
		{
			_obj.lblACBank.text = 'Select a Bank*';
			_obj.txtACIFSCCode.value = '';
			_obj.txtACIFSCCode.editable = true;
			_obj.txtACBankBranch.value = '';
			_obj.txtACCity.value = '';
			_obj.txtACAccNo.value = '';
			_obj.txtACConfirmAccNo.value = '';
			_obj.lblACAccType.text = 'Select Account Type*';
			
			_obj.txtACIFSCCode.show();
			_obj.txtACBankBranch.show();
			_obj.txtACCity.show();
			_obj.borderViewSAC42.show();
			_obj.borderViewSAC43.show();
			_obj.borderViewSAC44.show();
			_obj.txtACIFSCCode.setHeight(35);
			_obj.txtACBankBranch.setHeight(35);
			_obj.txtACCity.setHeight(35);
			_obj.txtACIFSCCode.setTop(15);
			_obj.txtACBankBranch.setTop(15);
			_obj.txtACCity.setTop(15);
		}
		else
		{
			_obj.txtACIFSCCode.editable = false;
			
			_obj.txtACIFSCCode.show();
			_obj.txtACBankBranch.show();
			_obj.txtACCity.show();
			_obj.borderViewSAC42.show();
			_obj.borderViewSAC43.show();
			_obj.borderViewSAC44.show();
			_obj.txtACIFSCCode.setHeight(35);
			_obj.txtACBankBranch.setHeight(35);
			_obj.txtACCity.setHeight(35);
			_obj.txtACIFSCCode.setTop(15);
			_obj.txtACBankBranch.setTop(15);
			_obj.txtACCity.setTop(15);
		}
		_obj.ifscCodeView.hide();
		_obj.ifscACView.show();
	}
	
	_obj.imgYes.addEventListener('click',function(e){
		if(_obj.imgYes.image === '/images/yes_unsel.png')
		{
			_obj.imgYes.image = '/images/yes_sel.png';
			_obj.imgNo.image = '/images/no_unsel.png';
			yesIFSC();
		}	
	});
	
	_obj.imgNo.addEventListener('click',function(e){
		if(_obj.imgNo.image === '/images/no_unsel.png')
		{
			_obj.imgYes.image = '/images/yes_unsel.png';
			_obj.imgNo.image = '/images/no_sel.png';
			noIFSC(1);
		}	
	});
	
	_obj.accACTypeView.addEventListener('click',function(e){
		var optionDialogAccType = require('/utils/OptionDialog').showOptionDialog('Account Type',['Savings','NRE','NRO'],-1);
		optionDialogAccType.show();
		
		setTimeout(function(){
			activityIndicator.hideIndicator();
		},200);
		
		optionDialogAccType.addEventListener('click',function(evt){
			if(optionDialogAccType.options[evt.index] !== L('btn_cancel'))
			{
				_obj.lblACAccType.text = optionDialogAccType.options[evt.index];
				
				if(optionDialogAccType.options[evt.index] === 'Savings')
				{
					_obj.accType = 'S';
				}else if(optionDialogAccType.options[evt.index] === 'NRE')
				{
					_obj.accType = 'NR';
				}else if(optionDialogAccType.options[evt.index] === 'Checking/Current')
				{
					_obj.accType = 'C';
				}else
				{
					_obj.accType = 'NO';
				} 
				
				optionDialogAccType = null;
			}
			else
			{
				optionDialogAccType = null;
				_obj.lblACAccType.text = 'Select Account Type*';
			}
		});
	});
	
	_obj.accDDTypeView.addEventListener('click',function(e){
		var optionDialogAccType = require('/utils/OptionDialog').showOptionDialog('Account Type',['Savings','NRE','Checking/Current','NRO'],-1);
		optionDialogAccType.show();
		
		setTimeout(function(){
			activityIndicator.hideIndicator();
		},200);
		
		optionDialogAccType.addEventListener('click',function(evt){
			if(optionDialogAccType.options[evt.index] !== L('btn_cancel'))
			{
				_obj.lblDDAccType.text = optionDialogAccType.options[evt.index];
				
				if(optionDialogAccType.options[evt.index] === 'Savings')
				{
					_obj.accType = 'S';
				}else if(optionDialogAccType.options[evt.index] === 'NRE')
				{
					_obj.accType = 'NR';
				}else if(optionDialogAccType.options[evt.index] === 'Checking/Current')
				{
					_obj.accType = 'C';
				}else
				{
					_obj.accType = 'NO';
				} 
				
				optionDialogAccType = null;
			}
			else
			{
				optionDialogAccType = null;
				_obj.lblDDAccType.text = 'Select Account Type*';
			}
		});
	});
	
	_obj.ddView.addEventListener('click',function(){
		activityIndicator.showIndicator();
		var ddOptions = [];
		
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETPARTDDLOCATION",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"destinationCountry":"'+countryCode[0]+'",'+
				'"destinationCurrency":"'+countryCode[1]+'",'+
				'"ddLocCity":""'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			
			if(e.result.responseFlag === "S")
			{
				for(var i=0;i<e.result.ddLocations.length;i++)
				{
					ddOptions.push(e.result.ddLocations[i].ddLocationName);
				}
				
				var optionDialogDD = require('/utils/OptionDialog').showOptionDialog('DD Payable at',ddOptions,-1);
				optionDialogDD.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogDD.addEventListener('click',function(evt){
					if(optionDialogDD.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblDD.text = optionDialogDD.options[evt.index];
						_obj.txtOtherLocation.value = optionDialogDD.options[evt.index];
						optionDialogDD = null;
					}
					else
					{
						optionDialogDD = null;
						_obj.lblDD.text = 'DD Payable at*';	
						_obj.txtOtherLocation.hintText = 'Other Locations';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addrecipient();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.mainView.add(_obj.scrollableView);
	_obj.globalView.add(_obj.mainView);
	_obj.winAddRecipient.add(_obj.globalView);
	_obj.winAddRecipient.open();
	
	_obj.scrollableView.views = [_obj.step1,_obj.step2,_obj.step3,_obj.step4];
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_addrecipient();
	});
	
	_obj.winAddRecipient.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_addrecipient();
				alertDialog = null;
			}
		});
	});
	
	function changeTabs(selected)
	{
		if(selected === 'step1')
		{
			_obj.tabSelView.left = 0;
			_obj.tabSelView.right = null;
			
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			
			_obj.lblStep1.color = TiFonts.FontStyle('whiteFont');
			_obj.lblStep1.backgroundColor = '#6F6F6F';
			_obj.lblStep2.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep2.backgroundColor = '#E9E9E9';
			_obj.lblStep3.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep3.backgroundColor = '#E9E9E9';
			_obj.lblStep4.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep4.backgroundColor = '#E9E9E9';
		}
		else if(selected === 'step2')
		{
			_obj.tabSelView.left = '25%';
			_obj.tabSelView.right = null;
			
			_obj.tabSelIconView.left = '25%';
			_obj.tabSelIconView.right = null;
			
			_obj.lblStep1.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep1.backgroundColor = '#E9E9E9';
			_obj.lblStep2.color = TiFonts.FontStyle('whiteFont');
			_obj.lblStep2.backgroundColor = '#6F6F6F';
			_obj.lblStep3.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep3.backgroundColor = '#E9E9E9';
			_obj.lblStep4.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep4.backgroundColor = '#E9E9E9';
		}
		else if(selected === 'step3')
		{
			_obj.tabSelView.left = '50%';
			_obj.tabSelView.right = null;
			
			_obj.tabSelIconView.left = '50%';
			_obj.tabSelIconView.right = null;;
			
			_obj.lblStep1.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep1.backgroundColor = '#E9E9E9';
			_obj.lblStep2.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep2.backgroundColor = '#E9E9E9';
			_obj.lblStep3.color = TiFonts.FontStyle('whiteFont');
			_obj.lblStep3.backgroundColor = '#6F6F6F';
			_obj.lblStep4.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep4.backgroundColor = '#E9E9E9';
		}
		else
		{
			_obj.tabSelView.left = null;
			_obj.tabSelView.right = 0;
			
			_obj.tabSelIconView.left = null;
			_obj.tabSelIconView.right = 0;
			
			_obj.lblStep1.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep1.backgroundColor = '#E9E9E9';
			_obj.lblStep2.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep2.backgroundColor = '#E9E9E9';
			_obj.lblStep3.color = TiFonts.FontStyle('blackFont');
			_obj.lblStep3.backgroundColor = '#E9E9E9';
			_obj.lblStep4.color = TiFonts.FontStyle('whiteFont');
			_obj.lblStep4.backgroundColor = '#6F6F6F';
		}
	}
	
	function destroy_addrecipient()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove add recipient start ##############');
			
			_obj.winAddRecipient.close();
			require('/utils/RemoveViews').removeViews(_obj.winAddRecipient);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('calculateAge',calculateAge);
			Ti.App.removeEventListener('destroy_addrecipient',destroy_addrecipient);
			require('/utils/Console').info('############## Remove add recipient end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_addrecipient', destroy_addrecipient);
}; // AddRecipientModal()
