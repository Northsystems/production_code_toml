exports.ViewRecipientModal = function()
{
	require('/lib/analytics').GATrackScreen('View Recipients');
	
	var _obj = {
		style : require('/styles/recipients/ViewRecipient').ViewRecipient,
		winViewRecipient : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		recipientView : null,
		tblRecipients : null,
	};
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//var origCC = origSplit[0]+'-'+origSplit[1];
	//alert(countryName[0]);
	
	_obj.winViewRecipient = Titanium.UI.createWindow(_obj.style.winViewRecipient);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winViewRecipient);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'View Recipients';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// View Recipients ///////////////////
	
	_obj.recipientView = Ti.UI.createView(_obj.style.recipientView);
	
	_obj.tblRecipients = Ti.UI.createTableView(_obj.style.tableView);
	
	_obj.tblRecipients.addEventListener('click',function(e){
		
		var params = {
			accName:e.row.accName,
			accType:e.row.accType,
			bankName:e.row.bankName,
			accNo:e.row.accNo,
			routNo:e.row.routNo,
			recType : e.row.recType,
			accId  : e.row.accId,
		};
		
		if(e.source.sel === 'Send Money')
		{
			if(Ti.App.Properties.getString('loginStatus') !== '0')
			{
				require('/utils/RemoveViews').removeAllScrollableViews();
				require('/utils/PageController').pageController('transfer');
			}
			else
			{
				if(Ti.App.Properties.getString('mPIN') === '1')
				{
					require('/js/Passcode').Passcode();
				}
				else
				{
					require('/js/LoginModal').LoginModal();
				}
			}
		}
		else if(e.source.sel === 'edit')
		{
			var params = {
				state: e.row.state,
				tin: e.row.tin,
				destCurrency: e.row.destCurrency,
				city: e.row.city,
				corresBankSwift: e.row.corresBankSwift,
				accNumber: e.row.accNumber,
				area: e.row.area,
				nickName:e.row.nickName,
				firstName: e.row.firstName,
				lastName: e.row.lastName, 
				nearestLogisticCity: e.row.nearestLogisticCity,
				bankCity:e.row.bankCity,
				deliveryMode: e.row.deliveryMode, 
				parentChildFlag: e.row.parentChildFlag,
				bankName: e.row.bankName,
			 	bankState: e.row.bankState,
				deleteBeneFlag: e.row.deleteBeneFlag,
				country: e.row.country,
				pincode: e.row.pincode,
				accType: e.row.accType,
				accHolderName: e.row.accHolderName,
				email: e.row.email,
				address: e.row.address,
				dob: e.row.dob,
				isPartnerBank: e.row.isPartnerBank,
				micr: e.row.micr,
				mobileNo: e.row.mobileNo,
				resPhone: e.row.resPhone,
				bankBranch: e.row.bankBranch,
				destCountry: e.row.destCountry, 
				recvBankTransferFlag: e.row.recvBankTransferFlag, 
				recvBankSameFlag: e.row.recvBankSameFlag,
			};
			
			require('/js/recipients/EditRecipientModal').EditRecipientModal(params);
		}
		else if(e.source.sel === 'delete')
		{
			var alertDialog = require('/utils/AlertDialog').showAlert('', 'Once you click on OK, the recipient will be deleted.', [L('btn_ok'), L('btn_cancel')]);
			alertDialog.show();
	
			alertDialog.addEventListener('click', function(evt) {
				alertDialog.hide();
				if (evt.index === 0 || evt.index === "0") {
					activityIndicator.showIndicator();
		
					var xhr = require('/utils/XHR');
					
					xhr.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"BENEFICIARYADDMODIFYDEL",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
							'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
							'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
							'"actionVar":"Delete",'+
						    '"nickName":"'+e.row.nickName+'",'+
						    '"firstName":"",'+
						    '"lastName":"",'+
						    '"address":"",'+
						    '"country":"",'+
						    '"state":"",'+
						    '"city":"",'+
						    '"area":"",'+
						    '"pincode":"",'+
						    '"email":"",'+
						    '"resPhone":"",'+
						    '"mobileNo":"",'+
						    '"deliveryMode":"",'+
						    '"destCountry":"",'+
						    '"destCurrency":"",'+
						    '"nearestLogisticCity":"",'+
						    '"accNumber":"",'+
						    '"accType":"",'+
						    '"micr":"",'+
						    '"bankName":"",'+
						    '"bankBranch":"",'+
						    '"bankCity":"",'+
						    '"bankState":"",'+
						    '"isPartnerBank":"",'+
						    '"recvBankSameFlag":"",'+
						    '"accHolderName":"",'+
						    '"corresBankName":"",'+
						    '"corresBankSwift":"",'+
						    '"otherDetails":"",'+
						    '"tin":"",'+
						    '"dob":""'+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
					
					function xhrSuccess(e) {
						require('/utils/Console').info('Result ======== ' + e.result);
						activityIndicator.hideIndicator();
						if(e.result.responseFlag === "S")
						{
							if(TiGlobals.osname === 'android')
							{
								require('/utils/AlertDialog').toast(e.result.message);
							}
							else
							{
								require('/utils/AlertDialog').iOSToast(e.result.message);
							}
							
							populateRecipients();
						}
						else
						{
							if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
							{
								require('/lib/session').session();
								destroy_viewrecipient();
							}
							else
							{
								require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
							}
						}
						
						xhr = null;
					}
			
					function xhrError(e) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr = null;
					}
				}
			});
		}
		else
		{
			
		}
	});
	
	var start = 1;
	var max = 100;
	var accountNo = 0;
	
	function populateRecipients() {
		activityIndicator.showIndicator();
		
		_obj.tblRecipients.setData([]);
		
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"BENEFICIARYLISTING",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"rangeFrom":"'+start+'",'+
				'"rangeTo":"'+max+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(e.result.responseFlag === "S")
			{
				for(var i=0; i<e.result.RecieverData.length; i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 106,
						backgroundColor : 'transparent',
						className : 'bank_account',
						state: e.result.RecieverData[i].state,
						tin: e.result.RecieverData[i].tin,
						destCurrency: e.result.RecieverData[i].destCurrency,
						city: e.result.RecieverData[i].city,
						corresBankSwift: e.result.RecieverData[i].corresBankSwift,
						accNumber: e.result.RecieverData[i].accNumber,
						area: e.result.RecieverData[i].area,
						nickName:e.result.RecieverData[i].nickName,
						firstName: e.result.RecieverData[i].firstName,
						lastName: e.result.RecieverData[i].lastName, 
						nearestLogisticCity: e.result.RecieverData[i].nearestLogisticCity,
						bankCity:e.result.RecieverData[i].bankCity,
						deliveryMode: e.result.RecieverData[i].deliveryMode, 
						parentChildFlag: e.result.RecieverData[i].parentChildFlag,
						bankName: e.result.RecieverData[i].bankName,
					 	bankState: e.result.RecieverData[i].bankState,
						deleteBeneFlag: e.result.RecieverData[i].deleteBeneFlag,
						country: e.result.RecieverData[i].country,
						pincode: e.result.RecieverData[i].pincode,
						accType: e.result.RecieverData[i].accType,
						accHolderName: e.result.RecieverData[i].accHolderName,
						email: e.result.RecieverData[i].email,
						address: e.result.RecieverData[i].address,
						dob: e.result.RecieverData[i].dob,
						isPartnerBank: e.result.RecieverData[i].isPartnerBank,
						micr: e.result.RecieverData[i].micr,
						mobileNo: e.result.RecieverData[i].mobileNo,
						resPhone: e.result.RecieverData[i].resPhone,
						bankBranch: e.result.RecieverData[i].bankBranch,
						destCountry: e.result.RecieverData[i].destCountry, 
						recvBankTransferFlag: e.result.RecieverData[i].recvBankTransferFlag, 
						recvBankSameFlag: e.result.RecieverData[i].recvBankSameFlag,
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var topView = Ti.UI.createView({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : '#6f6f6f',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var recipientView = Ti.UI.createView({
						height : 50,
						top : 0,
						left : 20,
						width:'50%',
					});
					
					var lblRecipientName = Ti.UI.createLabel({
						text : e.result.RecieverData[i].firstName +' '+e.result.RecieverData[i].lastName,
						height : 20,
						top : 5,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblBold14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var lblNickName = Ti.UI.createLabel({
						text : e.result.RecieverData[i].nickName,
						height : 20,
						top : 25,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var borderView = Ti.UI.createView({
						height : 50,
						top : 0,
						left : 0,
						width:1,
						backgroundColor:'#898989'
					});
					
					var lblDeliveryMode = Ti.UI.createLabel({
						text : e.result.RecieverData[i].deliveryMode === "DD" ? 'Demand Draft ' : 'Account Credit',
						height : Ti.UI.SIZE,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblBold14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var bottomView = Ti.UI.createView({
						top : 50,
						left : 0,
						right : 0,
						height : 56,
						backgroundColor : '#fff',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var bankView = Ti.UI.createView({
						top : 0,
						left : 20,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var lblBankName = Ti.UI.createLabel({
						text : e.result.RecieverData[i].bankName,
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var lblAccountNo = Ti.UI.createLabel({
						text : 'A/c No. - '+e.result.RecieverData[i].accNumber,
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var statusView = Ti.UI.createView({
						top : 0,
						left : 20,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var lblAccountStatus = Ti.UI.createLabel({
						text : 'Action: Send Money',
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont'),
						sel:e.result.RecieverData[i].accountStatus
					});
					
					var actionView = Ti.UI.createView({
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var lblEdit = Ti.UI.createLabel({
						text : 'Edit ',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont'),
						sel:'edit'
						
					});
					
					var lblDelete = Ti.UI.createLabel({
						text : '| Delete',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont'),
						sel:'delete'
					});
					
					recipientView.add(lblRecipientName);
					recipientView.add(lblNickName);
					topView.add(recipientView);
					topView.add(borderView);
					topView.add(lblDeliveryMode);
					bankView.add(lblBankName);
					bankView.add(lblAccountNo);
					bottomView.add(bankView);
					statusView.add(lblAccountStatus);
					actionView.add(lblEdit);
					actionView.add(lblDelete);	
					statusView.add(actionView);
					bottomView.add(statusView);
					row.add(topView);
					row.add(bottomView);
					_obj.tblRecipients.appendRow(row);
				}
				
				activityIndicator.hideIndicator();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_viewrecipient();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	populateRecipients();
	
	Ti.App.addEventListener('populateRecipients',populateRecipients);
	
	_obj.mainView.add(_obj.recipientView);
	_obj.recipientView.add(_obj.tblRecipients);
	_obj.globalView.add(_obj.mainView);
	_obj.winViewRecipient.add(_obj.globalView);
	_obj.winViewRecipient.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_viewrecipient();
	});
	
	function destroy_viewrecipient()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove view recipient list start ##############');
			
			_obj.winViewRecipient.close();
			require('/utils/RemoveViews').removeViews(_obj.winViewRecipient);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('populateRecipients',populateRecipients);
			Ti.App.removeEventListener('destroy_viewrecipient',destroy_viewrecipient);
			require('/utils/Console').info('############## Remove view recipient list end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_viewrecipient', destroy_viewrecipient);
}; // ViewRecipientModal()