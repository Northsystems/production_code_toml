exports.EditRecipientConfirmationModal = function(params,post)
{
	require('/lib/analytics').GATrackScreen('Edit Receiver Confirmation');
	
	var _obj = {
		style : require('/styles/recipients/AddRecipientConfirmation').AddRecipientConfirmation,
		winEditRecipientConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		tblDetails : null,
		btnSubmit : null,
		
		tblHeight : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winEditRecipientConfirmation = Titanium.UI.createWindow(_obj.style.winAddRecipientConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winEditRecipientConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Receiver Confirmation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// Bank Account Confirmation ///////////////////
	
	_obj.addRecipientView = Ti.UI.createScrollView(_obj.style.addRecipientView);
	_obj.tblDetails = Ti.UI.createTableView(_obj.style.tableView);
	
	
	for(var i=0; i<=13; i++)
	{
		var row = Ti.UI.createTableViewRow({
			top : 0,
			left : 0,
			right : 0,
			height : Ti.UI.SIZE,
			backgroundColor : '#FFF'
		});
		
		if(TiGlobals.osname !== 'android')
		{
			row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
		}
		
		var lblRecipientDetailsHead = Ti.UI.createLabel({
			top : 0,
			left : 0,
			right : 0,
			height:52,
			textAlign:'center',
			font:TiFonts.FontStyle('lblSwissNormal16'),
			color:TiFonts.FontStyle('redFont'),
			backgroundImage:'/images/bg_subtitle.jpg'
		});
		
		var lblRecipientDetails1 = Ti.UI.createLabel({
			height : 16,
			top : 10,
			left : 20,
			right : 20,
			textAlign : 'left',
			font : TiFonts.FontStyle('lblSwissNormal14'),
			color : TiFonts.FontStyle('greyFont')
		});

		var lblRecipientDetails2 = Ti.UI.createLabel({
			height : Ti.UI.SIZE,
			top : 30,
			bottom:10,
			left : 20,
			right : 20,
			textAlign : 'left',
			font : TiFonts.FontStyle('lblNormal14'),
			color : TiFonts.FontStyle('blackFont')
		});
		
		switch(i)
		{
			case 0:
				_obj.tblHeight = 52;
				row.height = 52;
				lblRecipientDetailsHead.text = 'Personal Details';
				
				row.add(lblRecipientDetailsHead);
			break;
			
			case 1:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'First Name';
				lblRecipientDetails2.text = params.firstName;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 2:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'Last Name';
				lblRecipientDetails2.text = params.lastName;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 3:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'Nick Name';
				lblRecipientDetails2.text = params.nickName;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 4:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'Date of Birth';
				lblRecipientDetails2.text = params.dob;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 5:
				_obj.tblHeight = _obj.tblHeight + 52;
				row.height = 52;
				lblRecipientDetailsHead.text = 'Contact Information';
				
				row.add(lblRecipientDetailsHead);
			break;
			
			case 6:
				_obj.tblHeight = _obj.tblHeight + 106;
				lblRecipientDetails2.height = 60;
				row.height = 106;
				lblRecipientDetails1.text = 'Address';
				lblRecipientDetails2.text = params.address;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 7:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'City';
				lblRecipientDetails2.text = params.city;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 8:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'Pincode';
				lblRecipientDetails2.text = params.pincode;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 9:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'State';
				lblRecipientDetails2.text = params.state;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 10:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'Country';
				lblRecipientDetails2.text = params.country;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 11:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'Email Address';
				lblRecipientDetails2.text = params.email;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 12:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'Mobile Number';
				lblRecipientDetails2.text = params.mobileNo;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
			case 13:
				_obj.tblHeight = _obj.tblHeight + 56;
				row.height = 56;
				lblRecipientDetails2.height = 16;
				lblRecipientDetails1.text = 'Phone Number';
				lblRecipientDetails2.text = params.resPhone;
				
				row.add(lblRecipientDetails1);
				row.add(lblRecipientDetails2);
			break;
			
		}
		
		_obj.tblDetails.appendRow(row);
	}
	
	_obj.tblDetails.height = _obj.tblHeight;
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	_obj.addRecipientView.add(_obj.tblDetails);
	_obj.addRecipientView.add(_obj.btnSubmit);
	_obj.mainView.add(_obj.addRecipientView);
	_obj.globalView.add(_obj.mainView);
	_obj.winEditRecipientConfirmation.add(_obj.globalView);
	_obj.winEditRecipientConfirmation.open();
	
	// Success Confirmation
	
	_obj.addBankConfirmationFinalView = Ti.UI.createScrollView(_obj.style.addBankConfirmationFinalView);
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.url = TiGlobals.staticPagesURL + 'bank_account_confirmation_mobile.php?email='+Ti.App.Properties.getString('sourceCountryInfoEmail')+'&phone='+Ti.App.Properties.getString('sourceCountryTollFree');
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.btnSendMoney = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSendMoney.title = 'SEND MONEY NOW';
	
	function successConfirmation()
	{
		_obj.mainView.remove(_obj.addRecipientView);
		
		_obj.addBankConfirmationFinalView.add(_obj.webView);
		_obj.addBankConfirmationFinalView.add(_obj.btnSendMoney);
		_obj.mainView.add(_obj.addBankConfirmationFinalView);
		
		_obj.btnSendMoney.addEventListener('click',function(e){
			if(Ti.App.Properties.getString('loginStatus') !== '0')
			{
				destroy_editrecipientconfirmation();
				require('/utils/RemoveViews').removeAllScrollableViews();
				require('/utils/PageController').pageController('transfer');
			}
			else
			{
				if(Ti.App.Properties.getString('mPIN') === '1')
				{
					require('/js/Passcode').Passcode();
				}
				else
				{
					require('/js/LoginModal').LoginModal();
				}
			}
		});
	}
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		activityIndicator.showIndicator();
			
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : post,
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				activityIndicator.hideIndicator();
			
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				Ti.App.fireEvent('populateRecipients');
				destroy_editrecipientconfirmation();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_editrecipientconfirmation();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_editrecipientconfirmation();
				alertDialog = null;
			}
		});
	});
	
	_obj.winEditRecipientConfirmation.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_editrecipientconfirmation();
				alertDialog = null;
			}
		});
	});
	
	function destroy_editrecipientconfirmation()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove recipient confirmation start ##############');
			
			_obj.winEditRecipientConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winEditRecipientConfirmation);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_editrecipientconfirmation',destroy_editrecipientconfirmation);
			require('/utils/Console').info('############## Remove recipient confirmation end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_editrecipientconfirmation', destroy_editrecipientconfirmation);
}; // EditRecipientConfirmationModal()