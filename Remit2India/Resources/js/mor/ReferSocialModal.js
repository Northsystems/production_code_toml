exports.ReferSocialModal = function()
{
	require('/lib/analytics').GATrackScreen('Refer through social networks');
	
	var _obj = {
		style : require('/styles/mor/ReferSocial').ReferSocial, // style
		winReferSocial : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblSectionHead : null,
		sectionHeaderBorder : null,
		lblNote : null,
		txtUserName : null,
		borderView1 : null,
		btnSubmit : null,
		lblLinkHeader : null,
		lblLink : null,
		lblSocialHeader : null,
		imgFB : null,
		imgTwitter : null,
		imgMainView : null,
		socialMainView : null,
		socialView : null,
		
		socialResult : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winReferSocial = Ti.UI.createWindow(_obj.style.winReferSocial);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winReferSocial);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Refer & Earn';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
	_obj.lblSectionHead.text = 'Refer through social networks';
	
	_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder);
	
	_obj.lblNote = Ti.UI.createLabel(_obj.style.lblNote);
	_obj.lblNote.text = 'Introduce your NRI friends to a service which offers them convenient & hassle-free money transfer solutions. Use this personalized link to refer your friends! You can even send this personalized link to your friends through email, SMS. or any other social media.';
	
	_obj.txtUserName = Ti.UI.createTextField(_obj.style.txt);
	_obj.txtUserName.hintText = 'Enter your ' + Ti.App.name + ' Login ID';
	_obj.borderView1 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.lblLinkHeader = Ti.UI.createLabel(_obj.style.lblLinkHeader);
	_obj.lblLinkHeader.text = 'Your personalized link';
	_obj.lblLinkHeader.visible = false;
	
	_obj.lblLink = Ti.UI.createLabel(_obj.style.lblLink);
	_obj.lblLink.text = '';
	_obj.lblLink.visible = false;
	
	_obj.socialView = Ti.UI.createView(_obj.style.socialView);
	_obj.socialView.visible = false;
	
	_obj.socialMainView = Ti.UI.createView(_obj.style.socialMainView);
	_obj.lblSocialHeader = Ti.UI.createLabel(_obj.style.lblSocialHeader);
	_obj.lblSocialHeader.text = 'Refer on Social Media';
	
	_obj.imgMainView = Ti.UI.createView(_obj.style.imgMainView);
	_obj.imgFB = Ti.UI.createImageView(_obj.style.imgView);
	_obj.imgFB.image = '/images/fb_mor.png';
	_obj.imgFB.sel = 'fb';
	_obj.imgTwitter = Ti.UI.createImageView(_obj.style.imgView);
	_obj.imgTwitter.image = '/images/tw_mor.png';
	_obj.imgTwitter.sel = 'tw';
	_obj.imgTwitter.left = 40;
	
	_obj.imgMainView.addEventListener('click',function(e){
		
		var title = _obj.socialResult.name;
		var description = _obj.socialResult.description;
		var link = _obj.lblLink.text;
		var appicon = _obj.socialResult.picture;
		var message = _obj.socialResult.twittertext;
		
		switch(e.source.sel)
		{
			case 'fb':
				
				///// POST to User Wall
				//Ti.API.info("IN FACEBOOK OPTION:------->");
				var facebook = require('facebook');
				facebook.initialize(1000);
				
				if (TiGlobals.osname === 'android') {
					//Ti.API.info("IN FACEBOOK OPTION11111:------->");
					_obj.winReferSocial.fbProxy = facebook.createActivityWorker({
						lifecycleContainer : _obj.winReferSocial
					});
				}
							
				facebook.addEventListener('shareCompleted', function (eShare) {
					//Ti.API.info("IN FACEBOOK OPTION2222:------->");
			        if (eShare.success) {
			            if(TiGlobals.osname === 'android')
						{
	                    	require('/utils/AlertDialog').toast('Message posted on Facebook Wall');
	                   	}
	                    else
	                    {
	                   		require('/utils/AlertDialog').iOSToast('Message posted on Facebook Wall');
	                    }
			        } else {
			        	//Ti.API.info("IN FACEBOOK OPTION33333:------->");
			            if(TiGlobals.osname === 'android')
						{
							//Ti.API.info("IN FACEBOOK OPTION4444:------->");
	                    	require('/utils/AlertDialog').toast('Failed to share');
	                   	}
	                    else
	                    {
	                    	//Ti.API.info("IN FACEBOOK OPTION55555:------->");
	                   		require('/utils/AlertDialog').iOSToast('Failed to share');
	                    }
			        }
			    });
					
			    if(facebook.getCanPresentShareDialog()) {
			        facebook.presentShareDialog({
			            link: link,
						name: title,
						picture: appicon,
						description: description,
						caption: title
			        });
			    } else {
			        facebook.presentWebShareDialog({
			            link: link,
						name: title,
						picture: appicon,
						description: description,
						caption: title
			        });
			    }
			break;
			
			case 'tw':
				require('/lib/toml_validations').twitterURL(message,link);
			break;
		}
	});
		
	_obj.lblLink.addEventListener('click',function(e){
		
		Ti.UI.Clipboard.clearText();
		Ti.UI.Clipboard.setText(_obj.lblLink.text);
		
		if(TiGlobals.osname === 'android')
		{
			require('/utils/AlertDialog').toast('Link copied to clipboard');
		}
		else
		{
			require('/utils/AlertDialog').iOSToast('Link copied to clipboard');
		}
	});
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		if(_obj.txtUserName.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your User Name',[L('btn_ok')]).show();
			_obj.txtUserName.value = '';
			_obj.txtUserName.focus();
    		return;		
		}
		else if((require('/lib/toml_validations').isEmail(_obj.txtUserName.value)) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid User Name',[L('btn_ok')]).show();
    		_obj.txtUserName.value = '';
    		_obj.txtUserName.focus();
			return;
		}
		else
		{
			
		}
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"MORREFERRAL",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+_obj.txtUserName.value.toUpperCase()+'",'+ 
				'"referType":"Refer Using Social Network"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			if(e.result.responseFlag === "S")
			{
				_obj.lblLinkHeader.visible = true;
				_obj.lblLink.visible = true;
				_obj.socialView.visible = true;
				_obj.lblLink.text = e.result.redirectionURL;
				
				activityIndicator.hideIndicator();
				
				var xhrSocial = require('/utils/XHR_BCM');
		
				xhrSocial.call({
					url : TiGlobals.appURLBCM,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"'+TiGlobals.bcmConfig+'",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"source":"'+TiGlobals.bcmSource+'",'+
						'"type":"socialInfo",'+
						'"platform":"'+TiGlobals.osname+'",'+
						'"corridor":"'+countryName[0]+'"'+
						'}',
					success : xhrSocialSuccess,
					error : xhrSocialError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
		
				function xhrSocialSuccess(e) {
					if(e.result.status === "S")
					{
						_obj.socialResult = e.result.response[0].socialInfo;
					}
					else
					{
						if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_refersocial();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
						}
					}
					xhrSocial = null;
				}
		
				function xhrSocialError(e) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhrSocial = null;
				}
			}
			else
			{
				activityIndicator.hideIndicator();
				
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_refersocial();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					_obj.lblLinkHeader.visible = false;
					_obj.lblLink.visible = false;
					_obj.socialView.visible = false;
					_obj.lblLink.text = '';
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblSectionHead);
	_obj.mainView.add(_obj.sectionHeaderBorder);
	_obj.mainView.add(_obj.lblNote);
	_obj.mainView.add(_obj.txtUserName);
	_obj.mainView.add(_obj.borderView1);
	_obj.mainView.add(_obj.btnSubmit);
	_obj.mainView.add(_obj.lblLinkHeader);
	_obj.mainView.add(_obj.lblLink);
	_obj.globalView.add(_obj.mainView);
	_obj.socialMainView.add(_obj.lblSocialHeader);
	_obj.imgMainView.add(_obj.imgFB);
	_obj.imgMainView.add(_obj.imgTwitter);
	_obj.socialMainView.add(_obj.imgMainView);
	_obj.socialView.add(_obj.socialMainView);
	_obj.globalView.add(_obj.socialView);
	_obj.winReferSocial.add(_obj.globalView);
	_obj.winReferSocial.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_refersocial();
	});
	
	_obj.winReferSocial.addEventListener('androidback',function(e){
		destroy_refersocial();
	});
	
	function destroy_refersocial()
	{
		try{
			
			require('/utils/Console').info('############## Remove refer social start ##############');
			
			_obj.winReferSocial.close();
			require('/utils/RemoveViews').removeViews(_obj.winReferSocial);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_refersocial',destroy_refersocial);
			require('/utils/Console').info('############## Remove refer social end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_refersocial', destroy_refersocial);
}; // ReferSocialModal()