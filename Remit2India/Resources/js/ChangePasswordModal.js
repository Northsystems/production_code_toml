exports.ChangePasswordModal = function()
{
	require('/lib/analytics').GATrackScreen('Change Password');
	
	var _obj = {
		style : require('/styles/ChangePassword').ChangePassword,
		winChangePassword : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		txtOldPassword : null,
		borderViewS11 : null,
		txtNewPassword : null,
		borderViewS12 : null,
		txtConfirmPassword : null,
		borderViewS13 : null,
		lblPassword : null,
		btnSave : null,
	};
	
	_obj.winChangePassword = Titanium.UI.createWindow(_obj.style.winChangePassword);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winChangePassword);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Edit Your Password';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.txtOldPassword = Ti.UI.createTextField(_obj.style.txtOldPassword);
	_obj.txtOldPassword.hintText = 'Current Password';
	_obj.txtOldPassword.maxLength = 20;
	_obj.borderViewS11 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtNewPassword = Ti.UI.createTextField(_obj.style.txtNewPassword);
	_obj.txtNewPassword.hintText = 'Password';
	_obj.txtNewPassword.maxLength = 20;
	_obj.borderViewS12 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtConfirmPassword = Ti.UI.createTextField(_obj.style.txtConfirmPassword);
	_obj.txtConfirmPassword.hintText = 'Re-type Password';
	_obj.txtConfirmPassword.maxLength = 20;
	_obj.borderViewS13 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.lblPassword = Ti.UI.createLabel(_obj.style.lblPassword);
	_obj.lblPassword.text = 'Must be 8-20 characters and contain letters and numbers';
	
	_obj.btnSave = Ti.UI.createButton(_obj.style.btnSave);
	_obj.btnSave.title = 'SAVE';
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.txtOldPassword);
	_obj.mainView.add(_obj.borderViewS11);
	_obj.mainView.add(_obj.txtNewPassword);
	_obj.mainView.add(_obj.borderViewS12);
	_obj.mainView.add(_obj.txtConfirmPassword);
	_obj.mainView.add(_obj.borderViewS13);
	_obj.mainView.add(_obj.lblPassword);
	_obj.mainView.add(_obj.btnSave);
	
	_obj.globalView.add(_obj.mainView);
	_obj.winChangePassword.add(_obj.globalView);
	_obj.winChangePassword.open();
	
	_obj.btnSave.addEventListener('click',function(e){
		
		if(_obj.txtOldPassword.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Old Password Field should not be left blank',[L('btn_ok')]).show();
    		_obj.txtOldPassword.value = '';
    		_obj.txtOldPassword.focus();
			return;
		}
		else if(_obj.txtNewPassword.value === '')
		{
			require('/utils/AlertDialog').showAlert('','New Password Field should not be left blank',[L('btn_ok')]).show();
    		_obj.txtNewPassword.value = '';
    		_obj.txtNewPassword.focus();
			return;
		}
		else if(_obj.txtOldPassword.value === _obj.txtNewPassword.value)
		{
			require('/utils/AlertDialog').showAlert('','New password should be different from old password',[L('btn_ok')]).show();
    		_obj.txtNewPassword.value = '';
    		_obj.txtNewPassword.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtNewPassword.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of new password',[L('btn_ok')]).show();
    		_obj.txtNewPassword.value = '';
    		_obj.txtNewPassword.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtNewPassword.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of new password',[L('btn_ok')]).show();
    		_obj.txtNewPassword.value = '';
    		_obj.txtNewPassword.focus();
			return;
		}
		else if(!((/\d/.test(_obj.txtNewPassword.value)) && (/[a-z]/i.test(_obj.txtNewPassword.value))))
		{
			require('/utils/AlertDialog').showAlert('','Password should be alpha-numeric',[L('btn_ok')]).show();
    		_obj.txtNewPassword.value = '';
    		_obj.txtNewPassword.focus();
			return;
		}
		else if(_obj.txtNewPassword.value.length < 8 || _obj.txtNewPassword.value.length > 20)
		{
			require('/utils/AlertDialog').showAlert('','Password should be of minimum 8 and maximum 20 characters',[L('btn_ok')]).show();
    		_obj.txtNewPassword.value = '';
    		_obj.txtNewPassword.focus();
			return;
		}
		else if(_obj.txtConfirmPassword.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Confirm password field should not be left blank',[L('btn_ok')]).show();
    		_obj.txtConfirmPassword.value = '';
    		_obj.txtConfirmPassword.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtConfirmPassword.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of confirm password',[L('btn_ok')]).show();
    		_obj.txtConfirmPassword.value = '';
    		_obj.txtConfirmPassword.focus();
			return;
		}
		else if(_obj.txtNewPassword.value !== _obj.txtConfirmPassword.value)
		{
			require('/utils/AlertDialog').showAlert('','Password mismatched please retype your password',[L('btn_ok')]).show();
    		_obj.txtConfirmPassword.value = '';
    		_obj.txtConfirmPassword.focus();
			return;
		}
		else
		{
			// do nothing
		}
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"UPDATEPASSWORD",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"dob":"",'+
				'"action":"CHANGEPWD",'+ 
				'"oldPassword":"'+require('/lib/Crypt/Base64').encode(_obj.txtOldPassword.value)+'",'+ 
				'"newPassword":"'+require('/lib/Crypt/Base64').encode(_obj.txtNewPassword.value)+'",'+
				'"confirmPassword":"'+require('/lib/Crypt/Base64').encode(_obj.txtConfirmPassword.value)+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
			
		function xhrSuccess(e) {
			
			activityIndicator.hideIndicator();
			
			if(e.result.responseFlag === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				
				destroy_changepassword();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_changepassword();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_changepassword();
	});
	
	function destroy_changepassword()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove ChangePassword start ##############');
			
			_obj.winChangePassword.close();
			require('/utils/RemoveViews').removeViews(_obj.winChangePassword);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_changepassword',destroy_changepassword);
			require('/utils/Console').info('############## Remove ChangePassword end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_changepassword', destroy_changepassword);
}; // ChangePasswordModal()