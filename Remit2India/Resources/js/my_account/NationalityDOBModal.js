exports.NationalityDOBModal = function()
{
	require('/lib/analytics').GATrackScreen('Account Summary');
	
	var _obj = {
		style : require('/styles/my_account/NationalityDOB').NationalityDOB, // style
		winNationalityDOB : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblName : null,
		lblNote : null,
		lblDetails : null,
		lblReceiverHeader : null,
		lblReceiverTxt : null,
		tblReceiver : null,
		btnSubmit : null,
		lblCitizenshipName : null,
		imgCitizenship : null,
		citizenshipView : null,
		
		receiverSel : [],
		receiverSelData : [],
		receiverSelDt : [],
		beneDataArray : [],
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winNationalityDOB = Ti.UI.createWindow(_obj.style.winNationalityDOB);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winNationalityDOB);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Account Summary';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.lblName = Ti.UI.createLabel(_obj.style.lblName);
	_obj.lblName.text = 'Dear ' + Ti.App.Properties.getString('firstName');
	
	_obj.lblNote = Ti.UI.createLabel(_obj.style.lblNote);
	_obj.lblNote.text = 'You are requested to provide us with the following additional details about yourself and your receiver\'s added.';
	
	_obj.lblDetails = Ti.UI.createLabel(_obj.style.lblDetails);
	_obj.lblDetails.text = 'Details about yourself';
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblName);
	_obj.mainView.add(_obj.lblNote);
	_obj.mainView.add(_obj.lblDetails);
	_obj.globalView.add(_obj.mainView);
	_obj.winNationalityDOB.add(_obj.globalView);
	_obj.winNationalityDOB.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_ok'), L('btn_cancel')],
			message:L('overview_proceed')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 1 || e.index === "1")
			{
				destroy_nationalitydob();
				alertDialog = null;
			}
		});
	});
	
	_obj.winNationalityDOB.addEventListener('androidback',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_ok'), L('btn_cancel')],
			message:L('overview_proceed')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 1 || e.index === "1")
			{
				destroy_nationalitydob();
				alertDialog = null;
			}
		});
	});
	
	function citizenship()
	{
		activityIndicator.showIndicator();
		var citizenshipOptions = [];
		
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+Ti.App.Properties.getString('destinationCountryCurName')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
				
				for(var i=0;i<splitArr.length;i++)
				{
					citizenshipOptions.push(splitArr[i]);
				}
				
				var optionDialogCitizenship = require('/utils/OptionDialog').showOptionDialog('Citizenship',citizenshipOptions,-1);
				optionDialogCitizenship.show();
				
				optionDialogCitizenship.addEventListener('click',function(evt){
					if(optionDialogCitizenship.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblCitizenshipName.text = optionDialogCitizenship.options[evt.index];
						optionDialogCitizenship = null;
					}
					else
					{
						optionDialogCitizenship = null;
						_obj.lblCitizenshipName.text = 'Citizenship*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_nationalitydob();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	var imgCheck = [];
	var dobView = [];
	var lblDOB = [];
	var imgDOB = [];
	
	function receiverList()
	{
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"BENEFICIARYLISTING",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"rangeFrom":"1",'+
				'"rangeTo":"100"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			
			activityIndicator.hideIndicator();
			
			if(e.result.responseFlag === "S")
			{
				
				// Receiver Section
				
				_obj.lblReceiverHeader = Ti.UI.createLabel(_obj.style.lblAddressHeader);
				_obj.lblReceiverHeader.text = 'Details about your receiver';
				
				_obj.lblReceiverTxt = Ti.UI.createLabel(_obj.style.lblAddress);
				_obj.lblReceiverTxt.text = 'Provide us your receiver\'s Date of Birth';
				
				_obj.tblReceiver = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblReceiver.height = 0;
				
				_obj.tblReceiver.addEventListener('click',function(evt){
					if(evt.row.rw !== -1)
					{
						if(evt.source.tap === 'top')
						{
							if(imgCheck[evt.row.rw].image === '/images/checkbox_sel.png')
							{
								imgCheck[evt.row.rw].image = '/images/checkbox_unsel.png';
								
								//Remove element from array
								var index = _obj.receiverSel.indexOf(evt.row.rw);
								_obj.receiverSel.splice(index, 1);
								
								var index1 = _obj.receiverSelData.indexOf(evt.row.recData);
								_obj.receiverSelData.splice(index1, 1);
								
								require('/utils/Console').info(_obj.receiverSel.join());
								require('/utils/Console').info(_obj.receiverSelData.join());
							}
							else
							{
								imgCheck[evt.row.rw].image = '/images/checkbox_sel.png';
								_obj.receiverSel.push(evt.row.rw);
								_obj.receiverSelData.push(evt.row.recData);
								
								require('/utils/Console').info(_obj.receiverSel.join());
								require('/utils/Console').info(_obj.receiverSelData.join());
							}
						}
					}
				});
				
				_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
				_obj.btnSubmit.title = 'SUBMIT';
				
				_obj.btnSubmit.addEventListener('click',function(evt){
					require('/utils/Console').info(_obj.receiverSel.join());
					require('/utils/Console').info(_obj.receiverSelData.join());
					
					_obj.receiverSelDt = [];
					_obj.beneDataArray = [[]];
					
					var beneDataArray = {
					    beneficiary: []
					};
					
					for(var i=0; i<_obj.receiverSel.length;i++)
					{
						_obj.receiverSelDt.push(lblDOB[_obj.receiverSel[i]].text);
						
						var receiverDataSplit = _obj.receiverSelData[i].split('~');
						var receiverDtSplit = _obj.receiverSelDt[i].split('-');
						
						beneDataArray.beneficiary.push({ 
					        "beneNickname" : receiverDataSplit[0],
					        "beneFirstName" : receiverDataSplit[1],
					        "beneLastName" : receiverDataSplit[2],
					        "birthDay"	: receiverDtSplit[0],
					        "birthMonth" : receiverDtSplit[1],
					        "birthYear" : receiverDtSplit[2]
					    });
					}
					
					require('/utils/Console').info(JSON.stringify(beneDataArray.beneficiary));
					
					if(_obj.lblCitizenshipName.text === 'Citizenship*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your Citizenship',[L('btn_ok')]).show();
			    		return;
					}
					else if(_obj.receiverSel.length === 0)
					{
						require('/utils/AlertDialog').showAlert('','Please select atleast one beneficiary\'s date of birth to process',[L('btn_ok')]).show();
			    		return;
					}
					else if(_obj.receiverSel.length > 0)
					{
						if(_obj.receiverSelDt.indexOf('Date of Birth*')>-1)
						{
							require('/utils/AlertDialog').showAlert('','Please select beneficiary\'s date of birth for the checked item',[L('btn_ok')]).show();
			    			return;
						}
					}
					else
					{
						
					}
					
					activityIndicator.showIndicator();
					var xhr = require('/utils/XHR');
			
					xhr.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"UPDATENATIONALITYANDBENEDOB",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
							'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
							'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
							'"senderNationality":"'+_obj.lblCitizenshipName.text+'",'+
							'"beneDataArray":'+JSON.stringify(beneDataArray.beneficiary)+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
			
					function xhrSuccess(e) {
						activityIndicator.hideIndicator();
						if(e.result.responseFlag === "S")
						{
							if(TiGlobals.osname === 'android')
							{
								require('/utils/AlertDialog').toast(e.result.message);
							}
							else
							{
								require('/utils/AlertDialog').iOSToast(e.result.message);
							}
							
							setTimeout(function(){
								destroy_nationalitydob();
							},500);
							
							require('/js/my_account/OverviewModal').OverviewModal();
						}
						else
						{
							if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
							{
								require('/lib/session').session();
								destroy_nationalitydob();
							}
							else
							{
								require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
							}
						}
						xhr = null;
					}
			
					function xhrError(e) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr = null;
					}
					
				});
				
				_obj.mainView.add(_obj.lblReceiverHeader);
				_obj.mainView.add(_obj.lblReceiverTxt);
				_obj.mainView.add(_obj.tblReceiver);
				_obj.mainView.add(_obj.btnSubmit);
				
				if(e.result.RecieverData.length === 0)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 40,
						backgroundColor : 'transparent',
						className : 'receiver_details',
						rw:-1
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMsg = Ti.UI.createLabel({
						text : 'We have not found any receiver',
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMsg);
					_obj.tblReceiver.appendRow(row);
				}
				else
				{
					for(var i=0; i<e.result.RecieverData.length; i++)
					{
						_obj.tblReceiver.height = parseInt(_obj.tblReceiver.height + 120);
						
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 120,
							backgroundColor : '#FFF',
							className : 'receiver_details',
							recData : e.result.RecieverData[i].nickName +'~' + e.result.RecieverData[i].firstName +'~' + e.result.RecieverData[i].lastName,
							rw:i
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var detailView = Ti.UI.createView({
							top : 0,
							left : 0,
							right : 0,
							height : 60,
							backgroundColor : '#6f6f6f',
							tap:'top'
						});
						
						var detailView1 = Ti.UI.createView({
							left:0,
							width:Ti.UI.SIZE,
							height:Ti.UI.SIZE,
							right : 60,
							layout:'vertical',
							tap:'top'
						});
						
						var lblReceiverNick = Ti.UI.createLabel({
							text:e.result.RecieverData[i].nickName,
							top : 0,
							left : 20,
							height : Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('whiteFont'),
							tap:'top'
						});
						
						var lblReceiverName = Ti.UI.createLabel({
							text:e.result.RecieverData[i].firstName +' ' + e.result.RecieverData[i].lastName,
							top : 2,
							left : 20,
							height : Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							tap:'top'
						});
						
						imgCheck[i] = Ti.UI.createImageView({
							right:20,
							image:'/images/checkbox_unsel.png',
						    width:15,
						    height:15,
							tap:'top',
							backgroundColor:'#FFF'
						});
						
						var detailBottomView = Ti.UI.createView({
							top : 60,
							left : 0,
							right : 0,
							height : 60,
							backgroundColor : '#fff',
							tap:'bottom'
						});
						
						dobView[i] = Ti.UI.createView(_obj.style.dobView);
						dobView[i].tap = 'bottom';
						lblDOB[i] = Ti.UI.createLabel(_obj.style.lblDOB);
						lblDOB[i].tap = 'bottom';
						lblDOB[i].text = 'Date of Birth*';
						
						if(e.result.RecieverData[i].dob !== '01-JAN-1900')
						{
							lblDOB[i].text = e.result.RecieverData[i].dob;	
						}
						
						imgDOB[i] = Ti.UI.createImageView(_obj.style.imgDOB);
						imgDOB[i].tap = 'bottom';
						var borderViewR = Ti.UI.createView(_obj.style.borderView);
						borderViewR.top = 48;
						
						dobView[i].addEventListener('click',function(e){
							require('/utils/DatePicker').DatePicker(lblDOB[e.row.rw],'dob18');
						});
						
						detailView1.add(lblReceiverNick);
						detailView1.add(lblReceiverName);
						detailView.add(detailView1);
						detailView.add(imgCheck[i]);
						
						dobView[i].add(lblDOB[i]);
						dobView[i].add(imgDOB[i]);
						detailBottomView.add(dobView[i]);
						detailBottomView.add(borderViewR);
						
						row.add(detailView);
						row.add(detailBottomView);
						_obj.tblReceiver.appendRow(row);
					}
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_nationalitydob();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function getNationality()
	{
		activityIndicator.showIndicator();
					
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETUSERDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"operatingMode":INTL",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
			
		function xhrSuccess(e) {
			
			if(e.result.responseFlag === "S")
			{
				if(e.result.nationality === '')
				{
					_obj.citizenshipView = Ti.UI.createView(_obj.style.ddView);	
					_obj.lblCitizenshipName = Ti.UI.createLabel(_obj.style.lblDD);
					_obj.lblCitizenshipName.text = 'Citizenship*';
					_obj.imgCitizenship = Ti.UI.createImageView(_obj.style.imgDD);
					_obj.borderView = Ti.UI.createView(_obj.style.borderView);	
					
					_obj.citizenshipView.add(_obj.lblCitizenshipName);
					_obj.citizenshipView.add(_obj.imgCitizenship);
					_obj.mainView.add(_obj.citizenshipView);
					_obj.mainView.add(_obj.borderView);
					
					_obj.citizenshipView.addEventListener('click',function(){
						citizenship();
					});
				}
				else
				{
					_obj.citizenshipView = Ti.UI.createView(_obj.style.citizenshipView);	
					_obj.lblCitizenshipTxt = Ti.UI.createLabel(_obj.style.lblCitizenshipTxt);
					_obj.lblCitizenshipTxt.text = 'Your Citizenship is of : ';
					_obj.lblCitizenshipName = Ti.UI.createLabel(_obj.style.lblCitizenshipName);
					_obj.lblCitizenshipName.text = e.result.nationality;
					
					_obj.citizenshipView.add(_obj.lblCitizenshipTxt);
					_obj.citizenshipView.add(_obj.lblCitizenshipName);
					_obj.mainView.add(_obj.citizenshipView);
					
				}
				
				receiverList();
				
			}
			else
			{
				activityIndicator.hideIndicator();
				
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_nationalitydob();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
				
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	getNationality();
	
	function destroy_nationalitydob()
	{
		try{
			
			require('/utils/Console').info('############## Remove NationalityDOB start ##############');
			
			_obj.winNationalityDOB.close();
			require('/utils/RemoveViews').removeViews(_obj.winNationalityDOB);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_nationalitydob',destroy_nationalitydob);
			require('/utils/Console').info('############## Remove NationalityDOB end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_nationalitydob', destroy_nationalitydob);
}; // NationalityDOBModal()