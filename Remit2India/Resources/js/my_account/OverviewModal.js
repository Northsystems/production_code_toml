exports.OverviewModal = function()
{
	require('/lib/analytics').GATrackScreen('Overview');
	
	var _obj = {
		style : require('/styles/my_account/Overview').Overview,
		winOverview : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		tabView : null,
		tabSelView : null,
		tabStep1 : null,
		tabStep2 : null,
		tabStep3 : null,
		imgStep1 : null,
		imgStep2 : null,
		imgStep3 : null,
		tabSelIconView : null,
		imgTabSel : null,
		scrollableView : null,
		
		step1 : null,
		transactionType : null,
		lblTransaction : null,
		lblTransaction : null,
		imgTransaction : null,
		tblTransaction : null,
		
		step2 : null,
		tblMessage : null,
		
		step3 : null,
		tblAlert : null,
		
		step1Check : 0,
		step2Check : 0,
		step3Check : 0,
		selTxn : '',
		
		start : 0,
		last : 0,
		page : 1,
		limit : 10,
		totalPages : 1,
		totalRecords : 0,
		lastDistance : 0,
		updating : false,
		
		msgCount : 0,
		alertCount : 0,
		f : null,
		imgUpload : null,
		image : null,
		data_to_send : null,
	};
	
	_obj.tab = 'txn';
	
	var countryName = Ti.App.Properties.getString('destinationCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	//alert(countryName[0]);
	
	_obj.winOverview = Titanium.UI.createWindow(_obj.style.winOverview);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winOverview);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Overview';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.tabStep1 = Ti.UI.createView(_obj.style.tabStep);
	_obj.tabStep1.sel = 'txn';
	_obj.imgStep1 = Ti.UI.createImageView(_obj.style.imgStep);
	_obj.imgStep1.sel = 'txn';
	_obj.imgStep1.image = '/images/track_sel.png';
	_obj.tabStep2 = Ti.UI.createView(_obj.style.tabStep);
	_obj.tabStep2.width = '34%';
	_obj.tabStep2.sel = 'msg';
	_obj.tabStep2.left = 1;
	_obj.imgStep2 = Ti.UI.createImageView(_obj.style.imgStep);
	_obj.imgStep2.sel = 'msg';
	_obj.imgStep2.image = '/images/message_unsel.png';
	_obj.tabStep3 = Ti.UI.createView(_obj.style.tabStep);
	_obj.tabStep3.sel = 'alert';
	_obj.tabStep3.left = 1;
	_obj.imgStep3 = Ti.UI.createImageView(_obj.style.imgStep);
	_obj.imgStep3.sel = 'alert';
	_obj.imgStep3.image = '/images/alerts_unsel.png';
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
	_obj.tabStep1.backgroundColor = '#6F6F6F';
	_obj.tabStep2.backgroundColor = '#E9E9E9';
	_obj.tabStep3.backgroundColor = '#E9E9E9';
	
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.tabStep1.add(_obj.imgStep1);
	_obj.tabView.add(_obj.tabStep1);
	_obj.tabStep2.add(_obj.imgStep2);
	_obj.tabView.add(_obj.tabStep2);
	_obj.tabStep3.add(_obj.imgStep3);
	_obj.tabView.add(_obj.tabStep3);
	_obj.mainView.add(_obj.tabView);
	_obj.mainView.add(_obj.tabSelView);
	_obj.mainView.add(_obj.tabSelIconView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	
	/////////////////// Step 1 ///////////////////
	
	_obj.step1 = Ti.UI.createView(_obj.style.step1);
	_obj.tblTransaction = Ti.UI.createTableView(_obj.style.tableView);
	
	_obj.tblTransaction.addEventListener('scroll',function(e){
		
		require('/utils/Console').info(_obj.page +' < '+ _obj.totalPages);
		if(_obj.page < _obj.totalPages)
		{
			if (TiGlobals.osname === 'android')
			{
		        if((e.firstVisibleItem + e.visibleItemCount) === (_obj.page * _obj.limit))
				{
					_obj.page = _obj.page + 1;
		            populateTxnStatusPaginate(_obj.selTxn);
			    }
		    }
		    else
		    {
		        var offset = e.contentOffset.y;
		        var height = e.size.height;
		        var total = offset + height;
		        var theEnd = e.contentSize.height;
		        var distance = theEnd - total;
		         
		        // going down is the only time we dynamically load,
		        // going up we can safely ignore -- note here that
		        // the values will be negative so we do the opposite
		        if (distance < _obj.lastDistance)
		        {
		            // adjust the % of rows scrolled before we decide to start fetching
		            var nearEnd = theEnd;
		 
		            if (!_obj.updating && (total >= nearEnd))
		            {
		               _obj.page = _obj.page + 1;
		               populateTxnStatusPaginate(_obj.selTxn);
		            }
		        }
		        _obj.lastDistance = distance;
		    }    
		}
	});	
	
	_obj.tblTransaction.addEventListener('click', function(e) {
		try{
			if(e.row.rw === 1)
			{
				if(e.source.sel === 'buy')
				{
					activityIndicator.showIndicator();
					
					var xhr = require('/utils/XHR');
					xhr.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"GETTRANSACTIONDETAILS",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"rtrn":"'+e.row.rtrn+'",'+
							'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
							'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
							'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
						
					function xhrSuccess(evt) {
						try{
						activityIndicator.hideIndicator();
						if(evt.result.responseFlag === "S")
						{
							if(evt.result.isVoucherBuy === 'No')
							{
								require('/utils/AlertDialog').showAlert('','Voucher is not available for purchase',[L('btn_ok')]).show();
							}
							else
							{
								require('/js/my_account/VoucherBuyModal').VoucherBuyModal(evt.result);
							}
						}
						else
						{
							if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
							{
								require('/lib/session').session();
								destroy_overview();
							}
							else
							{
								require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
							}
							
						}
						xhr = null;
						}catch(e){}
					}
			
					function xhrError(evt) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr = null;
					}
				}
				
				if(e.source.sel === 'view')
				{
					require('/js/my_account/TxnTrackerDetailsModal').TxnTrackerDetailsModal(e.row.rtrn);
				}
				
				if(e.source.sel === 'cancel')
				{
					var alertDialog = require('/utils/AlertDialog').showAlert('', 'Once you click on OK, the transaction will be deleted.', [L('btn_ok'), L('btn_cancel')]);
					alertDialog.show();
			
					alertDialog.addEventListener('click', function(evt) {
						alertDialog.hide();
						if (evt.index === 0 || evt.index === "0") {
							activityIndicator.showIndicator();
				
							var xhr = require('/utils/XHR');
							
							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"CANCELTRANSACTION",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
									'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
									'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
									'"rtrn":"'+e.row.rtrn+'"'+
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
							
							function xhrSuccess(e) {
								require('/utils/Console').info('Result ======== ' + e.result);
								activityIndicator.hideIndicator();
								
								if(e.result.responseFlag === "S")
								{
									if(TiGlobals.osname === 'android')
									{
										require('/utils/AlertDialog').toast(e.result.message);
									}
									else
									{
										require('/utils/AlertDialog').iOSToast(e.result.message);
									}
									
									trackTxn();
								}
								else
								{
									if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
									{
										require('/lib/session').session();
										destroy_overview();
									}
									else
									{
										require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
									}
								}
								
								xhr = null;
							}
					
							function xhrError(e) {
								activityIndicator.hideIndicator();
								require('/utils/Network').Network();
								xhr = null;
							}
						}
					});
				}
				
				if(e.source.sel === 'reschedule')
				{
					require('/js/my_account/KYCRescheduleModal').KYCRescheduleModal(e.row.rtrn);
				}
			}
		}catch(e){}
	});
	
	function trackTxn()
	{
		_obj.transactionType = Ti.UI.createView(_obj.style.ddView);
		_obj.lblTransaction = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblTransaction.text = 'All Transactions';
		_obj.imgTransaction = Ti.UI.createImageView(_obj.style.imgDD);
		
		populateTxnStatus('');
		
		_obj.transactionType.addEventListener('click',function(e){
			var optionDialogTxn = require('/utils/OptionDialog').showOptionDialog('Transactions',['All Transactions','Pending Transactions','Completed Transactions','Scheduled Transactions'],-1);
			optionDialogTxn.show();
			
			optionDialogTxn.addEventListener('click',function(evt){
				if(optionDialogTxn.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblTransaction.text = optionDialogTxn.options[evt.index];
					
					switch (evt.index)
					{
						case 0:
							populateTxnStatus('');
							_obj.selTxn = '';
						break;
						
						case 1:
							populateTxnStatus('Pending');
							_obj.selTxn = 'Pending';
						break;
						
						case 2:
							populateTxnStatus('Completed');
							_obj.selTxn = 'Completed';
						break;
						
						case 3:
							populateTxnStatus('Scheduled');
							_obj.selTxn = 'Scheduled';
						break;
					}
					
					optionDialogTxn = null;
				}
				else
				{
					optionDialogTxn = null;
				}
			});
		});
		
		_obj.transactionType.add(_obj.lblTransaction);
		_obj.transactionType.add(_obj.imgTransaction);
		_obj.step1.add(_obj.transactionType);
		_obj.step1.add(_obj.tblTransaction);
	}
	
	function populateTxnStatus(status)
	{
		_obj.start = 1;
		_obj.last = _obj.limit;
		
		_obj.tblTransaction.setData([]);
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"STATUSTRACKER",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"transactionFrom":"'+_obj.start+'",'+
				'"transactionTo":"'+_obj.last+'",'+
				'"transactionStatus":"'+status+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(status === 'Completed')
			{
				_obj.totalRecords = parseInt(e.result.completedCount);
			}
			else if(status === 'Pending')
			{
				_obj.totalRecords = parseInt(e.result.pendingCount);
			}
			else if(status === 'Scheduled')
			{
				_obj.totalRecords = parseInt(e.result.scheduledCount);
			}
			else
			{
				_obj.totalRecords = parseInt(e.result.totalCount);	
			}
			
			_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
			
			if(e.result.responseFlag === "S")
			{
				if(_obj.totalRecords === 0)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'track_txn',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : status === ''? 'No Transactions found' :'No ' + status + ' Transactions found',
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblTransaction.appendRow(row);
				}
				else
				{
					if(e.result.txnData.length === 1)
					{
						var reschedule = 1;
					}
					else
					{
						var reschedule = 0;
					}
					
					for(var i=0; i<e.result.txnData.length; i++)
					{
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 106,
							backgroundColor : 'transparent',
							className : 'track_txn',
							rtrn : e.result.txnData[i].rtrn,
							rw : 1
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var topView = Ti.UI.createView({
							top : 0,
							left : 0,
							right : 0,
							height : 50,
							backgroundColor : '#6f6f6f',
							layout:'horizontal',
							horizontalWrap:false
						});
						
						var detailTxnView = Ti.UI.createView({
							top : 0,
							left : 0,
							width : '50%',
							height : 50
						});
						
						var txnView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblDate = Ti.UI.createLabel({
							text : 'Date ' + e.result.txnData[i].instructionDate,
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblRTRN = Ti.UI.createLabel({
							text : 'RTRN ' + e.result.txnData[i].rtrn,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						var borderView = Ti.UI.createView({
							height : 50,
							top : 0,
							left : 0,
							width:1,
							backgroundColor:'#898989'
						});
						
						var detailSendView = Ti.UI.createView({
							left : 0,
							right : 0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblSendTxt = Ti.UI.createLabel({
							text:'Send Amount', 
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblAmount = Ti.UI.createLabel({
							text:e.result.txnData[i].amount, 
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						var bottomView = Ti.UI.createView({
							top : 50,
							left : 0,
							right : 0,
							height : 56,
							backgroundColor : '#fff',
							layout:'horizontal',
							horizontalWrap:false
						});
						
						var detailBenView = Ti.UI.createView({
							top : 0,
							left : 0,
							width : '50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var beneficiaryView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblBeneficiaryName = Ti.UI.createLabel({
							text : e.result.txnData[i].benefeciary,
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						var lblStatus = Ti.UI.createLabel({
							text : e.result.txnData[i].status,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var statusView = Ti.UI.createView({
							top : 0,
							left : 20,
							width:'50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var statusMainView = Ti.UI.createView({
							height : 16,
							top : 10,
							left : 0,
							width:Ti.UI.SIZE,
							layout:'horizontal',
							horizontalWrap:true
						});
						
						var lblAccountStatus = Ti.UI.createLabel({
							text : 'Action: ',
							height : 16,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont'),
							sel:e.result.txnData[i].accountStatus
						});
						
						var lblBuyVoucher = Ti.UI.createLabel({
							text : 'Buy Voucher',
							height : 16,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							sel:'buy'
						});
						
						var actionView = Ti.UI.createView({
							height : 16,
							top : 30,
							left : 0,
							width:Ti.UI.SIZE,
							layout:'horizontal',
							horizontalWrap:true
						});
						
						var lblView = Ti.UI.createLabel({
							text : 'View ',
							height : 20,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							sel:'view'
							
						});
						
						if(e.result.txnData[i].txnCancelApplicable === 'Y')
						{
							var lblCancel = Ti.UI.createLabel({
								text : '| Cancel',
								height : 20,
								top : 0,
								left : 0,
								width:Ti.UI.SIZE,
								textAlign: 'left',
								font : TiFonts.FontStyle('lblNormal12'),
								color : TiFonts.FontStyle('greyFont'),
								sel:'cancel'
							});
						}
						
						if(reschedule === 1)
						{
							var lblReSchedule = Ti.UI.createLabel({
								text : '| Call Reschedule',
								height : 20,
								top : 0,
								left : 0,
								width:Ti.UI.SIZE,
								textAlign: 'left',
								font : TiFonts.FontStyle('lblNormal12'),
								color : TiFonts.FontStyle('greyFont'),
								sel:'reschedule'
							});	
							
							lblAccountStatus.top = 5;	
							actionView.top = 25;
							actionView.height = Ti.UI.SIZE;
						}
						
						txnView.add(lblDate);
						txnView.add(lblRTRN);
						detailTxnView.add(txnView);
						topView.add(detailTxnView);
						topView.add(borderView);
						detailSendView.add(lblSendTxt);
						detailSendView.add(lblAmount);
						topView.add(detailSendView);
						beneficiaryView.add(lblBeneficiaryName);
						beneficiaryView.add(lblStatus);
						detailBenView.add(beneficiaryView);
						bottomView.add(detailBenView);
						statusMainView.add(lblAccountStatus);
						statusMainView.add(lblBuyVoucher);
						statusView.add(statusMainView);
						actionView.add(lblView);
						if(e.result.txnData[i].txnCancelApplicable === 'Y')
						{
							actionView.add(lblCancel);
						}	
						if(reschedule === 1)
						{
							actionView.add(lblReSchedule);
						}
						statusView.add(actionView);
						bottomView.add(statusView);
						row.add(topView);
						row.add(bottomView);
						_obj.tblTransaction.appendRow(row);
					}
				}
				activityIndicator.hideIndicator();
			}
			else
			{
				activityIndicator.hideIndicator();
				
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_overview();
				}
				else
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'track_txn',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblTransaction.appendRow(row);
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function populateTxnStatusPaginate(status)
	{
		_obj.updating = true;
		
		_obj.start = (((_obj.page * _obj.limit) + 1) - _obj.limit); 
		_obj.last = (_obj.page * _obj.limit);
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"STATUSTRACKER",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"transactionFrom":"'+_obj.start+'",'+
				'"transactionTo":"'+_obj.last+'",'+
				'"transactionStatus":"'+status+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(status === 'Completed')
			{
				_obj.totalRecords = parseInt(e.result.completedCount);
			}
			else if(status === 'Pending')
			{
				_obj.totalRecords = parseInt(e.result.pendingCount);
			}
			else if(status === 'Scheduled')
			{
				_obj.totalRecords = parseInt(e.result.scheduledCount);
			}
			else
			{
				//_obj.totalRecords = parseInt(e.result.totalCount);	
			}
			
			_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
			
			if(e.result.responseFlag === "S")
			{
				for(var i=0; i<e.result.txnData.length; i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 106,
						backgroundColor : 'transparent',
						className : 'track_txn',
						rtrn : e.result.txnData[i].rtrn,
						rw : 1
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var topView = Ti.UI.createView({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : '#6f6f6f',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailTxnView = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '50%',
						height : 50
					});
					
					var txnView = Ti.UI.createView({
						left : 0,
						right:0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblDate = Ti.UI.createLabel({
						text : 'Date ' + e.result.txnData[i].instructionDate,
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblRTRN = Ti.UI.createLabel({
						text : 'RTRN ' + e.result.txnData[i].rtrn,
						top : 2,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var borderView = Ti.UI.createView({
						height : 50,
						top : 0,
						left : 0,
						width:1,
						backgroundColor:'#898989'
					});
					
					var detailSendView = Ti.UI.createView({
						left : 0,
						right : 0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblSendTxt = Ti.UI.createLabel({
						text:'Send Amount', 
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblAmount = Ti.UI.createLabel({
						text:e.result.txnData[i].amount, 
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var bottomView = Ti.UI.createView({
						top : 50,
						left : 0,
						right : 0,
						height : 56,
						backgroundColor : '#fff',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailBenView = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var beneficiaryView = Ti.UI.createView({
						left : 0,
						right:0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblBeneficiaryName = Ti.UI.createLabel({
						text : e.result.txnData[i].benefeciary,
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var lblStatus = Ti.UI.createLabel({
						text : e.result.txnData[i].status,
						top : 2,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var statusView = Ti.UI.createView({
						top : 0,
						left : 20,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var statusMainView = Ti.UI.createView({
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						layout:'horizontal',
						horizontalWrap:true
					});
					
					var lblAccountStatus = Ti.UI.createLabel({
						text : 'Action: ',
						height : 16,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont'),
						sel:e.result.txnData[i].accountStatus
					});
					
					var lblBuyVoucher = Ti.UI.createLabel({
						text : 'Buy Voucher',
						height : 16,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						sel:'buy'
					});
					
					var actionView = Ti.UI.createView({
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						layout:'horizontal',
						horizontalWrap:true
					});
					
					var lblView = Ti.UI.createLabel({
						text : 'View ',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						sel:'view'
						
					});
					
					if(e.result.txnData[i].txnCancelApplicable === 'Y')
					{
						var lblCancel = Ti.UI.createLabel({
							text : '| Cancel',
							height : 20,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							sel:'cancel'
						});
					}
					
					txnView.add(lblDate);
					txnView.add(lblRTRN);
					detailTxnView.add(txnView);
					topView.add(detailTxnView);
					topView.add(borderView);
					detailSendView.add(lblSendTxt);
					detailSendView.add(lblAmount);
					topView.add(detailSendView);
					beneficiaryView.add(lblBeneficiaryName);
					beneficiaryView.add(lblStatus);
					detailBenView.add(beneficiaryView);
					bottomView.add(detailBenView);
					statusMainView.add(lblAccountStatus);
					statusMainView.add(lblBuyVoucher);
					statusView.add(statusMainView);
					actionView.add(lblView);
					if(e.result.txnData[i].txnCancelApplicable === 'Y')
					{
						actionView.add(lblCancel);
					}	
					statusView.add(actionView);
					bottomView.add(statusView);
					row.add(topView);
					row.add(bottomView);
					_obj.tblTransaction.appendRow(row);
					_obj.updating = false;
				}
				activityIndicator.hideIndicator();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_overview();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	trackTxn();
	_obj.step1Check = 1;
	
	/////////////////// Step 2 ///////////////////
	
	_obj.step2 = Ti.UI.createScrollView(_obj.style.step2);
	
	_obj.tblMessage = Ti.UI.createTableView(_obj.style.tableView);
	_obj.step2.add(_obj.tblMessage);
	
	/////////////////// Step 3 ///////////////////
	
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};
	
	function selectImage(e)
	{
		_obj.f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,e.row.docType+'.jpg');
		_obj.imgUpload = 0;
		
		var optionDialogPhoto = require('/utils/OptionDialog').showOptionDialog('',[L('takePicture'), L('selectPicture')],-1);
		optionDialogPhoto.show();
		
		optionDialogPhoto.addEventListener('click',function(evt){
			if(optionDialogPhoto.options[evt.index] !== L('btn_cancel'))
			{
				if(evt.index === 0)
				{
					// Take Picture
					
					Ti.Media.showCamera({
				        showControls:true,
				        mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
				        autohide:true,
				        allowEditing:false,
				        autorotate:true,
				        success:function(event) {
				        	
							_obj.image = event.media;
				        	_obj.f.write(_obj.image);
				        	
				        	if(_obj.f.size < 5000000)
				        	{
					        	_obj.imgView[e.row.sel].image = _obj.f.read();
					        	_obj.lblDocSize[e.row.sel].text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
					        	_obj.imgView[e.row.sel].show();
					        	_obj.btnUpload[e.row.sel].show();
					        }
					        else
					        {
					        	require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
					        }	
				        }
					});
				}
				
				if(evt.index === 1)
				{
					// Obtain an image from the gallery
					
			        Titanium.Media.openPhotoGallery({
			            success:function(event)
			            { 
			            	if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO)
			                {
			                	_obj.image = event.media;				            
							    _obj.f.write(_obj.image);
							    
							    if(_obj.f.size < 5000000)
					        	{
						        	_obj.imgView[e.row.sel].image = _obj.f.read();
						        	_obj.lblDocSize[e.row.sel].text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
						        	_obj.imgView[e.row.sel].show();
						        	_obj.btnUpload[e.row.sel].show();
						        }
						        else
						        {
						        	require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
						        }
			                }      
			            },
			            cancel:function()
			            {
			            }
			        });
				}
				
				optionDialogPhoto = null;
			}
		});	
	}
	
	_obj.step3 = Ti.UI.createScrollView(_obj.style.step3);
	
	_obj.tblAlert = Ti.UI.createTableView(_obj.style.tableView);
	_obj.step3.add(_obj.tblAlert);
	
	_obj.tblAlert.addEventListener('click',function(e){
		if(e.row.rw !== 0)
		{
			if(e.row.rw === 'bank')
			{
				activityIndicator.showIndicator();
				var xhr = require('/utils/XHR');
				
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETSENDERBANKACCLISTINFO",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
						'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
						'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
						'"origCountryCode":"'+origSplit[0]+'",'+ 
						'"origCurrencyCode":"'+origSplit[1]+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : TiGlobals.timer
				});
				
				function xhrSuccess(evt) {
					require('/utils/Console').info('Result ======== ' + evt.result);
					activityIndicator.hideIndicator();
					
					if(evt.result.responseFlag === "S")
					{
						for(var i=0; i<evt.result.bankData.length;i++)
						{
							if(evt.result.bankData[i].id === e.row.achId)	
							{
								var params = {
									accName:evt.result.bankData[i].accountHolder,
									accType:evt.result.bankData[i].accountType,
									bankName:evt.result.bankData[i].bankName,
									accNo:evt.result.bankData[i].accountNumber,
									routNo:evt.result.bankData[i].routingNumber,
									recType : evt.result.bankData[i].recordType,
									accId  : evt.result.bankData[i].id,
									pg:'overview'
								};
								
								require('/js/ach/VerifyBankAccountModal').VerifyBankAccountModal(params);
							}
						}
					}
					else
					{
						if(evt.result.displayBankMessage === L('invalid_session') || evt.result.displayBankMessage === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_overview();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',evt.result.displayBankMessage,[L('btn_ok')]).show();
						}
					}
					
					xhr = null;
				}
		
				function xhrError(evt) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}
			}
			
			if(e.row.rw === 'doc')
			{
				if(e.source.type === 'gallery')
				{
					if(TiGlobals.osname === 'android')
					{
						if (Ti.Media.hasCameraPermissions()) {
							selectImage(e);
						} else { 
						    Ti.Media.requestCameraPermissions(function(evt) {
					             if (e.success === true) {
					             	selectImage(e);
					             } else {
					                 /*alert("Access denied, error: " + e.error);*/
					             }
						    });
						}	
					}
					else
					{
						selectImage(e);
					}
					
					var xhr = require('/utils/XHR_BCM');
			
					xhr.call({
						url : TiGlobals.appURLBCM,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"'+TiGlobals.bcmConfig+'",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"source":"'+TiGlobals.bcmSource+'",'+
							'"type":"document",'+
							'"docType":"'+e.row.docType+'",'+
							'"platform":"'+TiGlobals.osname+'",'+
							'"corridor":"'+countryName[0]+'"'+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
			
					function xhrSuccess(evt) {
						if(evt.result.status === "S")
						{
							
							docType = e.row.docType;
							docRequestId = e.row.docRequestId;
							docTypeCode = evt.result.response.docType;
							docUploadPath = evt.result.response.uploadpath;
						}
						else
						{
							if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
							{
								require('/lib/session').session();
								destroy_overview();
							}
							else
							{
								require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
							}
						}
						xhr = null;
					}
			
					function xhrError(e) {
						require('/utils/Network').Network();
						xhr = null;
					}
				}
				
				if(e.source.type === 'upload')
				{
					var targetFile = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/" + Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + docRequestId + "_" + require('/utils/Date').today('-','dmy') + ".jpg";
					var filename = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + docRequestId + "_" + require('/utils/Date').today('-','dmy') + ".jpg";
					var docname = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + docRequestId + "_" + require('/utils/Date').today('-','dmy');
					var relativepath = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/";
					
					var xmlHttp = Ti.Network.createHTTPClient();
			      	activityIndicator.showIndicator();
		      		xmlHttp.onload = function(e)
					{
						if(xmlHttp.responseText === '1' || xmlHttp.responseText === 1)
						{
							
							var xhr = require('/utils/XHR');
							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"KYCVERIFICATIONDOCUPLOAD",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									//'"rtrn":"'+e.row.rtrn+'",'+
									'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
									'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
									'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
									'"countryCode":"'+countryCode[0]+'",'+
									'"docType":"'+docType+'",'+
									'"docRequestId":"'+docRequestId+'",'+
									'"docPath":"'+targetFile+'",'+
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
								
							function xhrSuccess(evt) {
								activityIndicator.hideIndicator();
								if(evt.result.responseFlag === "S")
								{
									if(TiGlobals.osname === 'android')
									{	
										require('/utils/AlertDialog').toast('Document uploaded successfully');
									}
									else
									{
										require('/utils/AlertDialog').iOSToast('Document uploaded successfully');
									}
									
									getDashboardAlerts();
								}
								else
								{
									if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
									{
										require('/lib/session').session();
										destroy_overview();
									}
									else
									{
										require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
									}
									
								}
								xhr = null;
							}
					
							function xhrError(evt) {
								activityIndicator.hideIndicator();
								require('/utils/Network').Network();
								xhr = null;
							}
								
						}
						else
						{
							activityIndicator.hideIndicator();
							require('/utils/AlertDialog').showAlert('','Error uploading document. Please try again later.',[L('btn_ok')]).show();	
						}
					};
					
					xmlHttp.onerror = function(e)
					{
						activityIndicator.hideIndicator();
					};
					
					require('/utils/Console').info(JSON.stringify({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadPath:relativepath}));
					
					xmlHttp.open('POST', TiGlobals.appUploadBCM + 'upload_doc.php',true);
					xmlHttp.send({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadpath:relativepath});
				}
			}
		}
	});
	
	function getInboxMessages()
	{
		activityIndicator.showIndicator();
					
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETINBOXMESSAGEDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
			
		function xhrSuccess(e) {
			
			if(e.result.responseFlag === "S")
			{
				if(e.result.messageDetails.length > 0)
				{
					_obj.msgCount === 1;
					_obj.imgStep2.image = '/images/message_unsel_set.png';
					
					for(var i=0; i<e.result.messageDetails.length;i++)
					{
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 50,
							backgroundColor : 'transparent',
							className : 'txn_msg',
							rw:0
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblMessage = Ti.UI.createLabel({
							text : require('/lib/toml_validations').removeHTMLTags(e.result.messageDetails[i].messageDate +'-' + e.result.messageDetails[i].message),
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'center',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						row.add(lblMessage);
						_obj.tblMessage.appendRow(row);
					}
					
				}
				
				// Get Alerts
				getDashboardAlerts();
				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_overview();
				}
				else
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'txn_msg',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblMessage.appendRow(row); 
				}
				
				// Get Alerts
				getDashboardAlerts();
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.imgView = [];
	_obj.btnUpload = [];
	_obj.lblDocSize = [];
	
	var docType = null;
	var docRequestId = null;
	var docTypeCode = null;
	var docUploadPath = null;
	
	function getDashboardAlerts()
	{
		_obj.tblAlert.setData([]);
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETDASHBOARDMESSAGEDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
			
		function xhrSuccess(e) {
			
			activityIndicator.hideIndicator();
			
			if(e.result.responseFlag === "S")
			{
				if(e.result.hasOwnProperty('importantData'))
				{
					var alertDialog = Ti.UI.createAlertDialog({
						buttonNames:[L('btn_ok')],
						message:require('/lib/toml_validations').removeHTMLTags(e.result.importantData)
					});
				
					alertDialog.show();
					
					alertDialog.addEventListener('click', function(e){
						alertDialog.hide();
						if(e.index === 0 || e.index === "0")
						{
							require('/js/my_account/NationalityDOBModal').NationalityDOBModal();
							destroy_overview();
							alertDialog = null;
						}
					});
				}
				
				if(e.result.hasOwnProperty('communicateData'))
				{
					try{
					for(var i=0; i<e.result.communicateData.length;i++)
					{
						_obj.alertCount === 1;
						_obj.imgStep3.image = '/images/alerts_unsel_set.png';
						
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 40,
							backgroundColor : 'transparent',
							className : 'txn_msg',
							rw:0
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblMessage = Ti.UI.createLabel({
							text : require('/lib/toml_validations').removeHTMLTags(e.result.communicateData[i].alertMessage),
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						row.add(lblMessage);
						_obj.tblAlert.appendRow(row);
					}
					}catch(e){}
				}
				
				if(e.result.hasOwnProperty('bankAccountData'))
				{
					 try{
					for(var i=0; i<e.result.bankAccountData.length;i++)
					{
						
						_obj.alertCount === 1;
						_obj.imgStep3.image = '/images/alerts_unsel_set.png';
						
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 40,
							backgroundColor : 'transparent',
							className : 'txn_msg',
							rw:'bank',
							achId:e.result.bankAccountData[i].achId
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblMessage = Ti.UI.createLabel({
							text : require('/lib/toml_validations').removeHTMLTags(e.result.bankAccountData[i].alertMessage),
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						row.add(lblMessage);
						_obj.tblAlert.appendRow(row);
					}
						}catch(e){}
						
					
				}
				
				if(e.result.hasOwnProperty('passwordVerificationData') && e.result.passwordVerificationData !== '')
				{
					try{
					_obj.alertCount === 1;
					_obj.imgStep3.image = '/images/alerts_unsel_set.png';
					
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 40,
						backgroundColor : 'transparent',
						className : 'txn_msg',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : require('/lib/toml_validations').removeHTMLTags(e.result.passwordVerificationData),
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblAlert.appendRow(row);
					}catch(e){}
				}
				
				if(e.result.hasOwnProperty('voucherData') && e.result.voucherData !== '')
				{
					try{
					_obj.alertCount === 1;
					_obj.imgStep3.image = '/images/alerts_unsel_set.png';
					
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 40,
						backgroundColor : 'transparent',
						className : 'txn_msg',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : require('/lib/toml_validations').removeHTMLTags(e.result.voucherData),
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblAlert.appendRow(row);
					}catch(e){}
					
				}
				
				if(e.result.hasOwnProperty('documentUploadData'))
				{
				   try{
					for(var i=0; i<e.result.documentUploadData.length;i++)
					{
						_obj.alertCount === 1;
						_obj.imgStep3.image = '/images/alerts_unsel_set.png';
						
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 100,
							backgroundColor : 'transparent',
							className : 'txn_msg',
							rw:'doc',
							sel:i,
							doc:e.result.documentUploadData[i].docType+'/'+e.result.documentUploadData[i].docRequestId,
							docType:e.result.documentUploadData[i].docType,
							docRequestId:e.result.documentUploadData[i].docRequestId,
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var detailView = Ti.UI.createView({
							top : 0,
							left : 0,
							right:0,
							height : 100,
							backgroundColor : '#fff',
							type:'gallery'
						});
						
						var lblDocName = Ti.UI.createLabel({
							text : require('/lib/toml_validations').removeHTMLTags(e.result.documentUploadData[i].alertMessage),
							top:0,
							left : 20,
							right : 100,
							height : 50,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('blackFont'),
							type:'gallery',
						});
						
						_obj.imgView[i] = Ti.UI.createImageView({
							left : 20,
							top:50,
							height : 40,
							width:40,
							visible:false,
							type:'gallery'
						});
						
						_obj.lblDocSize[i] = Ti.UI.createLabel({
							top:50,
							left : 70,
							right : 100,
							height : 50,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							type:'gallery'
						});
						
						_obj.btnUpload[i] = Ti.UI.createButton({
							backgroundImage:'/images/upload.png',
							right : 20,
							height : 60,
							width:60,
							visible:false,
							type:'upload'
						});
						
						detailView.add(lblDocName);
						detailView.add(_obj.imgView[i]);
						detailView.add(_obj.lblDocSize[i]);
						detailView.add(_obj.btnUpload[i]);
						row.add(detailView);
						_obj.tblAlert.appendRow(row);
					}
					}catch(e){}
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_overview();
				}
				else
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'txn_msg',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblAlert.appendRow(row);
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	getInboxMessages();
	
	_obj.mainView.add(_obj.scrollableView);
	_obj.globalView.add(_obj.mainView);
	_obj.winOverview.add(_obj.globalView);
	_obj.winOverview.open();
	
	_obj.scrollableView.views = [_obj.step1,_obj.step2,_obj.step3];
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_overview();
	});
	
	_obj.winOverview.addEventListener('androidback', function(){
		destroy_overview();
	});
	
	_obj.tabStep1.addEventListener('click',function(e){
		require('/utils/Console').info(e.source);
		if(e.source.sel === 'txn' && _obj.tab !== 'txn')
		{
			_obj.scrollableView.scrollToView(0);
		}
	});
	
	_obj.tabStep2.addEventListener('click',function(e){
		if(e.source.sel === 'msg' && _obj.tab !== 'msg')
		{
			_obj.scrollableView.scrollToView(1);
		}
	});
	
	_obj.tabStep3.addEventListener('click',function(e){
		if(e.source.sel === 'alert' && _obj.tab !== 'alert')
		{
			_obj.scrollableView.scrollToView(2);
		}
	});
	
	_obj.scrollableView.addEventListener('scrollend',function(e){
		if(e.currentPage === 0)
		{
			if(_obj.step1Check === 0)
			{
				trackTxn();
				_obj.step1Check = 1;
			}
		}
		
		if(e.currentPage === 1)
		{
			if(_obj.step2Check === 0)
			{
				_obj.step2Check = 1;
			}
		}
		
		if(e.currentPage === 2)
		{
			if(_obj.step3Check === 0)
			{
				_obj.step3Check = 1;
			}
		}
	});
	
	_obj.scrollableView.addEventListener('scroll',function(e){
		if(e.currentPage === 0)
		{
			changeTabs('txn');
			_obj.tab = 'txn';
		}
		
		if(e.currentPage === 1)
		{
			changeTabs('msg');
			_obj.tab = 'msg';
		}
		
		if(e.currentPage === 2)
		{
			changeTabs('alert');
			_obj.tab = 'alert';
		}
	});
	
	function changeTabs(selected)
	{
		if(selected === 'txn')
		{
			_obj.tabSelView.left = 0;
			_obj.tabSelView.width = '33%';
			_obj.tabSelView.right = null;
			
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			
			_obj.tabStep1.backgroundColor = '#6F6F6F';
			_obj.tabStep2.backgroundColor = '#E9E9E9';
			_obj.tabStep3.backgroundColor = '#E9E9E9';
			
			_obj.imgStep1.image = '/images/track_sel.png';
			_obj.imgStep2.image = _obj.msgCount === 0 ? '/images/message_unsel.png' : '/images/message_unsel_set.png' ;
			_obj.imgStep3.image = _obj.alertCount === 0 ? '/images/alerts_unsel.png' : '/images/alerts_unsel_set.png' ; 
		}
		else if(selected === 'msg')
		{
			_obj.tabSelView.left = '33.4%';
			_obj.tabSelView.width = '34%';
			_obj.tabSelView.right = null;
			
			_obj.tabSelIconView.left = '33%';
			_obj.tabSelIconView.right = null;
			
			_obj.tabStep1.backgroundColor = '#E9E9E9';
			_obj.tabStep2.backgroundColor = '#6F6F6F';
			_obj.tabStep3.backgroundColor = '#E9E9E9';
			
			_obj.imgStep1.image = '/images/track_unsel.png';
			_obj.imgStep2.image = '/images/message_sel.png';
			_obj.imgStep3.image = _obj.alertCount === 0 ? '/images/alerts_unsel.png' : '/images/alerts_unsel_set.png';
		}
		else if(selected === 'alert')
		{
			_obj.tabSelView.left = '67.8%';
			_obj.tabSelView.width = '33%';
			_obj.tabSelView.right = null;
			
			_obj.tabSelIconView.left = '67%';
			_obj.tabSelIconView.right = null;
			
			_obj.tabStep1.backgroundColor = '#E9E9E9';
			_obj.tabStep2.backgroundColor = '#E9E9E9';
			_obj.tabStep3.backgroundColor = '#6F6F6F';
			
			_obj.imgStep1.image = '/images/track_unsel.png';
			_obj.imgStep2.image = _obj.msgCount === 0 ? '/images/message_unsel.png' : '/images/message_unsel_set.png';
			_obj.imgStep3.image = '/images/alerts_sel.png';
		}
		else
		{
			
		}
	}
	
	function destroy_overview()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove overview start ##############');
			
			_obj.winOverview.close();
			require('/utils/RemoveViews').removeViews(_obj.winOverview);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_overview',destroy_overview);
			require('/utils/Console').info('############## Remove overview end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_overview', destroy_overview);
}; // OverviewModal()