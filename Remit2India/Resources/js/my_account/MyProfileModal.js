exports.MyProfileModal = function()
{
	require('/lib/analytics').GATrackScreen('My Profile');
	
	var _obj = {
		style : require('/styles/my_account/MyProfile').MyProfile,
		winMyProfile : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		scrollableView : null,
		
		// Profile Information
		txtFirstName : null,
		borderViewS11 : null,
		txtLastName : null,
		borderViewS12 : null,
		dobView : null,
		lblDOB : null,
		imgDOB : null,
		borderViewS13 : null,
		txtAddress : null,
		borderViewS14 : null,
		txtCity : null,
		borderViewS15 : null,
		txtState : null,
		borderViewS16 : null,
		txtZip : null,
		borderViewS17 : null,
		txtCountry : null,
		borderViewS18 : null,
		lblMobile : null,
		txtMobile : null,
		mobileView : null,
		borderViewS19 : null,
		txtDayNo : null,
		borderViewS110 : null,
		lblAlternateNo : null,
		imgAlternateNo : null,
		txtAlternateNo : null,
		alternateNoView : null,
		borderViewS111 : null,
		txtEmail : null,
		borderViewS112 : null,
		lblChangePassword : null,
		borderViewS113 : null,
		btnSave : null,
		stepView1 : null,
		
		tab : null,
		referenceNo : null,
		f : null,
		imgUpload : null,
		image : null,
		data_to_send : null,
	};
	
	_obj.tab = 'profile';
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winMyProfile = Titanium.UI.createWindow(_obj.style.winMyProfile);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winMyProfile);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'My Profile';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.lblMyProfile = Ti.UI.createLabel(_obj.style.lblMyProfile);
	_obj.lblMyProfile.text = 'Profile Information';
	_obj.lblMyDocuments = Ti.UI.createLabel(_obj.style.lblMyDocuments);
	_obj.lblMyDocuments.text = 'My Documents';
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
	
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.tabView.add(_obj.lblMyProfile);
	_obj.tabView.add(_obj.lblMyDocuments);
	_obj.tabView.add(_obj.tabSelView);
	_obj.mainView.add(_obj.tabView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.mainView.add(_obj.tabSelIconView);
	
	_obj.tabView.addEventListener('click',function(e){
		if(e.source.sel === 'profile' && _obj.tab !== 'profile')
		{
			_obj.scrollableView.movePrevious();
		}
		
		if(e.source.sel === 'documents' && _obj.tab !== 'documents')
		{
			_obj.scrollableView.moveNext();
		}
	});
	
	_obj.scrollableView.addEventListener('scroll',function(e){
		if(e.currentPage === 0)
		{
			changeTabs('profile');
			_obj.tab = 'profile';
		}
		
		if(e.currentPage === 1)
		{
			changeTabs('documents');
			_obj.tab = 'documents';
		}
	});
	
	_obj.scrollableView.addEventListener('scrollend',function(e){
		if(e.currentPage === 1)
		{
			myDocuments();
		}
	});
	
	function changeTabs(selected)
	{
		if(selected === 'profile')
		{
			_obj.tabSelView.left = 0;
			_obj.tabSelView.right = null;
			_obj.tabSelView.width = '49.6%';
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			_obj.tabSelIconView.width = '50%';
			_obj.lblMyProfile.color = TiFonts.FontStyle('whiteFont');
			_obj.lblMyProfile.backgroundColor = '#6F6F6F';
			_obj.lblMyDocuments.color = TiFonts.FontStyle('blackFont');
			_obj.lblMyDocuments.backgroundColor = '#E9E9E9';
		}
		else
		{
			_obj.tabSelView.left = null;
			_obj.tabSelView.right = 0;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = null;
			_obj.tabSelIconView.right = 0;
			_obj.tabSelIconView.width = '50%';
			_obj.lblMyDocuments.color = TiFonts.FontStyle('whiteFont');
			_obj.lblMyDocuments.backgroundColor = '#6F6F6F';
			_obj.lblMyProfile.color = TiFonts.FontStyle('blackFont');
			_obj.lblMyProfile.backgroundColor = '#E9E9E9';
		}
	}
	
	/////////////////// My Profile ///////////////////
	
	_obj.stepView1 = Ti.UI.createScrollView(_obj.style.stepView1);
	_obj.txtFirstName = Ti.UI.createTextField(_obj.style.txtFirstName);
	_obj.txtFirstName.editable = false;
	_obj.txtFirstName.hintText = 'First Name';
	_obj.txtFirstName.value = '';
	_obj.borderViewS11 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtLastName = Ti.UI.createTextField(_obj.style.txtLastName);
	_obj.txtLastName.editable = false;
	_obj.txtLastName.hintText = 'Last Name';
	_obj.txtLastName.value = '';
	_obj.borderViewS12 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.dobView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblDOB = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblDOB.text = 'Date of Birth*';
	_obj.imgDOB = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderViewS13 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtAddress = Ti.UI.createTextField(_obj.style.txtAddress);
	_obj.txtAddress.editable = false;
	_obj.txtAddress.value = '';
	_obj.txtAddress.hintText = 'Address';
	_obj.borderViewS14 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtCity = Ti.UI.createTextField(_obj.style.txtCity);
	_obj.txtCity.editable = false;
	_obj.txtCity.value = '';
	_obj.txtCity.hintText = 'City';
	_obj.borderViewS15 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtState = Ti.UI.createTextField(_obj.style.txtState);
	_obj.txtState.editable = false;
	_obj.txtState.value = '';
	_obj.txtState.hintText = 'State';
	_obj.borderViewS16 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtZip = Ti.UI.createTextField(_obj.style.txtZip);
	_obj.txtZip.editable = false;
	_obj.txtZip.value = '';
	_obj.txtZip.hintText = 'Zip Code';
	
	if(countryName[0] === 'Germany' || countryName[0] === 'Singapore')
	{
		_obj.txtZip.hintText = 'Postal Code';
	}
	else if(countryName[0] === 'United Arab Emirates')
	{
		_obj.txtZip.hintText = 'P. O. Box';
	}
	else if(countryName[0] === 'United Kingdom')
	{
		_obj.txtZip.hintText = 'Postcode';
	}
	else
	{
		_obj.txtZip.hintText = 'Zip Code';
	}
	_obj.borderViewS17 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtCountry = Ti.UI.createTextField(_obj.style.txtCountry);
	_obj.txtCountry.editable = false;
	_obj.txtCountry.value = '';
	_obj.txtCountry.hintText = 'Country';
	_obj.borderViewS18 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneMobile = Ti.UI.createButton(_obj.style.done);
	_obj.doneMobile.addEventListener('click',function(){
		_obj.txtMobile.blur();
	});
	
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblMobile = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.txtMobile = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtMobile.hintText = 'Mobile Number*';
	_obj.txtMobile.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtMobile.keyboardToolbar = [_obj.doneMobile];
	_obj.borderViewS19 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneDay = Ti.UI.createButton(_obj.style.done);
	_obj.doneDay.addEventListener('click',function(){
		_obj.txtDayNo.blur();
	});
	
	_obj.txtDayNo = Ti.UI.createTextField(_obj.style.txtDayNo);
	_obj.txtDayNo.hintText = 'Day Time Number*';
	_obj.txtDayNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDayNo.keyboardToolbar = [_obj.doneDay];
	_obj.borderViewS110 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneAlternateNo = Ti.UI.createButton(_obj.style.done);
	_obj.doneAlternateNo.addEventListener('click',function(){
		_obj.txtAlternateNo.blur();
	});
	
	_obj.alternateNoView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblAlternateNo = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblAlternateNo.text = 'ISD';
	_obj.imgAlternateNo = Ti.UI.createImageView(_obj.style.imgAlternateNo);
	_obj.txtAlternateNo = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtAlternateNo.maxLength = 15;
	_obj.txtAlternateNo.hintText = 'Alternate Number';
	_obj.txtAlternateNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtAlternateNo.keyboardToolbar = [_obj.doneAlternateNo];
	_obj.borderViewS111 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtEmail = Ti.UI.createTextField(_obj.style.txtEmail);
	_obj.txtEmail.editable = false;
	_obj.txtEmail.hintText = 'Email ID (Username)*';
	_obj.txtEmail.value = '';
	_obj.borderViewS112 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.lblChangePassword = Ti.UI.createLabel(_obj.style.lblChangePassword);
	_obj.lblChangePassword.text = 'Change Password';
	_obj.borderViewS113 = Ti.UI.createView(_obj.style.borderViewChangePassword);
	_obj.borderViewS113.top = 5;
	
	_obj.btnSave = Ti.UI.createButton(_obj.style.btnSave);
	_obj.btnSave.title = 'SAVE';
	
	_obj.stepView1.add(_obj.txtFirstName);
	_obj.stepView1.add(_obj.borderViewS11);
	
	_obj.stepView1.add(_obj.txtLastName);
	_obj.stepView1.add(_obj.borderViewS12);
	
	_obj.dobView.add(_obj.lblDOB);
	_obj.dobView.add(_obj.imgDOB);
	_obj.stepView1.add(_obj.dobView);
	_obj.stepView1.add(_obj.borderViewS13);
	
	_obj.stepView1.add(_obj.txtAddress);
	_obj.stepView1.add(_obj.borderViewS14);
	
	_obj.stepView1.add(_obj.txtCity);
	_obj.stepView1.add(_obj.borderViewS15);
	
	_obj.stepView1.add(_obj.txtState);
	_obj.stepView1.add(_obj.borderViewS16);
	
	_obj.stepView1.add(_obj.txtZip);
	_obj.stepView1.add(_obj.borderViewS17);
	
	_obj.stepView1.add(_obj.txtCountry);
	_obj.stepView1.add(_obj.borderViewS18);
	
	_obj.mobileView.add(_obj.lblMobile);
	_obj.mobileView.add(_obj.txtMobile);
	_obj.stepView1.add(_obj.mobileView);
	_obj.stepView1.add(_obj.borderViewS19);
	
	_obj.stepView1.add(_obj.txtDayNo);
	_obj.stepView1.add(_obj.borderViewS110);
	
	_obj.alternateNoView.add(_obj.lblAlternateNo);
	_obj.alternateNoView.add(_obj.imgAlternateNo);
	_obj.alternateNoView.add(_obj.txtAlternateNo);
	_obj.stepView1.add(_obj.alternateNoView);
	_obj.stepView1.add(_obj.borderViewS111);
	
	_obj.stepView1.add(_obj.txtEmail);
	_obj.stepView1.add(_obj.borderViewS112);
	
	_obj.stepView1.add(_obj.lblChangePassword);
	_obj.stepView1.add(_obj.borderViewS113);
	
	_obj.stepView1.add(_obj.btnSave);
	
	function alternateNo()
	{
		activityIndicator.showIndicator();
		var isdOptions = [];
		
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"isdListing",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			if(e.result.status === "S")
			{
				for(var i=0;i<e.result.response[0].isdListing.length;i++)
				{
					isdOptions.push('+'+e.result.response[0].isdListing[i]);
				}
				
				var optionDialogISD = require('/utils/OptionDialog').showOptionDialog('ISD',isdOptions,-1);
				optionDialogISD.show();
				
				setTimeout(function(){
					activityIndicator.hideIndicator();
				},200);
				
				optionDialogISD.addEventListener('click',function(evt){
					if(optionDialogISD.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblAlternateNo.text = optionDialogISD.options[evt.index];
						optionDialogISD = null;
					}
					else
					{
						optionDialogISD = null;
						_obj.lblAlternateNo.text = 'ISD';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_myprofile();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.lblAlternateNo.addEventListener('click',function(e){
		alternateNo();
	});
	
	_obj.imgAlternateNo.addEventListener('click',function(e){
		alternateNo();
	});
	
	function getProfileData()
	{
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETUSERDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				_obj.referenceNo = e.result.referenceNo;
				
				_obj.txtFirstName.value = e.result.firstName;
				_obj.txtLastName.value = e.result.lastName;
				_obj.lblDOB.text = e.result.dob;
				_obj.txtAddress.value = e.result.address;
				_obj.txtCity.value = e.result.city;
				_obj.txtState.value = e.result.state;
				_obj.txtZip.value = e.result.pinCode;
				_obj.txtCountry.value = e.result.country;
				
				var splitMobile = e.result.mobileNo.split('-');
				
				_obj.lblMobile.text =  + splitMobile[0];
				_obj.txtMobile.value = splitMobile[1];
				
				var splitDayNo = e.result.resPhoneNo.split('-');
				
				for(var i=0; i<splitDayNo.length; i++)
				{
					_obj.txtDayNo.value = _obj.txtDayNo.value + splitDayNo[i];
				}
				
				_obj.txtEmail.value = e.result.emailId;
				
				if(e.result.alternateNo !== "")
				{
					var splitAlternate = e.result.alternateNo.split('-');
				
					_obj.lblAlternateNo.text = splitAlternate[0];
					_obj.txtAlternateNo.value = splitAlternate[1];
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_myprofile();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	getProfileData();
	
	// Set HintText for mobile and day nos
	
	switch(countryName[0]){
		case 'United States' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10; 
			
			break;
		}
		case 'United Kingdom' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;			
			break;
		}
		case 'Australia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;
			break;
		 }
		case 'Singapore' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			_obj.txtDayNo.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDayNo.maxLength = 8;
			break;
		 }
		 case 'Germany' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			_obj.txtDayNo.hintText = 'Day Time Number*';
			_obj.txtDayNo.maxLength = 14;
			break;
		 }			 
		 case 'Canada' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Belgium' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDayNo.maxLength = 8;
			break;
		}		
		case 'Cyprus' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}			
		case 'Finland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}			
		case 'France' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Greece' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Ireland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Italy' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Japan' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Luxembourg' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Malta' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}		
		case 'Netherlands' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Portugal' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;
			break;
		}
		case 'Slovakia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Slovenia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		case 'Hong Kong' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		
		case 'Austria' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		
		case 'New Zealand' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;
			break;
		}
		
		case 'Spain' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDayNo.maxLength = 10;
			break;
		}
		
		case 'United Arab Emirates' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDayNo.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDayNo.maxLength = 9;
			break;
		}
		
		default:
		{
			break;
		}
	}
	
	_obj.lblChangePassword.addEventListener('touchend',function(e){
		require('/js/ChangePasswordModal').ChangePasswordModal();
	});
	
	_obj.btnSave.addEventListener('click',function(e){
		
    	////////////////////// Mobile No ////////////////////
		if(_obj.txtMobile.value === '' || _obj.txtMobile.value === 'Mobile Number*')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(countryName[0] === 'United Kingdom')
		{ 						
			if(_obj.txtMobile.value.charAt(0) === 0 && _obj.txtMobile.value != '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Mobile Number without 0 in the beginning',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
			else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
		|| countryName[0] === 'Finland' || countryName[0] === 'France' 
		|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
		|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
		|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
		 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal' || countryName[0] === 'Belgium' || countryName[0] === 'Netherlands')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(countryName[0] === 'Germany')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 11))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
				return;
			}
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Cyprus')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 11) && countryName[0] === 'New Zealand')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if((_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Spain')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if((_obj.txtMobile.value.length < 10 || _obj.txtMobile.value.length > 11) && countryName[0] === 'Austria')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else if((_obj.txtMobile.value.length < 7 || _obj.txtMobile.value.length > 9) && ((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')))
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
			return;
		}
		else
		{
			
		}
		
		////////////////////// Daytime No ////////////////////
		//if UAE than it is not cumpusory
		if(countryName[0] != 'United Arab Emirates' && countryName[0] != 'UAE') 
		{		
			if(_obj.txtDayNo.value === '' || _obj.txtDayNo.value === 'Day Time Number*')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.value = '';
	    		_obj.txtDayNo.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDayNo.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.value = '';
	    		_obj.txtDayNo.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDayNo.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.value = '';
	    		_obj.txtDayNo.focus();
				return;
			}
			else if((_obj.txtDayNo.value.length < 9 || _obj.txtDayNo.value.length > 10) && countryName[0] === 'Spain')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.value = '';
	    		_obj.txtDayNo.focus();
				return;
			}
			else if(_obj.txtDayNo.value.length != 0 && _obj.txtDayNo.value.length < 7 && countryName[0] === 'New Zealand')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.value = '';
	    		_obj.txtDayNo.focus();
				return;
			}
			else if((_obj.txtDayNo.value.length < 3 || _obj.txtDayNo.value.length > 9) && countryName[0] === 'Germany')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.value = '';
	    		_obj.txtDayNo.focus();
				return;
			}
			else if(countryName[0] === 'United Kingdom')
			{
				if(_obj.txtDayNo.value.charAt(0) === 0 && _obj.txtDayNo.value != '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your Day Time Number without 0 in the beginning',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				}
				else if(_obj.txtDayNo.value.length != 0 && (_obj.txtDayNo.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				}
			}
			else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
			|| countryName[0] === 'Finland' || countryName[0] === 'France' 
			|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
			|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
			|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
			 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
			{ 									
				if(_obj.txtDayNo.value.length != 0 && (_obj.txtDayNo.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				}
			}
			else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal')
			{ 									
				if(_obj.txtDayNo.value.length != 0 && (_obj.txtDayNo.value.length < 9))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				}
			}
			else if(countryName[0] === 'Belgium' || countryName[0] === 'Netherlands' || countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
			{ 									
				if(_obj.txtDayNo.value.length != 0 && (_obj.txtDayNo.value.length < 8))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDayNo.value = '';
		    		_obj.txtDayNo.focus();
					return;
				}
			}
		}
		if(((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')) && _obj.txtDayNo.value.length > 0)
		{
			if(_obj.txtDayNo.value.length < 7 || _obj.txtDayNo.value.length > 9)
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDayNo.value = '';
	    		_obj.txtDayNo.focus();
				return;
			}
		}
		
		if(_obj.txtMobile.value === _obj.txtDayNo.value)
		{
			require('/utils/AlertDialog').showAlert('','Day Time No. and Mobile No. cannot be the same',[L('btn_ok')]).show();
    		_obj.txtDayNo.value = '';
    		_obj.txtDayNo.focus();
			return;
		}
		
		if(_obj.txtAlternateNo.value.length !== 0)
		{
			if(_obj.lblAlternateNo.text === 'ISD' && _obj.txtAlternateNo.value !== '')
			{
				require('/utils/AlertDialog').showAlert('','Please select ISD code for alternate no.',[L('btn_ok')]).show();
				_obj.lblAlternateNo.text = 'ISD';
	    		return;
			}
		}
		else
		{
			if(_obj.lblAlternateNo.text === 'ISD' || _obj.txtAlternateNo.value === '')
			{
				var alternateNo = '';
			}
			else
			{
				var alternateNo = _obj.lblAlternateNo.text +'-'+_obj.txtAlternateNo.value;
			}
		}
		
		activityIndicator.showIndicator();
					
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"UPDATEUSERDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"referenceNo":"'+_obj.referenceNo+'",'+
				'"mobileNo":"'+_obj.lblMobile.text+_obj.txtMobile.value+'",'+ 
				'"resPhoneNo":"'+_obj.txtDayNo.value+'",'+ 
				'"alternateNo":"'+alternateNo+'",'+
				'"firstName":"",'+
				'"lastName":"",'+
				'"dob":"",'+
				'"address":"",'+ 
				'"state":"",'+
				'"city":"",'+
				'"zip":"",'+
				'"country":"",'+
				'"rmailId":"",'+
				'"gender":"",'+
				'"knowAboutUs":"",'+
				'"nationality":"",'+
				'"occupation":"",'+
				'"fieldsToUpdate":{"mobileNoFlag":"Y","resPhoneNoFlag":"Y","alternateNoFlag":"Y","firstNameFlag":"N","lastNameFlag":"N","emailIdFlag":"N","nationalityFlag":"N","occupationFlag":"N","stateFlag":"N","cityFlag":"N","pinCodeFlag":"N"}'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
			
		function xhrSuccess(e) {
			
			if(e.result.responseFlag === "S")
			{
				activityIndicator.hideIndicator();
				
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_myprofile();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	/////////////////// Step 2 ///////////////////
	
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};
	
	function selectImage(e) 
	{
	   _obj.f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,e.row.docType+'.jpg');
		_obj.imgUpload = 0;
		
		var optionDialogPhoto = require('/utils/OptionDialog').showOptionDialog('',[L('takePicture'), L('selectPicture')],-1);
		optionDialogPhoto.show();
		
		optionDialogPhoto.addEventListener('click',function(evt){
			if(optionDialogPhoto.options[evt.index] !== L('btn_cancel'))
			{
				if(evt.index === 0)
				{
					// Take Picture
					
					Ti.Media.showCamera({
				        showControls:true,
				        mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
				        autohide:true,
				        allowEditing:false,
				        autorotate:true,
				        success:function(event) {
				        	
							_obj.image = event.media;
				        	_obj.f.write(_obj.image);
				        	
				        	if(_obj.f.size < 5000000)
				        	{
				        		if(TiGlobals.osname === 'android')
				        		{
				        			_obj.imgView[e.row.rw].image = _obj.f.nativePath;	
				        		}
				        		else
				        		{
				        			_obj.imgView[e.row.rw].image = _obj.f.resolve();
				        		}
					        	
					        	_obj.lblDocSize[e.row.rw].text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
					        	_obj.imgView[e.row.rw].show();
					        	_obj.btnUpload[e.row.rw].show();
					        }
					        else
					        {
					        	require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
					        }	
				        }
					});
				}
				
				if(evt.index === 1)
				{
					// Obtain an image from the gallery
					
			        Titanium.Media.openPhotoGallery({
			            success:function(event)
			            { 
			            	if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO)
			                {
			                	_obj.image = event.media;				            
							    _obj.f.write(_obj.image);
							    
							    if(_obj.f.size < 5000000)
					        	{
						        	_obj.imgView[e.row.rw].image = _obj.f.read();
						        	_obj.lblDocSize[e.row.rw].text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
						        	_obj.imgView[e.row.rw].show();
						        	_obj.btnUpload[e.row.rw].show();
						        }
						        else
						        {
						        	require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
						        }
			                }      
			            },
			            cancel:function()
			            {
			            }
			        });
				}
				
				optionDialogPhoto = null;
			}
		});	
	};
	
	_obj.stepView2 = Ti.UI.createScrollView(_obj.style.stepView2);
	
	_obj.tblUpload = Ti.UI.createTableView(_obj.style.tableView);
	
	_obj.imgView = [];
	_obj.btnUpload = [];
	_obj.lblDocSize = [];
	
	var docType = null;
	var docRequestId = null;
	var docTypeCode = null;
	var docUploadPath = null;
	
	_obj.tblUpload.addEventListener('click',function(e){
		
		if(e.source.type === 'gallery' && e.row.rw !== '')
		{
			//Ti.API.info("In e.source.type === 'gallery':--");
			if(TiGlobals.osname === 'android')
			{
				if (Ti.Media.hasCameraPermissions()) {
					selectImage(e);
				} else { 
				    Ti.Media.requestCameraPermissions(function(evt) {
			             if (e.success === true) {
			             	selectImage(e);
			             } else {
			                 /*alert("Access denied, error: " + e.error);*/
			             }
				    });
				}	
			}
			else
			{
				selectImage(e);
			}
			
			var xhr = require('/utils/XHR_BCM');
	
			xhr.call({
				url : TiGlobals.appURLBCM,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"'+TiGlobals.bcmConfig+'",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"source":"'+TiGlobals.bcmSource+'",'+
					'"type":"document",'+
					'"docType":"'+e.row.docType+'",'+
					'"platform":"'+TiGlobals.osname+'",'+
					'"corridor":"'+countryName[0]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(evt) {
				if(evt.result.status === "S")
				{
					
					docType = e.row.docType;
					docRequestId = e.row.docRequestId;
					docTypeCode = evt.result.response.docType;
					docUploadPath = evt.result.response.uploadpath;
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_myprofile();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}
	
			function xhrError(e) {
				require('/utils/Network').Network();
				xhr = null;
			}
		}
		
		if(e.source.type === 'upload' && e.row.rw !== '')
		{
			//Ti.API.info("In e.source.type===upload:--");
			var targetFile = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/" + Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + docRequestId + "_" + require('/utils/Date').today('-','dmy') + ".jpg";
			var filename = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + docRequestId + "_" + require('/utils/Date').today('-','dmy') + ".jpg";
			var docname = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + docRequestId + "_" + require('/utils/Date').today('-','dmy');
			var relativepath = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/";
			
			var xmlHttp = Ti.Network.createHTTPClient();
	      	activityIndicator.showIndicator();
      	   	xmlHttp.onload = function(e)
			{
				//Ti.API.info("In onload3:--",xmlHttp.responseText);
				var test = JSON.parse(xmlHttp.responseText);
				//Ti.API.info("In xmlresponseText:--",test.result);
				//Ti.API.info("In xmlresponseText:--",test.path);
				
				if(test.result === 'true' || test.result === true || test.result === 1)
				{
					//Ti.API.info("Inside if:--");
					var xhr = require('/utils/XHR');
					xhr.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"KYCVERIFICATIONDOCUPLOAD",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							//'"rtrn":"'+e.row.rtrn+'",'+
							'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
							'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
							'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
							'"countryCode":"'+countryCode[0]+'",'+
							'"docType":"'+docType+'",'+
							'"docRequestId":"'+docRequestId+'",'+
							'"docPath":"'+targetFile+'",'+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
						
					function xhrSuccess(evt) {
						activityIndicator.hideIndicator();
						//Ti.API.info("In xhrSuccess:--");
						if(evt.result.responseFlag === "S")
						{
								//Ti.API.info("In responseFlag S:--");

							if(TiGlobals.osname === 'android')
							{	
								require('/utils/AlertDialog').toast('Document uploaded successfully');
							}
							else
							{
								require('/utils/AlertDialog').iOSToast('Document uploaded successfully');
							}
							
							myDocuments();
						}
						else
						{
							if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
							{
								require('/lib/session').session();
								destroy_myprofile();
							}
							else
							{
								require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
							}
							
						}
						xhr = null;
					}
			
					function xhrError(evt) {
							//Ti.API.info("In xhrError:--");

						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr = null;
					}
					
				}
				else
				{
					activityIndicator.hideIndicator();
					require('/utils/AlertDialog').showAlert('','Error uploading document. Please try again later.',[L('btn_ok')]).show();	
				}
			};
			
			xmlHttp.onerror = function(e)
			{
				activityIndicator.hideIndicator();
			};
				
				//Ti.API.info("In xhrSuccess:--");

			require('/utils/Console').info(JSON.stringify({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadPath:relativepath}));
				//Ti.API.info("In xhrSuccess1:--");

			xmlHttp.open('POST', TiGlobals.appUploadBCM + 'upload_doc.php',true);
				//Ti.API.info("In xhrSuccess2:--");

			xmlHttp.send({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadpath:relativepath});
				//Ti.API.info("In xhrSuccess3:--");

		}
	});
	
	function myDocuments()
	{
			//Ti.API.info("In myDocuments:--");

		_obj.tblUpload.setData([]);
		activityIndicator.showIndicator();
					
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETKYCDOCUMENTDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"originatingCountry":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
			
		function xhrSuccess(e) {
			
			activityIndicator.hideIndicator();
			
			if(e.result.responseFlag === "S")
			{
				_obj.tblUpload.height = 100*e.result.docArrList.length;
				
				for(var i=0; i<e.result.docArrList.length;i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 100,
						className : 'upload',
						comments:e.result.docArrList[i].comments,
						docType:e.result.docArrList[i].docType,
						docRequestId:e.result.docArrList[i].docRequestId,
						rw:i
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var detailView = Ti.UI.createView({
						top : 0,
						left : 0,
						right:0,
						height : 100,
						backgroundColor : '#fff',
						type:'gallery'
					});
					
					var lblDocName = Ti.UI.createLabel({
						text:e.result.docArrList[i].docType,
						top:0,
						left : 20,
						right : 100,
						height : 50,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('blackFont'),
						type:'gallery',
					});
					
					_obj.imgView[i] = Ti.UI.createImageView({
						left : 20,
						top:50,
						height : 40,
						width:40,
						visible:false,
						type:'gallery'
					});
					
					_obj.lblDocSize[i] = Ti.UI.createLabel({
						top:50,
						left : 70,
						right : 100,
						height : 50,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						type:'gallery'
					});
					
					_obj.btnUpload[i] = Ti.UI.createButton({
						backgroundImage:'/images/upload.png',
						right : 20,
						height : 60,
						width:60,
						visible:false,
						type:'upload'
					});
					
					detailView.add(lblDocName);
					detailView.add(_obj.imgView[i]);
					detailView.add(_obj.lblDocSize[i]);
					detailView.add(_obj.btnUpload[i]);
					row.add(detailView);
					_obj.tblUpload.appendRow(row);
				}
				
				_obj.stepView2.add(_obj.tblUpload);
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_myprofile();
				}
				else
				{
					_obj.tblUpload.height = 50;
			
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'account_details',
						rw:''
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblUpload.appendRow(row);
					_obj.stepView2.add(_obj.tblUpload);
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_myprofile();
				alertDialog = null;
			}
		});
	});
	
	function postlayout() 
	{
		_obj.borderViewS113.width = _obj.lblChangePassword.getRect().width;
		_obj.winMyProfile.removeEventListener('postlayout', postlayout);
	}
	
	_obj.winMyProfile.addEventListener('postlayout', postlayout);
	
	_obj.scrollableView.views = [_obj.stepView1,_obj.stepView2];
	_obj.mainView.add(_obj.scrollableView);
	_obj.globalView.add(_obj.mainView);
	_obj.winMyProfile.add(_obj.globalView);
	_obj.winMyProfile.open();
	
	_obj.winMyProfile.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_myprofile();
				alertDialog = null;
			}
		});
	});
	
	function destroy_myprofile()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove myprofile start ##############');
			
			_obj.winMyProfile.close();
			require('/utils/RemoveViews').removeViews(_obj.winMyProfile);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_myprofile',destroy_myprofile);
			require('/utils/Console').info('############## Remove myprofile end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_myprofile', destroy_myprofile);
}; // MyProfileModal()
