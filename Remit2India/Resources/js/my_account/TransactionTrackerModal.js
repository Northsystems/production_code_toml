exports.TransactionTrackerModal = function()
{
	require('/lib/analytics').GATrackScreen('Transaction Tracker');
	
	var _obj = {
		style : require('/styles/my_account/Overview').Overview,
		winTransactionTracker : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblTransaction : null,
		imgTransaction : null,
		transactionType : null,
		tblTransaction : null,
		step1 : null,
		
		start : 0,
		last : 0,
		page : 1,
		limit : 10,
		totalPages : 1,
		totalRecords : 0,
		lastDistance : 0,
		updating : false,
		selTxn : '',
	};
	
	var countryName = Ti.App.Properties.getString('destinationCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	//alert(countryName[0]);
	
	_obj.winTransactionTracker = Titanium.UI.createWindow(_obj.style.winOverview);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winTransactionTracker);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Transaction Tracker';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	_obj.step1 = Ti.UI.createView(_obj.style.step1);
	_obj.step1.top = 40;
	_obj.tblTransaction = Ti.UI.createTableView(_obj.style.tableView);
	
	_obj.tblTransaction.addEventListener('scroll',function(e){
		
		require('/utils/Console').info(_obj.page +' < '+ _obj.totalPages);
		if(_obj.page < _obj.totalPages)
		{
			if (TiGlobals.osname === 'android')
			{
		        if((e.firstVisibleItem + e.visibleItemCount) === (_obj.page * _obj.limit))
				{
					_obj.page = _obj.page + 1;
		            populateTxnStatusPaginate(_obj.selTxn);
			    }
		    }
		    else
		    {
		        var offset = e.contentOffset.y;
		        var height = e.size.height;
		        var total = offset + height;
		        var theEnd = e.contentSize.height;
		        var distance = theEnd - total;
		         
		        // going down is the only time we dynamically load,
		        // going up we can safely ignore -- note here that
		        // the values will be negative so we do the opposite
		        if (distance < _obj.lastDistance)
		        {
		            // adjust the % of rows scrolled before we decide to start fetching
		            var nearEnd = theEnd;
		 
		            if (!_obj.updating && (total >= nearEnd))
		            {
		               _obj.page = _obj.page + 1;
		               populateTxnStatusPaginate(_obj.selTxn);
		            }
		        }
		        _obj.lastDistance = distance;
		    }    
		}
	});	
	
	_obj.tblTransaction.addEventListener('click', function(e) {
		try{
			if(e.row.rw === 1)
			{
				if(e.source.sel === 'buy')
				{
					activityIndicator.showIndicator();
					
					var xhr = require('/utils/XHR');
					xhr.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"GETTRANSACTIONDETAILS",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"rtrn":"'+e.row.rtrn+'",'+
							'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
							'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
							'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
						
					function xhrSuccess(evt) {
						activityIndicator.hideIndicator();
						if(evt.result.responseFlag === "S")
						{
							if(evt.result.isVoucherBuy === 'Yes')
							{
								require('/js/my_account/VoucherBuyModal').VoucherBuyModal(evt.result);
								
							}
							else
							{
								//require('/utils/AlertDialog').showAlert('','Voucher is not available for purchase',[L('btn_ok')]).show();
							}
						}
						else
						{
							if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
							{
								require('/lib/session').session();
								destroy_transactiontracker();
							}
							else
							{
								require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
							}
							
						}
						xhr = null;
					}
			
					function xhrError(evt) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr = null;
					}
				}
				
				if(e.source.sel === 'view')
				{
					
					require('/js/my_account/TxnTrackerDetailsModal').TxnTrackerDetailsModal(e.row.rtrn);
					destroy_transactiontracker();
					//require('/js/my_account/TxnTrackerDetailsModal').TxnTrackerDetailsModal(e._obj.tblTransaction.rtrn);
				}
				
				if(e.source.sel === 'cancel')
				{
					var alertDialog = require('/utils/AlertDialog').showAlert('', 'Once you click on OK, the transaction will be cancelled.', [L('btn_ok'), L('btn_cancel')]);
					alertDialog.show();
			
					alertDialog.addEventListener('click', function(evt) {
						alertDialog.hide();
						if (evt.index === 0 || evt.index === "0") {
							activityIndicator.showIndicator();
				
							var xhr = require('/utils/XHR');
							
							xhr.call({
								url : TiGlobals.appURLTOML,
								get : '',
								post : '{' +
									'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
									'"requestName":"CANCELTRANSACTION",'+
									'"partnerId":"'+TiGlobals.partnerId+'",'+
									'"channelId":"'+TiGlobals.channelId+'",'+
									'"ipAddress":"'+TiGlobals.ipAddress+'",'+
									'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
									'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
									'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
									'"rtrn":"'+e.row.rtrn+'"'+
									'}',
								success : xhrSuccess,
								error : xhrError,
								contentType : 'application/json',
								timeout : TiGlobals.timer
							});
							
							function xhrSuccess(e) {
								require('/utils/Console').info('Result ======== ' + e.result);
								activityIndicator.hideIndicator();
								
								if(e.result.responseFlag === "S")
								{
									if(TiGlobals.osname === 'android')
									{
										require('/utils/AlertDialog').toast(e.result.message);
									}
									else
									{
										require('/utils/AlertDialog').iOSToast(e.result.message);
									}
									
									trackTxn();
								}
								else
								{
									if(e.result.displayBankMessage === L('invalid_session') || e.result.message === 'Invalid Session')
									{
										require('/lib/session').session();
										destroy_transactiontracker();
									}
									else
									{
										require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
									}
								}
								
								xhr = null;
							}
					
							function xhrError(e) {
								activityIndicator.hideIndicator();
								require('/utils/Network').Network();
								xhr = null;
							}
						}
					});
				}
				
				if(e.source.sel === 'reschedule')
				{
					require('/js/my_account/KYCRescheduleModal').KYCRescheduleModal(e.row.rtrn);
				}
			}
		}catch(e){}
	});
	
	function trackTxn()
	{
		_obj.transactionType = Ti.UI.createView(_obj.style.ddView);
		_obj.lblTransaction = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblTransaction.text = 'All Transactions';
		_obj.imgTransaction = Ti.UI.createImageView(_obj.style.imgDD);
		
		populateTxnStatus('');
		
		_obj.transactionType.addEventListener('click',function(e){
			var optionDialogTxn = require('/utils/OptionDialog').showOptionDialog('Transactions',['All Transactions','Pending Transactions','Completed Transactions','Scheduled Transactions'],-1);
			optionDialogTxn.show();
			
			optionDialogTxn.addEventListener('click',function(evt){
				if(optionDialogTxn.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblTransaction.text = optionDialogTxn.options[evt.index];
					
					switch (evt.index)
					{
						case 0:
							populateTxnStatus('');
							_obj.selTxn = '';
						break;
						
						case 1:
							populateTxnStatus('Pending');
							_obj.selTxn = 'Pending';
						break;
						
						case 2:
							populateTxnStatus('Completed');
							_obj.selTxn = 'Completed';
						break;
						
						case 3:
							populateTxnStatus('Scheduled');
							_obj.selTxn = 'Scheduled';
						break;
					}
					
					optionDialogTxn = null;
				}
				else
				{
					optionDialogTxn = null;
				}
			});
		});
		
		_obj.transactionType.add(_obj.lblTransaction);
		_obj.transactionType.add(_obj.imgTransaction);
		_obj.step1.add(_obj.transactionType);
		_obj.step1.add(_obj.tblTransaction);
	}
	
	_obj.mainView.add(_obj.step1);
	_obj.globalView.add(_obj.mainView);
	_obj.winTransactionTracker.add(_obj.globalView);
	_obj.winTransactionTracker.open();
	
	function populateTxnStatus(status)
	{
		_obj.start = 1;
		_obj.last = _obj.limit;
		
		_obj.tblTransaction.setData([]);
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"STATUSTRACKER",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"transactionFrom":"'+_obj.start+'",'+
				'"transactionTo":"'+_obj.last+'",'+
				'"transactionStatus":"'+status+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(status === 'Completed')
			{
				_obj.totalRecords = parseInt(e.result.completedCount);
			}
			else if(status === 'Pending')
			{
				_obj.totalRecords = parseInt(e.result.pendingCount);
			}
			else if(status === 'Scheduled')
			{
				_obj.totalRecords = parseInt(e.result.scheduledCount);
			}
			else
			{
				_obj.totalRecords = parseInt(e.result.totalCount);	
			}
			
			_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
			
			if(e.result.responseFlag === "S")
			{
				if(_obj.totalRecords === 0)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'track_txn',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : status === ''? 'No Transactions found' :'No ' + status + ' Transactions found',
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblTransaction.appendRow(row);
				}
				else
				{
					if(e.result.txnData.length === 1)
					{
						var reschedule = 1;
					}
					else
					{
						var reschedule = 0;
					}
					
					for(var i=0; i<e.result.txnData.length; i++)
					{
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 106,
							backgroundColor : 'transparent',
							className : 'track_txn',
							rtrn : e.result.txnData[i].rtrn,
							rw : 1
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var topView = Ti.UI.createView({
							top : 0,
							left : 0,
							right : 0,
							height : 50,
							backgroundColor : '#6f6f6f',
							layout:'horizontal',
							horizontalWrap:false
						});
						
						var detailTxnView = Ti.UI.createView({
							top : 0,
							left : 0,
							width : '50%',
							height : 50
						});
						
						var txnView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblDate = Ti.UI.createLabel({
							text : 'Date ' + e.result.txnData[i].instructionDate,
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblRTRN = Ti.UI.createLabel({
							text : 'RTRN ' + e.result.txnData[i].rtrn,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						var borderView = Ti.UI.createView({
							height : 50,
							top : 0,
							left : 0,
							width:1,
							backgroundColor:'#898989'
						});
						
						var detailSendView = Ti.UI.createView({
							left : 0,
							right : 0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblSendTxt = Ti.UI.createLabel({
							text:'Send Amount', 
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var lblAmount = Ti.UI.createLabel({
							text:e.result.txnData[i].amount, 
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('whiteFont')
						});
						
						var bottomView = Ti.UI.createView({
							top : 50,
							left : 0,
							right : 0,
							height : 56,
							backgroundColor : '#fff',
							layout:'horizontal',
							horizontalWrap:false
						});
						
						var detailBenView = Ti.UI.createView({
							top : 0,
							left : 0,
							width : '50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var beneficiaryView = Ti.UI.createView({
							left : 0,
							right:0,
							height : Ti.UI.SIZE,
							layout:'vertical'
						});
						
						var lblBeneficiaryName = Ti.UI.createLabel({
							text : e.result.txnData[i].benefeciary,
							top : 0,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('blackFont')
						});
						
						var lblStatus = Ti.UI.createLabel({
							text : e.result.txnData[i].status,
							top : 2,
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						var statusView = Ti.UI.createView({
							top : 0,
							left : 20,
							width:'50%',
							height : 56,
							backgroundColor : '#fff'
						});
						
						var statusMainView = Ti.UI.createView({
							height : 16,
							top : 10,
							left : 0,
							width:Ti.UI.SIZE,
							layout:'horizontal',
							horizontalWrap:true
						});
						
						var lblAccountStatus = Ti.UI.createLabel({
							text : 'Action: ',
							height : 16,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('blackFont'),
							sel:e.result.txnData[i].accountStatus
						});
					//if(e.result.isVoucherBuy === 'Yes'){
						var lblBuyVoucher = Ti.UI.createLabel({
							text : 'Buy Voucher',
							height : 16,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							sel:'buy'
						});
						//}else{
							
						//}
						var actionView = Ti.UI.createView({
							height : 16,
							top : 30,
							left : 0,
							width:Ti.UI.SIZE,
							layout:'horizontal',
							horizontalWrap:true
						});
						
						var lblView = Ti.UI.createLabel({
							text : 'View ',
							height : 20,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							sel:'view'
							
						});
						
						if(e.result.txnData[i].txnCancelApplicable === 'Y')
						{
							var lblCancel = Ti.UI.createLabel({
								text : '| Cancel',
								height : 20,
								top : 0,
								left : 0,
								width:Ti.UI.SIZE,
								textAlign: 'left',
								font : TiFonts.FontStyle('lblNormal12'),
								color : TiFonts.FontStyle('greyFont'),
								sel:'cancel'
							});
						}
						
						if(reschedule === 1)
						{
							var lblReSchedule = Ti.UI.createLabel({
								text : '| Call Reschedule',
								height : 20,
								top : 0,
								left : 0,
								width:Ti.UI.SIZE,
								textAlign: 'left',
								font : TiFonts.FontStyle('lblNormal12'),
								color : TiFonts.FontStyle('greyFont'),
								sel:'reschedule'
							});	
							
							//lblAccountStatus.top = 5;	
							actionView.top = 25;
							actionView.height = Ti.UI.SIZE;
						}
						
						txnView.add(lblDate);
						txnView.add(lblRTRN);
						detailTxnView.add(txnView);
						topView.add(detailTxnView);
						topView.add(borderView);
						detailSendView.add(lblSendTxt);
						detailSendView.add(lblAmount);
						topView.add(detailSendView);
						beneficiaryView.add(lblBeneficiaryName);
						beneficiaryView.add(lblStatus);
						detailBenView.add(beneficiaryView);
						bottomView.add(detailBenView);
						statusMainView.add(lblAccountStatus);
						
						//if(e.result.isVoucherBuy === 'Yes'){
						statusMainView.add(lblBuyVoucher);
						//}
						statusView.add(statusMainView);
						actionView.add(lblView);
						if(e.result.txnData[i].txnCancelApplicable === 'Y')
						{
							actionView.add(lblCancel);
						}	
						if(reschedule === 1)
						{
							actionView.add(lblReSchedule);
						}
						statusView.add(actionView);
						bottomView.add(statusView);
						row.add(topView);
						row.add(bottomView);
						_obj.tblTransaction.appendRow(row);
					}
				}
				activityIndicator.hideIndicator();
			}
			else
			{
				activityIndicator.hideIndicator();
				
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_transactiontracker();
				}
				else
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'track_txn',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblTransaction.appendRow(row);
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
		}
	}
	
	function populateTxnStatusPaginate(status)
	{
		_obj.updating = true;
		
		_obj.start = (((_obj.page * _obj.limit) + 1) - _obj.limit); 
		_obj.last = (_obj.page * _obj.limit);
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"STATUSTRACKER",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"transactionFrom":"'+_obj.start+'",'+
				'"transactionTo":"'+_obj.last+'",'+
				'"transactionStatus":"'+status+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(status === 'Completed')
			{
				_obj.totalRecords = parseInt(e.result.completedCount);
			}
			else if(status === 'Pending')
			{
				_obj.totalRecords = parseInt(e.result.pendingCount);
			}
			else if(status === 'Scheduled')
			{
				_obj.totalRecords = parseInt(e.result.scheduledCount);
			}
			else
			{
				//_obj.totalRecords = parseInt(e.result.totalCount);	
			}
			
			_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
			
			if(e.result.responseFlag === "S")
			{
				for(var i=0; i<e.result.txnData.length; i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 106,
						backgroundColor : 'transparent',
						className : 'track_txn',
						rtrn : e.result.txnData[i].rtrn,
						rw : 1
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var topView = Ti.UI.createView({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : '#6f6f6f',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailTxnView = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '50%',
						height : 50
					});
					
					var txnView = Ti.UI.createView({
						left : 0,
						right:0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblDate = Ti.UI.createLabel({
						text : 'Date ' + e.result.txnData[i].instructionDate,
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblRTRN = Ti.UI.createLabel({
						text : 'RTRN ' + e.result.txnData[i].rtrn,
						top : 2,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var borderView = Ti.UI.createView({
						height : 50,
						top : 0,
						left : 0,
						width:1,
						backgroundColor:'#898989'
					});
					
					var detailSendView = Ti.UI.createView({
						left : 0,
						right : 0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblSendTxt = Ti.UI.createLabel({
						text:'Send Amount', 
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblAmount = Ti.UI.createLabel({
						text:e.result.txnData[i].amount, 
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var bottomView = Ti.UI.createView({
						top : 50,
						left : 0,
						right : 0,
						height : 56,
						backgroundColor : '#fff',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var detailBenView = Ti.UI.createView({
						top : 0,
						left : 0,
						width : '50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var beneficiaryView = Ti.UI.createView({
						left : 0,
						right:0,
						height : Ti.UI.SIZE,
						layout:'vertical'
					});
					
					var lblBeneficiaryName = Ti.UI.createLabel({
						text : e.result.txnData[i].benefeciary,
						top : 0,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var lblStatus = Ti.UI.createLabel({
						text : e.result.txnData[i].status,
						top : 2,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var statusView = Ti.UI.createView({
						top : 0,
						left : 10,
						right : 10,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var statusMainView = Ti.UI.createView({
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						layout:'horizontal',
						horizontalWrap:true
					});
					
					var lblAccountStatus = Ti.UI.createLabel({
						text : 'Action: ',
						height : 16,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('blackFont'),
						sel:e.result.txnData[i].accountStatus
					});
					//if(e.result.isVoucherBuy === 'Yes'){
					var lblBuyVoucher = Ti.UI.createLabel({
						text : 'Buy Voucher',
						height : 16,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						sel:'buy'
					});
					//}
					var actionView = Ti.UI.createView({
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						layout:'horizontal',
						horizontalWrap:true
					});
					
					var lblView = Ti.UI.createLabel({
						text : 'View ',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont'),
						sel:'view'
						
					});
					
					if(e.result.txnData[i].txnCancelApplicable === 'Y')
					{
						var lblCancel = Ti.UI.createLabel({
							text : '| Cancel',
							height : 20,
							top : 0,
							left : 0,
							width:Ti.UI.SIZE,
							textAlign: 'left',
							font : TiFonts.FontStyle('lblNormal12'),
							color : TiFonts.FontStyle('greyFont'),
							sel:'cancel'
						});
					}
					
					txnView.add(lblDate);
					txnView.add(lblRTRN);
					detailTxnView.add(txnView);
					topView.add(detailTxnView);
					topView.add(borderView);
					detailSendView.add(lblSendTxt);
					detailSendView.add(lblAmount);
					topView.add(detailSendView);
					beneficiaryView.add(lblBeneficiaryName);
					beneficiaryView.add(lblStatus);
					detailBenView.add(beneficiaryView);
					bottomView.add(detailBenView);
					statusMainView.add(lblAccountStatus);
					
					//if(e.result.isVoucherBuy === 'Yes'){
					statusMainView.add(lblBuyVoucher);
					//}else{
						
					//}
					statusView.add(statusMainView);
					actionView.add(lblView);
					if(e.result.txnData[i].txnCancelApplicable === 'Y')
					{
						actionView.add(lblCancel);
					}	
					statusView.add(actionView);
					bottomView.add(statusView);
					row.add(topView);
					row.add(bottomView);
					_obj.tblTransaction.appendRow(row);
					_obj.updating = false;
				}
				activityIndicator.hideIndicator();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_transactiontracker();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	trackTxn();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_transactiontracker();
	});
	
	_obj.winTransactionTracker.addEventListener('androidback', function(){
		destroy_transactiontracker();
	});
	
	function destroy_transactiontracker()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove transaction tracker start ##############');
			
			_obj.winTransactionTracker.close();
			require('/utils/RemoveViews').removeViews(_obj.winTransactionTracker);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_transactiontracker',destroy_transactiontracker);
			require('/utils/Console').info('############## Remove transaction tracker end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_transactiontracker', destroy_transactiontracker);
}; // TransactionTrackerModal()
