exports.MessagesModal = function()
{
	require('/lib/analytics').GATrackScreen('Messages');
	
	var _obj = {
		style : require('/styles/my_account/Overview').Overview,
		winMessages : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		tblMessage : null,
		step1 : null
	};
	
	var countryName = Ti.App.Properties.getString('destinationCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	//alert(countryName[0]);
	
	_obj.winMessages = Titanium.UI.createWindow(_obj.style.winOverview);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winMessages);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Messages';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.step1 = Ti.UI.createView(_obj.style.step1);
	_obj.step1.top = 40;
	_obj.tblMessage = Ti.UI.createTableView(_obj.style.tableView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.step1.add(_obj.tblMessage);
	_obj.mainView.add(_obj.step1);
	_obj.globalView.add(_obj.mainView);
	_obj.winMessages.add(_obj.globalView);
	_obj.winMessages.open();
	
	function getInboxMessages()
	{
		activityIndicator.showIndicator();
					
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETINBOXMESSAGEDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});
			
		function xhrSuccess(e) {
			
			if(e.result.responseFlag === "S")
			{
				if(e.result.messageDetails.length > 0)
				{
					_obj.msgCount === 1;
					_obj.imgStep2.image = '/images/message_unsel_set.png';
					
					for(var i=0; i<e.result.messageDetails.length;i++)
					{
						var row = Ti.UI.createTableViewRow({
							top : 0,
							left : 0,
							right : 0,
							height : 50,
							backgroundColor : 'transparent',
							className : 'txn_msg',
							rw:0
						});
						
						if(TiGlobals.osname !== 'android')
						{
							row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
						}
						
						var lblMessage = Ti.UI.createLabel({
							text : require('/lib/toml_validations').removeHTMLTags(e.result.messageDetails[i].messageDate +'-' + e.result.messageDetails[i].message),
							left : 20,
							right : 20,
							height:Ti.UI.SIZE,
							width:Ti.UI.SIZE,
							textAlign: 'center',
							font : TiFonts.FontStyle('lblNormal14'),
							color : TiFonts.FontStyle('greyFont')
						});
						
						row.add(lblMessage);
						_obj.tblMessage.appendRow(row);
					}
					
				}				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_messages();
				}
				else
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'txn_msg',
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						width:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblMessage.appendRow(row); 
				}
			}
			
			activityIndicator.hideIndicator();
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	getInboxMessages();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_messages();
	});
	
	_obj.winMessages.addEventListener('androidback', function(){
		destroy_messages();
	});
	
	function destroy_messages()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove messages start ##############');
			
			_obj.winMessages.close();
			require('/utils/RemoveViews').removeViews(_obj.winMessages);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_messages',destroy_messages);
			require('/utils/Console').info('############## Remove messages end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_messages', destroy_messages);
}; // MessagesModal()