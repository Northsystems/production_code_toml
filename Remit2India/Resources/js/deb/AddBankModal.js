exports.AddBankModal = function()
{
	require('/lib/analytics').GATrackScreen('Add Bank DEB');
	
	var _obj = {
		style : require('/styles/deb/AddBank').AddBank,
		winAddBank : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		addBankView : null,
		txtFirstName : null,
		borderViewS11 : null,
		txtLastName : null,
		borderViewS12 : null,
		lblBankName : null,
		imgBankName : null,
		bankNameView : null,
		txtBankName : null,	
		borderViewS13 : null,
		txtNickName : null,
		borderViewS14 : null,
		lblAccountType : null,
		lblSaving : null,
		txtAccountNo : null,
		borderViewS15 : null,
		txtBLZCode : null,
		borderViewS16 : null,
		imgTerms : null,
		lblTerms : null,
		lblTerms1 : null,
		termsView : null,
		btnSubmit : null,
		
		bankNameOption : [],
		bankNameSelOption : [],
		bankNameSel : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winAddBank = Titanium.UI.createWindow(_obj.style.winAddBank);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winAddBank);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Add Bank Details';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
	_obj.lblSectionHead.text = 'NetBanking Transfers';
	_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblSectionHead);
	_obj.mainView.add(_obj.sectionHeaderBorder);

	/////////////////// Add Bank ///////////////////
	
	_obj.addBankView = Ti.UI.createScrollView(_obj.style.addBankView);
	
	_obj.txtFirstName = Ti.UI.createTextField(_obj.style.txtFirstName);
	_obj.txtFirstName.hintText = 'First Name*';
	_obj.txtFirstName.maxLength = 25;
	_obj.txtFirstName.value = Ti.App.Properties.getString('firstName');
	_obj.borderViewS11 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtLastName = Ti.UI.createTextField(_obj.style.txtLastName);
	_obj.txtLastName.hintText = 'Last Name*';
	_obj.txtLastName.maxLength = 25;
	_obj.txtLastName.value = Ti.App.Properties.getString('lastName');
	_obj.borderViewS12 = Ti.UI.createView(_obj.style.borderView);
	
	if(countryCode[0] === 'NL' || countryCode[0] === 'BE')
	{
		_obj.bankNameView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblBankName = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblBankName.text = 'Select a Bank Name*';
		_obj.imgBankName = Ti.UI.createImageView(_obj.style.imgDD);
	}
	else
	{
		_obj.txtBankName = Ti.UI.createTextField(_obj.style.txtBankName);
		_obj.txtBankName.hintText = 'Bank Name*';	
		_obj.txtBankName.maxLength = 25;
	}
	_obj.borderViewS13 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtNickName = Ti.UI.createTextField(_obj.style.txtNickName);
	_obj.txtNickName.hintText = 'Nick Name for Your Bank Account*';
	_obj.txtNickName.maxLength = 25;
	_obj.borderViewS14 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.lblAccountType = Ti.UI.createLabel(_obj.style.lblAccountType);
	_obj.lblAccountType.text = 'Account Type';
	
	_obj.lblSaving = Ti.UI.createLabel(_obj.style.lblAccount);
	_obj.lblSaving.text = 'Saving';
	
	_obj.doneAccountNo = Ti.UI.createButton(_obj.style.done);
	_obj.doneAccountNo.addEventListener('click',function(){
		_obj.txtAccountNo.blur();
	});
	
	_obj.txtAccountNo = Ti.UI.createTextField(_obj.style.txtAccountNo);
	_obj.txtAccountNo.hintText = 'Account Number*';
	//_obj.txtAccountNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtAccountNo.keyboardType = Titanium.UI.KEYBOARD_DECIMAL_PAD;
	_obj.txtAccountNo.keyboardToolbar = [_obj.doneAccountNo];
	_obj.txtAccountNo.maxLength = 17;
	_obj.borderViewS15 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneBLZCode = Ti.UI.createButton(_obj.style.done);
	_obj.doneBLZCode.addEventListener('click',function(){
		_obj.txtBLZCode.blur();
	});
	
	_obj.txtBLZCode = Ti.UI.createTextField(_obj.style.txtBLZCode);
	_obj.txtBLZCode.hintText = 'BLZ Code/Sort Code*';
	_obj.txtBLZCode.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtBLZCode.keyboardToolbar = [_obj.doneBLZCode];
	_obj.txtBLZCode.maxLength = 8;
	
	if(countryCode[0] === 'BE')
	{
		_obj.txtBLZCode.maxLength = 3;
	}
	else if (countryCode[0] === 'NL')
	{
		_obj.txtBLZCode.editable = false;
	}
	else if (countryCode[0] === 'GER')
	{
		_obj.txtBLZCode.maxLength = 8;
	}
	else
	{
		_obj.txtBLZCode.maxLength = 8;
	
	}
	_obj.borderViewS16 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.termsView = Ti.UI.createView(_obj.style.termsView);
	_obj.imgTerms = Ti.UI.createImageView(_obj.style.imgTerms);
	_obj.lblTerms = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblTerms1 = Ti.UI.createLabel(_obj.style.lblTerms1);
	_obj.lblTerms.text = 'I agree to the ';
	_obj.lblTerms1.text = 'Terms & Conditions';
	
	_obj.terms = 'N';
	_obj.imgTerms.addEventListener('click',function(e){
		if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
		{
			_obj.imgTerms.image = '/images/checkbox_sel.png';
			_obj.terms = 'Y';
		}
		else
		{
			_obj.imgTerms.image = '/images/checkbox_unsel.png';
			_obj.terms = 'N';
		}
	});
	
	_obj.lblTerms.addEventListener('click',function(e){
		if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
		{
			_obj.imgTerms.image = '/images/checkbox_sel.png';
			_obj.terms = 'Y';
		}
		else
		{
			_obj.imgTerms.image = '/images/checkbox_unsel.png';
			_obj.terms = 'N';
		}
	});
	
	_obj.lblTerms1.addEventListener('click',function(e){
		require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','about','terms-and-conditions');
	});
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.addBankView.add(_obj.txtFirstName);
	_obj.addBankView.add(_obj.borderViewS11);
	_obj.addBankView.add(_obj.txtLastName);
	_obj.addBankView.add(_obj.borderViewS12);
	if(countryCode[0] === 'NL' || countryCode[0] === 'BE')
	{
		_obj.bankNameView.add(_obj.lblBankName);
		_obj.bankNameView.add(_obj.imgBankName);
		_obj.addBankView.add(_obj.bankNameView);
	}
	else
	{
		_obj.addBankView.add(_obj.txtBankName);	
	}
	_obj.addBankView.add(_obj.borderViewS13);
	_obj.addBankView.add(_obj.txtNickName);
	_obj.addBankView.add(_obj.borderViewS14);
	_obj.addBankView.add(_obj.lblAccountType);
	_obj.addBankView.add(_obj.lblSaving);
	_obj.addBankView.add(_obj.txtAccountNo);
	_obj.addBankView.add(_obj.borderViewS15);
	_obj.addBankView.add(_obj.txtBLZCode);
	_obj.addBankView.add(_obj.borderViewS16);
	_obj.termsView.add(_obj.imgTerms);
	_obj.termsView.add(_obj.lblTerms);
	_obj.termsView.add(_obj.lblTerms1);
	_obj.addBankView.add(_obj.termsView);
	_obj.addBankView.add(_obj.btnSubmit);
	_obj.mainView.add(_obj.addBankView);
	_obj.globalView.add(_obj.mainView);
	_obj.winAddBank.add(_obj.globalView);
	_obj.winAddBank.open();
	
	if(countryCode[0] === 'NL' || countryCode[0] === 'BE')
	{
		_obj.bankNameView.addEventListener('click',function(e){
			
			_obj.bankNameOption = [];
			_obj.bankNameSelOption = [];
			_obj.bankNameSel = '';
			
			activityIndicator.showIndicator();
			var xhr = require('/utils/XHR_BCM');
	
			xhr.call({
				url : TiGlobals.appURLBCM,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"'+TiGlobals.bcmConfig+'",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"source":"'+TiGlobals.bcmSource+'",'+
					'"type":"bankInfo",'+
					'"platform":"'+TiGlobals.osname+'",'+
					'"corridor":"'+countryName[0]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : TiGlobals.timer
			});
	
			function xhrSuccess(e) {
				try{
				if(e.result.status === "S")
				{
					if(countryCode[0] === 'NL')
					{
						for(var i=0;i<e.result.response[0].bankInfo.NL.length;i++)
						{
							_obj.bankNameOption.push(e.result.response[0].bankInfo.NL[i].name);
							_obj.bankNameSelOption.push(e.result.response[0].bankInfo.NL[i].value);
						}
					}
					
					if(countryCode[0] === 'BE')
					{
						for(var i=0;i<e.result.response[0].bankInfo.BE.length;i++)
						{
							_obj.bankNameOption.push(e.result.response[0].bankInfo.BE[i].name);
							_obj.bankNameSelOption.push(e.result.response[0].bankInfo.BE[i].value);
						}
					}
					
					var optionDialogBankName = require('/utils/OptionDialog').showOptionDialog('Select Bank Name',_obj.bankNameOption,-1);
					optionDialogBankName.show();
					
					setTimeout(function(){
						activityIndicator.hideIndicator();
					},200);
					
					optionDialogBankName.addEventListener('click',function(evt){
						if(optionDialogBankName.options[evt.index] !== L('btn_cancel'))
						{
							_obj.lblBankName.text = optionDialogBankName.options[evt.index];
							_obj.bankNameSel = _obj.bankNameSelOption[evt.index];
							optionDialogBankName = null;
							
							if(countryCode[0] === 'NL')
							{
								var mySplitResult = _obj.bankNameSel.split("~");
							
								if(_obj.bankNameSel !== ''){
									_obj.txtBLZCode.value = mySplitResult[0];
								}else{
									_obj.txtBLZCode.value = '';
								}
							}
						}
						else
						{
							optionDialogBankName = null;
							_obj.lblBankName.text = 'Select a Bank Name*';
						}
					});
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_addbank();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
				}catch(e){}
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		});
	}
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		////////////////////// First Name ////////////////////
    	if(_obj.txtFirstName.value === '' || _obj.txtFirstName.value === 'First Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
			return;
		}
		else if(_obj.txtFirstName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in First Name field',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
	    	return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of First Name',[L('btn_ok')]).show();
    		_obj.txtFirstName.value = '';
    		_obj.txtFirstName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtFirstName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter your First Name',[L('btn_ok')]).show();
			_obj.txtFirstName.value = '';
			_obj.txtFirstName.focus();
	    	return;
		}
		else if(_obj.txtFirstName.value.length <= 1 && _obj.txtFirstName.value.length > 0)
		{	
			require('/utils/AlertDialog').showAlert('','First Name cannot be a single alphabet. Please enter your complete First Name',[L('btn_ok')]).show();
    		_obj.txtFirstName.value = '';
    		_obj.txtFirstName.focus();
    		return;
		}
		else
		{
			
		}
		
		////////////////////// Last Name ////////////////////
		if(_obj.txtLastName.value === '' || _obj.txtLastName.value === 'Last Name')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
    		return;
		}
		else if(_obj.txtLastName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Last Name field',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').validateStringValueOnly(_obj.txtLastName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
			return;
		}
		else if(_obj.txtFirstName.value.length <= 1 && _obj.txtFirstName.value.length > 0) // Last NAME SHOULD BE GREATER THEN 1
		{	
			require('/utils/AlertDialog').showAlert('','Last Name cannot be a single alphabet. Please enter your complete Last Name',[L('btn_ok')]).show();
    		_obj.txtLastName.value = '';
    		_obj.txtLastName.focus();
			return;
		}
		else
		{
			
		}
		
		//Bank Name
		
		if(countryCode[0] === 'NL' || countryCode[0] === 'BE')
		{
			if(_obj.lblBankName.text === 'Select a Bank Name*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a Bank Name',[L('btn_ok')]).show();
	    		return;
			}
			else
			{
				var mySplitResult = _obj.bankNameSel.split("~");
				_obj.bankNameSel = mySplitResult[1];
			}
		}
		else
		{
			if(_obj.txtBankName.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter Bank Name',[L('btn_ok')]).show();
	    		_obj.txtBankName.value = '';
	    		_obj.txtBankName.focus();
				return;
			}
			else if(require('/lib/toml_validations').validate_special_char(_obj.txtBankName.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Invalid special character entered for Bank Name',[L('btn_ok')]).show();
	    		_obj.txtBankName.value = '';
	    		_obj.txtBankName.focus();
				return;
			}
			else if(require('/lib/toml_validations').validateAlphaNumAtleastOneChar(_obj.txtBankName.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Bank Name',[L('btn_ok')]).show();
	    		_obj.txtBankName.value = '';
	    		_obj.txtBankName.focus();
				return;
			}
			else
			{
				_obj.bankNameSel = _obj.txtBankName.value;
			}
		}
		
		////////////////////// Nick Name ////////////////////
		if(_obj.txtNickName.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter Bank Account Nick Name',[L('btn_ok')]).show();
    		_obj.txtNickName.value = '';
    		_obj.txtNickName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtNickName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Nick Name',[L('btn_ok')]).show();
    		_obj.txtNickName.value = '';
    		_obj.txtNickName.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtNickName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Nick Name',[L('btn_ok')]).show();
    		_obj.txtNickName.value = '';
    		_obj.txtNickName.focus();
    		return;
		}
		else if(require('/lib/toml_validations').validateAlphaNumAtleastOneChar(_obj.txtNickName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in Nick Name',[L('btn_ok')]).show();
    		_obj.txtNickName.value = '';
    		_obj.txtNickName.focus();
    		return;
		}
		else
		{
			
		}
		
		//Account Number
		if(_obj.txtAccountNo.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Account Number',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAccountNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Account Number',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else if(_obj.txtAccountNo.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Account Number must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_special_char(_obj.txtAccountNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Special characters are not allowed in Account Number',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else if(require('/lib/toml_validations').validate_ispositive(_obj.txtAccountNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Account Number',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else if(_obj.txtAccountNo.value.length > 20)
		{
			require('/utils/AlertDialog').showAlert('','The Account Number cannot be greater than 20',[L('btn_ok')]).show();
    		_obj.txtAccountNo.value = '';
    		_obj.txtAccountNo.focus();
			return;
		}
		else
		{
			
		}
		
		if(countryCode[0] === 'BE')
		{
			var acctNo = _obj.txtAccountNo.value;
			var acctNoSubStr = acctNo.substr(0,3);
			
			var bnkCode = _obj.bankNameSel;
			var bnkCodeSubStr = bnkCode.substr(0,3);
	
			if(acctNoSubStr != bnkCodeSubStr)
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid bank account No.',[L('btn_ok')]).show();
	    		_obj.txtAccountNo.value = '';
	    		_obj.txtAccountNo.focus();
				return;
			}
			else
			{
				
			}
		}
		
		//BLZ Code
		if(countryCode[0] !== 'NL')
		{
			if(_obj.txtBLZCode.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter BLZ Code/Sort Code',[L('btn_ok')]).show();
	    		_obj.txtBLZCode.value = '';
	    		_obj.txtBLZCode.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtBLZCode.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of BLZ Code/Sort Code',[L('btn_ok')]).show();
	    		_obj.txtBLZCode.value = '';
	    		_obj.txtBLZCode.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtBLZCode.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of BLZ Code/Sort Code',[L('btn_ok')]).show();
	    		_obj.txtBLZCode.value = '';
	    		_obj.txtBLZCode.focus();
	    		return;
			}
			else if(require('/lib/toml_validations').validate_special_char(_obj.txtBLZCode.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Special characters are not allowed in BLZ Code/Sort Code',[L('btn_ok')]).show();
	    		_obj.txtBLZCode.value = '';
	    		_obj.txtBLZCode.focus();
				return;
			}
			else if(_obj.txtBLZCode.value.search("[^0-9]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','BLZ Code/Sort Code must be a Numeric Value',[L('btn_ok')]).show();
	    		_obj.txtBLZCode.value = '';
	    		_obj.txtBLZCode.focus();
				return;
			}
			else 
			{
				
			}
		}
		
		if (countryCode[0] === 'GER')
		{
			if(_obj.txtBLZCode.value.length < 6 || _obj.txtBLZCode.value.length > 8)
			{
				require('/utils/AlertDialog').showAlert('','BLZ Code/Sort Code must contain 6 to 8 digits',[L('btn_ok')]).show();
	    		_obj.txtBLZCode.value = '';
	    		_obj.txtBLZCode.focus();
				return;
			}
		}
		
		//Terms and Condition
		if(_obj.terms === 'N')
		{
			require('/utils/AlertDialog').showAlert('','Please accept the terms and conditions',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		var params = {
			firstName:_obj.txtFirstName.value,
		    lastName:_obj.txtLastName.value,
		    nickName:_obj.txtNickName.value,
		    bankName:_obj.bankNameSel,
		    accountType:'S',
		    accountNumber:_obj.txtAccountNo.value,
		    sortCode:_obj.txtBLZCode.value
		};
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"SENDERBANKACCADDPRECONF",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"countryCode":"'+countryCode[0]+'",'+
				'"currencyCode":"'+countryCode[1]+'",'+
				'"paymodeCode":"DEB",'+
				'"firstName":"'+params.firstName+'",'+
			    '"lastName":"'+params.lastName+'",'+
			    '"bankName":"'+params.bankName+'",'+
			    '"bankAccNickName":"'+params.nickName+'",'+
			    '"accountType":"'+params.accountType+'",'+
			    '"accountNumber":"'+params.accountNumber+'",'+
			    '"sortCode":"'+params.sortCode+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				params = {
					firstName:_obj.txtFirstName.value,
				    lastName:_obj.txtLastName.value,
				    nickName:_obj.txtNickName.value,
				    bankName:_obj.bankNameSel,
				    accountType:'S',
				    accountNumber:_obj.txtAccountNo.value,
				    sortCode:_obj.txtBLZCode.value,
				    referenceNo:e.result.referenceNo
				};
				
				require('/js/deb/AddBankPreConfirmationModal').AddBankPreConfirmationModal(params);
				destroy_addbank();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_addbank();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_addbank();
	});
	
	_obj.winAddBank.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_addbank();
				alertDialog = null;
			}
		});
	});
	
	function destroy_addbank()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove add bank start ##############');
			
			_obj.winAddBank.close();
			require('/utils/RemoveViews').removeViews(_obj.winAddBank);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_addbank',destroy_addbank);
			require('/utils/Console').info('############## Remove add bank end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_addbank', destroy_addbank);
}; // AddBankModal()