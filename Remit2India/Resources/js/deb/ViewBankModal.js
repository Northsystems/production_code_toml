exports.ViewBankModal = function()
{
	require('/lib/analytics').GATrackScreen('View Bank Accounts DEB');
	
	var _obj = {
		style : require('/styles/deb/ViewBank').ViewBank,
		winViewBank : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		bankAccountView : null,
		tblBankAccounts : null,
		
		start : 0,
		last : 0,
		page : 1,
		limit : 10,
		totalPages : 1,
		totalRecords : 0,
		lastDistance : 0,
		updating : false,
	};
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//var origCC = origSplit[0]+'-'+origSplit[1];
	//alert(countryName[0]);
	
	_obj.winViewBank = Titanium.UI.createWindow(_obj.style.winViewBank);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winViewBank);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'View Bank Details';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
	_obj.lblSectionHead.text = 'NetBanking Transfers';
	_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblSectionHead);
	_obj.mainView.add(_obj.sectionHeaderBorder);

	/////////////////// Add Bank ///////////////////
	
	_obj.bankAccountView = Ti.UI.createView(_obj.style.bankAccountView);
	
	_obj.tblBankAccounts = Ti.UI.createTableView(_obj.style.tableView);
	
	_obj.tblBankAccounts.addEventListener('scroll',function(e){
		
		require('/utils/Console').info(_obj.page +' < '+ _obj.totalPages);
		if(_obj.page < _obj.totalPages)
		{
			if (TiGlobals.osname === 'android')
			{
		        if((e.firstVisibleItem + e.visibleItemCount) === (_obj.page * _obj.limit))
				{
					_obj.page = _obj.page + 1;
		            populateBankAccountsPaginate();
			    }
		    }
		    else
		    {
		        var offset = e.contentOffset.y;
		        var height = e.size.height;
		        var total = offset + height;
		        var theEnd = e.contentSize.height;
		        var distance = theEnd - total;
		         
		        // going down is the only time we dynamically load,
		        // going up we can safely ignore -- note here that
		        // the values will be negative so we do the opposite
		        if (distance < _obj.lastDistance)
		        {
		            // adjust the % of rows scrolled before we decide to start fetching
		            var nearEnd = theEnd;
		 
		            if (!_obj.updating && (total >= nearEnd))
		            {
		               _obj.page = _obj.page + 1;
		               populateBankAccountsPaginate(_obj.selTxn);
		            }
		        }
		        _obj.lastDistance = distance;
		    }    
		}
	});
	
	_obj.tblBankAccounts.addEventListener('click',function(e){
		
		var params = {
			accName:e.row.accName,
			accType:e.row.accType,
			bankName:e.row.bankName,
			accNo:e.row.accNo,
			bankCode:e.row.bankCode,
			accId  : e.row.accId,
			pg:'bank'
		};
		
		if(e.source.sel === 'view')
		{
			require('/js/deb/ViewBankDetailsModal').ViewBankDetailsModal(params);
		}
		else if(e.source.sel === 'delete')
		{
			var alertDialog = require('/utils/AlertDialog').showAlert('', 'Once you click on OK, the account will be deleted.', [L('btn_ok'), L('btn_cancel')]);
			alertDialog.show();
	
			alertDialog.addEventListener('click', function(evt) {
				alertDialog.hide();
				if (evt.index === 0 || evt.index === "0") {
					activityIndicator.showIndicator();
		
					var xhr = require('/utils/XHR');
					
					xhr.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"SENDERBANKACCDELETE",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
							'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
							'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
							'"accountNo":"",'+ 
							'"accountId":"'+e.row.accId+'",'+
							'"payModeCode":"DEB",'+ 
							'"routingNo":"'+e.row.routNo+'",'+
							'"recordType":""'+
							'}',
						success : xhrSuccess,
						error : xhrError,
						contentType : 'application/json',
						timeout : TiGlobals.timer
					});
					
					function xhrSuccess(e) {
						try{
						require('/utils/Console').info('Result ======== ' + e.result);
						activityIndicator.hideIndicator();
						
						if(e.result.responseFlag === "S")
						{
							if(TiGlobals.osname === 'android')
							{
								require('/utils/AlertDialog').toast(e.result.displayBankMessage);
							}
							else
							{
								require('/utils/AlertDialog').iOSToast(e.result.displayBankMessage);
							}
							
							populateBankAccounts();
						}
						else
						{
							if(e.result.displayBankMessage === L('invalid_session') || e.result.displayBankMessage === 'Invalid Session')
							{
								require('/lib/session').session();
								destroy_viewbank();
							}
							else
							{
								require('/utils/AlertDialog').showAlert('',e.result.displayBankMessage,[L('btn_ok')]).show();
							}
						}
						
						xhr = null;
						}catch(e){}
					}
			
					function xhrError(e) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr = null;
					}
				}
			});
		}
		else
		{
			
		}
	});
	
	function populateBankAccounts() {
		activityIndicator.showIndicator();
		
		_obj.tblBankAccounts.setData([]);
		_obj.start = 1;
		_obj.last = _obj.limit;
		
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETSENDERBANKACCLISTINFO",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"origCountryCode":"'+origSplit[0]+'",'+ 
				'"origCurrencyCode":"'+origSplit[1]+'",'+
				'"rangeFrom":"'+_obj.start+'",'+
				'"rangeTo":"'+_obj.last+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			require('/utils/Console').info('Result ======== ' + e.result);
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				_obj.totalRecords = parseInt(e.result.totalCount);	
				_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
				
				for(var i=0; i<e.result.bankData.length; i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 96,
						backgroundColor : 'transparent',
						className : 'bank_account',
						accName:e.result.bankData[i].accountHolder,
						accType:e.result.bankData[i].accountType,
						bankName:e.result.bankData[i].bankName,
						accNo:e.result.bankData[i].accountNumber,
						bankCode:e.result.bankData[i].bankCode,
						accId:e.result.bankData[i].id,
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var topView = Ti.UI.createView({
						top : 0,
						left : 0,
						right : 0,
						height : 40,
						backgroundColor : '#6f6f6f',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var lblAccountHolderName = Ti.UI.createLabel({
						text : e.result.bankData[i].accountHolder,
						height : 40,
						top : 0,
						left : 20,
						width:'50%',
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var bottomView = Ti.UI.createView({
						top : 40,
						left : 0,
						right : 0,
						height : 56,
						backgroundColor : '#fff',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var bankView = Ti.UI.createView({
						top : 0,
						left : 20,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var lblBankName = Ti.UI.createLabel({
						text : e.result.bankData[i].bankName,
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var lblBankCode = Ti.UI.createLabel({
						text : e.result.bankData[i].bankCode,
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var statusView = Ti.UI.createView({
						top : 0,
						left : 20,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var lblAccountStatus = Ti.UI.createLabel({
						text : e.result.bankData[i].accountStatus,
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('blackFont'),
						sel:e.result.bankData[i].accountStatus
					});
					
					var actionView = Ti.UI.createView({
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var lblAction = Ti.UI.createLabel({
						text : 'Action: ',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var lblView = Ti.UI.createLabel({
						text : 'View ',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont'),
						sel:'view'
						
					});
					
					var lblDelete = Ti.UI.createLabel({
						text : '| Delete',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont'),
						sel:'delete'
					});
					
					topView.add(lblAccountHolderName);
					bankView.add(lblBankName);
					bankView.add(lblBankCode);
					bottomView.add(bankView);
					statusView.add(lblAccountStatus);
					actionView.add(lblAction);
					actionView.add(lblView);
					
					if(e.result.bankData[i].accountStatus === 'Active' && e.result.bankData[i].displayActionStatus === 'Delete')
					{
						actionView.add(lblDelete);	
					}
					
					statusView.add(actionView);
					bottomView.add(statusView);
					row.add(topView);
					row.add(bottomView);
					_obj.tblBankAccounts.appendRow(row);
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_viewbank();
				}
				else
				{
					_obj.tblBankAccounts.height = 50;
			
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'account_details',
						rw:''
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblBankAccounts.appendRow(row);
				}
			}
			
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();			
			xhr = null;
		}
	}
	
	function populateBankAccountsPaginate() {
		activityIndicator.showIndicator();
		
		_obj.updating = true;
		
		_obj.start = (((_obj.page * _obj.limit) + 1) - _obj.limit); 
		_obj.last = (_obj.page * _obj.limit);
		
		var xhr = require('/utils/XHR');

		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETSENDERBANKACCLISTINFO",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"origCountryCode":"'+origSplit[0]+'",'+ 
				'"origCurrencyCode":"'+origSplit[1]+'",'+
				'"rangeFrom":"'+_obj.start+'",'+
				'"rangeTo":"'+_obj.last+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : TiGlobals.timer
		});

		function xhrSuccess(e) {
			try{
			require('/utils/Console').info('Result ======== ' + e.result);
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				_obj.totalRecords = parseInt(e.result.totalCount);	
				_obj.totalPages = Math.ceil(parseInt(_obj.totalRecords)/_obj.limit); // Calculate total pages
				
				for(var i=0; i<e.result.bankData.length; i++)
				{
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 96,
						backgroundColor : 'transparent',
						className : 'bank_account',
						accName:e.result.bankData[i].accountHolder,
						accType:e.result.bankData[i].accountType,
						bankName:e.result.bankData[i].bankName,
						accNo:e.result.bankData[i].accountNumber,
						bankCode:e.result.bankData[i].bankCode,
						accId:e.result.bankData[i].id,
						rw:0
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var topView = Ti.UI.createView({
						top : 0,
						left : 0,
						right : 0,
						height : 40,
						backgroundColor : '#6f6f6f',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var lblAccountHolderName = Ti.UI.createLabel({
						text : e.result.bankData[i].accountHolder,
						height : 40,
						top : 0,
						left : 20,
						width:'50%',
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					var bottomView = Ti.UI.createView({
						top : 40,
						left : 0,
						right : 0,
						height : 56,
						backgroundColor : '#fff',
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var bankView = Ti.UI.createView({
						top : 0,
						left : 20,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var lblBankName = Ti.UI.createLabel({
						text : e.result.bankData[i].bankName,
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var lblBankCode = Ti.UI.createLabel({
						text : e.result.bankData[i].bankCode,
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var statusView = Ti.UI.createView({
						top : 0,
						left : 20,
						width:'50%',
						height : 56,
						backgroundColor : '#fff'
					});
					
					var lblAccountStatus = Ti.UI.createLabel({
						text : e.result.bankData[i].accountStatus,
						height : 16,
						top : 10,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('blackFont'),
						sel:e.result.bankData[i].accountStatus
					});
					
					var actionView = Ti.UI.createView({
						height : 16,
						top : 30,
						left : 0,
						width:Ti.UI.SIZE,
						layout:'horizontal',
						horizontalWrap:false
					});
					
					var lblAction = Ti.UI.createLabel({
						text : 'Action: ',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					var lblView = Ti.UI.createLabel({
						text : 'View ',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont'),
						sel:'view'
						
					});
					
					var lblDelete = Ti.UI.createLabel({
						text : '| Delete',
						height : 20,
						top : 0,
						left : 0,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal10'),
						color : TiFonts.FontStyle('blackFont'),
						sel:'delete'
					});
					
					topView.add(lblAccountHolderName);
					bankView.add(lblBankName);
					bankView.add(lblBankCode);
					bottomView.add(bankView);
					statusView.add(lblAccountStatus);
					actionView.add(lblAction);
					actionView.add(lblView);
					
					if(e.result.bankData[i].accountStatus === 'Active' && e.result.bankData[i].displayActionStatus === 'Delete')
					{
						actionView.add(lblDelete);	
					}
					
					statusView.add(actionView);
					bottomView.add(statusView);
					row.add(topView);
					row.add(bottomView);
					_obj.tblBankAccounts.appendRow(row);
					_obj.updating = false;
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_viewbank();
				}
				else
				{
					_obj.tblBankAccounts.height = 50;
			
					var row = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 50,
						backgroundColor : 'transparent',
						className : 'account_details',
						rw:''
					});
					
					if(TiGlobals.osname !== 'android')
					{
						row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblMessage = Ti.UI.createLabel({
						text : e.result.message,
						left : 20,
						right : 20,
						height:Ti.UI.SIZE,
						textAlign: 'center',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					row.add(lblMessage);
					_obj.tblBankAccounts.appendRow(row);
				}
			}
			
			xhr = null;
			}catch(e){}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();			
			xhr = null;
		}
	}
	
	populateBankAccounts();
	
	Ti.App.addEventListener('populateBankAccounts',populateBankAccounts);
	
	_obj.mainView.add(_obj.bankAccountView);
	_obj.bankAccountView.add(_obj.tblBankAccounts);
	_obj.globalView.add(_obj.mainView);
	_obj.winViewBank.add(_obj.globalView);
	_obj.winViewBank.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_viewbank();
	});
	
	function destroy_viewbank()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove view bank list start ##############');
			
			_obj.winViewBank.close();
			require('/utils/RemoveViews').removeViews(_obj.winViewBank);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('populateBankAccounts',populateBankAccounts);
			Ti.App.removeEventListener('destroy_viewbank',destroy_viewbank);
			require('/utils/Console').info('############## Remove view bank list end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_viewbank', destroy_viewbank);
}; // ViewBankModal()