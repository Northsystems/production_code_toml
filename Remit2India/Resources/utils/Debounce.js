exports.debounce = function(obj)
{
	var currentTime = new Date().getTime();
	//Ti.API.info('Curr Time = ' + currentTime);
    if (parseInt(currentTime - obj.clickTime) < 1000) {
    	//Ti.API.info('Diff Time = ' + parseInt(currentTime - obj.clickTime));
        return false;
    }
    obj.clickTime = currentTime;
    return true;
};