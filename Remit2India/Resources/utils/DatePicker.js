exports.DatePicker = function(obj, condition) {
	var today = require('/utils/Date').today('/', 'dmy').split('/');

	if (condition === 'dob18') {
		var _minDate = new Date();
		_minDate.setFullYear(1920);
		_minDate.setMonth(0);
		_minDate.setDate(01);

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear() - 18);
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate());
		
		var value = _maxDate;
	} 
	else if (condition === 'normal') {
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear());
		_minDate.setMonth(_minDate.getMonth());
		_minDate.setDate(_minDate.getDate());

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear() + 10);
		_maxDate.setFullYear(_maxDate.getFullYear());
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate() + 14);
		
		var value = _maxDate;
	}
	else {
		var _minDate = new Date();
		_minDate.setFullYear(_minDate.getFullYear());
		_minDate.setMonth(_minDate.getMonth());
		_minDate.setDate(_minDate.getDate());

		var _maxDate = new Date();
		_maxDate.setFullYear(_maxDate.getFullYear());
		_maxDate.setMonth(_maxDate.getMonth());
		_maxDate.setDate(_maxDate.getDate()+15);
		
		var value = _minDate;
	}

	if (TiGlobals.osname === 'android') {
		Ti.UI.Android.hideSoftKeyboard();

		var _picker = Ti.UI.createPicker({
			top : 10,
			left : 10,
			right : 10,
			type : Ti.UI.PICKER_TYPE_DATE,
			minDate : _minDate,
			maxDate : _maxDate,
			backgroundColor : '#fff',
			value : value 
		});

		_picker.showDatePickerDialog({
			value : value,
			callback : function(e) {
				if (e.cancel) {

				} else {
					var _pickerdateSplit = [];
					_pickerdateSplit = e.value.toLocaleString().split(' ');
					//Ti.API.info(e.value);
					Ti.App.Properties.setObject('dt', e.value);
					var day = _pickerdateSplit[2];
					var month = require('/utils/Date').monthNo(_pickerdateSplit[1]);
					var year = _pickerdateSplit[3];

					obj.text = day + '/' + month + '/' + year;
					Ti.App.fireEvent('calculateAge',{data:e.value});
				}
			}
		});
	} else {
		var _winPicker = Ti.UI.createWindow({
			backgroundColor : 'transparent'
		});

		var _viewPickerModal = Ti.UI.createScrollView({
			top : '0dp',
			contentHeight : Ti.UI.SIZE,
			backgroundColor : 'transparent',
			disableBounce : true,
			scrollType : 'vertical'
		});

		var _codeViewPicker = Ti.UI.createView({
			backgroundColor : '#6f6f6f',
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			borderRadius : 5,
			layout : 'vertical'
		});

		var _picker = Ti.UI.createPicker({
			top : 10,
			width : (TiGlobals.platformWidth - 20),
			type : Ti.UI.PICKER_TYPE_DATE,
			backgroundColor : '#fff',
			minDate : _minDate,
			maxDate : _maxDate,
			value : new Date(parseInt(today[2]), parseInt(today[1] - 1), parseInt(today[0]))
		});

		var _btnPickerView = Titanium.UI.createView({
			top : 10,
			height : 35,
			left : 10,
			right : 10,
			bottom : 10
		});

		var _btnPickerCancel = Titanium.UI.createButton({
			title : L('btn_cancel'),
			left : '0dp',
			height : 35,
			width : '48%',
			textAlign : 'center',
			font : TiFonts.FontStyle('lblBold14'),
			color : TiFonts.FontStyle('whiteFont'),
			borderRadius : 3,
			backgroundColor : '#202020'
		});

		var _btnPickerModal = Ti.UI.createButton({
			title : L('btn_proceed'),
			right : '0dp',
			height : 35,
			width : '48%',
			textAlign : 'center',
			font : TiFonts.FontStyle('lblBold14'),
			color : TiFonts.FontStyle('whiteFont'),
			borderRadius : 3,
			backgroundColor : '#f4343b'
		});

		_codeViewPicker.add(_picker);
		_btnPickerView.add(_btnPickerCancel);
		_btnPickerView.add(_btnPickerModal);
		_codeViewPicker.add(_btnPickerView);
		_viewPickerModal.add(_codeViewPicker);
		_winPicker.add(_viewPickerModal);
		_winPicker.open();

		_btnPickerCancel.addEventListener('click', function(e) {
			_winPicker.close();
			_winPicker.remove(_viewPickerModal);
			_codeViewPicker.remove(_picker);
			_btnPickerView.remove(_btnPickerCancel);
			_btnPickerView.remove(_btnPickerModal);
			_codeViewPicker.remove(_btnPickerView);
			_viewPickerModal.remove(_codeViewPicker);

			_winPicker = null;
			_viewPickerModal = null;
			_picker = null;
			_btnPickerCancel = null;
			_btnPickerModal = null;
			_btnPickerView = null;
			_codeViewPicker = null;
			_pickerdate = null;
		});

		var _pickerdate = '';

		_picker.addEventListener('change', function(e) {
			_pickerdate = e.value.toLocaleString();
		});

		_btnPickerModal.addEventListener('click', function(e) {
			if (_pickerdate !== '') {
				_pickerdate = _picker.value;
				var day = _pickerdate.getDate();
				day = day.toString();

				if (day.length < 2) {
					day = '0' + day;
				}

				var month = _pickerdate.getMonth();
				month = month + 1;
				month = month.toString();

				if (month.length < 2) {
					month = '0' + month;
				}

				var year = _pickerdate.getFullYear();
				obj.text = day + '/' + month + '/' + year;

				_winPicker.close();
				_winPicker.remove(_viewPickerModal);
				_codeViewPicker.remove(_picker);
				_btnPickerView.remove(_btnPickerCancel);
				_btnPickerView.remove(_btnPickerModal);
				_codeViewPicker.remove(_btnPickerView);
				_viewPickerModal.remove(_codeViewPicker);

				_winPicker = null;
				_viewPickerModal = null;
				_picker = null;
				_btnPickerCancel = null;
				_btnPickerModal = null;
				_btnPickerView = null;
				_codeViewPicker = null;
				_pickerdate = null;
			} else {
				
				if (condition === 'dob18') {
					_pickerdate = _picker.maxDate;
				} else {
					_pickerdate = _picker.minDate;
				}
				
				var day = _pickerdate.getDate();
				day = day.toString();

				if (day.length < 2) {
					day = '0' + day;
				}

				var month = _pickerdate.getMonth();
				month = month + 1;
				month = month.toString();

				if (month.length < 2) {
					month = '0' + month;
				}

				var year = _pickerdate.getFullYear();
				obj.text = day + '/' + month + '/' + year;
				
				_winPicker.close();
				_winPicker.remove(_viewPickerModal);
				_codeViewPicker.remove(_picker);
				_btnPickerView.remove(_btnPickerCancel);
				_btnPickerView.remove(_btnPickerModal);
				_codeViewPicker.remove(_btnPickerView);
				_viewPickerModal.remove(_codeViewPicker);

				_winPicker = null;
				_viewPickerModal = null;
				_picker = null;
				_btnPickerCancel = null;
				_btnPickerModal = null;
				_btnPickerView = null;
				_codeViewPicker = null;
				_pickerdate = null;
			}
		});

		_picker.selectionIndicator = true;
	}
}; 