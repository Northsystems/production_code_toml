exports.today = function(separator,format)
{
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	
	if(dd<10) {
	    dd='0'+dd;
	} 
	
	if(mm<10) {
	    mm='0'+mm;
	} 
	
	if(format === 'ymd')
	{
		today = yyyy + separator + mm + separator + dd;
	}
	else if(format === 'dmy')
	{
		today = dd + separator + mm + separator + yyyy;	
	}
	else
	{
		today = mm + separator + dd + separator + yyyy;
	}
	
	return today;
};

// Month

function month(val){
	var m;
	switch(val)
	{
		case '01':
		m = "January";
		break;
		
		case '02':
		m = "February";
		break;
		
		case '03':
		m = "March";
		break;
		
		case '04':
		m = "April";
		break;
		
		case '05':
		m = "May";
		break;
		
		case '06':
		m = "June";
		break;
		
		case '07':
		m = "July";
		break;
		
		case '08':
		m = "August";
		break;
		
		case '09':
		m = "September";
		break;
		
		case '10':
		m = "October";
		break;
		
		case '11':
		m = "November";
		break;
		
		case '12':
		m = "December";
		break;
		
		default:
		m = "Month of Purchase";
		break;
	}
	return m;
};

exports.monthShort = function (val){
	var m;
	switch(val)
	{
		case '01':
		m = "Jan";
		break;
		
		case '02':
		m = "Feb";
		break;
		
		case '03':
		m = "Mar";
		break;
		
		case '04':
		m = "Apr";
		break;
		
		case '05':
		m = "May";
		break;
		
		case '06':
		m = "Jun";
		break;
		
		case '07':
		m = "Jul";
		break;
		
		case '08':
		m = "Aug";
		break;
		
		case '09':
		m = "Sep";
		break;
		
		case '10':
		m = "Oct";
		break;
		
		case '11':
		m = "Nov";
		break;
		
		case '12':
		m = "Dec";
		break;
		
		default:
		m = "";
		break;
	}
	return m;
};

exports.monthNo = function (val){
	var m;
	switch(val.toUpperCase())
	{
		case 'JAN':
		m = "01";
		break;
		
		case 'FEB':
		m = "02";
		break;
		
		case 'MAR':
		m = "03";
		break;
		
		case 'APR':
		m = "04";
		break;
		
		case 'MAY':
		m = "05";
		break;
		
		case 'JUN':
		m = "06";
		break;
		
		case 'JUL':
		m = "07";
		break;
		
		case 'AUG':
		m = "08";
		break;
		
		case 'SEP':
		m = "09";
		break;
		
		case 'OCT':
		m = "10";
		break;
		
		case 'NOV':
		m = "11";
		break;
		
		case 'DEC':
		m = "12";
		break;
		
		default:
		m = "";
		break;
	}
	return m;
};

exports.time12Hrs = function()
{
	var date = new Date();
	var hours = date.getHours();
  	var minutes = date.getMinutes();
  	var ampm = hours >= 12 ? 'PM' : 'AM';
  	hours = hours % 12;
  	hours = hours ? hours : 12; // the hour '0' should be '12'
  	minutes = minutes < 10 ? '0'+minutes : minutes;
  	
  	if(hours<10) {
	    hours='0'+hours;
	} 
	
	if(minutes<10) {
	    minutes='0'+minutes;
	} 
  	
  	var strTime = hours + ':' + minutes + ' ' + ampm;
  	return strTime;
};

exports.time24Hrs = function()
{
	var date = new Date();
	var hours = date.getHours();
  	var minutes = date.getMinutes();
  	
  	if(hours<10) {
	    hours='0'+hours;
	} 
	
	if(minutes<10) {
	    minutes='0'+minutes;
	} 
  	
  	var strTime = hours + ':' + minutes;
  	return strTime;
};

function monthNo(val){
	var m;
	switch(val)
	{
		case 'Jan':
		m = "01";
		break;
		
		case 'Feb':
		m = "02";
		break;
		
		case 'Mar':
		m = "03";
		break;
		
		case 'Apr':
		m = "04";
		break;
		
		case 'May':
		m = "05";
		break;
		
		case 'Jun':
		m = "06";
		break;
		
		case 'Jul':
		m = "07";
		break;
		
		case 'Aug':
		m = "08";
		break;
		
		case 'Sep':
		m = "09";
		break;
		
		case 'Oct':
		m = "10";
		break;
		
		case 'Nov':
		m = "11";
		break;
		
		case 'Dec':
		m = "12";
		break;
		
		default:
		m = "";
		break;
	}
	return m;
}

exports.validateDate = function(D1, D2, D3, to_date)
{	
	 var servdate_val = to_date.split(',');
	 var date = servdate_val[0]; 
	 var month = servdate_val[1];
	 var year = servdate_val[2];
	 var frmdate = D1;
	 var frmmonth = D2;
	 var frmyear = D3;
	
	 if((frmdate !="") && (frmmonth != "") && (frmyear != ""))
	 { 	
		if(parseFloat(frmyear) < parseFloat(year))
		{
			require('/utils/AlertDialog').showAlert('',"The 'Start Date' for Easy EMI payments should be a future date. Please select a date later than today's date",[L('btn_ok')]).show();
			return false;
		}
	    else
		{
		   if(parseFloat(frmyear) === parseFloat(year))
		   {			  
				if(parseFloat(frmmonth) < parseFloat(month))
				{
					require('/utils/AlertDialog').showAlert('',"The 'Start Date' for Recurring Payment should be a future date. Please select a date later than today's date",[L('btn_ok')]).show();
					return false;
				}
				if(parseFloat(frmmonth) == parseFloat(month))
				{
					if(parseFloat(frmdate) <= parseFloat(date))
					{
					 	require('/utils/AlertDialog').showAlert('',"The 'Start Date' for Recurring Payment should be a future date. Please select a date later than today's date",[L('btn_ok')]).show();
						return false;
					}
				}
		   }
		   
		   	var diff = parseInt(frmmonth) - parseInt(month);
			if(year !== frmyear)
			{
				if((parseInt(diff) > 6) || (parseInt(diff) > -6))
				{
					require('/utils/AlertDialog').showAlert('','Please book an SI within 6 months from the present date',[L('btn_ok')]).show();
					return false;
				}
			}
			else
			{
				if(parseInt(diff) > 6)
				{
					require('/utils/AlertDialog').showAlert('','Please book an SI within 6 months from the present date',[L('btn_ok')]).show();
					return false;
				}
			}
		}
	  }
};