// Controls flow of Pages

exports.pageController = function(pgToDisplay)
{
	Ti.App.fireEvent('sections',{'data':pgToDisplay});
};

exports.pageBackController = function(pgToDestroy)
{
	if(pgToDestroy !== 'dashboard')
	{
		_scrollableView.movePrevious();
		setTimeout(function(){
			Ti.App.fireEvent('destroy_'+pgToDestroy);
			_scrollableView.removeView(_scrollableView.views[viewArr.length-1]);
			viewArr.pop();
		},300);
	}
};