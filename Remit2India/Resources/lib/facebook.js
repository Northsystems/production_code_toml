exports.facebookAuthentication = function(_self,callbacktype,callback){  // callbacktype === page/modal/function
	
	if(Ti.Network.online)
	{
		fb.initialize(1000);
		fb.permissions = ['email'];
		
		fb.addEventListener('login', FBLogin = function(evt) {
			if (evt.success) {
		        fb.requestWithGraphPath('me',{fields:'id,first_name,last_name,email'}, 'GET', function(evnt) {
		        	if (evnt.success) {
				    	var result = JSON.parse(evnt.result);
				    	
				    	var params = {
				    		social:'Facebook',
				    		fname:result.first_name,
				    		lname:result.last_name,
				    		email:result.email
				    	};
				    	
				    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
				    } else if (evnt.error) {
				    	alert(evnt.error);
				    } else {
				    }
				});
			} else if (evt.error) {
		    	alert(evt.error);
		    } else if (evt.cancelled) {
		    }
		    else{
		    	require('/utils/AlertDialog').showAlert('',Ti.App.name + L('allowFB'),[L('btn_ok')]).show();
		    }
		    
		    fb.removeEventListener('login', FBLogin);
			
		});
		
		if (!fb.loggedIn) 
		{
			fb.authorize();
		}
		else
		{
	        fb.requestWithGraphPath('me',{fields:'id,first_name,last_name,email'}, 'GET', function(evnt) {
	        	if (evnt.success) {
			    	var result = JSON.parse(evnt.result);
			    	
			    	var params = {
			    		social:'Facebook',
			    		fname:result.first_name,
			    		lname:result.last_name,
			    		email:result.email
			    	};
			    	
			    	require('/js/RegisterSocialModal').RegisterSocialModal(params);
			    } else if (evnt.error) {
			    	alert(evnt.error);
			    } else {
			    }
			});
		}
	}
	else
	{
		require('/utils/Network').Network();
	}
};