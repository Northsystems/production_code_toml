
////////////////// Logged In ///////////////////////

exports.loggedIn = function () {
  Ti.App.Properties.getString('ownerId') === null?Ti.App.Properties.setString('ownerId','0'):'';
  Ti.App.Properties.getString('loginId') === null?Ti.App.Properties.setString('loginId','0'):'';
  Ti.App.Properties.getString('loginStatus') === null?Ti.App.Properties.setString('loginStatus','0'):'';
  
  //Source Country
  Ti.App.Properties.getString('sourceCountryCurCode') === null?Ti.App.Properties.setString('sourceCountryCurCode','0'):'';
  Ti.App.Properties.getString('sourceCountryCurName') === null?Ti.App.Properties.setString('sourceCountryCurName','0'):'';
  Ti.App.Properties.getString('sourceCountryInfoEmail') === null?Ti.App.Properties.setString('sourceCountryInfoEmail','0'):'';
  Ti.App.Properties.getString('sourceCountryISDN') === null?Ti.App.Properties.setString('sourceCountryISDN','0'):'';
  Ti.App.Properties.getString('sourceCountryTollFree') === null?Ti.App.Properties.setString('sourceCountryTollFree','0'):'';  
  
  //Destination Country
  Ti.App.Properties.getString('destinationCountryCurName') === null?Ti.App.Properties.setString('destinationCountryCurName','India'):'';
  Ti.App.Properties.getString('destinationCountryCurCode') === null?Ti.App.Properties.setString('destinationCountryCurCode','IND-INR'):'';
  Ti.App.Properties.getString('destinationCountryISDN') === null?Ti.App.Properties.setString('destinationCountryISDN','91'):'';
  
  Ti.App.Properties.getString('deliveryMode') === null?Ti.App.Properties.setString('deliveryMode','DC'):'';
  
  // Paymode Code & Name
  
  Ti.App.Properties.getString('indicativePaymodeName') === null?Ti.App.Properties.setString('indicativePaymodeName',''):'';
  Ti.App.Properties.getString('indicativePaymodeCode') === null?Ti.App.Properties.setString('indicativePaymodeCode',''):'';
  
  Ti.App.Properties.getString('guaranteedPaymodeName') === null?Ti.App.Properties.setString('guaranteedPaymodeName',''):'';
  Ti.App.Properties.getString('guaranteedPaymodeCode') === null?Ti.App.Properties.setString('guaranteedPaymodeCode',''):'';
  
  //mPIN
  Ti.App.Properties.getString('mPIN') === null?Ti.App.Properties.setString('mPIN',''):'';
};

////////////////// UUID ///////////////////////

exports.UUID = function () {
  Ti.App.Properties.getString('UUID') === null?Ti.App.Properties.setString('UUID',Ti.Platform.createUUID()):'';
};

////////////////// Location //////////////////

exports.location = function (){
	
	Ti.App.Properties.getString('cityId') === null?Ti.App.Properties.setString('cityId','207637'):'';
	Ti.App.Properties.getString('cityName') === null?Ti.App.Properties.setString('cityName','Zurich'):'';
	Ti.App.Properties.getString('cityLat') === null?Ti.App.Properties.setString('cityLat','47.3667'):'';
	Ti.App.Properties.getString('cityLon') === null?Ti.App.Properties.setString('cityLon','8.55'):'';
};

////////////////// Timestamp //////////////////

exports.timestamp = function (){
	
	if(Ti.App.Properties.getString('timestamp') === null)
	{
		Ti.App.Properties.setString('timestamp',Math.floor(new Date().getTime()/1000));
	}
};