 exports.getCurrentPosition = function(fireEvent)
{
	function locationServices()
	{
		if (Ti.Geolocation.locationServicesEnabled) 
		{
			if(TiGlobals.osname === 'android')
			{
				Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_HIGH;
			}
			else
			{
				Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
			    Ti.Geolocation.distanceFilter = 10;
			    Ti.Geolocation.purpose = 'Get nearby bars';
			}
			
			function updatePosition(e)
			{
				if (!e.success || e.error) 
			    {
			        //Ti.API.error("[LocationManager] " + (new Date()) + " Error: " + JSON.stringify (e.error));
			        var result = {
						error : 1,
						lat : 0,
						lon : 0
					};
					
			       return result;
			    } 
			    else 
			    {
			    	require('/utils/Console').info("[LocationManager] lat,lon: " + e.coords.latitude + ", " + e.coords.longitude);
			        require('/utils/Console').info(e.coords);
			        
			        var result = {
						error : 0,
						lat : e.coords.latitude,
						lon : e.coords.longitude
					};
					
			       Ti.App.fireEvent(fireEvent,result);
			    }
			    
			    Ti.Geolocation.removeEventListener('location', updatePosition);    
			}
			
			Ti.Geolocation.addEventListener('location', updatePosition);
		} 
		else 
		{	
			if(TiGlobals.osname === 'android')
			{
				require('/utils/AlertDialog').showAlert('Location Services','Please enable location services from settings',['OK']).show();
			}
			else
			{
				require('/utils/AlertDialog').showAlert('Location Services','Please enable location services from Settings -> General -> Restrictions -> Location Services',['OK']).show();
			}
		}
	}
	
	if(TiGlobals.osname === 'android')
	{
		locationServices();
	}
	else
	{
		var authorization = Titanium.Geolocation.locationServicesAuthorization;
		require('/utils/Console').info('Authorization: ' + authorization);
		if (authorization == Titanium.Geolocation.AUTHORIZATION_DENIED) {
			var alertDialog = require('/utils/AlertDialog').showAlert('Location Services','Please enable location services from Settings -> General -> Restrictions -> Location Services',['OK']);
			alertDialog.show();
			
			alertDialog.addEventListener('click', function(evt) {
				alertDialog.hide();
				if (evt.index === 0 || evt.index === "0") {
					if (Ti.Platform.canOpenURL(Ti.App.iOS.applicationOpenSettingsURL)) {
					    Ti.Platform.openURL(Ti.App.iOS.applicationOpenSettingsURL);
					}
				}
			});
			
			var result = {
				error : 1,
				lat : 0,
				lon : 0
			};
				
			Ti.App.fireEvent(fireEvent,result);
            return;
		}
		else if (authorization == Titanium.Geolocation.AUTHORIZATION_RESTRICTED) {
			var alertDialog = require('/utils/AlertDialog').showAlert('Location Services','Please enable location services from Settings -> General -> Restrictions -> Location Services',['OK']);
			alertDialog.show();
			
			alertDialog.addEventListener('click', function(evt) {
				alertDialog.hide();
				if (evt.index === 0 || evt.index === "0") {
					if (Ti.Platform.canOpenURL(Ti.App.iOS.applicationOpenSettingsURL)) {
					    Ti.Platform.openURL(Ti.App.iOS.applicationOpenSettingsURL);
					}
				}
			});
			
			var result = {
				error : 1,
				lat : 0,
				lon : 0
			};
				
			Ti.App.fireEvent(fireEvent,result);
            return;
		}
		else
		{
			locationServices();
		}
		
		/*Ti.Geolocation.getCurrentPosition(function(e) {
	        if (e.error) {
	        	var alertDialog = require('/utils/AlertDialog').showAlert('Location Services','Please enable location services from Settings -> General -> Restrictions -> Location Services',['OK']);
				alertDialog.show();
				
				alertDialog.addEventListener('click', function(evt) {
					alertDialog.hide();
					if (evt.index === 0 || evt.index === "0") {
						if (Ti.Platform.canOpenURL(Ti.App.iOS.applicationOpenSettingsURL)) {
						    Ti.Platform.openURL(Ti.App.iOS.applicationOpenSettingsURL);
						}
					}
				});
				
				var result = {
					error : 1,
					lat : 0,
					lon : 0
				};
					
				Ti.App.fireEvent(fireEvent,result);
	            return;
	        }else{
				locationServices();
	        }
	    });*/
	}
};
