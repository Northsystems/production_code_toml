// Image Loader with fadeIn-fadeOut effect

exports.ImageLoader = function(parentView, imageView, imageURL, style)
{
	var activityIndicator = Ti.UI.createActivityIndicator({
		style:style
	});
	
	parentView.add(activityIndicator);
	activityIndicator.show();
	
	imageView.addEventListener("load",function () {
		if(imageView.toBlob() !== null) {
			try{
		        activityIndicator.hide();
		        imageView.animate({
		        	opacity:1.0,
		        	duration:300
		        });
		        
	       }catch(e){require('/utils/Console').info(e);}
	    }
	});
	
	imageView.setImage(imageURL);
};
