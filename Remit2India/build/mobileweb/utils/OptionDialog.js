exports.showOptionDialog = function(title,options,selected)
{
	var optionDialog = Ti.UI.createOptionDialog();
	
	options.push(L('btn_cancel'));
	
	optionDialog.setTitle(title);
	optionDialog.options = options;
	optionDialog.setCancel(options.length-1);
	
	//optionDialog.selectedIndex = selected === -1 ? options.length-1 : selected;
	
	if(TiGlobals.osname === 'iphone' || TiGlobals.osname === 'ipad')
	{
		optionDialog.opaquebackground = true;
	}
	
	return optionDialog;
};