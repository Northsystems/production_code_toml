function MyAccount(_self) {
	Ti.App.Properties.setString('pg', 'myaccount');
	
	pushView(Ti.App.Properties.getString('pg'));
	require('/lib/analytics').GATrackScreen(Ti.App.Properties.getString('pg'));

	var _obj = {
		style : require('/styles/MyAccount').MyAccount, // style
		globalView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		tblMyAccount : null,
		
		bankType : null,
	};
	
	Ti.App.fireEvent('showLabel', {
		data : L('dashboard')
	});
	Ti.App.fireEvent('menuColor', {
		data : 'dashboard'
	});
	// Menu select
	Ti.App.fireEvent('showHeader');
	Ti.App.fireEvent('showMenu');
	Ti.App.fireEvent('showSearch');
	
	if(TiGlobals.osname !== 'android')
	{
		Ti.App.fireEvent('hideBack');
	}
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	//////////////////////////// Create UI ////////////////////////////
		
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);

	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'My Account';
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack);
	
	_obj.imgBack.addEventListener('click',function(){
		popView();
	});
	
	_obj.tblMyAccount = Ti.UI.createTableView(_obj.style.tableView);
	
	function myAccount()
	{
		for(var i=0; i<10; i++)
		{
			var row = Ti.UI.createTableViewRow({
				top : 0,
				left : 0,
				right : 0,
				height : 130,
				backgroundColor : i%2 === 0 ? '#efeff0' : '#e3e3e3',
				className : 'my_account',
				rw:i
			});
			
			if(TiGlobals.osname !== 'android')
			{
				row.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
			}
			
			var imgView = Ti.UI.createImageView({
				width : 99,
				height : 67
			});
			
			switch (i)
			{
				case 0:
					imgView.image = '/images/my_overview.png';
				break;
				
				case 1:
					imgView.image = '/images/my_profile.png';
				break;
				
				case 2:
					imgView.image = '/images/my_bank_account.png';
				break;
				
				case 3:
					imgView.image = '/images/my_transaction_tracker.png';
				break;
				
				case 4:
					imgView.image = '/images/my_messages.png';
				break;
				
				case 5:
					imgView.image = '/images/my_recipients.png';
				break;
				
				case 6:
					imgView.image = '/images/my_schedule_payments.png';
				break;
				
				case 7:
					imgView.image = '/images/my_perks.png';
				break;
				
				case 8:
					imgView.image = '/images/change_mpin.png';
				break;
				
				case 9:
					imgView.image = '/images/my_logout.png';
				break;	
			}

			var imgArrow = Ti.UI.createImageView({
				image : '/images/link_arrow_red.png',
				right : 20,
				width : 10,
				height : 15
			});
			
			row.add(imgView);
			row.add(imgArrow);
			_obj.tblMyAccount.appendRow(row);
		}
	}
	
	myAccount();
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.tblMyAccount);
	_obj.globalView.add(_obj.mainView);
	
	_obj.tblMyAccount.addEventListener('click', function(event){
		switch (event.row.rw)
		{
			case 0:
				//Overview
				if(Titanium.Network.online)
				{
					require('/js/my_account/OverviewModal').OverviewModal();
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 1:
				//My Profile
				if(Titanium.Network.online)
				{
					require('/js/my_account/MyProfileModal').MyProfileModal();
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 2:
				//Bank Account
							
				if(countryCode[0] === 'US')
				{
					var optionDialogBank = require('/utils/OptionDialog').showOptionDialog('Bank Account',['Add Bank Details','View Bank Details'],-1);
					optionDialogBank.show();
					
					optionDialogBank.addEventListener('click',function(evt){
						if(optionDialogBank.options[evt.index] !== L('btn_cancel'))
						{
							if(evt.index === 0)
							{
								// First check KYC then ACH Add Bank Account
								
								activityIndicator.showIndicator();
		
								var xhr = require('/utils/XHR');
								xhr.call({
									url : TiGlobals.appURLTOML,
									get : '',
									post : '{' +
										'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
										'"requestName":"GETTRANSACTIONINFO",'+
										'"partnerId":"'+TiGlobals.partnerId+'",'+
										'"channelId":"'+TiGlobals.channelId+'",'+
										'"ipAddress":"'+TiGlobals.ipAddress+'",'+
										'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
										'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
										'"paymodeCode":"",'+
										'"originatingCountry":"'+origSplit[0]+'",'+ 
										'"originatingCurrency":"'+origSplit[1]+'",'+
										'"destinationCountry":"'+destSplit[0]+'",'+
										'"destinationCurrency":"'+destSplit[1]+'"'+
										'}',
									success : xhrSuccess,
									error : xhrError,
									contentType : 'application/json',
									timeout : 10000
								});
						
								function xhrSuccess(e) {
									activityIndicator.hideIndicator();
									if(e.result.responseFlag === "S")
									{
										if(e.result.txnParameter[0].isKYCDetailsPresent === 'No')
										{
											require('/js/kyc/us/KYCModal').KYCModal('bank');
										}
										else
										{
											require('/js/ach/AddBankModal').AddBankModal();											
										}
									}
									else
									{
										if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
										{
											require('/lib/session').session();
										}
										else
										{
											require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
										}
									}
								}
						
								function xhrError(e) {
									activityIndicator.hideIndicator();
									require('/utils/Network').Network();
									xhr = null;
								}	
							}
							else
							{
								require('/js/ach/ViewBankModal').ViewBankModal();
							}
							optionDialogBank = null;
						}
						else
						{
							optionDialogBank = null;
						}
					});				
				}
				else if(countryCode[0] === 'UK' || countryCode[0] === 'NL' || countryCode[0] === 'GER' || countryCode[0] === 'BE')
				{
					var optionDialogBank = require('/utils/OptionDialog').showOptionDialog('Bank Account',['Add Bank Details','View Bank Details'],-1);
					optionDialogBank.show();
					
					optionDialogBank.addEventListener('click',function(evt){
						if(optionDialogBank.options[evt.index] !== L('btn_cancel'))
						{
							if(evt.index === 0)
							{
								require('/js/deb/AddBankModal').AddBankModal();
							}
							else
							{
								require('/js/deb/ViewBankModal').ViewBankModal();
							}
							optionDialogBank = null;
						}
						else
						{
							optionDialogBank = null;
						}
					});	
				}
				else
				{
					require('/utils/AlertDialog').showAlert('','You can not add bank details for this corridor',[L('btn_ok')]).show();
				}
			break;
			
			case 3:
				if(Titanium.Network.online)
				{
					if(Ti.App.Properties.getString('loginStatus') !== '0')
					{
						require('/js/my_account/TransactionTrackerModal').TransactionTrackerModal();
					}
					else
					{
						if(Ti.App.Properties.getString('mPIN') === '1')
						{
							require('/js/Passcode').Passcode();
						}
						else
						{
							require('/js/LoginModal').LoginModal();
						}
					}
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 4:
				//Messages
				if(Titanium.Network.online)
				{
					require('/js/my_account/MessagesModal').MessagesModal();
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 5:
				if(Titanium.Network.online)
				{
					var optionDialogBank = require('/utils/OptionDialog').showOptionDialog('Recipients',['Add Recipients','View Recipients'],-1);
					optionDialogBank.show();
					
					optionDialogBank.addEventListener('click',function(evt){
						if(optionDialogBank.options[evt.index] !== L('btn_cancel'))
						{
							if(evt.index === 0)
							{
								require('/js/recipients/AddRecipientModal').AddRecipientModal();												
							}
							else
							{
								require('/js/recipients/ViewRecipientModal').ViewRecipientModal();
							}
							optionDialogBank = null;
						}
					});
				} 
				else 
				{
					require('/utils/Network').Network();
				}				
			break;
			
			case 6:
				if(Titanium.Network.online)
				{
					if(countryCode[0] === 'US')
					{
						if(Ti.App.Properties.getString('loginStatus') !== '0')
						{
							require('/js/schedule_payment/ScheduleTransactionModal').ScheduleTransactionModal();
						}
						else
						{
							if(Ti.App.Properties.getString('mPIN') === '1')
							{
								require('/js/Passcode').Passcode();
							}
							else
							{
								require('/js/LoginModal').LoginModal();
							}
						}
					}
					else
					{
						require('/utils/AlertDialog').showAlert('','Schedule Payments is currently available for customers transacting from United States',[L('btn_ok')]).show();
					}
				} 
				else 
				{
					require('/utils/Network').Network();
				}
			break;
			
			case 7:
				require('/js/MORModal').MORModal();
			break;
			
			case 8:
				if(Ti.App.Properties.getString('mPIN') === '1')
				{
					require('/js/ChangeMPINModal').ChangeMPINModal();
				}
				else
				{
					var alertDialog = require('/utils/AlertDialog').showAlert('', 'You have not created mPIN for this login. Do you wish to create an mPIN?', [L('btn_yes'),L('btn_no')]);
					alertDialog.show();
				
					alertDialog.addEventListener('click', function(e) {
						alertDialog.hide();
						if(e.index === 0 || e.index === "0") 
						{
							require('/js/CreateMPINModal').CreateMPINModal();
						}
						alertDialog=null;
					});
				}
			break;
			
			case 9:
				// Logout
				var alertDialog = require('/utils/AlertDialog').showAlert('', L('logout'), [L('btn_yes'),L('btn_no')]);
				alertDialog.show();
			
				alertDialog.addEventListener('click', function(e) {
					alertDialog.hide();
					if(e.index === 0 || e.index === "0") 
					{
						Ti.App.Properties.setString('loginStatus','0');
						
						//Ti.App.Properties.setString('ownerId','0');
						//Ti.App.Properties.setString('loginId','0');
						//Ti.App.Properties.setString('sessionId','0');
						
						Ti.App.fireEvent('loginData');
						require('/utils/RemoveViews').removeAllScrollableViews();
					}
					alertDialog=null;
				});
			break;
		}
	});
		
	function destroy_myaccount() {
		try {
			if (_obj.globalView === null) {
				return;
			}

			require('/utils/Console').info('############## Remove myaccount start ##############');

			require('/utils/RemoveViews').removeViews(_obj.globalView);
			_obj = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_myaccount', destroy_myaccount);
			require('/utils/Console').info('############## Remove myaccount end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}


	Ti.App.addEventListener('destroy_myaccount', destroy_myaccount);

	return _obj.globalView;

};// MyAccount()

module.exports = MyAccount;