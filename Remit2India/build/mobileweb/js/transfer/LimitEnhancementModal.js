exports.LimitEnhancementModal = function(conf)
{
	require('/lib/analytics').GATrackScreen('Limit Enhancement');
	
	var _obj = {
		style : require('/styles/transfer/LimitEnhancement').LimitEnhancement,
		winLimitEnhancement : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		scrollableView : null,
		
		// Instruction View
		
		instructionView : null,
		lblInstructions : null,
		lblInstructions : null,
		lblInstruction1 : null,
		lblInstruction2 : null,
		lblInstruction3 : null,
		noteView : null,
		lblNote : null,
		lblPt1 : null,
		btnStart : null,
		
		//Step 1
		stepView1 : null,
		lblStateHead : null,
		lblState : null,
		txtEmail : null,
		borderViewS11 : null,
		txtPassport : null,
		borderViewS12 : null,
		lblMarital : null,
		imgMarital : null,
		maritalView : null,
		borderViewS13 : null,
		lblWorkPhone : null,
		txtCC : null,
		txtAC : null,
		txtNo : null,
		phoneView : null,
		borderCC : null,
		borderAC : null,
		borderNo : null,
		phoneBorderView : null,
		lblDayPhone : null,
		txtDayCC : null,
		txtDayAC : null,
		txtDayNo : null,
		dayView : null,
		borderDayCC : null,
		borderDayAC : null,
		borderDayNo : null,
		phoneDayBorderView : null,
		lblMobilePhone : null,
		txtMobileCC : null,
		txtMobileNo : null,
		mobileView : null,
		borderMobileCC : null,
		borderMobileNo : null,
		phoneMobileBorderView : null,
		lblSSNTxt : null,
		txtSSN1 : null,
		borderSSN1 : null,
		txtSSN2 : null,
		borderSSN2 : null,
		txtSSN3 : null,
		borderSSN3 : null,
		ssnTxtView : null,
		btnStep1 : null,
		
		//Step 2
		stepView2 : null,
		txtOrganization : null,
		borderViewS21 : null,		
		txtDesignation : null,
		borderViewS22 : null,
		txtSource : null,
		borderViewS23 : null,
		lblStay : null,
		imgStay : null,
		stayView : null,
		borderViewS24 : null,
		lblIncome : null,
		imgIncome : null,
		incomeView : null,
		borderViewS25 : null,
		lblAmount : null,
		imgAmount : null,
		amountView : null,
		borderViewS26 : null,
		btnStep2 : null,
		
		// Step 3
		stepView3 : null,
		lblReceiverNameHead : null,
		lblReceiver : null,
		lblPaymodeHead : null,
		lblPaymode : null,
		lblPurpose : null,
		imgPurpose : null,
		purposeView : null,
		borderViewS31 : null,
		txtRelation : null,
		borderViewS32 : null,
		lblSendingAmountHead : null,
		lblSendingAmount : null,
		btnStep3 : null,
		
		//Step 4
		stepView4 : null,
		lblKYCTxt1 : null,
		lblDOB : null,
		imgDOB : null,
		dobView : null,
		borderViewS41 : null,
		lblTime1 : null,
		imgTime1 : null,
		lblTimeOR : null,
		timeView1 : null,
		lblTime2 : null,
		imgTime2 : null,
		timeView2 : null,
		ddTimeView : null,
		borderViewS42 : null,
		borderViewS43 : null,
		ddBorderTimeView : null,
		lblNoteTime : null,
		lblNote1 : null,
		lblNote2 : null,
		noteView : null,
		btnStep4 : null,
		
		purposeOptions : [],
		purposeCodeOptions : [],
		purposeCode : null,
	
		selStay : null,
		selIncome : null,
		selAmount : null,
		timeOptions : [],
		purposeOptions : [],
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.dob = require('/utils/Date').today('/','dmy').split('/');
	
	_obj.winLimitEnhancement = Titanium.UI.createWindow(_obj.style.winLimitEnhancement);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winLimitEnhancement);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Help us know you better';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	// Instruction View
	
	function instructions() 
	{
		_obj.instructionView = Ti.UI.createScrollView(_obj.style.instructionView);
	
		_obj.lblInstructions = Ti.UI.createLabel(_obj.style.lblInstructions);
		_obj.lblInstructions.text = 'Instructions';
		
		_obj.lblInstruction1 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblInstruction1.text = '\u00B7 Fill out all details in the Know Your Customer(KYC) form displayed below. After filling up this form, you will be allowed to book a single transaction for the total amount you have requested for.';
		
		_obj.lblInstruction2 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblInstruction2.text = '\u00B7 Go ahead and book one single transaction for your requested amount - immediately. Remember, your limits will be reset after this transaction so do not enter an amount lesser than you have requested.';
		
		_obj.lblInstruction3 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblInstruction3.text = '\u00B7 A Remit2india Compliance Officer will get in touch with you either by phone or email to verify the details and approve your transaction. Please respond to this call or email at the earliest.';
		
		_obj.noteView = Ti.UI.createView(_obj.style.noteView);
		
		_obj.lblNote = Ti.UI.createLabel(_obj.style.lblNote);
		_obj.lblNote.text = 'Note';
		
		_obj.lblPt1 = Ti.UI.createLabel(_obj.style.lblPt);
		_obj.lblPt1.text = '\u00B7 Unlike before, you do not need to wait for our representative to inform you of your limit increase - you can transact for your requested amount immediately. This is convenient for you and helps process your transaction faster. Moreover, you do not need any assistance from a Remit2India representative to book this transaction.';
		_obj.lblPt1.bottom = 20;
		
		_obj.btnStart = Ti.UI.createButton(_obj.style.btnStep);
		_obj.btnStart.title = 'START NOW';
		
		_obj.instructionView.add(_obj.lblInstructions);
		_obj.instructionView.add(_obj.lblInstruction1);
		_obj.instructionView.add(_obj.lblInstruction2);
		_obj.instructionView.add(_obj.lblInstruction3);
		_obj.noteView.add(_obj.lblNote);
		_obj.noteView.add(_obj.lblPt1);
		_obj.instructionView.add(_obj.noteView);
		_obj.instructionView.add(_obj.btnStart);
		_obj.mainView.add(_obj.instructionView);	
		
		_obj.btnStart.addEventListener('click',function(){
			_obj.mainView.remove(_obj.instructionView);
			_obj.instructionView.remove(_obj.lblInstructions);
			_obj.instructionView.remove(_obj.lblInstruction1);
			_obj.instructionView.remove(_obj.lblInstruction2);
			_obj.instructionView.remove(_obj.lblInstruction3);
			_obj.noteView.remove(_obj.lblNote);
			_obj.noteView.remove(_obj.lblPt1);
			_obj.instructionView.remove(_obj.noteView);
			_obj.instructionView.remove(_obj.btnStart);
			
			limitEnhancement();
		});
	}
	
	instructions();
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	_obj.globalView.add(_obj.mainView);
	_obj.winLimitEnhancement.add(_obj.globalView);
	_obj.winLimitEnhancement.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_limitenhancement();
	});
	
	function limitEnhancement()
	{
		_obj.tabView = Ti.UI.createView(_obj.style.tabView);
		_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
		_obj.lblStep1 = Ti.UI.createLabel(_obj.style.lblStep);
		_obj.lblStep1.text = 'Step 1';
		_obj.lblStep2 = Ti.UI.createLabel(_obj.style.lblStep);
		_obj.lblStep2.text = 'Step 2';
		_obj.lblStep2.left = 1;
		_obj.lblStep3 = Ti.UI.createLabel(_obj.style.lblStep);
		_obj.lblStep3.text = 'Step 3';
		_obj.lblStep3.left = 1;
		_obj.lblStep4 = Ti.UI.createLabel(_obj.style.lblStep);
		_obj.lblStep4.text = 'Step 4';
		_obj.lblStep4.left = 1;
		_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
		_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
		_obj.lblStep1.color = TiFonts.FontStyle('whiteFont');
		_obj.lblStep1.backgroundColor = '#6F6F6F';
		_obj.lblStep2.color = TiFonts.FontStyle('blackFont');
		_obj.lblStep2.backgroundColor = '#E9E9E9';
		_obj.lblStep3.color = TiFonts.FontStyle('blackFont');
		_obj.lblStep3.backgroundColor = '#E9E9E9';
		_obj.lblStep4.color = TiFonts.FontStyle('blackFont');
		_obj.lblStep4.backgroundColor = '#E9E9E9';
		
		_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
		_obj.lblSectionHead.text = 'Personal Details';
		_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder); 
		
		_obj.tabView.add(_obj.lblStep1);
		_obj.tabView.add(_obj.lblStep2);
		_obj.tabView.add(_obj.lblStep3);
		_obj.tabView.add(_obj.lblStep4);
		_obj.mainView.add(_obj.tabView);
		_obj.mainView.add(_obj.tabSelView);
		_obj.mainView.add(_obj.tabSelIconView);
		_obj.tabSelIconView.add(_obj.imgTabSel);
		_obj.mainView.add(_obj.lblSectionHead);
		_obj.mainView.add(_obj.sectionHeaderBorder);
	
		_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
		
		/////////////////// Step 1 ///////////////////
		
		_obj.stepView1 = Ti.UI.createScrollView(_obj.style.stepView1);
		
		_obj.lblStateHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblStateHead.text = 'State';
		_obj.lblState = Ti.UI.createLabel(_obj.style.lblState);
		
		_obj.txtEmail = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtEmail.editable = false;
		_obj.txtEmail.top = 5;
		_obj.borderViewS11 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtPassport = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtPassport.maxLength = 9;
		_obj.txtPassport.hintText = 'Passport (Not for US Customers)';
		_obj.borderViewS12 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.maritalView = Ti.UI.createView(_obj.style.maritalView);
		_obj.lblMarital = Ti.UI.createLabel(_obj.style.lblMarital);
		_obj.lblMarital.text = 'Marital Status';
		_obj.imgMarital = Ti.UI.createImageView(_obj.style.imgMarital);
		_obj.borderViewS13 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.maritalView.addEventListener('click',function(e){
			var optionDialogMarital = require('/utils/OptionDialog').showOptionDialog('Marital Status',['Single','Married'],-1);
			optionDialogMarital.show();
			
			setTimeout(function(){
				activityIndicator.hideIndicator();
			},200);
			
			optionDialogMarital.addEventListener('click',function(evt){
				if(optionDialogMarital.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblMarital.text = optionDialogMarital.options[evt.index];
					optionDialogMarital = null;
				}
				else
				{
					optionDialogMarital = null;
					_obj.lblMarital.text = 'Marital Status';
				}
			});	
		});
		
		_obj.lblWorkPhone = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblWorkPhone.text = 'Work Phone';
		
		_obj.doneWCC = Ti.UI.createButton(_obj.style.done);
		_obj.doneWCC.addEventListener('click',function(){
			_obj.txtCC.blur();
		});
		
		_obj.doneWAC = Ti.UI.createButton(_obj.style.done);
		_obj.doneWAC.addEventListener('click',function(){
			_obj.txtAC.blur();
		});
		
		_obj.doneWNo = Ti.UI.createButton(_obj.style.done);
		_obj.doneWNo.addEventListener('click',function(){
			_obj.txtNo.blur();
		});
		
		_obj.phoneView = Ti.UI.createView(_obj.style.phoneView);
		_obj.txtCC = Ti.UI.createTextField(_obj.style.txtCC);
		_obj.txtCC.hintText = 'Country C';
		_obj.txtCC.value = Ti.App.Properties.getString('sourceCountryISDN');
		_obj.txtCC.editable = false;
		_obj.txtCC.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtCC.keyboardToolbar = [_obj.doneWCC];
		
		_obj.txtAC = Ti.UI.createTextField(_obj.style.txtAC);
		_obj.txtAC.hintText = 'Area C';
		_obj.txtAC.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtAC.keyboardToolbar = [_obj.doneWAC];
		
		_obj.txtNo = Ti.UI.createTextField(_obj.style.txtNo);
		_obj.txtNo.hintText = 'Work Phone';
		_obj.txtNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtNo.keyboardToolbar = [_obj.doneWNo];
		
		_obj.phoneBorderView = Ti.UI.createView(_obj.style.phoneBorderView);
		_obj.borderCC = Ti.UI.createView(_obj.style.borderCC);
		_obj.borderAC = Ti.UI.createView(_obj.style.borderAC);
		_obj.borderNo = Ti.UI.createView(_obj.style.borderNo);
		
		_obj.lblDayPhone = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblDayPhone.text = 'Day Time Phone';
		
		_obj.doneDCC = Ti.UI.createButton(_obj.style.done);
		_obj.doneDCC.addEventListener('click',function(){
			_obj.txtDayCC.blur();
		});
		
		_obj.doneDAC = Ti.UI.createButton(_obj.style.done);
		_obj.doneDAC.addEventListener('click',function(){
			_obj.txtDayAC.blur();
		});
		
		_obj.doneDNo = Ti.UI.createButton(_obj.style.done);
		_obj.doneDNo.addEventListener('click',function(){
			_obj.txtDayNo.blur();
		});
		
		_obj.dayView = Ti.UI.createView(_obj.style.phoneView);
		_obj.txtDayCC = Ti.UI.createTextField(_obj.style.txtCC);
		_obj.txtDayCC.hintText = 'Country C';
		_obj.txtDayCC.value = Ti.App.Properties.getString('sourceCountryISDN');
		_obj.txtDayCC.editable = false;
		_obj.txtDayCC.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtDayCC.keyboardToolbar = [_obj.doneDCC];
		
		_obj.txtDayAC = Ti.UI.createTextField(_obj.style.txtAC);
		_obj.txtDayAC.hintText = 'Area C';
		_obj.txtDayAC.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtDayAC.keyboardToolbar = [_obj.doneDAC];
		
		_obj.txtDayNo = Ti.UI.createTextField(_obj.style.txtNo);
		_obj.txtDayNo.hintText = 'Day Time Phone';
		_obj.txtDayNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtDayNo.keyboardToolbar = [_obj.doneDNo];
		
		_obj.phoneDayBorderView = Ti.UI.createView(_obj.style.phoneBorderView);
		_obj.borderDayCC = Ti.UI.createView(_obj.style.borderCC);
		_obj.borderDayAC = Ti.UI.createView(_obj.style.borderAC);
		_obj.borderDayNo = Ti.UI.createView(_obj.style.borderNo);
		
		_obj.lblMobilePhone = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblMobilePhone.text = 'Mobile';
		
		_obj.doneMCC = Ti.UI.createButton(_obj.style.done);
		_obj.doneMCC.addEventListener('click',function(){
			_obj.txtMobileCC.blur();
		});
		
		_obj.doneMNo = Ti.UI.createButton(_obj.style.done);
		_obj.doneMNo.addEventListener('click',function(){
			_obj.txtMobileNo.blur();
		});
		
		_obj.mobileView = Ti.UI.createView(_obj.style.phoneView);
		_obj.txtMobileCC = Ti.UI.createTextField(_obj.style.txtCC);
		_obj.txtMobileCC.hintText = 'Country C';
		_obj.txtMobileCC.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtMobileCC.keyboardToolbar = [_obj.doneMCC];
		
		_obj.txtMobileNo = Ti.UI.createTextField(_obj.style.txtNo);
		_obj.txtMobileNo.hintText = 'Mobile';
		_obj.txtMobileNo.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtMobileNo.keyboardToolbar = [_obj.doneMNo];
		
		_obj.phoneMobileBorderView = Ti.UI.createView(_obj.style.phoneBorderView);
		_obj.borderMobileCC = Ti.UI.createView(_obj.style.borderCC);
		_obj.borderMobileNo = Ti.UI.createView(_obj.style.borderNo);
		
		_obj.lblSSNTxt = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblSSNTxt.text = 'Social Security Number';
		
		_obj.ssnTxtView = Ti.UI.createView(_obj.style.ssnTxtView);
		
		_obj.doneSSN1 = Ti.UI.createButton(_obj.style.done);
		_obj.doneSSN1.addEventListener('click',function(){
			_obj.txtSSN1.blur();
		});
		
		_obj.txtSSN1 = Ti.UI.createTextField(_obj.style.txtSSN1);
		_obj.txtSSN1.hintText = 'xxx';
		_obj.txtSSN1.maxLength = 3;
		_obj.txtSSN1.passwordMask = true;
		_obj.txtSSN1.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtSSN1.keyboardToolbar = [_obj.doneSSN1];
		_obj.borderSSN1 = Ti.UI.createView(_obj.style.borderSSN1);
		
		_obj.doneSSN2 = Ti.UI.createButton(_obj.style.done);
		_obj.doneSSN2.addEventListener('click',function(){
			_obj.txtSSN2.blur();
		});
		
		_obj.txtSSN2 = Ti.UI.createTextField(_obj.style.txtSSN2);
		_obj.txtSSN2.hintText = 'xx';
		_obj.txtSSN2.maxLength = 2;
		_obj.txtSSN2.passwordMask = true;
		_obj.txtSSN2.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtSSN2.keyboardToolbar = [_obj.doneSSN2];
		_obj.borderSSN2 = Ti.UI.createView(_obj.style.borderSSN2);
		
		_obj.doneSSN3 = Ti.UI.createButton(_obj.style.done);
		_obj.doneSSN3.addEventListener('click',function(){
			_obj.txtSSN3.blur();
		});
		
		_obj.txtSSN3 = Ti.UI.createTextField(_obj.style.txtSSN3);
		_obj.txtSSN3.hintText = 'xxxx';
		_obj.txtSSN3.maxLength = 4;
		_obj.txtSSN3.passwordMask = true;
		_obj.txtSSN3.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtSSN3.keyboardToolbar = [_obj.doneSSN3];
		_obj.borderSSN3 = Ti.UI.createView(_obj.style.borderSSN3);
		
		_obj.btnStep1 = Ti.UI.createButton(_obj.style.btnStep);
		_obj.btnStep1.title = 'CONTINUE';
		
		_obj.stepView1.add(_obj.lblStateHead);
		_obj.stepView1.add(_obj.lblState);
		_obj.stepView1.add(_obj.txtEmail);
		_obj.stepView1.add(_obj.borderViewS11);
		_obj.stepView1.add(_obj.txtPassport);
		_obj.stepView1.add(_obj.borderViewS12);
		_obj.maritalView.add(_obj.lblMarital);
		_obj.maritalView.add(_obj.imgMarital);
		_obj.stepView1.add(_obj.maritalView);
		_obj.stepView1.add(_obj.borderViewS13);
		
		_obj.stepView1.add(_obj.lblWorkPhone);
		_obj.phoneView.add(_obj.txtCC);
		_obj.phoneView.add(_obj.txtAC);
		_obj.phoneView.add(_obj.txtNo);
		_obj.stepView1.add(_obj.phoneView);
		_obj.phoneBorderView.add(_obj.borderCC);
		_obj.phoneBorderView.add(_obj.borderAC);
		_obj.phoneBorderView.add(_obj.borderNo);
		_obj.stepView1.add(_obj.phoneBorderView);
		
		_obj.stepView1.add(_obj.lblDayPhone);
		_obj.dayView.add(_obj.txtDayCC);
		_obj.dayView.add(_obj.txtDayAC);
		_obj.dayView.add(_obj.txtDayNo);
		_obj.stepView1.add(_obj.dayView);
		_obj.phoneDayBorderView.add(_obj.borderDayCC);
		_obj.phoneDayBorderView.add(_obj.borderDayAC);
		_obj.phoneDayBorderView.add(_obj.borderDayNo);
		_obj.stepView1.add(_obj.phoneDayBorderView);
		
		_obj.stepView1.add(_obj.lblMobilePhone);
		_obj.mobileView.add(_obj.txtMobileCC);
		_obj.mobileView.add(_obj.txtMobileNo);
		_obj.stepView1.add(_obj.mobileView);
		_obj.phoneMobileBorderView.add(_obj.borderMobileCC);
		_obj.phoneMobileBorderView.add(_obj.borderMobileNo);
		_obj.stepView1.add(_obj.phoneMobileBorderView);
		
		if(origSplit[0] === 'US')
		{
			_obj.stepView1.add(_obj.lblSSNTxt);
			_obj.ssnTxtView.add(_obj.txtSSN1);
			_obj.ssnTxtView.add(_obj.borderSSN1);
			_obj.ssnTxtView.add(_obj.txtSSN2);
			_obj.ssnTxtView.add(_obj.borderSSN2);
			_obj.ssnTxtView.add(_obj.txtSSN3);
			_obj.ssnTxtView.add(_obj.borderSSN3);
			_obj.stepView1.add(_obj.ssnTxtView);	
		}
		
		_obj.stepView1.add(_obj.btnStep1);
		
		_obj.btnStep1.addEventListener('click',function(e){
			
			if(origSplit[0] != 'US')
			{
				if(_obj.txtPassport.value == "")
				{
					require('/utils/AlertDialog').showAlert('','Please provide a Passport No.',[L('btn_ok')]).show();
		    		_obj.txtPassport.value = '';
		    		_obj.txtPassport.focus();
					return;
				}
				else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtPassport.value) == false)
				{
					require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Passport No.',[L('btn_ok')]).show();
		    		_obj.txtPassport.value = '';
		    		_obj.txtPassport.focus();
					return;		
				}
				else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtPassport.value) == false)
				{
					require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Passport No.',[L('btn_ok')]).show();
		    		_obj.txtPassport.value = '';
		    		_obj.txtPassport.focus();
					return;		
				}
				else {
					
				}
				
				if(origSplit[0] == 'UAE')
				{
					if(_obj.txtPassport.value.length != 9)
					{
						require('/utils/AlertDialog').showAlert('','Passport No. should be of 9 characters',[L('btn_ok')]).show();
			    		_obj.txtPassport.value = '';
			    		_obj.txtPassport.focus();
						return;
					}
					else
					{
						if(_obj.txtPassport.value.length > 0)
						{
							var validchars = '';	
							validchars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
							var currChar, prevChar, nextChar;
							var validAlph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
							var validNum = "0123456789";
							var validAlphFlag = false;
							var validNumFlag = false;
							for(i = 0; i <= _obj.txtPassport.value.length - 1; i++)
							{
								currChar = passportNo_obj.value.charAt(i);
								prevChar = passportNo_obj.value.charAt(i-1);
								nextChar = passportNo_obj.value.charAt(i+1);
								if(!(validAlph.indexOf(currChar) == -1) && validAlphFlag == false)
								{
									validAlphFlag = true;
								}
								if(!(validNum.indexOf(currChar) == -1) && validNumFlag == false)
								{
									validNumFlag = true;
								}
								
								if(validchars.indexOf(currChar) == -1)
								{
									require('/utils/AlertDialog').showAlert('','Invalid character "' + currChar + '" entered in the passport Number',[L('btn_ok')]).show();
						    		_obj.txtPassport.value = '';
						    		_obj.txtPassport.focus();
									return;
								}
		
								if(currChar == " " && prevChar == " " || prevChar == " " && currChar == " " && nextChar == " " || prevChar == " " && currChar == " " && i == _obj.txtPassport.value.length-1 || currChar == " " && i == _obj.txtPassport.value.length-1)
								{
									if(i == _obj.txtPassport.value.length-1)
									{
										require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of passport Number',[L('btn_ok')]).show();
							    		_obj.txtPassport.value = '';
							    		_obj.txtPassport.focus();
										return;
									}
									else
									{
										require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the passport Number. Only one space is allowed in middle',[L('btn_ok')]).show();
							    		_obj.txtPassport.value = '';
							    		_obj.txtPassport.focus();
										return;
									}		
									break;
								}
							} 	
							if((validAlphFlag == false && validNumFlag == false) || (validAlphFlag == true && validNumFlag == false))
							{
								require('/utils/AlertDialog').showAlert('','Passport number should be alphanumeric',[L('btn_ok')]).show();
					    		_obj.txtPassport.value = '';
					    		_obj.txtPassport.focus();
								return;
							}  					
						}
					}
				}
			}
			
			if(_obj.lblMarital.text === 'Marital Status')
			{
				require('/utils/AlertDialog').showAlert('','Please select a marital status',[L('btn_ok')]).show();
	    		return;
			}
			else {
				
			}
			
			var workPhone = _obj.txtCC.value+_obj.txtAC.value+_obj.txtNo.value;
			
			if(_obj.txtCC.value.trim() == "" || _obj.txtAC.value.trim() == "" || _obj.txtNo.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please provide a valid office contact number',[L('btn_ok')]).show();
	    		return;
			}
			
			if(workPhone.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please provide a valid office contact number',[L('btn_ok')]).show();
	    		return;			
			}
			else if(workPhone.trim().length > 25)
			{
				require('/utils/AlertDialog').showAlert('','Office contact number cannot exceed 25 characters',[L('btn_ok')]).show();
	    		return;
			}
			else if(isNaN(workPhone))
			{
				require('/utils/AlertDialog').showAlert('','Office contact number must be numeric',[L('btn_ok')]).show();
	    		return;
			}
			
			var resPhone = _obj.txtDayCC.value+_obj.txtDayAC.value+_obj.txtDayNo.value;
			
			if(origSplit[0] != "UAE" && (_obj.txtDayCC.value.trim() == "" || _obj.txtDayAC.value.trim() == "" || _obj.txtDayNo.value.trim() == ""))
			{
				require('/utils/AlertDialog').showAlert('','Please provide a valid day time contact number',[L('btn_ok')]).show();
	    		return;
			}
			
			if(resPhone.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please provide a valid day time contact number',[L('btn_ok')]).show();
	    		return;
			}
			else if(resPhone.trim().length > 25)
			{
				require('/utils/AlertDialog').showAlert('','Day time contact number cannot exceed 25 characters',[L('btn_ok')]).show();
	    		return;
			}
			else if(isNaN(resPhone))
			{
				require('/utils/AlertDialog').showAlert('','Day time contact number must be numeric',[L('btn_ok')]).show();
	    		return;
			}
			if(origSplit[0] == "NZL")
			{
				if(resPhn2.length < 3 ) 
				{
					require('/utils/AlertDialog').showAlert('','Please enter valid Area Code for Day Time Phone',[L('btn_ok')]).show();
	    			return;
				}
				if(resPhn3.length < 4 ) 
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Phone',[L('btn_ok')]).show();
	    			return;
				}		
			}
			if(origSplit[0] == "NL")
			{
				if(resPhn3.length != 0 && (resPhn3.length < 5 || resPhn3.length > 10) ) 
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Phone',[L('btn_ok')]).show();
	    			return;
				}
			}
			
			var mobileSite = _obj.txtMobileCC.value+_obj.txtMobileNo.value;
			if(_obj.txtMobileCC.value.trim() == "" || _obj.txtMobileNo.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please provide a valid mobile number',[L('btn_ok')]).show();
	    		return;
			}
			
			if(mobileSite.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please provide a valid mobile number',[L('btn_ok')]).show();
	    		return;
			}
			else if(isNaN(mobileSite))
			{
				require('/utils/AlertDialog').showAlert('','Mobile number must be numeric',[L('btn_ok')]).show();
	    		return;
			}
			
			if(origSplit[0] == 'AUS')
			{
				if(_obj.txtMobileNo.value.length != 9) 
				{
					require('/utils/AlertDialog').showAlert('','Please provide a valid mobile number',[L('btn_ok')]).show();
	    			return;
				}
			}
			if(origSplit[0] == 'UAE')
			{
				if(_obj.txtMobileNo.value.length != 8) 
				{
					require('/utils/AlertDialog').showAlert('','Please provide a valid mobile number',[L('btn_ok')]).show();
	    			return;
				}
			}
			if(origSplit[0] == 'NZL')
			{
				if(_obj.txtMobileNo.value.length!=0 && (_obj.txtMobileNo.value.length < 7 || _obj.txtMobileNo.value.length > 11) ) 
				{
					require('/utils/AlertDialog').showAlert('','Please provide a valid mobile number',[L('btn_ok')]).show();
	    			return;
				}
			}
			if(origSplit[0] == 'NL')
			{
				if(_obj.txtMobileNo.value.length!=0 && (_obj.txtMobileNo.value.length < 8 || _obj.txtMobileNo.value.length > 14) ) 
				{
					require('/utils/AlertDialog').showAlert('','Please provide a valid mobile number',[L('btn_ok')]).show();
	    			return;
				}
		
			}
			
			if(origSplit[0] == 'US')
			{
				var ssn1 = _obj.txtSSN1.value;
				var ssn2 = _obj.txtSSN2.value;
				var ssn3 = _obj.txtSSN3.value;
				var ssn = ssn1+ssn2+ssn3;
				
				if(ssn1.trim() == "" || ssn2.trim() == "" || ssn3.trim() == "")
				{
					require('/utils/AlertDialog').showAlert('','Please provide your valid Social Security Number',[L('btn_ok')]).show();
	    			return;
				}
				else if(ssn.trim() == "")
				{
					require('/utils/AlertDialog').showAlert('','Please provide your valid Social Security Number',[L('btn_ok')]).show();
	    			return;
				}
				else 
				{
					if(ssn1.length != 3)
					{
						require('/utils/AlertDialog').showAlert('','The first section of Social Security Number should be of 3 digits',[L('btn_ok')]).show();
	    				return;				
					}
					if(ssn2.length != 2)
					{
						require('/utils/AlertDialog').showAlert('','The second section of Social Security Number should be of 2 digits',[L('btn_ok')]).show();
	    				return;
					}
					if(ssn3.length != 4)
					{
						require('/utils/AlertDialog').showAlert('','The third section of Social Security Number should be of 4 digits',[L('btn_ok')]).show();
	    				return;
					}
					if(isNaN(ssn1) || isNaN(ssn2) || isNaN(ssn3))
					{
						require('/utils/AlertDialog').showAlert('','Please provide your valid Social Security Number',[L('btn_ok')]).show();
	    				return;
					}	
				}			
			}
			
			_obj.scrollableView.moveNext();
			_obj.lblSectionHead.text = 'Professional Details';
			changeTabs('step2');
		});
		
		/////////////////// Step 2 ///////////////////
		
		_obj.stepView2 = Ti.UI.createScrollView(_obj.style.stepView2);
		
		_obj.txtOrganization = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtOrganization.hintText = "Name of the Organization";
		_obj.borderViewS21 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtDesignation = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtDesignation.hintText = "Designation";
		_obj.borderViewS22 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtSource = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtSource.hintText = "Other source of income";
		_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.stayView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblStay = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblStay.text = 'Stay';
		_obj.imgStay = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewS24 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.stayView.addEventListener('click',function(e){
			var optionDialogStay = require('/utils/OptionDialog').showOptionDialog('Stay',['Less than a year','1 - 3 years','3 - 5 years','More than 5 years'],-1);
			optionDialogStay.show();
			
			setTimeout(function(){
				activityIndicator.hideIndicator();
			},200);
			
			optionDialogStay.addEventListener('click',function(evt){
				if(optionDialogStay.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblStay.text = optionDialogStay.options[evt.index];
					
					switch(evt.index)
					{
						case 0:
							_obj.selStay = 'less than 1 year';
						break;
						
						case 1:
							_obj.selStay = '1-3 years';
						break;
						
						case 2:
							_obj.selStay = '3-5 years';
						break;
						
						case 3:
							_obj.selStay = 'more than 5 years';
						break;
					}
					
					optionDialogStay = null;
				}
				else
				{
					optionDialogStay = null;
					_obj.lblStay.text = 'Stay';
					_obj.selStay = null;
				}
			});	
		});
		
		_obj.incomeView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblIncome = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblIncome.text = 'Household Income';
		_obj.imgIncome = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewS25 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.incomeView.addEventListener('click',function(e){
			var optionDialogIncome = require('/utils/OptionDialog').showOptionDialog('Household Income',['Less than 50,000','50,001 - 1,00,000','1,00,001 - 2,00,000','2,00,001 - 3,00,000','3,00,001 - 4,00,000','4,00,001 - 5,00,000','5,00,001 and above'],-1);
			optionDialogIncome.show();
			
			setTimeout(function(){
				activityIndicator.hideIndicator();
			},200);
			
			optionDialogIncome.addEventListener('click',function(evt){
				if(optionDialogIncome.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblIncome.text = optionDialogIncome.options[evt.index];
					
					switch(evt.index)
					{
						case 0:
							_obj.selIncome = 'Less than 50000';
						break;
						
						case 1:
							_obj.selIncome = '50001 - 100000';
						break;
						
						case 2:
							_obj.selIncome = '100001 - 200000';
						break;
						
						case 3:
							_obj.selIncome = '200001 - 300000';
						break;
						
						case 4:
							_obj.selIncome = '300001 - 400000';
						break;
						
						case 5:
							_obj.selIncome = '400001 - 500000';
						break;
						
						case 6:
							_obj.selIncome = '500001 and above';
						break;
					}
					
					optionDialogIncome = null;
				}
				else
				{
					optionDialogIncome = null;
					_obj.lblIncome.text = 'Household Income';
					_obj.selIncome = null;
				}
			});
		});		
		
		_obj.amountView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblAmount = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblAmount.text = 'Amount of Other Income';
		_obj.imgAmount = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewS26 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.amountView.addEventListener('click',function(e){
			var optionDialogAmount = require('/utils/OptionDialog').showOptionDialog('Amount of Other Income',['Less than 10,000','10,000 - 30,000','30,000 - 50,000','More than 50,000'],-1);
			optionDialogAmount.show();
			
			setTimeout(function(){
				activityIndicator.hideIndicator();
			},200);
			
			optionDialogAmount.addEventListener('click',function(evt){
				if(optionDialogAmount.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblAmount.text = optionDialogAmount.options[evt.index];
					
					switch(evt.index)
					{
						case 0:
							_obj.selAmount = 'Less than 10000';
						break;
						
						case 1:
							_obj.selAmount = '10000-30000';
						break;
						
						case 2:
							_obj.selAmount = '30000-50000';
						break;
						
						case 3:
							_obj.selAmount = 'More than 50000';
						break;
					}
					
					optionDialogAmount = null;
				}
				else
				{
					optionDialogAmount = null;
					_obj.lblAmount.text = 'Amount of Other Income';
					_obj.selAmount = null;
				}
			});
		});	
		
		_obj.btnStep2 = Ti.UI.createButton(_obj.style.btnStep);
		_obj.btnStep2.title = 'CONTINUE';
		
		_obj.stepView2.add(_obj.txtOrganization);
		_obj.stepView2.add(_obj.borderViewS21);		
		_obj.stepView2.add(_obj.txtDesignation);
		_obj.stepView2.add(_obj.borderViewS22);
		_obj.stepView2.add(_obj.txtSource);
		_obj.stepView2.add(_obj.borderViewS23);
		_obj.stayView.add(_obj.lblStay);
		_obj.stayView.add(_obj.imgStay);
		_obj.stepView2.add(_obj.stayView);
		_obj.stepView2.add(_obj.borderViewS24);
		_obj.incomeView.add(_obj.lblIncome);
		_obj.incomeView.add(_obj.imgIncome);
		_obj.stepView2.add(_obj.incomeView);
		_obj.stepView2.add(_obj.borderViewS25);
		_obj.amountView.add(_obj.lblAmount);
		_obj.amountView.add(_obj.imgAmount);
		_obj.stepView2.add(_obj.amountView);
		_obj.stepView2.add(_obj.borderViewS26);
		_obj.stepView2.add(_obj.btnStep2);
		
		_obj.btnStep2.addEventListener('click',function(e){
			
			if(_obj.txtOrganization.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Organisation Name',[L('btn_ok')]).show();
	    		_obj.txtOrganization.value = '';
	    		_obj.txtOrganization.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtOrganization.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Organisation Name',[L('btn_ok')]).show();
	    		_obj.txtOrganization.value = '';
	    		_obj.txtOrganization.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtOrganization.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Organisation Name',[L('btn_ok')]).show();
	    		_obj.txtOrganization.value = '';
	    		_obj.txtOrganization.focus();
				return;		
			}
			else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtOrganization.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtOrganization.value.charAt(i)+' entered for '+_obj.txtOrganization.hintText+' field',[L('btn_ok')]).show();
	    		_obj.txtOrganization.value = '';
	    		_obj.txtOrganization.focus();
				return;
			}
			
			if(_obj.txtDesignation.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Designation',[L('btn_ok')]).show();
	    		_obj.txtDesignation.value = '';
	    		_obj.txtDesignation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDesignation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Designation',[L('btn_ok')]).show();
	    		_obj.txtDesignation.value = '';
	    		_obj.txtDesignation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDesignation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Designation',[L('btn_ok')]).show();
	    		_obj.txtDesignation.value = '';
	    		_obj.txtDesignation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtDesignation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtDesignation.value.charAt(i)+' entered for '+_obj.txtDesignation.hintText+' field',[L('btn_ok')]).show();
	    		_obj.txtDesignation.value = '';
	    		_obj.txtDesignation.focus();
				return;
			}
			
			if(_obj.txtSource.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please enter the Other Source of Income',[L('btn_ok')]).show();
	    		_obj.txtSource.value = '';
	    		_obj.txtSource.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtSource.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Other Source of Income',[L('btn_ok')]).show();
	    		_obj.txtSource.value = '';
	    		_obj.txtSource.focus();
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtSource.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Other Source of Income',[L('btn_ok')]).show();
	    		_obj.txtSource.value = '';
	    		_obj.txtSource.focus();
				return;		
			}
			else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtSource.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtSource.value.charAt(i)+' entered for '+_obj.txtSource.hintText+' field',[L('btn_ok')]).show();
	    		_obj.txtSource.value = '';
	    		_obj.txtSource.focus();
				return;
			}
			
			if(_obj.lblStay.text === 'Stay')
			{
				require('/utils/AlertDialog').showAlert('','Please enter the Years in current job/profession',[L('btn_ok')]).show();
	    		return;		
			}
			
			if(_obj.lblIncome.text === 'Household Income')
			{
				require('/utils/AlertDialog').showAlert('','Please select the Annual Income',[L('btn_ok')]).show();
	    		return;		
			}
			
			if(_obj.lblAmount.text === 'Amount of Other Income')
			{
				require('/utils/AlertDialog').showAlert('','Please select the Annual Income',[L('btn_ok')]).show();
	    		return;		
			}
			
			_obj.lblSectionHead.text = 'Receiver Details';
			_obj.scrollableView.moveNext();
			changeTabs('step3');
		});
		
		/////////////////// Step 3 ///////////////////
		
		_obj.stepView3 = Ti.UI.createScrollView(_obj.style.stepView3);
		
		_obj.lblReceiverNameHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblReceiverNameHead.text = 'Receiver';
		_obj.lblReceiver = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblReceiver.text = conf['receiverNickName'];
		
		_obj.lblPaymodeHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblPaymodeHead.text = 'Pay Mode';
		_obj.lblPaymode = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblPaymode.text = conf['paymode'];
		
		_obj.purposeView = Ti.UI.createView(_obj.style.ddView);
		_obj.lblPurpose = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblPurpose.text = 'Purpose of Sending Money';
		_obj.imgPurpose = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.purposeView.addEventListener('click',function(e){
			
			if(_obj.lblPaymode.text === 'Select Your Payment Mode*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a paymode code',[L('btn_ok')]).show();
				return;
			}
			
			activityIndicator.showIndicator();
			var xhr = require('/utils/XHR');
			
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"GETPURPOSELIST",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"paymodeCode":"'+conf['paymodeshort']+'",'+
					'"deliveryModeCode":"'+Ti.App.Properties.getString('deliveryMode')+'",'+
					'"purposeCode":"",'+
					'"purposeDesc":"",'+
					'"destCountry":"'+countryCode[0]+'",'+
			    	'"destCurrency":"'+countryCode[1]+'",'+
					'"origCountry":"'+origSplit[0]+'",'+ 
					'"origCurrency":"'+origSplit[1]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : 10000
			});
			
			function xhrSuccess(evt) {
				require('/utils/Console').info('Result ======== ' + evt.result);
				activityIndicator.hideIndicator();
				
				if(evt.result.responseFlag === "S")
				{
					// Populate Purpose
					_obj.purposeOptions = [];
					
					for(var i=0;i<evt.result.purposeListData.length;i++)
					{
						_obj.purposeOptions.push(evt.result.purposeListData[i].purposeLabel);
						_obj.purposeCodeOptions.push(evt.result.purposeListData[i].purposeCode+'~'+evt.result.purposeListData[i].purposeDesc);
					}
					
					var optionDialogPurpose = require('/utils/OptionDialog').showOptionDialog('Purpose',_obj.purposeOptions,-1);
					optionDialogPurpose.show();
					
					optionDialogPurpose.addEventListener('click',function(evt){
						if(optionDialogPurpose.options[evt.index] !== L('btn_cancel'))
						{
							_obj.lblPurpose.text = optionDialogPurpose.options[evt.index];
							_obj.purposeCode = _obj.purposeCodeOptions[evt.index];
							optionDialogPurpose = null;
						}
						else
						{
							optionDialogPurpose = null;
							_obj.purposeCode = null;
							_obj.lblPurpose.text = 'Select Purpose of Remittance*';
						}
					});
				}
				else
				{
					if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_limitenhancement();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
					}
				}
				
				xhr = null;
			}
	
			function xhrError(evt) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		});	
		
		_obj.txtRelation = Ti.UI.createTextField(_obj.style.txtField);
		_obj.txtRelation.hintText = "Relation with Receiver";
		_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.lblSendingAmountHead = Ti.UI.createLabel(_obj.style.lblStateHead);
		_obj.lblSendingAmountHead.text = 'Your sending amount';
		_obj.lblSendingAmount = Ti.UI.createLabel(_obj.style.lblState);
		_obj.lblSendingAmount.text = origSplit[1] +' '+ conf['amount'];
		
		_obj.btnStep3 = Ti.UI.createButton(_obj.style.btnStep);
		_obj.btnStep3.title = 'CONTINUE';
		
		_obj.stepView3.add(_obj.lblReceiverNameHead);
		_obj.stepView3.add(_obj.lblReceiver);
		_obj.stepView3.add(_obj.lblPaymodeHead);
		_obj.stepView3.add(_obj.lblPaymode);
		_obj.purposeView.add(_obj.lblPurpose);
		_obj.purposeView.add(_obj.imgPurpose);
		_obj.stepView3.add(_obj.purposeView);
		_obj.stepView3.add(_obj.borderViewS31);
		_obj.stepView3.add(_obj.txtRelation);
		_obj.stepView3.add(_obj.borderViewS32);
		_obj.stepView3.add(_obj.lblSendingAmountHead);
		_obj.stepView3.add(_obj.lblSendingAmount);
		_obj.stepView3.add(_obj.btnStep3);
		
		_obj.btnStep3.addEventListener('click',function(e){
			
			if(_obj.lblPurpose.text === 'Purpose of Sending Money')
			{
				require('/utils/AlertDialog').showAlert('','Please select a purpose of sending money',[L('btn_ok')]).show();
	    		return;		
			}
			else if(_obj.txtRelation.value.trim() == "")
			{
				require('/utils/AlertDialog').showAlert('','Please enter your relationship with the Receiver',[L('btn_ok')]).show();
	    		_obj.txtRelation.value = '';
	    		_obj.txtRelation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtRelation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Relationship field',[L('btn_ok')]).show();
	    		_obj.txtRelation.value = '';
	    		_obj.txtRelation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtRelation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Please remove unnecessary spaces in Relationship field',[L('btn_ok')]).show();
	    		_obj.txtRelation.value = '';
	    		_obj.txtRelation.focus();
				return;		
			}
			else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtRelation.value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtRelation.value.charAt(i)+' entered for '+_obj.txtRelation.hintText+' field',[L('btn_ok')]).show();
	    		_obj.txtRelation.value = '';
	    		_obj.txtRelation.focus();
				return;
			}
			
			_obj.lblSectionHead.text = 'KYC Calling Details';
			_obj.scrollableView.moveNext();
			changeTabs('step4');
		});
		
		/////////////////// Step 4 ///////////////////
		
		_obj.stepView4 = Ti.UI.createScrollView(_obj.style.stepView4);
		
		_obj.lblKYCTxt1 = Ti.UI.createLabel(_obj.style.lblKYCTxt1);
		_obj.lblKYCTxt1.text = 'Please select the date and time for KYC Calling. Our compliance team will get in touch with you at either of the time provided by you. The transaction may be on hold in the interim period till the KYC verification is completed.';
		
		_obj.dobView = Ti.UI.createView(_obj.style.dobView);
		_obj.lblDOB = Ti.UI.createLabel(_obj.style.lblDOB);
		_obj.lblDOB.text = 'Date';
		_obj.imgDOB = Ti.UI.createImageView(_obj.style.imgDOB);
		_obj.borderViewS41 = Ti.UI.createView(_obj.style.borderView);
		
		_obj.dobView.addEventListener('click',function(e){
			require('/utils/DatePicker').DatePicker(_obj.lblDOB,'normal');
		});
		
		_obj.ddTimeView = Ti.UI.createView(_obj.style.ddTimeView);
		_obj.timeView1 = Ti.UI.createView(_obj.style.ddTimeView1);
		_obj.lblTime1 = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblTime1.text = 'Time';
		_obj.imgTime1 = Ti.UI.createImageView(_obj.style.imgDD);
		
		_obj.lblTimeOR = Ti.UI.createLabel(_obj.style.lblTimeOR);
		_obj.lblTimeOR.text = 'OR';
		
		_obj.timeView2 = Ti.UI.createView(_obj.style.ddTimeView2);
		_obj.lblTime2 = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblTime2.text = 'Time';
		_obj.imgTime2 = Ti.UI.createImageView(_obj.style.imgDD);
		
		function getAvailableTime(type)
		{
			var optionDialogTime = require('/utils/OptionDialog').showOptionDialog('Available Time',_obj.timeOptions,-1);
			optionDialogTime.show();
			
			setTimeout(function(){
				activityIndicator.hideIndicator();
			},200);
			
			optionDialogTime.addEventListener('click',function(evt){
				if(optionDialogTime.options[evt.index] !== L('btn_cancel'))
				{
					if(type === 't1')
					{
						_obj.lblTime1.text = optionDialogTime.options[evt.index];
					}
					else
					{
						_obj.lblTime2.text = optionDialogTime.options[evt.index];
					}
					
					optionDialogTime = null;
					_obj.timeOptions.pop(); // Pops out cancel
				}
				else
				{
					_obj.timeOptions.pop(); // Pops out cancel
					optionDialogTime = null;
					if(type === 't1')
					{
						_obj.lblTime1.text = 'Time';
					}
					else
					{
						_obj.lblTime2.text = 'Time';
					}
				}
			});
		}
		
		_obj.timeView1.addEventListener('click',function(e){
			getAvailableTime('t1');
		});
		
		_obj.timeView2.addEventListener('click',function(e){
			getAvailableTime('t2');
		});
		
		_obj.ddBorderTimeView = Ti.UI.createView(_obj.style.ddBorderTimeView);
		_obj.borderViewS42 = Ti.UI.createView(_obj.style.borderTimeView1);
		_obj.borderViewS43 = Ti.UI.createView(_obj.style.borderTimeView2);
		
		_obj.noteView = Ti.UI.createView(_obj.style.noteView);
		
		_obj.lblNoteTime = Ti.UI.createLabel(_obj.style.lblNote);
		_obj.lblNoteTime.text = 'Note';
		
		_obj.lblNote1 = Ti.UI.createLabel(_obj.style.lblNoteTxt);
		_obj.lblNote1.text = '\u00B7 Your approval for the limits enhancement will be applicable for a single transaction only. Now go ahead and send the amount entered here immediately. Your transaction shall not be processed till verification is completed.';
		_obj.lblNote1.bottom = null; 
		
		_obj.lblNote2 = Ti.UI.createLabel(_obj.style.lblNoteTxt);
		_obj.lblNote2.text = '\u00B7 Post filling of the KYC form, your limit reset will be done on successful Compliance verification. Your enhanced limit will expire on 7th day from the day of reset of your remittance amount/transaction limit.';
		
		_obj.btnStep4 = Ti.UI.createButton(_obj.style.btnStep);
		_obj.btnStep4.title = 'SUBMIT';
		
		_obj.stepView4.add(_obj.lblKYCTxt1);
		_obj.dobView.add(_obj.lblDOB);
		_obj.dobView.add(_obj.imgDOB);
		_obj.stepView4.add(_obj.dobView);
		_obj.stepView4.add(_obj.borderViewS41);
		_obj.timeView1.add(_obj.lblTime1);
		_obj.timeView1.add(_obj.imgTime1);
		_obj.ddTimeView.add(_obj.lblTimeOR);
		_obj.ddTimeView.add(_obj.timeView1);
		_obj.timeView2.add(_obj.lblTime2);
		_obj.timeView2.add(_obj.imgTime2);
		_obj.ddTimeView.add(_obj.timeView2);
		_obj.stepView4.add(_obj.ddTimeView);
		_obj.ddBorderTimeView.add(_obj.borderViewS42);
		_obj.ddBorderTimeView.add(_obj.borderViewS43);
		_obj.stepView4.add(_obj.ddBorderTimeView);
		_obj.noteView.add(_obj.lblNoteTime);
		_obj.noteView.add(_obj.lblNote1);
		_obj.noteView.add(_obj.lblNote2);
		_obj.stepView4.add(_obj.noteView);
		_obj.stepView4.add(_obj.btnStep4);
		
		_obj.btnStep4.addEventListener('click',function(e){
			
			var sourceTimeSplit = Date().split(' ');
			var sourceHourSplit = sourceTimeSplit[4].split(':');
			var ch = parseInt(sourceHourSplit[0]);
			
			var today = sourceTimeSplit[2]+'-'+sourceTimeSplit[1]+'-'+sourceTimeSplit[3];
			
			var dateSplit = _obj.lblDOB.text.split('/');
			var dt = dateSplit[0]+'-'+require('/utils/Date').monthShort(dateSplit[1])+'-'+dateSplit[2];
			
			var time1Split = _obj.lblTime1.text.split('-');
			var time2Split = _obj.lblTime2.text.split('-');
			
			require('/utils/Console').info('parseInt(time1Split[0]) <= (ch+4)' + parseInt(time1Split[0]) +' ===== '+ (ch+4) + '=====' +(parseInt(time1Split[0]) <= (ch+4)));
			
			require('/utils/Console').info('((parseInt(time1Split[0]) <= (ch+4)) && (today === dt))' + parseInt(time1Split[0]) +' ===== '+ (ch+4) + '=====' + today + '---' +dt + '=========' + (today === dt));
			
			if(_obj.lblDOB.text === 'Date')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Calling Date',[L('btn_ok')]).show();
	    		return;
			}
			else
			{
				if(_obj.lblTime1.text === 'Time'){
					require('/utils/AlertDialog').showAlert('','Please choose a Calling Time',[L('btn_ok')]).show();
	    			return;
				}
				else if((parseInt(time1Split[0]) <= (ch+4)) && (today === dt)){
					require('/utils/AlertDialog').showAlert('','Please choose a Calling Time 4 hrs from the current time',[L('btn_ok')]).show();
	    			return;
				}
				
				if(_obj.lblTime2.text === 'Time'){
					require('/utils/AlertDialog').showAlert('','Please choose an Alternate Calling Time',[L('btn_ok')]).show();
	    			return;
				}
				else if(parseInt(time2Split[0]) <= parseInt(time1Split[0])){
					require('/utils/AlertDialog').showAlert('','Please choose a valid Alternate Calling Time',[L('btn_ok')]).show();
	    			return;
				}
			}
			
			activityIndicator.showIndicator();
			var dobSplit = _obj.lblDOB.text.split('/');
			var dob = dobSplit[0] + '-' + dobSplit[1] + '-' + dobSplit[2];
				
			var xhr = require('/utils/XHR');
			
			if(origSplit[0] === 'US')
			{
				var ssn1 = _obj.txtSSN1.value;
				var ssn2 = _obj.txtSSN2.value;
				var ssn3 = _obj.txtSSN3.value;
				var ssn = ssn1+ssn2+ssn3;
			}
			else
			{
				var ssn = ssn1+ssn2+ssn3;	
			}
			
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"TXNLIMITENHANCEMENT",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"originatingCountry":"'+origSplit[0]+'",'+ 
					'"destinationCountry":"'+destSplit[0]+'",'+
					'"destinationCurrency":"'+destSplit[1]+'",'+
					'"sendingCurrency":"'+origSplit[1]+'",'+
					'"state":"'+_obj.lblState.text+'",'+
					'"paymodeCode":"'+conf['paymode']+'",'+
					'"txnNoIncrease":"1",'+
					'"txnAmtIncrease":"1000",'+
					'"leadFlag":"I",'+
					'"supervisorName":"F",'+
					'"responseDate":"SYSDATE",'+
					'"purpose":"F",'+
					'"prevEmployer":"F",'+
					'"prevDesignation":"F",'+
					'"education":"F",'+
					'"stayOverseas":"F",'+
					'"firstName":"'+conf['receiverFName']+'",'+ 
					'"lastName":"'+conf['receiverLName']+'",'+ 
					'"email":"'+conf['receiverLoginid']+'",'+ 
					'"bene1Name":"'+conf['receiverNickName']+'",'+ 
					'"passportNo":"'+_obj.txtPassport.value+'",'+
					'"maritalStatus":"'+_obj.lblMarital.text+'",'+
					'"offPhone":"'+_obj.txtCC.value+_obj.txtAC.value+_obj.txtNo.value+'",'+
					'"resPhone":"'+_obj.txtDayCC.value+_obj.txtDayAC.value+_obj.txtDayNo.value+'",'+
					'"mobileSite":"'+_obj.txtMobileCC.value+_obj.txtMobileNo.value+'",'+
					'"ssn":"'+ssn+'",'+
					'"organisation":"'+_obj.txtOrganization.value+'",'+
					'"designation":"'+_obj.txtDesignation.value+'",'+
					'"otherIncomeSrc":"'+_obj.txtSource.value+'",'+
					'"experience":"'+_obj.selStay+'",'+
					'"annualIncome":"'+_obj.selIncome+'",'+
					'"otherIncome":"'+_obj.selAmount+'",'+
					'"remitPurpose":"'+_obj.lblPurpose.text+'",'+
					'"bene1Relation":"'+_obj.txtRelation.value+'",'+
					'"contactDate":"'+dt+'",'+
					'"timeFrom":"'+time1Split[0]+':00",'+
					'"timeTo":"'+time1Split[1]+':00",'+	
					'"timeFrom1":"'+time2Split[0]+':00",'+
					'"timeTo1":"'+time2Split[1]+':00"'+
					'}',
									
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : 10000
			});
	
			function xhrSuccess(e) {
				activityIndicator.hideIndicator();
				if(e.result.responseFlag === "S")
				{
					if(TiGlobals.osname === 'android')
					{
						require('/utils/AlertDialog').toast(e.result.message);
					}
					else
					{
						require('/utils/AlertDialog').iOSToast(e.result.message);
					}
					
					destroy_limitenhancement();
					setTimeout(function(){
						require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
					},200);
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_limitenhancement();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		});
		
		function changeTabs(selected)
		{
			if(selected === 'step1')
			{
				_obj.tabSelView.left = 0;
				_obj.tabSelView.right = null;
				
				_obj.tabSelIconView.left = 0;
				_obj.tabSelIconView.right = null;
				
				_obj.lblStep1.color = TiFonts.FontStyle('whiteFont');
				_obj.lblStep1.backgroundColor = '#6F6F6F';
				_obj.lblStep2.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep2.backgroundColor = '#E9E9E9';
				_obj.lblStep3.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep3.backgroundColor = '#E9E9E9';
				_obj.lblStep4.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep4.backgroundColor = '#E9E9E9';
			}
			else if(selected === 'step2')
			{
				_obj.tabSelView.left = '25%';
				_obj.tabSelView.right = null;
				
				_obj.tabSelIconView.left = '25%';
				_obj.tabSelIconView.right = null;
				
				_obj.lblStep1.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep1.backgroundColor = '#E9E9E9';
				_obj.lblStep2.color = TiFonts.FontStyle('whiteFont');
				_obj.lblStep2.backgroundColor = '#6F6F6F';
				_obj.lblStep3.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep3.backgroundColor = '#E9E9E9';
				_obj.lblStep4.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep4.backgroundColor = '#E9E9E9';
			}
			else if(selected === 'step3')
			{
				_obj.tabSelView.left = '50%';
				_obj.tabSelView.right = null;
				
				_obj.tabSelIconView.left = '50%';
				_obj.tabSelIconView.right = null;;
				
				_obj.lblStep1.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep1.backgroundColor = '#E9E9E9';
				_obj.lblStep2.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep2.backgroundColor = '#E9E9E9';
				_obj.lblStep3.color = TiFonts.FontStyle('whiteFont');
				_obj.lblStep3.backgroundColor = '#6F6F6F';
				_obj.lblStep4.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep4.backgroundColor = '#E9E9E9';
			}
			else
			{
				_obj.tabSelView.left = null;
				_obj.tabSelView.right = 0;
				
				_obj.tabSelIconView.left = null;
				_obj.tabSelIconView.right = 0;
				
				_obj.lblStep1.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep1.backgroundColor = '#E9E9E9';
				_obj.lblStep2.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep2.backgroundColor = '#E9E9E9';
				_obj.lblStep3.color = TiFonts.FontStyle('blackFont');
				_obj.lblStep3.backgroundColor = '#E9E9E9';
				_obj.lblStep4.color = TiFonts.FontStyle('whiteFont');
				_obj.lblStep4.backgroundColor = '#6F6F6F';
			}
		}
		
		_obj.scrollableView.views = [_obj.stepView1,_obj.stepView2,_obj.stepView3,_obj.stepView4];
		_obj.mainView.add(_obj.scrollableView);
		
		// Get User details
		
		function getProfileData()
		{
			activityIndicator.showIndicator();
			
			var xhr = require('/utils/XHR');
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"GETUSERDETAILS",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : 10000
			});
	
			function xhrSuccess(e) {
				if(e.result.responseFlag === "S")
				{
					_obj.lblState.text = e.result.state;
					_obj.txtEmail.value = e.result.emailId;
					
					///////// GETKYCCALLINGTIMERANGEDTLS for time ///////
					
					var xhr1 = require('/utils/XHR');
					
					xhr1.call({
						url : TiGlobals.appURLTOML,
						get : '',
						post : '{' +
							'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
							'"requestName":"GETKYCCALLINGTIMERANGEDTLS",'+
							'"partnerId":"'+TiGlobals.partnerId+'",'+
							'"channelId":"'+TiGlobals.channelId+'",'+
							'"ipAddress":"'+TiGlobals.ipAddress+'",'+
							'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
							'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
							'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
							'"originatingCountry":"'+origSplit[0]+'",'+ 
							'"destinationCountry":"'+destSplit[0]+'"'+
							'}',
						success : xhr1Success,
						error : xhr1Error,
						contentType : 'application/json',
						timeout : 10000
					});
			
					function xhr1Success(e) {
						require('/utils/Console').info('Result ======== ' + e.result);
						_obj.transactionInfoResult = e.result;
						
						if(e.result.responseFlag === "S")
						{
							for(var i=0; i<e.result.timeRanges.length;i++)
							{
								_obj.timeOptions.push(e.result.timeRanges[i]);
							}
						}
						else
						{
							if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
							{
								require('/lib/session').session();
								destroy_limitenhancement();
							}
							else
							{
								require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
							}
						}
						xhr1 = null;
						activityIndicator.hideIndicator();
					}
			
					function xhr1Error(e) {
						activityIndicator.hideIndicator();
						require('/utils/Network').Network();
						xhr1 = null;
					}
					
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_limitenhancement();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		}
		
		function getTransactionInfo()
		{
			var xhr = require('/utils/XHR');
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"GETTRANSACTIONINFO",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'",'+
					'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
					'"payModeCode":"",'+
					'"originatingCountry":"'+origSplit[0]+'",'+ 
					'"originatingCurrency":"'+origSplit[1]+'",'+
					'"destinationCountry":"'+destSplit[0]+'",'+
					'"destinationCurrency":"'+destSplit[1]+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : 10000
			});
	
			function xhrSuccess(e) {
				require('/utils/Console').info('Result ======== ' + e.result);
				
				if(e.result.responseFlag === "S")
				{
					_obj.purposeOptions = [];
				
					for(var i=0;i<e.result.purposeData.length;i++)
					{
						_obj.purposeOptions.push(e.result.purposeData[i].purposeName);
					}
				}
				else
				{
					if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
					{
						require('/lib/session').session();
						destroy_limitenhancement();
					}
					else
					{
						require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
					}
				}
				xhr = null;
			}
	
			function xhrError(e) {
				require('/utils/Network').Network();
				xhr = null;
			}
		}
		
		getProfileData();
		getTransactionInfo();
	}
	
	_obj.winLimitEnhancement.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_limitenhancement();
				alertDialog = null;
			}
		});
	});
	
	function destroy_limitenhancement()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove limit enhancement start ##############');
			
			_obj.winLimitEnhancement.close();
			require('/utils/RemoveViews').removeViews(_obj.winLimitEnhancement);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_limitenhancement',destroy_limitenhancement);
			require('/utils/Console').info('############## Remove limit enhancement end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_limitenhancement', destroy_limitenhancement);
}; // LimitEnhancementModal()