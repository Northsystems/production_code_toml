exports.MobileAlertsMoreModal = function()
{
	require('/lib/analytics').GATrackScreen('Transfer Mobile Alerts More Details');
	
	var _obj = {
		style : require('/styles/transfer/MobileAlertsMore').MobileAlertsMore,
		winMobileAlertsMore : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		webView : null,
		currCode : null,
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	_obj.winMobileAlertsMore = Ti.UI.createWindow(_obj.style.winMobileAlertsMore);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winMobileAlertsMore);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Mobile Alerts';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.url = TiGlobals.staticPagesURL + 'mobile-alerts-more.php?corridor='+_obj.currCode[0];
	
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	_obj.mainView.add(_obj.webView);
	_obj.globalView.add(_obj.mainView);
	_obj.winMobileAlertsMore.add(_obj.globalView);
	_obj.winMobileAlertsMore.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_mobilealertsmore();
	});
	
	_obj.winMobileAlertsMore.addEventListener('androidback', function(){
		destroy_mobilealertsmore();
	});
	
	function destroy_mobilealertsmore()
	{
		try{
			
			require('/utils/Console').info('############## Remove mobile alerts more start ##############');
			
			_obj.winMobileAlertsMore.close();
			require('/utils/RemoveViews').removeViews(_obj.winMobileAlertsMore);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_mobilealertsmore',destroy_mobilealertsmore);
			require('/utils/Console').info('############## Remove mobile alerts more end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_mobilealertsmore', destroy_mobilealertsmore);
}; // MobileAlertsMoreModal()