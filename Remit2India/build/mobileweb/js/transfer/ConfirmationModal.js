exports.ConfirmationModal = function(conf)
{
	require('/lib/analytics').GATrackScreen('Transfer Money Confirmation');
	
	var _obj = {
		style : require('/styles/transfer/Confirmation').Confirmation,
		winConfirmation : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		imgInfo : null,
		lblInfo : null,
		infoTextView : null,
		lblAddressHeader : null,
		lblAddress : null,
		lblPaymentHeader : null,
		tblPaymentDetails : null,
		lblAmountConvertedHeader : null,
		lblAmountConverted : null,
		convertedView : null,
		tblConverted : null,
		lblRecipientAmountHeader : null,
		lblRecipientAmount : null,
		recipientAmountView : null,
		lblSummary : null,
		lblSummaryReceipt : null,
		summaryView : null,
		imgIndicator : null,
		indicatorView : null,
		tblSummary : null,
		txtTraceNo : null,
		btnSubmit : null,
		
		isFRT : 'Y',
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	// FRT
	var paymodeData = Ti.App.Properties.getString('paymodeData');
	
	for(var f=0; f<paymodeData.length; f++)
	{
		if(paymodeData[f].paymodeCode === conf['paymodeshort'])
		{
			_obj.isFRT = paymodeData[f].isFRTPaymode;
		}
	}
	
	_obj.winConfirmation = Ti.UI.createWindow(_obj.style.winConfirmation);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winConfirmation);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Confirmation';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);

	_obj.globalView.add(_obj.mainView);
	_obj.winConfirmation.add(_obj.globalView);
	_obj.winConfirmation.open();
	
	function navigateURL(params)
	{
		var winNavigate = Ti.UI.createWindow(_obj.style.winConfirmation);
		var globalView = Ti.UI.createView(_obj.style.globalView);	
		var headerView = Ti.UI.createView(_obj.style.headerView);
		var lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
		lblHeader.text = 'Confirmation';
		var imgClose = Ti.UI.createImageView(_obj.style.imgClose);
		var headerBorder = Ti.UI.createView(_obj.style.headerBorder);
		var mainView = Ti.UI.createScrollView(_obj.style.mainView);
		var webView = Ti.UI.createWebView(_obj.style.webView);
		
		if(conf['paymodeshort'] === 'DEB')
		{
			webView.url = params.debURL;
		}
		else if(conf['paymodeshort'] === 'POLI')
		{
			Ti.API.info("IN POLI++++++");
		    webView.url = params.navigateURL;
			//webView.url = 'https:\/\/txn.apac.paywithpoli.com';
		}
		else if(conf['paymodeshort'] === 'CCARD')
		{
			webView.url = params.navigateURL;
		}else
		{
			Ti.API.info("IN POLI***********");
		}
		
		headerView.add(lblHeader);
		headerView.add(imgClose);
		headerView.add(headerBorder);
		globalView.add(headerView);
		mainView.add(webView);
		globalView.add(mainView);
		winNavigate.add(globalView);
		winNavigate.open();
		
		winNavigate.addEventListener('androidback',function(evt){
			/*var alertDialog = Ti.UI.createAlertDialog({
				buttonNames:[L('btn_yes'), L('btn_no')],
				message:L('cancel_transaction')
			});
		
			alertDialog.show();
			
			alertDialog.addEventListener('click', function(e){
				alertDialog.hide();
				if(e.index === 0 || e.index === "0")
				{*/
					require('/utils/RemoveViews').removeAllScrollableViews();
					winNavigate.close();
					headerView.remove(lblHeader);
					headerView.remove(imgClose);
					headerView.remove(headerBorder);
					globalView.remove(headerView);
					mainView.remove(webView);
					globalView.remove(mainView);
					winNavigate.remove(globalView);
					
					winNavigate = null;
					globalView = null;
					headerView = null;
					lblHeader = null;
					imgClose = null;
					headerBorder = null;
					mainView = null;
					webView = null;
					
					destroy_confirmation();
					//alertDialog = null;
				/*}
			});*/
		});
		
		imgClose.addEventListener('click',function(evt){
			/*var alertDialog = Ti.UI.createAlertDialog({
				buttonNames:[L('btn_yes'), L('btn_no')],
				message:L('cancel_transaction')
			});
		
			alertDialog.show();
			
			alertDialog.addEventListener('click', function(e){
				alertDialog.hide();
				if(e.index === 0 || e.index === "0")
				{*/
					require('/utils/RemoveViews').removeAllScrollableViews();
					winNavigate.close();
					headerView.remove(lblHeader);
					headerView.remove(imgClose);
					headerView.remove(headerBorder);
					globalView.remove(headerView);
					mainView.remove(webView);
					globalView.remove(mainView);
					winNavigate.remove(globalView);
					
					winNavigate = null;
					globalView = null;	
					headerView = null;
					lblHeader = null;
					imgClose = null;
					headerBorder = null;
					mainView = null;
					webView = null;
					
					destroy_confirmation();
					//alertDialog = null;
				/*}
			});*/
		});
	}
	
	// Check confirmation
	
	function confirmation()
	{
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 10000000) + 10000) +'",'+
				'"requestName":"BOOKTXN",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"MobileApp",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'",'+
				'"paymodeCode":"'+conf['paymode']+'",'+
			    '"receiverNickName":"'+conf['receiverNickName']+'",'+
			    '"receiverOwnerId":"'+conf['receiverOwnerId']+'",'+
			    '"receiverLoginid":"'+conf['receiverLoginid']+'",'+
			    '"receiverParentChildflag":"'+conf['receiverParentChildflag']+'",'+
			    '"amount":"'+conf['amount']+'",'+
			    '"accountId":"'+conf['accountId']+'",'+
			    '"bankName":"'+conf['bankName']+'",'+
			    '"bankBranch":"'+conf['bankBranch']+'",'+
			    '"accountNo":"'+conf['accountNo']+'",'+
			    '"purpose":"'+conf['purpose']+'",'+
			    '"personalMessage":"",'+
			    '"promoCode":"'+conf['promoCode']+'",'+
			    '"mobileAlerts":"'+conf['mobileAlerts']+'",'+
			    '"transactionInsurance":"'+conf['transactionInsurance']+'",'+
			    '"fxVoucherRedeem":"'+conf['fxVoucherRedeem']+'",'+
			    '"agentSpecialCode":"'+conf['agentSpecialCode']+'",'+
			    '"MORRefferalAmnt":"'+conf['morRefferalAmnt']+'",'+
			    '"programType":"'+conf['programType']+'",'+ 
			    '"txnRefId":"'+conf['txnRefId']+'",'+
			    '"contactDate":"'+conf['contactDate']+'",'+
			    '"timeFrom":"'+conf['timeFrom']+'",'+
			    '"timeTo":"'+conf['timeTo']+'",'+
			    '"timeFrom1":"'+conf['timeFrom1']+'",'+
			    '"timeTo1":"'+conf['timeTo1']+'",'+
			    '"voucherCode":"'+conf['voucherCode']+'",'+
			    '"voucherQty":"'+conf['voucherQty']+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 60000
		});

		function xhrSuccess(e) {
			
			require('/utils/Console').info('Result ======== ' + e.result);
			
			activityIndicator.hideIndicator();
			
			if(e.result.responseFlag === 'S')
			{
				
				if(conf['paymodeshort'] === 'DEB')
				{
					var params = {
						debHash : e.result.debDetails[0].hash,
						debFields : e.result.debDetails[0].parameterName,
						debData : e.result.debDetails[0].parameterValues,
						debURL : e.result.debDetails[0].url
					};
					
					navigateURL(params);
				}
				else if(conf['paymodeshort'] === 'POLI')
				{
					Ti.API.info("IN POLI----------");
					var params = {
						navigateURL : e.result.navigateURL
					};
					Ti.API.info("IN POLI----------",JSON.stringify(params));
					navigateURL(params);
				}
				else if(conf['paymodeshort'] === 'CCARD')
				{
					var params = {
						navigateURL : e.result.navigateURL
					};
					
					navigateURL(params);
				}
				else
				{
					
				}
				
				conf['txnRefId'] = e.result.rtrn;
				
				_obj.lblAddressHeader = Ti.UI.createLabel(_obj.style.lblAddressHeader);
				_obj.lblAddressHeader.text = 'Your transaction is confirmed!';
				
				_obj.lblAddress = Ti.UI.createLabel(_obj.style.lblAddress);
				_obj.lblAddress.text = 'Thank you '+Ti.App.Properties.getString('firstName')+' for choosing Remit2India for sending money to India. Remittance Transaction Reference Number (RTRN) has to be provided for each transaction and use the same for all communications with us. An email confirmation will be sent to ' + Ti.App.Properties.getString('loginId').toLowerCase();
				
				_obj.paymentView = Ti.UI.createView(_obj.style.paymentView);
				_obj.lblPaymentHeader = Ti.UI.createLabel(_obj.style.lblPaymentHeader);
				_obj.lblPaymentHeader.textAlign = 'center';
				_obj.lblPaymentHeader.text = 'Provisional Receipt';
				
				_obj.tblPaymentDetails = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblPaymentDetails.height = 0;
				
				for(var i=0; i<=3; i++)
				{
					var rowPD = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : 'transparent',
						className : 'bank_account_details'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						rowPD.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKeyPD = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblValuePD = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					switch(i)
					{
						case 0:
							if(e.result.hasOwnProperty('rtrn'))
							{
								lblKeyPD.text = 'RTRN No.';
								lblValuePD.text = e.result.rtrn;
								
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetails.appendRow(rowPD);
								_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
							}
						break;
						
						case 1:
							if(e.result.hasOwnProperty('transferAmount'))
							{
								lblKeyPD.text = 'Transfer Amount';
								lblValuePD.text = origSplit[1] + ' ' + e.result.transferAmount;
								
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetails.appendRow(rowPD);
								_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
							}
						break;
						
						case 2:
							if(e.result.hasOwnProperty('programFee') && e.result.programFee !== '')
							{
								var voucher = '';
								
								if(e.result.hasOwnProperty('benefitsData') && e.result.benefitsData !== '')
								{ 
									if(e.result.benefitsData[0].hasOwnProperty('ep'))
									{
										voucher = '(FXVoucher)';
									}
									else if(e.result.benefitsData[0].hasOwnProperty('fw'))
									{
										voucher = '(Freeway Voucher)';
									}  
								}
								
								lblKeyPD.text = 'Program Fee ' + voucher;
								lblValuePD.text = e.result.programFee;
								
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetails.appendRow(rowPD);
								_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
							}
						break;
						
						case 3:
						    if(e.result.hasOwnProperty('netApplicableFee'))
							{
								lblKeyPD.text = 'Net Applicable Fee';
								lblValuePD.text = e.result.netApplicableFee;
								
								rowPD.add(lblKeyPD);
								rowPD.add(lblValuePD);
								_obj.tblPaymentDetails.appendRow(rowPD);
								_obj.tblPaymentDetails.height = parseInt(_obj.tblPaymentDetails.height + 60);
							}
						break;
					}
				}
				
				_obj.convertedView = Ti.UI.createView(_obj.style.convertedView);
				_obj.lblAmountConvertedHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
				_obj.lblAmountConvertedHeader.text = 'Amount to be converted';
				_obj.lblAmountConverted = Ti.UI.createLabel(_obj.style.lblAmountConverted);
				_obj.lblAmountConverted.text = origSplit[1] + ' ' + e.result.amountToBeConverted;
				
				_obj.tblConverted = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblConverted.height = 0;
				
				for(var i=0; i<=3; i++)
				{
					var rowCon = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : 'transparent',
						className : 'bank_account_details'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						rowCon.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKeyCon = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblValueCon = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('blackFont')
					});
					
					switch(i)
					{
						case 0:
							if(e.result.hasOwnProperty('exchangeRate'))
							{
								lblKeyCon.text = 'Exchange Rate';
								lblValueCon.text = '1 ' + origSplit[1] + ' = ' + destSplit[1] + ' ' + e.result.exchangeRate;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblConverted.appendRow(rowCon);
								_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
							}
							
						break;
						
						case 1:
							if(e.result.hasOwnProperty('netExchangeRate'))
							{
								lblKeyCon.text = 'Exchange Rate with Benefit';
								lblValueCon.text = '1 ' + origSplit[1] + ' = ' + destSplit[1] + ' ' + e.result.netExchangeRate;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblConverted.appendRow(rowCon);
								_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
							}
						break;
						
						case 2:
							if(e.result.hasOwnProperty('convertedAmount'))
							{
								lblKeyCon.text = 'Converted Amount';
								lblValueCon.text = destSplit[1] + ' ' + e.result.convertedAmount;
								
								rowCon.add(lblKeyCon);
								rowCon.add(lblValueCon);
								_obj.tblConverted.appendRow(rowCon);
								_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
							}	
						break;
						
						case 3:
							if(e.result.hasOwnProperty('serviceTax'))
							{
								if(_obj.isFRT === 'Y')
								{
									lblKeyCon.text = 'Service Tax';
									lblValueCon.text = '(-) ' + destSplit[1] + ' ' + e.result.serviceTax;
									
									rowCon.add(lblKeyCon);
									rowCon.add(lblValueCon);
									_obj.tblConverted.appendRow(rowCon);
									_obj.tblConverted.height = parseInt(_obj.tblConverted.height + 60);
								}
							}
						break;
					}
					
				}
				
				_obj.SwachhBharatView = Ti.UI.createView(_obj.style.convertedView);
				_obj.lblSwachhBharatHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
				_obj.lblSwachhBharatHeader.text = 'Swachh Bharat Cess';
				_obj.lblSwachhBharatAmount = Ti.UI.createLabel(_obj.style.lblAmountConverted);
				_obj.lblSwachhBharatAmount.text = destSplit[1] + ' ' + e.result.sbcTax;
				
				_obj.recipientAmountView = Ti.UI.createView(_obj.style.convertedView);
				_obj.lblRecipientAmountHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
				_obj.lblRecipientAmountHeader.text = 'Amount to Recipient';
				_obj.lblRecipientAmount = Ti.UI.createLabel(_obj.style.lblAmountConverted);
				_obj.lblRecipientAmount.text = destSplit[1] + ' ' + e.result.indicativeAmountRecipient;
				
				_obj.recipientAmountToView = Ti.UI.createView(_obj.style.convertedView);
				_obj.lblRecipientAmountToHeader = Ti.UI.createLabel(_obj.style.lblAmountConvertedHeader);
				_obj.lblRecipientAmountToHeader.text = 'Indicative Destination Amount';
				_obj.lblRecipientAmountTo = Ti.UI.createLabel(_obj.style.lblAmountConverted);
				_obj.lblRecipientAmountTo.text = destSplit[1] + ' ' + e.result.convertedAmount;
				
				/*_obj.summaryView = Ti.UI.createView(_obj.style.summaryView);
				_obj.lblSummary = Ti.UI.createLabel(_obj.style.lblSummary);
				_obj.lblSummary.text = 'Summary of confirmation';
				_obj.lblSummaryReceipt = Ti.UI.createLabel(_obj.style.lblSummaryReceipt);
				_obj.lblSummaryReceipt.text = 'Not a Receipt';
				
				_obj.indicatorView = Ti.UI.createView(_obj.style.indicatorView);
				_obj.imgIndicator = Ti.UI.createImageView(_obj.style.imgIndicator);*/
				
				_obj.tblSummary = Ti.UI.createTableView(_obj.style.tableView);
				_obj.tblSummary.height = 0;
				_obj.tblSummary.separatorColor = '#595959';
				_obj.tblSummary.backgroundColor = '#303030';
				
				for(var i=0; i<=4; i++)
				{
					var rowSummary = Ti.UI.createTableViewRow({
						top : 0,
						left : 0,
						right : 0,
						height : 60,
						backgroundColor : '#303030',
						className : 'bank_account_details'
					});
					
					if(TiGlobals.osname !== 'android')
					{
						rowSummary.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
					}
					
					var lblKeySummary = Ti.UI.createLabel({
						top : 10,
						left : 20,
						height : 15,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal12'),
						color : TiFonts.FontStyle('greyFont')
					});
					
					var lblValueSummary = Ti.UI.createLabel({
						top : 30,
						height : 20,
						left : 20,
						width:Ti.UI.SIZE,
						textAlign: 'left',
						font : TiFonts.FontStyle('lblNormal14'),
						color : TiFonts.FontStyle('whiteFont')
					});
					
					switch(i)
					{
						case 0:
							if(e.result.hasOwnProperty('receiverData'))
							{
								lblKeySummary.text = 'Remittance transfer request received from';
								lblValueSummary.text = e.result.receiverData[0].firstName +' '+ e.result.receiverData[0].lastname +' '+ e.result.receiverData[0].micr;
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
						
						case 1:
							if(e.result.hasOwnProperty('receiverData'))
							{
								lblKeySummary.text = 'Delivery Mode';
								lblValueSummary.text = e.result.receiverData[0].deliveryModeCodeDesc;
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
						
						case 2:
							if(e.result.hasOwnProperty('purpose'))
							{
								var purposeSplit = e.result.purpose.split('~');
									
								lblKeySummary.text = 'Purpose of Remittance';
								lblValueSummary.text = purposeSplit[1];	
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
						
						case 3:
							if(e.result.hasOwnProperty('paymodeData'))
							{
								lblKeySummary.text = 'Sending option selected';
								lblValueSummary.text = e.result.paymodeData[0].paymodeDesc;
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
						
						case 4:
							if(e.result.hasOwnProperty('deliveryDate') && (origSplit[0] === 'US'))
							{
								lblKeySummary.text = 'Date Available: ' + e.result.deliveryDate;
								lblValueSummary.text = '(The money may be available sooner to the receiver)';
								lblValueSummary.font = TiFonts.FontStyle('lblNormal12');
								
								rowSummary.add(lblKeySummary);
								rowSummary.add(lblValueSummary);
								_obj.tblSummary.appendRow(rowSummary);
								_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
							}
						break;
					}
					
				}
				
				// Nostro Details
				
				if(e.result.hasOwnProperty('nostroDetails'))
				{
					if(e.result.nostroDetails.length>0)
					{
						for(var i=3; i>=0; i--)
						{
							var rowSummary = Ti.UI.createTableViewRow({
								top : 0,
								left : 0,
								right : 0,
								height : 60,
								backgroundColor : '#303030',
								className : 'bank_account_details'
							});
							
							if(TiGlobals.osname !== 'android')
							{
								rowSummary.selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
							}
							
							var lblKeySummary = Ti.UI.createLabel({
								top : 10,
								left : 20,
								height : 15,
								width:Ti.UI.SIZE,
								textAlign: 'left',
								font : TiFonts.FontStyle('lblNormal12'),
								color : TiFonts.FontStyle('greyFont')
							});
							
							var lblValueSummary = Ti.UI.createLabel({
								top : 30,
								height : 20,
								left : 20,
								width:Ti.UI.SIZE,
								textAlign: 'left',
								font : TiFonts.FontStyle('lblNormal14'),
								color : TiFonts.FontStyle('whiteFont')
							});
							
							switch(i)
							{
								case 3:
									
									try{
										var nostroSplit = e.result.nostroDetails[0].field1.split('|');
									
										lblKeySummary.text = nostroSplit[0];
										lblValueSummary.text = nostroSplit[1];
										
										rowSummary.add(lblKeySummary);
										rowSummary.add(lblValueSummary);
										_obj.tblSummary.appendRow(rowSummary);
										_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}catch(e){}
								break;
								
								case 2:
									try{
										var nostroSplit = e.result.nostroDetails[0].field2.split('|');
									
										lblKeySummary.text = nostroSplit[0];
										lblValueSummary.text = nostroSplit[1];
										
										rowSummary.add(lblKeySummary);
										rowSummary.add(lblValueSummary);
										_obj.tblSummary.appendRow(rowSummary);
										_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}catch(e){}
								break;
								
								case 1:
									try{
										var nostroSplit = e.result.nostroDetails[0].field3.split('|');
									
										lblKeySummary.text = nostroSplit[0];
										lblValueSummary.text = nostroSplit[1];
										
										rowSummary.add(lblKeySummary);
										rowSummary.add(lblValueSummary);
										_obj.tblSummary.appendRow(rowSummary);
										_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}catch(e){}
								break;
								
								case 0:
									try{
										var nostroSplit = e.result.nostroDetails[0].field4.split('|');
									
										lblKeySummary.text = nostroSplit[0];
										lblValueSummary.text = nostroSplit[1];
										
										rowSummary.add(lblKeySummary);
										rowSummary.add(lblValueSummary);
										_obj.tblSummary.appendRow(rowSummary);
										_obj.tblSummary.height = parseInt(_obj.tblSummary.height + 60);
									}catch(e){}
								break;
							}	
						}
					}	
				}
				
				if(origSplit[0] === 'US')
				{
					_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
					_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
					_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
					
					if(conf['paymodeshort'] === "WIRE")
					{
						_obj.lblInfo.text = "Recipient may receive less due to fees charged by recipient's bank and foreign taxes. Service Tax is applicable as per the Service Tax (Amendment) rules, 2012 of the Government of India & is recoverable from any foreign currency transfer into India. The Service Tax shall be computed and deducted from the converted amount based on the final exchange rate as indicated in the final receipt. We shall advise you of the final exchange rate as applicable to this remittance transfer through an updated receipt once we receive clear funds. You are required to wire transfer the funds before the end of business day today. In case of non-receipt of funds as per this timeline, TOML may cancel the remittance transfer request without any prior notice. In case the funds are received by us after the cancellation of remittance transfer request, you may book a fresh remittance transfer or request for a refund.";	
					}
					else if(conf['paymodeshort'] === "ACH")
					{
						_obj.lblInfo.text = "Recipient may receive less due to fees charged by recipient's bank and foreign taxes. Service Tax is applicable as per the Service Tax (Amendment) rules, 2012 of the Government of India & is recoverable from any foreign currency transfer into India. The Service Tax shall be computed and deducted from the converted amount based on the final exchange rate as indicated in the final receipt. We shall advise you of the final exchange rate as applicable to this remittance transfer through an updated receipt once we receive clear funds.";
					}
					
					
					// Error Rights
		
					_obj.errorRightsView = Ti.UI.createView(_obj.style.collapsibleView);
					_obj.errorRightsView.top = 20;
					_obj.lblErrorRights = Ti.UI.createLabel(_obj.style.lblCollapsible);
					_obj.lblErrorRights.text = 'Your Error Resolution Rights:';
					_obj.imgErrorRights = Ti.UI.createImageView(_obj.style.imgCollapsible);
					
					_obj.errorRightsTextView = Ti.UI.createView(_obj.style.collapsibleTextView);
					_obj.errorRightsTextView.height = 0;
					 
					_obj.errorRightsTxt = Ti.UI.createLabel(_obj.style.collapsibleTxt);
					_obj.errorRightsTxt.height = Ti.UI.SIZE;
					_obj.errorRightsTxt.text =  'You have a right to dispute errors in your transaction. If you think there is an error, contact us within 180 days at 1-888-736-4886 or www.remit2india.com. You can also contact us for a written explanation of your rights.\n\n Note: You may feel free to use the above number to report frauds.';
					
					_obj.errorRightsView.addEventListener('click',function(e){
						if(_obj.imgErrorRights.image === '/images/transfer_hide.jpg')
						{
							_obj.imgErrorRights.image ='/images/transfer_show.jpg';
							_obj.errorRightsTextView.height = Ti.UI.SIZE;
						}
						else
						{
							_obj.imgErrorRights.image ='/images/transfer_hide.jpg';
							_obj.errorRightsTextView.height = 0;
						}
					});
					
					// Cancellation Rights
						
					_obj.cancelRightsView = Ti.UI.createView(_obj.style.collapsibleView);
					_obj.cancelRightsView.top = 2;
					_obj.lblCancelRights = Ti.UI.createLabel(_obj.style.lblCollapsible);
					_obj.lblCancelRights.text = 'Your Cancellation Rights:';
					_obj.imgCancelRights = Ti.UI.createImageView(_obj.style.imgCollapsible);
					
					_obj.cancelRightsTextView = Ti.UI.createView(_obj.style.collapsibleTextView);
					_obj.cancelRightsTextView.height = 0;
					 
					_obj.cancelRightsTxt = Ti.UI.createLabel(_obj.style.collapsibleTxt);
					_obj.cancelRightsTxt.height = Ti.UI.SIZE;
					_obj.cancelRightsTxt.text =  'You can cancel for a full refund within 3 business days of the date of booking of the remittance transfer request (including the day of booking of the remittance transfer request).';
					
					_obj.cancelRightsView.addEventListener('click',function(e){
						if(_obj.imgCancelRights.image === '/images/transfer_hide.jpg')
						{
							_obj.imgCancelRights.image ='/images/transfer_show.jpg';
							_obj.cancelRightsTextView.height = Ti.UI.SIZE;
						}
						else
						{
							_obj.imgCancelRights.image ='/images/transfer_hide.jpg';
							_obj.cancelRightsTextView.height = 0;
						}
					});
					
					// Complaint
						
					_obj.complaintView = Ti.UI.createView(_obj.style.collapsibleView);
					_obj.complaintView.top = 2;
					_obj.lblComplaint = Ti.UI.createLabel(_obj.style.lblCollapsible);
					_obj.lblComplaint.text = 'Complaint Resolution:';
					_obj.imgComplaint = Ti.UI.createImageView(_obj.style.imgCollapsible);
					
					_obj.complaintTextView = Ti.UI.createView(_obj.style.collapsibleTextView);
					_obj.complaintTextView.height = 0;
					 
					_obj.complaintTxt = Ti.UI.createLabel(_obj.style.collapsibleTxt);
					_obj.complaintTxt.height = Ti.UI.SIZE;
					
					var txt = 'For questions or complaints about us, please contact:\n\n'+ 
					'STATE OF ALASKA\n'+ 
					'DEPARTMENT OF COMMERCE, COMMUNITY AND ECONOMIC DEVELOPMENT\n'+
					'DIVISION OF BANKING AND SECURITIES\n'+
					'(907) 269-4584\n'+
					'http://www.dced.state.ak.us/\n\n'+
					
					'STATE OF MINNESOTA \n'+
					'DEPARTMENT OF COMMERCE \n'+
					'FINANCIAL INSTITUTIONS DIVISION\n'+ 
					'651-539-1600\n'+
					'https://mn.gov/commerce/banking-and-finance/financial-institutions/licensing/money-transmitters/\n\n'+
					
					'Illinois Division of Financial Institutions (“DFI”)\n'+
					'320 W. Washington\n'+
					'Springfield, IL 62701\n'+
					'1-888-298-8089 (toll-free)\n'+
					'217-782-3704 (main)\n'+
					'http://www.idfpr.com/DFI/DFIComplaintForm.asp/\n\n'+
					 
					'Contact Number of Consumer Financial Protection Bureau (CFPB):\n'+
					'(855) 411-CFPB (2372)TTY/TDD (855) 729-CFPB (2372)\n'+
					'8 a.m. – 8 p.m. Eastern, Monday–Friday\n'+
					'180+ languages available\n'+
					'http://www.consumerfinance.gov\n';
					
					_obj.complaintTxt.text =  txt;
						
					_obj.complaintView.addEventListener('click',function(e){
						if(_obj.imgComplaint.image === '/images/transfer_hide.jpg')
						{
							_obj.imgComplaint.image ='/images/transfer_show.jpg';
							_obj.complaintTextView.height = Ti.UI.SIZE;
						}
						else
						{
							_obj.imgComplaint.image ='/images/transfer_hide.jpg';
							_obj.complaintTextView.height = 0;
						}
					});
				}
				
				if(origSplit[0] === 'UK')
				{
					_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
					_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
					_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
					
					_obj.lblInfo.text = "Our Nostro account details have changed from Axis Bank to Karnataka Bank. Kindly note the same before initiating the fund transfers.";
				}
				
				if(countryName[1] === 'EURO' || countryName[1] === 'Euro' || countryName[1] === 'euro')
				{
					_obj.infoTextView = Ti.UI.createView(_obj.style.infoTextView);
					_obj.imgInfo = Ti.UI.createImageView(_obj.style.imgInfo);
					_obj.lblInfo = Ti.UI.createLabel(_obj.style.lblInfo);
					
					_obj.lblInfo.text = "Our Correspondent Bank details have changed. Kindly note the same before initiating the fund transfers.";
				}
				
				_obj.btnServiceTax = Ti.UI.createButton(_obj.style.btnSubmit);
				_obj.btnServiceTax.backgroundColor = '#000';
				_obj.btnServiceTax.title = 'Service Tax';
				_obj.btnServiceTax.bottom = 20;
				
				_obj.btnServiceTax.addEventListener('click',function(e){
					require('/js/transfer/ServiceTaxModal').ServiceTaxModal();
				});
				
				_obj.mainView.add(_obj.lblAddressHeader);
				_obj.mainView.add(_obj.lblAddress);
				
				_obj.paymentView.add(_obj.lblPaymentHeader);
				_obj.mainView.add(_obj.tblPaymentDetails);
				
				_obj.convertedView.add(_obj.lblAmountConvertedHeader);
				_obj.convertedView.add(_obj.lblAmountConverted);
				_obj.mainView.add(_obj.convertedView);
				_obj.mainView.add(_obj.tblConverted);
				
				if(_obj.isFRT === 'Y')
				{
					_obj.SwachhBharatView.add(_obj.lblSwachhBharatHeader);
					_obj.SwachhBharatView.add(_obj.lblSwachhBharatAmount);
					_obj.mainView.add(_obj.SwachhBharatView);
				}
				
				if(_obj.isFRT === 'Y')
				{
					_obj.recipientAmountView.add(_obj.lblRecipientAmountHeader);
					_obj.recipientAmountView.add(_obj.lblRecipientAmount);
					_obj.mainView.add(_obj.recipientAmountView);
				}
				
				if(_obj.isFRT === 'Y')
				{
					_obj.recipientAmountToView.add(_obj.lblRecipientAmountToHeader);
					_obj.recipientAmountToView.add(_obj.lblRecipientAmountTo);
					_obj.mainView.add(_obj.recipientAmountToView);
				}
				
				/*_obj.summaryView.add(_obj.lblSummary);
				_obj.summaryView.add(_obj.lblSummaryReceipt);
				_obj.mainView.add(_obj.summaryView);
				_obj.indicatorView.add(_obj.imgIndicator);
				_obj.mainView.add(_obj.indicatorView);*/
				_obj.mainView.add(_obj.tblSummary);
				
				if(conf['paymode'] === 'CIP')
				{
					_obj.txtTraceNo = Ti.UI.createTextField(_obj.style.txtTraceNo);
					_obj.txtTraceNo.hintText = 'Internet Banking Trace Number';
					_obj.txtTraceNo.maxLength = 25;
					_obj.traceBorderView = Ti.UI.createView(_obj.style.borderView);
					
					_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
					_obj.btnSubmit.title = 'SUBMIT';
					
					_obj.mainView.add(_obj.txtTraceNo);
					_obj.mainView.add(_obj.traceBorderView);
					_obj.mainView.add(_obj.btnSubmit);
					
					_obj.btnSubmit.addEventListener('click',function(e){
						if(_obj.txtTraceNo.value === '')
						{
							require('/utils/AlertDialog').showAlert('','Please enter Internet Banking Trace Number',[L('btn_ok')]).show();
				    		_obj.txtTraceNo.value = '';
				    		_obj.txtTraceNo.focus();
				    		return;
						}
						else if(_obj.txtTraceNo.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('','Internet Banking Trace Number must contain a numeric value',[L('btn_ok')]).show();
				    		_obj.txtTraceNo.value = '';
				    		_obj.txtTraceNo.focus();
				    		return;
						}
						else
						{
							
						}
						
						activityIndicator.showIndicator();
						var xhr = require('/utils/XHR');
						xhr.call({
							url : TiGlobals.appURLTOML,
							get : '',
							post : '{' +
								'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
								'"requestName":"UPDATECIPTRACENUMBER",'+
								'"partnerId":"'+TiGlobals.partnerId+'",'+
								'"channelId":"'+TiGlobals.channelId+'",'+
								'"ipAddress":"'+TiGlobals.ipAddress+'",'+
								'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
								'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
								'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
								'"originatingCurrency":"'+origSplit[1]+'",'+
								'"rtrn":"'+conf["txnRefId"]+'",'+
								'"bankTxnNo":"'+_obj.txtTraceNo.value+'"'+
								'}',
							success : xhrSuccess,
							error : xhrError,
							contentType : 'application/json',
							timeout : 10000
						});
				
						function xhrSuccess(e) {
							require('/utils/Console').info('Result ======== ' + e.result);
							
							if(e.result.responseFlag === "S")
							{
								activityIndicator.hideIndicator();
								
								if(TiGlobals.osname === 'android')
								{
									require('/utils/AlertDialog').toast(e.result.message);
								}
								else
								{
									require('/utils/AlertDialog').iOSToast(e.result.message);
								}
							}
							else
							{
								if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
								{
									require('/lib/session').session();
									destroy_confirmation();
								}
								else
								{
									require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
								}
							}
							xhr = null;
						}
				
						function xhrError(e) {
							activityIndicator.hideIndicator();
							require('/utils/Network').Network();
							xhr = null;
						}
					});
				}
				
				_obj.btnBAT = Ti.UI.createButton(_obj.style.btnSubmit);
				_obj.btnBAT.title = 'BOOK ANOTHER TRANSACTION';
				
				if(origSplit[0] === 'US')
				{
					_obj.infoTextView.add(_obj.imgInfo);
					_obj.infoTextView.add(_obj.lblInfo);
					_obj.mainView.add(_obj.infoTextView);
				}
				
				if(origSplit[0] === 'UK')
				{
					_obj.infoTextView.add(_obj.imgInfo);
					_obj.infoTextView.add(_obj.lblInfo);
					_obj.mainView.add(_obj.infoTextView);
				}
				
				if(countryName[1] === 'EURO' || countryName[1] === 'Euro' || countryName[1] === 'euro')
				{
					_obj.infoTextView.add(_obj.imgInfo);
					_obj.infoTextView.add(_obj.lblInfo);
					_obj.mainView.add(_obj.infoTextView);
				}
				
				if(origSplit[0] === 'US')
				{
					if(_obj.isFRT === 'Y')
					{
						_obj.mainView.add(_obj.btnServiceTax);
					}
				}
				
				if(origSplit[0] === 'US')
				{
					_obj.errorRightsView.add(_obj.lblErrorRights);
					_obj.errorRightsView.add(_obj.imgErrorRights);
					_obj.mainView.add(_obj.errorRightsView);
					_obj.errorRightsTextView.add(_obj.errorRightsTxt);
					_obj.mainView.add(_obj.errorRightsTextView);
					
					_obj.cancelRightsView.add(_obj.lblCancelRights);
					_obj.cancelRightsView.add(_obj.imgCancelRights);
					_obj.mainView.add(_obj.cancelRightsView);
					_obj.cancelRightsTextView.add(_obj.cancelRightsTxt);
					_obj.mainView.add(_obj.cancelRightsTextView);
					
					_obj.complaintView.add(_obj.lblComplaint);
					_obj.complaintView.add(_obj.imgComplaint);
					_obj.mainView.add(_obj.complaintView);
					_obj.complaintTextView.add(_obj.complaintTxt);
					_obj.mainView.add(_obj.complaintTextView);
				}
				
				_obj.mainView.add(_obj.btnBAT);
				
				_obj.btnBAT.addEventListener('click',function(e){
					destroy_confirmation();
					require('/utils/RemoveViews').removeAllScrollableViews();
					require('/utils/PageController').pageController('transfer');
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_confirmation();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
				
				setTimeout(function(){
					destroy_confirmation();
					require('/js/transfer/PreConfirmationModal').PreConfirmationModal(conf);
				},1000);
			}
		}
		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	confirmation();
	
	_obj.imgClose.addEventListener('click',function(e){
		require('/utils/RemoveViews').removeAllScrollableViews();
		destroy_confirmation();
	});
	
	_obj.winConfirmation.addEventListener('androidback',function(e){
		require('/utils/RemoveViews').removeAllScrollableViews();
		destroy_confirmation();
	});
	
	function destroy_confirmation()
	{
		try{
			
			require('/utils/Console').info('############## Remove conf start ##############');
			
			_obj.winConfirmation.close();
			require('/utils/RemoveViews').removeViews(_obj.winConfirmation);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_confirmation',destroy_confirmation);
			require('/utils/Console').info('############## Remove conf end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_confirmation', destroy_confirmation);
}; // Login()