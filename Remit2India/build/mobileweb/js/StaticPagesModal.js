exports.StaticPagesModal = function(page,section,url_slug)
{
	require('/lib/analytics').GATrackScreen(page);

	var _obj = {
		style : require('/styles/StaticPages').StaticPages, // style
		globalView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		webView : null,
		currCode : null
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	// CreateUI
	
	_obj.winModal = Ti.UI.createWindow(_obj.style.winModal);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = page;
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.enableZoomControls = true;
	_obj.webView.scalesPageToFit = false;
	
	//_obj.webView.url = TiGlobals.staticPagesURL + 'master-static.php?section='+section+'&url_slug='+url_slug+'&corridor='+_obj.currCode[0];
	_obj.webView.url = TiGlobals.staticPagesURL + 'terms-condition.php?section=about&url_slug=terms-and-conditions&corridor='+_obj.currCode[0];
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.webView);
	_obj.globalView.add(_obj.mainView);
	_obj.winModal.add(_obj.globalView);
	_obj.winModal.open();
	
	function destroy_receiving_options() {
		try {
			require('/utils/Console').info('############## Remove win modal start ##############');
			_obj.winModal.close();
			require('/utils/RemoveViews').removeViews(_obj.winModal,_obj.globalView);
			_obj = null;

			require('/utils/Console').info('############## Remove win modal end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}
}; // StaticPagesModal()