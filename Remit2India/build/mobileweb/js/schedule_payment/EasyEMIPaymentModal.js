exports.EasyEMIPaymentModal = function()
{
	require('/lib/analytics').GATrackScreen('Easy EMI Payments');
	
	var _obj = {
		style : require('/styles/schedule_payment/EasyEMIPayment').EasyEMIPayment,
		winEasyEMIPayment : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		addPaymentView : null,
		lblCountry : null,
		lblCountryTxt : null,
		countryView : null,
		borderCountryView : null,
		lblCurrency : null,
		lblCurrencyTxt : null,
		currencyView : null,
		borderCurrencyView : null,
		lblDOB : null,
		imgDOB : null,
		dobView : null,
		borderView1 : null,
		lblReceiver : null,
		imgReceiver : null,
		receiverView : null,
		borderView2 : null,
		lblBank : null,
		imgBank : null,
		bankView : null,
		borderView3 : null,
		txtAmount : null,
		borderView4 : null,
		lblPeriod : null,
		imgPeriod : null,
		periodView : null,
		borderView5 : null,
		lblTotal : null,
		imgTotal : null,
		totalView : null,
		borderView6 : null,
		btnSubmit : null,
		
		receiverOptions : [],
		receiverDetails : [],
		bankOptions : [],
		bankDetails : [],
		bankSel : null,
		periodSel : null,
		dob : null,
		minMaxAmount : null,
		minAmount : null,
		maxAmount : null,
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.dob = require('/utils/Date').today('/','dmy').split('/');
	
	_obj.winEasyEMIPayment = Titanium.UI.createWindow(_obj.style.winEasyEMIPayment);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winEasyEMIPayment);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Easy EMI Payments';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);

	/////////////////// Add Easy EMI ///////////////////
	
	_obj.addPaymentView = Ti.UI.createScrollView(_obj.style.addPaymentView);
	
	_obj.countryView = Ti.UI.createView(_obj.style.topView);
	_obj.lblCountry = Ti.UI.createLabel(_obj.style.lblTopHeader);
	_obj.lblCountry.text = 'Country';
	_obj.lblCountryTxt = Ti.UI.createLabel(_obj.style.lblTopTxt);
	_obj.lblCountryTxt.text = countryName[0];
	_obj.borderCountryView = Ti.UI.createView(_obj.style.sectionBorder);
	
	_obj.currencyView = Ti.UI.createView(_obj.style.topView);
	_obj.lblCurrency = Ti.UI.createLabel(_obj.style.lblTopHeader);
	_obj.lblCurrency.text = 'Currency';
	_obj.lblCurrencyTxt = Ti.UI.createLabel(_obj.style.lblTopTxt);
	_obj.lblCurrencyTxt.text = origSplit[1];
	_obj.borderCurrencyView = Ti.UI.createView(_obj.style.sectionBorder);
	
	_obj.dobView = Ti.UI.createView(_obj.style.dobView);
	_obj.lblDOB = Ti.UI.createLabel(_obj.style.lblDOB);
	_obj.lblDOB.text = 'Start Date*';
	_obj.imgDOB = Ti.UI.createImageView(_obj.style.imgDOB);
	_obj.borderView1 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.receiverView = Ti.UI.createView(_obj.style.ddView);
	_obj.lblReceiver = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblReceiver.text = 'Select Receiver*';
	_obj.imgReceiver = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderView2 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.bankView = Ti.UI.createView(_obj.style.ddView);
	_obj.lblBank = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblBank.text = 'Select a Bank Account*';
	_obj.imgBank = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderView3 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.done = Ti.UI.createButton(_obj.style.done);
	_obj.done.addEventListener('click',function(){
		_obj.txtAmount.blur();
	});
	
	_obj.txtAmount = Ti.UI.createTextField(_obj.style.txtField);
	_obj.txtAmount.hintText = 'Amount to be debited regularly*';
	_obj.txtAmount.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtAmount.keyboardToolbar = [_obj.done];
	_obj.borderView4 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.periodView = Ti.UI.createView(_obj.style.ddView);
	_obj.lblPeriod = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblPeriod.text = 'Select Periodicity of payment*';
	_obj.imgPeriod = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderView5 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.totalView = Ti.UI.createView(_obj.style.ddView);
	_obj.lblTotal = Ti.UI.createLabel(_obj.style.lblDD);
	_obj.lblTotal.text = 'Select No. of Payments*';
	_obj.imgTotal = Ti.UI.createImageView(_obj.style.imgDD);
	_obj.borderView6 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.countryView.add(_obj.lblCountry);
	_obj.countryView.add(_obj.lblCountryTxt);
	_obj.addPaymentView.add(_obj.countryView);
	_obj.addPaymentView.add(_obj.borderCountryView);

	_obj.currencyView.add(_obj.lblCurrency);
	_obj.currencyView.add(_obj.lblCurrencyTxt);
	_obj.addPaymentView.add(_obj.currencyView);
	_obj.addPaymentView.add(_obj.borderCurrencyView);

	_obj.dobView.add(_obj.lblDOB);
	_obj.dobView.add(_obj.imgDOB);
	_obj.addPaymentView.add(_obj.dobView);
	_obj.addPaymentView.add(_obj.borderView1);
	
	_obj.receiverView.add(_obj.lblReceiver);
	_obj.receiverView.add(_obj.imgReceiver);
	_obj.addPaymentView.add(_obj.receiverView);
	_obj.addPaymentView.add(_obj.borderView2);
	
	_obj.bankView.add(_obj.lblBank);
	_obj.bankView.add(_obj.imgBank);
	_obj.addPaymentView.add(_obj.bankView);
	_obj.addPaymentView.add(_obj.borderView3);
	
	_obj.addPaymentView.add(_obj.txtAmount);
	_obj.addPaymentView.add(_obj.borderView4);
	
	_obj.periodView.add(_obj.lblPeriod);
	_obj.periodView.add(_obj.imgPeriod);
	_obj.addPaymentView.add(_obj.periodView);
	_obj.addPaymentView.add(_obj.borderView5);
	
	_obj.totalView.add(_obj.lblTotal);
	_obj.totalView.add(_obj.imgTotal);
	_obj.addPaymentView.add(_obj.totalView);
	_obj.addPaymentView.add(_obj.borderView6);
		
	_obj.addPaymentView.add(_obj.btnSubmit);
	_obj.mainView.add(_obj.addPaymentView);
	_obj.globalView.add(_obj.mainView);
	_obj.winEasyEMIPayment.add(_obj.globalView);
	_obj.winEasyEMIPayment.open();
	
	_obj.dobView.addEventListener('click',function(e){
		require('/utils/DatePicker').DatePicker(_obj.lblDOB,'normal');
	});
	
	_obj.receiverView.addEventListener('click',function(e){
		var optionDialogReceiver = require('/utils/OptionDialog').showOptionDialog('Receiver',_obj.receiverOptions,-1);
		optionDialogReceiver.show();
		
		optionDialogReceiver.addEventListener('click',function(evt){
			if(optionDialogReceiver.options[evt.index] !== L('btn_cancel'))
			{
				_obj.lblReceiver.text = optionDialogReceiver.options[evt.index];
				_obj.receiverSel = _obj.receiverDetails[evt.index];
				optionDialogReceiver = null;
			}
			else
			{
				optionDialogReceiver = null;
				_obj.lblReceiver.text = 'Select Receiver*';
			}
		});	
	});
	
	_obj.bankView.addEventListener('click',function(e){
		var optionDialogBank = require('/utils/OptionDialog').showOptionDialog('Bank Accounts',_obj.bankOptions,-1);
		optionDialogBank.show();
		
		optionDialogBank.addEventListener('click',function(evt){
			if(optionDialogBank.options[evt.index] !== L('btn_cancel'))
			{
				_obj.lblBank.text = optionDialogBank.options[evt.index];
				_obj.bankSel = _obj.bankDetails[evt.index];
				optionDialogBank = null;
			}
			else
			{
				optionDialogBank = null;
				_obj.lblBank.text = 'Select a Bank Account*';
			}
		});	
	});
	
	_obj.periodView.addEventListener('click',function(e){
		var optionDialogPeriod = require('/utils/OptionDialog').showOptionDialog('Period',['Monthly','Quarterly'],-1);
		optionDialogPeriod.show();
		
		optionDialogPeriod.addEventListener('click',function(evt){
			if(optionDialogPeriod.options[evt.index] !== L('btn_cancel'))
			{
				_obj.lblPeriod.text = optionDialogPeriod.options[evt.index];
				if(evt.index === 0)
				{
					_obj.periodSel = '30';	
				}
				else if(evt.index === 1)
				{
					_obj.periodSel = '90';	
				}
				else
				{
					
				}
				optionDialogPeriod = null;
			}
			else
			{
				optionDialogPeriod = null;
				_obj.lblPeriod.text = 'Select Periodicity of payment*';
			}
		});	
	});
	
	_obj.totalView.addEventListener('click',function(e){
		var optionDialogTotal = require('/utils/OptionDialog').showOptionDialog('No. of Payments',['3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'],-1);
		optionDialogTotal.show();
		
		optionDialogTotal.addEventListener('click',function(evt){
			if(optionDialogTotal.options[evt.index] !== L('btn_cancel'))
			{
				_obj.lblTotal.text = optionDialogTotal.options[evt.index];
				optionDialogTotal = null;
			}
			else
			{
				optionDialogTotal = null;
				_obj.lblTotal.text = 'Select No. of Payments*';
			}
		});	
	});
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		var selectedDate = _obj.lblDOB.text.split('/');
		
		if(_obj.lblDOB.text === 'Start Date*')
		{
			require('/utils/AlertDialog').showAlert('','Please select start date',[L('btn_ok')]).show();
			return;
		}
		else if(require('/utils/Date').validateDate(selectedDate[0], selectedDate[1], selectedDate[2], _obj.dob.toString()) === false)
		{
			return;
		}
		else
		{
			
		}
		
		if(_obj.lblReceiver.text === 'Select Receiver*')
		{
			require('/utils/AlertDialog').showAlert('','Please select a Receiver',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		if(_obj.lblBank.text === 'Select a Bank Account*')
		{
			require('/utils/AlertDialog').showAlert('','Please select a Bank Account',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		if(_obj.txtAmount.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter the Amount you wish to Remit through Easy EMI',[L('btn_ok')]).show();
			_obj.txtAmount.value = '';
			_obj.txtAmount.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAmount.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Remittance Amount',[L('btn_ok')]).show();
			_obj.txtAmount.value = '';
			_obj.txtAmount.focus();
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtAmount.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Remittance Amount',[L('btn_ok')]).show();
			_obj.txtAmount.value = '';
			_obj.txtAmount.focus();
			return;
		}
		else if(_obj.txtAmount.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Amount must be a Numeric Value',[L('btn_ok')]).show();
    		_obj.txtAmount.value = '';
    		_obj.txtAmount.focus();
			return;
		}
		else
		{
			var noOfPayments = parseInt(_obj.lblTotal.text);
			var txnAmount = parseInt(_obj.txtAmount.value);
			
			if(txnAmount != null)
			{
				var totalAmount = noOfPayments*txnAmount;
				if (totalAmount > _obj.totalAmount)
				{
					require('/utils/AlertDialog').showAlert('','The Recurring Payments sum amount has to be less than '+_obj.totalAmount,[L('btn_ok')]).show();
		    		_obj.txtAmount.value = '';
		    		_obj.txtAmount.focus();
					return;
				}				
			}
			if((parseInt(_obj.txtAmount.value) > parseInt(_obj.maxAmount)) || (parseInt(_obj.txtAmount.value) < parseInt(_obj.minAmount)))
			{
				require('/utils/AlertDialog').showAlert('','Please enter an SI amount between '+_obj.minAmount+' and '+_obj.maxAmount,[L('btn_ok')]).show();
	    		_obj.txtAmount.value = '';
	    		_obj.txtAmount.focus();
				return;

			}
		}
		
		if(_obj.lblPeriod.text === 'Select Periodicity of payment*')
		{
			require('/utils/AlertDialog').showAlert('','Please select periodicity of payment',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		if(_obj.lblTotal.text === 'Select No. of Payments*')
		{
			require('/utils/AlertDialog').showAlert('','Please select number of payments to be made',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		var bankDetails = _obj.bankSel.split('~');
		
		var params = {
			paymodeCode : 'ACH',
			receiverNickName : _obj.lblReceiver.text,
			amountToBeDebited : _obj.txtAmount.value,
			accountId : bankDetails[1],
			accountNo : bankDetails[2],
			startDate : _obj.lblDOB.text,
			sipFrequency : _obj.periodSel,
			totalNoOfPayments : _obj.lblTotal.text,
			minAmount : _obj.minAmount,
			maxAmount : _obj.minAmount,
			siType : 'NORMAL'
		};
		
		require('/js/schedule_payment/ScheduleTransactionPreConfirmationModal').ScheduleTransactionPreConfirmationModal(params);
		destroy_easyemipayment();
	});
	
	/////// Get Txn Info
	
	function getTransactionInfo()
	{
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETTRANSACTIONINFO",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"payModeCode":"",'+
				'"originatingCountry":"'+origSplit[0]+'",'+ 
				'"originatingCurrency":"'+origSplit[1]+'",'+
				'"destinationCountry":"'+destSplit[0]+'",'+
				'"destinationCurrency":"'+destSplit[1]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Result ======== ' + e.result);
			
			if(e.result.responseFlag === "S")
			{
				for(var i=0; i<e.result.receiverData.length; i++)
				{
					_obj.receiverOptions.push(e.result.receiverData[i].nickName);
					_obj.receiverDetails.push(e.result.receiverData[i].nickName +'~' + e.result.receiverData[i].parentChildFlag +'~' + e.result.receiverData[i].ownerId +'~' + e.result.receiverData[i].loginId +'~' + e.result.receiverData[i].firstName +'~' + e.result.receiverData[i].lastname);
				}
				
				for(var j=0; j<e.result.achData.length; j++)
				{
					_obj.bankOptions.push(e.result.achData[j].bankName +' - '+ e.result.achData[j].accNo);
					_obj.bankDetails.push(e.result.achData[j].bankName +'~' + e.result.achData[j].achId +'~' + e.result.achData[j].accNo);
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_easyemipayment();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
			activityIndicator.hideIndicator();
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function config()
	{
		var xhr = require('/utils/XHR_BCM');
		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"SIMinMaxAmount",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			if(e.result.status === "S")
			{
				_obj.totalAmount = e.result.response[0].SIMinMaxAmount.MAXAMOUNT;
				_obj.minAmount = e.result.response[0].SIMinMaxAmount.SIMIN;
				_obj.maxAmount = e.result.response[0].SIMinMaxAmount.SIMAX;
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_easyemipayment();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	getTransactionInfo();
	config();
	
	_obj.imgClose.addEventListener('click',function(e){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_easyemipayment();
				require('/js/schedule_payment/ScheduleTransactionModal').ScheduleTransactionModal();
				alertDialog = null;
			}
		});
	});
	
	_obj.winEasyEMIPayment.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_easyemipayment();
				require('/js/schedule_payment/ScheduleTransactionModal').ScheduleTransactionModal();
				alertDialog = null;
			}
		});
	});
	
	function destroy_easyemipayment()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove Easy EMI Payment start ##############');
			
			_obj.winEasyEMIPayment.close();
			require('/utils/RemoveViews').removeViews(_obj.winEasyEMIPayment);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_easyemipayment',destroy_easyemipayment);
			require('/utils/Console').info('############## Remove Easy EMI Payment end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_easyemipayment', destroy_easyemipayment);
}; // AddBankModal()