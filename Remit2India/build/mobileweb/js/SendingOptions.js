function SendingOptions(_self) {
	Ti.App.Properties.setString('pg', 'sending_options');
	
	pushView(Ti.App.Properties.getString('pg'));
	require('/lib/analytics').GATrackScreen('Sending & Receiving Options');

	var _obj = {
		style : require('/styles/StaticPages').StaticPages, // style
		globalView : null,
		mainView : null,
		lblHeader : null,
		imgBack : null,
		headerView : null,
		webView : null,
		currCode : null
	};
	
	_obj.currCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	// CreateUI
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Sending & Receiving Options';
	_obj.imgBack = Ti.UI.createImageView(_obj.style.imgBack);
	
	_obj.imgBack.addEventListener('click',function(){
		popView();
	});
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.webView = Ti.UI.createWebView(_obj.style.webView);
	_obj.webView.enableZoomControls = true;
	_obj.webView.scalesPageToFit = false;
	
	_obj.webView.url = TiGlobals.staticPagesURL + 'master-static.php?section=sending-receiving&url_slug=sending&corridor='+_obj.currCode[0];
	if (TiGlobals.osname !== 'android') {
		_obj.webView.hideLoadIndicator = true;	
	}
	
	_obj.webView.addEventListener('beforeload',function(e){
		activityIndicator.showIndicator();
	});
	
	_obj.webView.addEventListener('load',function(e){
		activityIndicator.hideIndicator();
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgBack);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.webView);
	_obj.globalView.add(_obj.mainView);
	
	function destroy_sending_options() {
		try {
			require('/utils/Console').info('############## Remove sending options start ##############');

			require('/utils/RemoveViews').removeViews(_obj.globalView);
			_obj = null;

			// Remove event listeners
			Ti.App.removeEventListener('destroy_sending_options', destroy_sending_options);
			require('/utils/Console').info('############## Remove sending options end ##############');
		} catch(e) {
			require('/utils/Console').info(e);
		}
	}


	Ti.App.addEventListener('destroy_sending_options', destroy_sending_options);

	return _obj.globalView;

};// SendingOptions()

module.exports = SendingOptions;