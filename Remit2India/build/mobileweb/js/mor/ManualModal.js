exports.ManualModal = function()
{
	require('/lib/analytics').GATrackScreen('Manually enter details');
	
	var _obj = {
		style : require('/styles/mor/Manual').Manual, // style
		winManual : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		lblSectionHead : null,
		sectionHeaderBorder : null,
		lblTopCount : null,
		lblTopTxt : null,
		topHeadView : null,
		txtUserName : null,
		borderView1 : null,
		lblMainHeadCount : null,
		lblMainHeadTxt : null,
		mainHeadView : null,
		friendMainView : null,
		moreBorder1 : null,
		imgAddView : null,
		moreBorder2 : null,
		imgTerms : null,
		lblTerms : null,
		lblTerms1 : null,
		termsView : null,
		btnSubmit : null,
		
		terms : null,
		count : null,
		countryOptions : [],
		isdnOptions : [],
	};
	
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	
	var origSplit = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	var origCC = origSplit[0]+'-'+origSplit[1];
	
	var destSplit = Ti.App.Properties.getString('destinationCountryCurCode').split('-');
	var destCC = destSplit[0]+'-'+destSplit[1];
	
	_obj.winManual = Ti.UI.createWindow(_obj.style.winManual);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winManual);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Refer & Earn';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.mainView = Ti.UI.createScrollView(_obj.style.mainView);
	
	_obj.lblSectionHead = Ti.UI.createLabel(_obj.style.lblSectionHead);
	_obj.lblSectionHead.text = 'Manually enter details';
	
	_obj.sectionHeaderBorder = Ti.UI.createView(_obj.style.sectionHeaderBorder);
	
	_obj.topHeadView = Ti.UI.createView(_obj.style.headView);
	_obj.lblTopCount = Ti.UI.createLabel(_obj.style.lblHeadCount);
	_obj.lblTopCount.text = '1';
	_obj.lblTopTxt = Ti.UI.createLabel(_obj.style.lblHeadTxt);
	_obj.lblTopTxt.text = 'Please enter your Remit2India register User Name';
	
	_obj.txtUserName = Ti.UI.createTextField(_obj.style.txt);
	_obj.txtUserName.hintText = 'User Name*';
	_obj.borderView1 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.mainHeadView = Ti.UI.createView(_obj.style.headView);
	_obj.mainHeadView.top = 15;
	_obj.lblMainHeadCount = Ti.UI.createLabel(_obj.style.lblHeadCount);
	_obj.lblMainHeadCount.text = '2';
	_obj.lblMainHeadTxt = Ti.UI.createLabel(_obj.style.lblHeadTxt);
	_obj.lblMainHeadTxt.text = 'Add all your friends';
	
	_obj.friendMainView = Ti.UI.createView(_obj.style.friendMainView);
	
	_obj.friendHeadView = [];
	_obj.lblFriendHeadCount = [];
	_obj.lblFriendHeadTxt = [];
	_obj.txtFriendName = [];
	_obj.txtEmail = [];
	_obj.borderFName = [];
	_obj.borderEmailView = [];
	_obj.countryView = [];	
	_obj.lblCountryName = [];
	_obj.imgCountry = [];
	_obj.bordercountryView = [];
	
	_obj.doneWCC = [];
	_obj.doneWAC = [];
	_obj.doneWNo = [];
	_obj.phoneView = [];
	_obj.txtCC = [];
	_obj.txtAC = [];
	_obj.txtNo = [];
	_obj.phoneBorderView = [];
	_obj.borderCC = [];
	_obj.borderAC = [];
	_obj.borderNo = [];
	
	function addFriend(i)
	{
		_obj.friendHeadView[i] = Ti.UI.createView(_obj.style.friendHeadView);
		_obj.friendHeadView[i].top = i===1 ? 0 : 15;
		_obj.lblFriendHeadCount[i] = Ti.UI.createLabel(_obj.style.lblFriendHeadCount);
		_obj.lblFriendHeadCount[i].text = i;
		_obj.lblFriendHeadTxt[i] = Ti.UI.createLabel(_obj.style.lblFriendHeadTxt);
		_obj.lblFriendHeadTxt[i].text = 'Add your friend';	
		
		_obj.txtFriendName[i] = Ti.UI.createTextField(_obj.style.txt);
		_obj.txtFriendName[i].hintText = "Friend\'s Name";
		_obj.borderFName[i] = Ti.UI.createView(_obj.style.borderView);
		
		_obj.txtEmail[i] = Ti.UI.createTextField(_obj.style.txt);
		_obj.txtEmail[i].hintText = "Email Address";
		_obj.borderEmailView[i] = Ti.UI.createView(_obj.style.borderView);
		
		_obj.countryView[i] = Ti.UI.createView(_obj.style.ddView);
		_obj.countryView[i].sel = i;
		_obj.lblCountryName[i] = Ti.UI.createLabel(_obj.style.lblDD);
		_obj.lblCountryName[i].text = 'Select Country';
		_obj.lblCountryName[i].sel = i;
		_obj.imgCountry[i] = Ti.UI.createImageView(_obj.style.imgDD);
		_obj.imgCountry[i].sel = i;
		_obj.bordercountryView[i] = Ti.UI.createView(_obj.style.borderView);
		
		_obj.doneWCC[i] = Ti.UI.createButton(_obj.style.done);
		_obj.doneWCC[i].addEventListener('click',function(){
			_obj.txtCC[i].blur();
		});
		
		_obj.doneWAC[i] = Ti.UI.createButton(_obj.style.done);
		_obj.doneWAC[i].addEventListener('click',function(){
			_obj.txtAC[i].blur();
		});
		
		_obj.doneWNo[i] = Ti.UI.createButton(_obj.style.done);
		_obj.doneWNo[i].addEventListener('click',function(){
			_obj.txtNo[i].blur();
		});
		
		_obj.phoneView[i] = Ti.UI.createView(_obj.style.phoneView);
		_obj.txtCC[i] = Ti.UI.createTextField(_obj.style.txtCC);
		_obj.txtCC[i].hintText = 'Country C';
		_obj.txtCC[i].keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtCC[i].keyboardToolbar = [_obj.doneWCC[i]];
		_obj.txtCC[i].editable = false;
		
		_obj.txtAC[i] = Ti.UI.createTextField(_obj.style.txtAC);
		_obj.txtAC[i].hintText = 'Area C';
		_obj.txtAC[i].keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtAC[i].keyboardToolbar = [_obj.doneWAC[i]];
		_obj.txtAC[i].maxLength = 3;
		
		_obj.txtNo[i] = Ti.UI.createTextField(_obj.style.txtNo);
		_obj.txtNo[i].hintText = 'Number';
		_obj.txtNo[i].keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
		_obj.txtNo[i].keyboardToolbar = [_obj.doneWNo[i]];
		_obj.txtNo[i].maxLength = 15;
		
		_obj.phoneBorderView[i] = Ti.UI.createView(_obj.style.phoneBorderView);
		_obj.borderCC[i] = Ti.UI.createView(_obj.style.borderCC);
		_obj.borderAC[i] = Ti.UI.createView(_obj.style.borderAC);
		_obj.borderNo[i] = Ti.UI.createView(_obj.style.borderNo);
		
		_obj.friendHeadView[i].add(_obj.lblFriendHeadCount[i]);
		_obj.friendHeadView[i].add(_obj.lblFriendHeadTxt[i]);
		_obj.friendMainView.add(_obj.friendHeadView[i]);
		_obj.friendMainView.add(_obj.txtFriendName[i]);
		_obj.friendMainView.add(_obj.borderFName[i]);
		_obj.friendMainView.add(_obj.txtEmail[i]);
		_obj.friendMainView.add(_obj.borderEmailView[i]);
		_obj.countryView[i].add(_obj.lblCountryName[i]);
		_obj.countryView[i].add(_obj.imgCountry[i]);
		_obj.friendMainView.add(_obj.countryView[i]);
		_obj.friendMainView.add(_obj.bordercountryView[i]);
		_obj.phoneView[i].add(_obj.txtCC[i]);
		_obj.phoneView[i].add(_obj.txtAC[i]);
		_obj.phoneView[i].add(_obj.txtNo[i]);
		_obj.friendMainView.add(_obj.phoneView[i]);
		_obj.phoneBorderView[i].add(_obj.borderCC[i]);
		_obj.phoneBorderView[i].add(_obj.borderAC[i]);
		_obj.phoneBorderView[i].add(_obj.borderNo[i]);
		_obj.friendMainView.add(_obj.phoneBorderView[i]);
		
		_obj.countryView[i].addEventListener('click',function(e){
			var optionDialogCitizenship = require('/utils/OptionDialog').showOptionDialog('Citizenship',_obj.countryOptions,-1);
			optionDialogCitizenship.show();
			
			optionDialogCitizenship.addEventListener('click',function(evt){
				if(optionDialogCitizenship.options[evt.index] !== L('btn_cancel'))
				{
					_obj.lblCountryName[e.source.sel].text = optionDialogCitizenship.options[evt.index];
					_obj.txtCC[e.source.sel].value = _obj.isdnOptions[evt.index];
					optionDialogCitizenship = null;
				}
				else
				{
					optionDialogCitizenship = null;
					_obj.lblCountryName[e.source.sel].text = 'Select Country';
				}
			});	
		});
	}
	
	_obj.count = 1;
	addFriend(_obj.count);
	
	_obj.moreBorder1 = Ti.UI.createView(_obj.style.sectionHeaderBorder);
	_obj.moreBorder1.top = 15;
	
	_obj.imgAddView = Ti.UI.createImageView(_obj.style.imgAddView);
	
	_obj.moreBorder2 = Ti.UI.createView(_obj.style.sectionHeaderBorder);
	_obj.moreBorder2.top = 15;
	
	_obj.imgAddView.addEventListener('click',function(e){
		_obj.count += 1;
		addFriend(_obj.count);
	});
	
	_obj.termsView = Ti.UI.createView(_obj.style.termsView);
	_obj.imgTerms = Ti.UI.createImageView(_obj.style.imgTerms);
	_obj.lblTerms = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblTerms1 = Ti.UI.createLabel(_obj.style.lblTerms1);
	_obj.lblTerms.text = 'I agree to the ';
	_obj.lblTerms1.text = 'Terms & Conditions';
	
	_obj.terms = 'N';
	_obj.imgTerms.addEventListener('click',function(e){
		if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
		{
			_obj.imgTerms.image = '/images/checkbox_sel.png';
			_obj.terms = 'Y';
		}
		else
		{
			_obj.imgTerms.image = '/images/checkbox_unsel.png';
			_obj.terms = 'N';
		}
	});
	
	_obj.lblTerms.addEventListener('click',function(e){
		if(_obj.imgTerms.image === '/images/checkbox_unsel.png')
		{
			_obj.imgTerms.image = '/images/checkbox_sel.png';
			_obj.terms = 'Y';
		}
		else
		{
			_obj.imgTerms.image = '/images/checkbox_unsel.png';
			_obj.terms = 'N';
		}
	});
	
	_obj.lblTerms1.addEventListener('click',function(e){
		require('/js/StaticPagesModal').StaticPagesModal('Terms & Conditions','about','terms-and-conditions');
	});
	
	_obj.btnSubmit = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmit.title = 'SUBMIT';
	
	_obj.btnSubmit.addEventListener('click',function(e){
		
		var friendDataArray = {
		    frdReferList: []
		};
		
		if(_obj.txtUserName.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your User Name',[L('btn_ok')]).show();
			_obj.txtUserName.value = '';
			_obj.txtUserName.focus();
    		return;		
		}
		else if((require('/lib/toml_validations').isEmail(_obj.txtUserName.value)) === false)
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid User Name',[L('btn_ok')]).show();
    		_obj.txtUserName.value = '';
    		_obj.txtUserName.focus();
			return;
		}
		else
		{
			
		}
		
		for(var i=1; i<=_obj.count;i++)
		{
			if(_obj.txtFriendName[i].value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter Name',[L('btn_ok')]).show();
				_obj.txtFriendName[i].value = '';
				_obj.txtFriendName[i].focus();
	    		return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtFriendName[i].value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of name',[L('btn_ok')]).show();
				_obj.txtFriendName[i].value = '';
				_obj.txtFriendName[i].focus();
	    		return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtFriendName[i].value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of name',[L('btn_ok')]).show();
				_obj.txtFriendName[i].value = '';
				_obj.txtFriendName[i].focus();
	    		return;
			}
			else if(_obj.txtFriendName[i].value.search("[^a-zA-Z ]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Only alphabets and space are allowed in name',[L('btn_ok')]).show();
				_obj.txtFriendName[i].value = '';
				_obj.txtFriendName[i].focus();
	    		return;
			}
			else
			{
				
			}
			
			if(_obj.txtEmail[i].value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a email address',[L('btn_ok')]).show();
				_obj.txtEmail[i].value = '';
				_obj.txtEmail[i].focus();
	    		return;
				
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtEmail[i].value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of email address',[L('btn_ok')]).show();
				_obj.txtEmail[i].value = '';
				_obj.txtEmail[i].focus();
	    		return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtEmail[i].value) == false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of email address',[L('btn_ok')]).show();
				_obj.txtEmail[i].value = '';
				_obj.txtEmail[i].focus();
	    		return;
			}
			else if(require('/lib/toml_validations').isEmail(_obj.txtEmail[i].value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid email address',[L('btn_ok')]).show();
				_obj.txtEmail[i].value = '';
				_obj.txtEmail[i].focus();
	    		return;
			}
			else if(_obj.txtEmail[i].value.length > 100)
			{
				require('/utils/AlertDialog').showAlert('','Email address is greter than 100 characters',[L('btn_ok')]).show();
				_obj.txtEmail[i].value = '';
				_obj.txtEmail[i].focus();
	    		return;
			}
			else
			{
				
			}
			
			if(_obj.lblCountryName[i].text === 'Select Country')
			{
				require('/utils/AlertDialog').showAlert('','Please select country from list',[L('btn_ok')]).show();
				return;
			}
			
			if(_obj.txtCC[i].value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please select country from list',[L('btn_ok')]).show();
				return;
			}
			else
			{
				
			}
			
			if(_obj.txtAC[i].value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter area code',[L('btn_ok')]).show();
				_obj.txtAC[i].value = '';
				_obj.txtAC[i].focus();
	    		return;
			}
			else if(_obj.txtAC[i].value.search("[^0-9]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Area code must be a Numeric Value',[L('btn_ok')]).show();
				_obj.txtAC[i].value = '';
				_obj.txtAC[i].focus();
	    		return;
			}
			else
			{
				
			}
			
			if(_obj.txtNo[i].value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter phone number',[L('btn_ok')]).show();
				_obj.txtNo[i].value = '';
				_obj.txtNo[i].focus();
	    		return;
			}
			else if(_obj.txtNo[i].value.search("[^0-9]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Phone number must be a Numeric Value',[L('btn_ok')]).show();
				_obj.txtNo[i].value = '';
				_obj.txtNo[i].focus();
	    		return;
			}
			else
			{
			}
			
			var cntryCode = _obj.txtCC[i].value;
			var areaCode = _obj.txtAC[i].value;
			var phnNo = _obj.txtNo[i].value;
			
			if(countryName[0] === "Germany")
			{
				if(areaCode.length < 2 || areaCode.length > 5)
				{
					require('/utils/AlertDialog').showAlert('','Length of area code has to be 2 to 5 digits',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else
				{
					
				}
				
				if(phnNo.length < 3 || phnNo.length > 9)
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number has to be  3 to 9 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else
				{
					
				}
			}
			else if(countryName[0] === "United States" || countryName[0] === "Canada")
			{
				if((areaCode.length) != 3)
				{
					require('/utils/AlertDialog').showAlert('','Length of area code has to be 3 digits',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else
				{
					
				}
				
				if((phnNo.length) != 7)
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number has to be  7 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				
				var frndPhnLen = cntryCode + areaCode + phnNo;
				if((frndPhnLen.length) > 25)
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number cannot exceed 25 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else
				{
					
				}
			}
			else if(countryName[0] === "Singapore")
			{
				if((phnNo.length) != 8)
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number has to be 8 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else
				{
					
				}
			}
			else if(countryName[0] === "Portugal")
			{
				if((phnNo.length) != 9)
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number has to be 9 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else
				{
					
				}
			}
			else if(countryName[0] === "United Arab Emirates")  //United Arab Emirates
			{
				if((areaCode.length < 1) || (areaCode.length > 2))
				{ 
					require('/utils/AlertDialog').showAlert('','Length of area code has to be 1 or 2 digits',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else
				{
					
				}
				
				if((phnNo.length < 7) || (phnNo.length > 10 ))
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number should be 7 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				
				var resPhonelen = cntryCode + areaCode + phnNo;
				if((resPhonelen.length) > 25)
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number cannot exceed 25 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else
				{
						
				}
			}
			else if(countryName[0] === "Hong Kong")	//Hong Kong
			{  
				if((areaCode.length < 1) || (areaCode.length > 2))
				{
					require('/utils/AlertDialog').showAlert('','Length of area code has to be 1 or 2 digit(s)',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else
				{
					
				}
				
				if((phnNo.length < 7) || (phnNo.length > 8))
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number should be 7 or 8 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				
				var resPhonelen = cntryCode + areaCode + phnNo;
				if((resPhonelen.length) > 13)
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number cannot exceed 13 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				
				else
				{
					
				}
				
			}
			else if(countryName[0] === "New Zealand" )  //New Zealand
			{
				if((areaCode.length < 1) || (areaCode.length > 2 ))
				{ 
					require('/utils/AlertDialog').showAlert('','Length of area code has to be 1 or 2 digit(s)',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else
				{
					
				}
				
				if(phnNo.length != 7)
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number should be 7 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				
				var resPhonelen = cntryCode + areaCode + phnNo;
				if((resPhonelen.length) > 20)
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number cannot exceed 20 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else
				{
						
				}				
			}
			else if(countryName[0] === "United Kingdom")	//For United Kingdom 
			{
				if(areaCode.charAt(0) == 0)
				{
					require('/utils/AlertDialog').showAlert('','Please enter your area code without 0 in the beginning',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else if(areaCode.length == 1)
				{
					require('/utils/AlertDialog').showAlert('','Length of area code has to be 2 to 4 digits',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else
				{
					
				}
				
				if((areaCode.length == 2) && (phnNo.length != 8))
				{
					require('/utils/AlertDialog').showAlert('','For 2 digits area code, day phone has to be 8 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else if((areaCode.length == 3) && (phnNo.length != 7))
				{
					require('/utils/AlertDialog').showAlert('','For 3 digits area code, day phone has to be 7 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}				
				else if((areaCode.length == 4) && (phnNo.length != 5) && (phnNo.length != 6))
				{
					require('/utils/AlertDialog').showAlert('','For 4 digits area code, day phone length has to be 5/6 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else
				{
					
				}
			}
			else if(countryName[0] === "Belgium")	//Belgium
			{
				if(areaCode.charAt(0) == 0)
				{
					require('/utils/AlertDialog').showAlert('','Please enter your area code without 0 in the beginning',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else
				{
					
				}
				
				if((areaCode.length == 1) && (phnNo.length != 7))
				{
					require('/utils/AlertDialog').showAlert('','For 1 digit area code, day phone has to be 7 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else if((areaCode.length == 2) && (phnNo.length != 6))
				{
					require('/utils/AlertDialog').showAlert('','For 2 digits area code, day phone has to be 6 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
				else
				{
					
				}
			}
			else if(countryName[0] === "Netherlands")
			{
				if(areaCode.charAt(0) == 0)
				{				
					require('/utils/AlertDialog').showAlert('','Please enter your area code without 0 in the beginning',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else if(areaCode.length != 2 && areaCode.length != 3)
				{
					require('/utils/AlertDialog').showAlert('','Length of area code for phone number has to be 2 or 3 digits',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else
				{
					
				}
				
				if((areaCode.length == 2) && (phnNo.length != 7))
				{
					require('/utils/AlertDialog').showAlert('','Length of area code for phone number has to be 7 digits',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}  
				else if((areaCode.length == 3) && (phnNo.length != 6))
				{
					require('/utils/AlertDialog').showAlert('','Length of area code for phone number has to be 6 digits',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				else
				{
					
				}  
			}
			else
			{
				var finalFlag = false;
				if(countryName[0] === "Australia")
				{
					if(areaCode.length == 1)
					{
						require('/utils/AlertDialog').showAlert('','Length of area code for phone number has to be 2 digits',[L('btn_ok')]).show();
						_obj.txtAC[i].value = '';
						_obj.txtAC[i].focus();
			    		return;
					}
					else
					{
						
					} 
				}
				else 
				{
					if(areaCode.length != 3 && areaCode.length != 4)
					{
						require('/utils/AlertDialog').showAlert('','Length of area code for phone number has to be 3 or 4 digits',[L('btn_ok')]).show();
						_obj.txtAC[i].value = '';
						_obj.txtAC[i].focus();
			    		return;
					}
					else
					{
						
					}
				}
				
				if((areaCode.length == 3) && (phnNo.length != 7))
				{
					require('/utils/AlertDialog').showAlert('','Length of area code for phone number has to be 7 digits',[L('btn_ok')]).show();
					_obj.txtAC[i].value = '';
					_obj.txtAC[i].focus();
		    		return;
				}
				if((areaCode.length == 4) && (phnNo.length != 6))
				{
					require('/utils/AlertDialog').showAlert('','Length of phone number has to be 6 digits',[L('btn_ok')]).show();
					_obj.txtNo[i].value = '';
					_obj.txtNo[i].focus();
		    		return;
				}
			}
		
			var resPhonelen;
	
			if((countryName[0] === "Singapore") || (countryName[0] === "Portugal"))
			{
				resPhonelen = cntryCode + phnNo;
			}
			else
			{
				resPhonelen = cntryCode + areaCode + phnNo;
			}
	
			if((resPhonelen.length) > 25)
			{
				require('/utils/AlertDialog').showAlert('','Length of phone number cannot exceed 25 digits',[L('btn_ok')]).show();
				_obj.txtNo[i].value = '';
				_obj.txtNo[i].focus();
	    		return;
			}
			else
			{
				
			}
			
			friendDataArray.frdReferList.push({ 
		        "name" : _obj.txtFriendName[i].value, 
				"email" : _obj.txtEmail[i].value, 
				"phoneNumber" : _obj.txtNo[i].value, 
				"country" : _obj.lblCountryName[i].text, 
				"countryCode" : _obj.txtCC[i].value, 
				"areaCode" : _obj.txtAC[i].value
		    });
		}
		
		//Terms and Condition
		if(_obj.terms === 'N')
		{
			require('/utils/AlertDialog').showAlert('','Please accept the terms and conditions',[L('btn_ok')]).show();
			return;
		}
		else
		{
			
		}
		
		
		require('/utils/Console').info(JSON.stringify({"frdReferList":friendDataArray.frdReferList}));
		
		activityIndicator.showIndicator();
		
		var xhr = require('/utils/XHR');
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"MORREFERRAL",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+_obj.txtUserName.value.toUpperCase()+'",'+ 
				'"referType":"Refer by Manual",'+
				'"frndReferArray":'+JSON.stringify({"frdReferList":friendDataArray.frdReferList})+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				
				setTimeout(function(){
					destroy_manual();
				},500);
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_manual();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.globalView.add(_obj.headerView);
	_obj.mainView.add(_obj.lblSectionHead);
	_obj.mainView.add(_obj.sectionHeaderBorder);
	_obj.topHeadView.add(_obj.lblTopCount);
	_obj.topHeadView.add(_obj.lblTopTxt);
	_obj.mainView.add(_obj.topHeadView);
	_obj.mainView.add(_obj.txtUserName);
	_obj.mainView.add(_obj.borderView1);
	_obj.mainHeadView.add(_obj.lblMainHeadCount);
	_obj.mainHeadView.add(_obj.lblMainHeadTxt);
	_obj.mainView.add(_obj.mainHeadView);
	_obj.mainView.add(_obj.friendMainView);
	_obj.mainView.add(_obj.moreBorder1);
	_obj.mainView.add(_obj.imgAddView);
	_obj.mainView.add(_obj.moreBorder2);
	_obj.termsView.add(_obj.imgTerms);
	_obj.termsView.add(_obj.lblTerms);
	_obj.termsView.add(_obj.lblTerms1);
	_obj.mainView.add(_obj.termsView);
	_obj.mainView.add(_obj.btnSubmit);
	_obj.globalView.add(_obj.mainView);
	_obj.winManual.add(_obj.globalView);
	_obj.winManual.open();
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_manual();
	});
	
	_obj.winManual.addEventListener('androidback',function(e){
		destroy_manual();
	});
	
	function getCorridor()
	{
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCORRIDORDTL",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"origCtyCurCode":"",'+
				'"destCtyCurCode":"",'+
				'"corridorRequestType":"3"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				for(var i=0;i<e.result.corridorData[0].origCtyCurData.length;i++)
				{
					var splitArr = e.result.corridorData[0].origCtyCurData[i].origCtyCurName.split('~');
					
					_obj.countryOptions.push(splitArr[0]);
					_obj.isdnOptions.push(e.result.corridorData[0].origCtyCurData[i].isdnCode);
				}
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_manual();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	getCorridor();
	
	function destroy_manual()
	{
		try{
			
			require('/utils/Console').info('############## Remove manual start ##############');
			
			_obj.winManual.close();
			require('/utils/RemoveViews').removeViews(_obj.winManual);
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_manual',destroy_manual);
			require('/utils/Console').info('############## Remove manual end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_manual', destroy_manual);
}; // ManualModal()