exports.KYCModal = function()
{
	require('/lib/analytics').GATrackScreen('KYC Form Australia');
	
	var _obj = {
		style : require('/styles/kyc/aus/KYC').KYC,
		winKYC : null,
		globalView : null,
		mainView : null,
		headerView : null,
		lblHeader : null,
		imgClose : null,
		headerBorder : null,
		
		kycView : null,
		//txtAptNo : null,
		//borderViewS21 : null,
		txtStreet1 : null,
		borderViewS23 : null,
		txtStreetName : null,
		borderViewSNAme : null,
		txtStreet2 : null,
		borderViewS24 : null,
		txtLocality : null,
		borderViewS25 : null,
		txtSubLocality : null,
		borderViewS26 : null,
		streetTypeView : null,
		txtCity : null,
		imgCity : null,
		borderViewS27 : null,
		stateView : null,
		lblState : null,
		imgState : null,
		borderViewS28 : null,
		txtZip : null,
		borderViewS29 : null,
		
		doneDay : null,
		dayView : null,
		lblDay : null,
		txtDay : null,
		borderViewS30 : null,
		
		doneMobile : null,
		mobileView : null,
		lblMobile : null,
		txtMobile : null,
		borderViewS31 : null,
		
		tabView : null,
		tabSelView : null,
		lblPassport : null,
		lblMedicare : null,
		tabSelIconView : null,
		imgTabSel : null,
		scrollableView : null,
		
		btnSubmitPassport : null,
		btnSubmitDriving : null,
		
		flat : null,
		bldgNo : null,
		street1 : null,
		street2 : null,
		streetType : null,
		locality : null,
		sub_locality : null,
		city : null,
		state : null,
		tab : null, 
		term : null,
		term1 : null,
		
		f : null,
		imgUpload : null,
		image : null,
		data_to_send : null,
	};
	
	_obj.tab = 'passport';
	var countryName = Ti.App.Properties.getString('sourceCountryCurName').split('~');
	var countryCode = Ti.App.Properties.getString('sourceCountryCurCode').split('~');
	//alert(countryName[0]);
	
	_obj.winKYC = Titanium.UI.createWindow(_obj.style.winKYC);
	
	// Activity Indicator Assign
	var ActivityIndicator = require('/utils/ActivityIndicator');
	var activityIndicator = new ActivityIndicator(_obj.winKYC);
	
	_obj.globalView = Ti.UI.createView(_obj.style.globalView);
	
	_obj.mainView = Ti.UI.createView(_obj.style.mainView);
	
	_obj.headerView = Ti.UI.createView(_obj.style.headerView);
	
	_obj.lblHeader = Ti.UI.createLabel(_obj.style.lblHeader);
	_obj.lblHeader.text = 'Help us know you better';
	
	_obj.imgClose = Ti.UI.createImageView(_obj.style.imgClose);
	
	_obj.headerBorder = Ti.UI.createView(_obj.style.headerBorder);
	
	_obj.headerView.add(_obj.lblHeader);
	_obj.headerView.add(_obj.imgClose);
	_obj.headerView.add(_obj.headerBorder);
	_obj.mainView.add(_obj.headerView);
	
	/////////////////// KYC ///////////////////
	
	_obj.kycView = Ti.UI.createScrollView(_obj.style.kycView);
	
	_obj.txtStreet1 = Ti.UI.createTextField(_obj.style.txtStreet1);
	_obj.txtStreet1.hintText = "Street No.*";
	_obj.txtStreet1.maxLength = 25;
	_obj.borderViewS23 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtStreetName = Ti.UI.createTextField(_obj.style.txtStreet1);
	_obj.txtStreetName.hintText = "Street Name*";
	_obj.txtStreetName.maxLength = 25;
	_obj.borderViewSName = Ti.UI.createView(_obj.style.borderView);
	
	_obj.streetTypeView = Ti.UI.createView(_obj.style.cityView);
	_obj.lblStreetType = Ti.UI.createLabel(_obj.style.txtCity);
	_obj.lblStreetType.top = 0;
	_obj.lblStreetType.left = 0;
	_obj.lblStreetType.text = 'Select Street Type*';
	_obj.imgStreetType = Ti.UI.createImageView(_obj.style.imgCity);
	_obj.borderViewSST = Ti.UI.createView(_obj.style.borderView);
	
	_obj.streetTypeView.addEventListener('click',function(){
		activityIndicator.showIndicator();
		var streetOptions = [];
		var streetCodeOptions = [];
		
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"streetType",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.status === "S")
			{
				for(var i=0;i<e.result.response[0].streetType.length;i++)
				{
					streetOptions.push(e.result.response[0].streetType[i].street);
					streetCodeOptions.push(e.result.response[0].streetType[i].code);
				}
				
				var optionDialogStreetType = require('/utils/OptionDialog').showOptionDialog('Street Type',streetOptions,-1);
				optionDialogStreetType.show();
				
				optionDialogStreetType.addEventListener('click',function(evt){
					if(optionDialogStreetType.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblStreetType.text = optionDialogStreetType.options[evt.index];
						_obj.streetType = streetCodeOptions[evt.index]; 
						optionDialogStreetType = null;
					}
					else
					{
						optionDialogStreetType = null;
						_obj.lblStreetType.text = 'Select Street Type*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	});
	
	_obj.txtCity = Ti.UI.createTextField(_obj.style.txtCity);
	_obj.txtCity.maxLength = 25;
	_obj.txtCity.hintText = 'City*';
	_obj.borderViewS27 = Ti.UI.createView(_obj.style.borderView);
	
	if(countryName[0] !== 'Singapore')
	{
		if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
		{
			
			_obj.stateView = Ti.UI.createView(_obj.style.stateView);
			_obj.lblState = Ti.UI.createLabel(_obj.style.lblState);
			_obj.imgState = Ti.UI.createImageView(_obj.style.imgState);
			_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
			
			if(countryName[0] === 'United Arab Emirates')
			{
				_obj.lblState.text = 'Select Emirates*';	
			}
			else
			{
				_obj.lblState.text = 'Select State*';	
			}
			
			_obj.stateView.addEventListener('click',function(){
				activityIndicator.showIndicator();
				var stateOptions = [];
				
				var xhr = require('/utils/XHR');
		
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"country":"'+countryName[0]+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : 10000
				});
		
				function xhrSuccess(e) {
					activityIndicator.hideIndicator();
					if(e.result.responseFlag === "S")
					{
						var splitArr = e.result.countryRelatedStates.split('~');
				
						for(var i=0;i<splitArr.length;i++)
						{
							stateOptions.push(splitArr[i]);
						}
						
						var optionDialogState = require('/utils/OptionDialog').showOptionDialog('State',stateOptions,-1);
						optionDialogState.show();
						
						optionDialogState.addEventListener('click',function(evt){
							if(optionDialogState.options[evt.index] !== L('btn_cancel'))
							{
								_obj.lblState.text = optionDialogState.options[evt.index];
								optionDialogState = null;
							}
							else
							{
								optionDialogState = null;
								
								if(countryName[0] === 'United Arab Emirates')
								{
									_obj.lblState.text = 'Select Emirates*';	
								}
								else
								{
									_obj.lblState.text = 'Select State*';	
								}
							}
						});	
					}
					else
					{
						if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_kyc();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
						}
					}
					xhr = null;
				}
		
				function xhrError(e) {
					activityIndicator.hideIndicator();
				    require('/utils/Network').Network();
					xhr = null;
				}
			});

        }
        else
        {
			_obj.lblState = Ti.UI.createTextField(_obj.style.txtCity);
			_obj.lblState.hintText = 'State*';
			_obj.borderViewS28 = Ti.UI.createView(_obj.style.borderView);
        }
	}
	
	_obj.txtZip = Ti.UI.createTextField(_obj.style.txtZip);
	_obj.txtZip.hintText = 'Zip Code*';
	
	_obj.doneZip = Ti.UI.createButton(_obj.style.done);
	_obj.doneZip.addEventListener('click',function(){
		_obj.txtZip.blur();
	});
	
	////// Pin code hintText ///////
	
	switch(countryName[0]){
		case 'United States' :
		{
			_obj.txtZip.hintText = 'Zip Code (5 digits)*';
			_obj.txtZip.maxLength = 5;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Canada' :
		{
			_obj.txtZip.hintText = 'Zip Code (7 alpha-numeric)*';
			_obj.txtZip.maxLength = 7;
			break;
		}
		case 'Australia' :
		{
			_obj.txtZip.hintText = 'Zip Code (4 digits)*';
			_obj.txtZip.maxLength = 4;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Hong Kong' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Japan' :
		{
			_obj.txtZip.hintText = 'Zip Code (7 digits)*';
			_obj.txtZip.maxLength = 7;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'New Zealand' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Singapore' :
		{
			_obj.txtZip.hintText = 'Postal Code (6 digits)*';
			_obj.txtZip.maxLength = 6;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Ireland' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'United Kingdom' :
		{
			_obj.txtZip.hintText = 'Postcode (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'United Arab Emirates' :
		{
			_obj.txtZip.hintText = 'P. O. Box (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Austria' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Belgium' :
		{
			_obj.txtZip.hintText = 'Zip Code (4 digits)*';
			_obj.txtZip.maxLength = 4;
			_obj.txtZip.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
			_obj.txtZip.keyboardToolbar = [_obj.doneZip];
			break;
		}
		case 'Cyprus' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Finland' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'France' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Germany' :
		{
			_obj.txtZip.hintText = 'Postal Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}		 
		case 'Greece' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Italy' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Luxembourg' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Malta' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}		
		case 'Netherlands' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Portugal' :
		{
			_obj.txtZip.hintText = 'Zip Code (25 alpha-numeric)*';
			_obj.txtZip.maxLength = 25;
			break;
		}
		case 'Slovakia' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Slovenia' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		case 'Spain' :
		{
			_obj.txtZip.hintText = 'Zip Code (8 alpha-numeric)*';
			_obj.txtZip.maxLength = 8;
			break;
		}
		default:
		{
			break;
		}
	}
	
	_obj.borderViewS29 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneDay = Ti.UI.createButton(_obj.style.done);
	_obj.doneDay.addEventListener('click',function(){
		_obj.txtDay.blur();
	});
	
	_obj.dayView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblDay = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblDay.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtDay = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtDay.hintText = 'Day Time Number*';
	_obj.txtDay.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtDay.keyboardToolbar = [_obj.doneDay];
	_obj.borderViewS30 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.doneMobile = Ti.UI.createButton(_obj.style.done);
	_obj.doneMobile.addEventListener('click',function(){
		_obj.txtMobile.blur();
	});
	
	_obj.mobileView = Ti.UI.createView(_obj.style.mobileView);
	_obj.lblMobile = Ti.UI.createLabel(_obj.style.lblMobile);
	_obj.lblMobile.text = Ti.App.Properties.getString('sourceCountryISDN') + '-';
	_obj.txtMobile = Ti.UI.createTextField(_obj.style.txtMobile);
	_obj.txtMobile.hintText = 'Mobile Number*';
	_obj.txtMobile.keyboardType = Ti.UI.KEYBOARD_TYPE_NUMBER_PAD;
	_obj.txtMobile.keyboardToolbar = [_obj.doneMobile];
	_obj.borderViewS31 = Ti.UI.createView(_obj.style.borderView);
	
	// Set HintText for mobile and day nos
	
	switch(countryName[0]){
		case 'United States' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'United Kingdom' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;			break;
		}
		case 'Australia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		 }
		case 'Singapore' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			_obj.txtDay.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDay.maxLength = 8;
			break;
		 }
		 case 'Germany' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			_obj.txtDay.hintText = 'Day Time Number*';
			_obj.txtDay.maxLength = 14;
			break;
		 }			 
		 case 'Canada' :
		 {
		 	_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Belgium' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (8 digits)*';
			_obj.txtDay.maxLength = 8;
			break;
		}		
		case 'Cyprus' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}			
		case 'Finland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}			
		case 'France' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Greece' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Ireland' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Italy' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Japan' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Luxembourg' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Malta' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}		
		case 'Netherlands' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		}
		case 'Portugal' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		}
		case 'Slovakia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Slovenia' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		case 'Hong Kong' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (8 digits)*';
			_obj.txtMobile.maxLength = 8;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		
		case 'Austria' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (10 digits)*';
			_obj.txtMobile.maxLength = 10;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		
		case 'New Zealand' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (11 digits)*';
			_obj.txtMobile.maxLength = 11;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		}
		
		case 'Spain' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (10 digits)*';
			_obj.txtDay.maxLength = 10;
			break;
		}
		
		case 'United Arab Emirates' :
		{
			_obj.txtMobile.hintText = 'Mobile Number (9 digits)*';
			_obj.txtMobile.maxLength = 9;

			_obj.txtDay.hintText = 'Day Time Number (9 digits)*';
			_obj.txtDay.maxLength = 9;
			break;
		}
		
		default:
		{
			break;
		}
	}
	
	_obj.tabView = Ti.UI.createView(_obj.style.tabView);
	_obj.tabSelView = Ti.UI.createView(_obj.style.tabSelView);
	_obj.tabSelView.width = '100%';
	_obj.lblPassport = Ti.UI.createLabel(_obj.style.lblPassport);
	_obj.lblPassport.width = '100%';
	_obj.lblPassport.text = 'Passport Details';
	_obj.lblMedicare = Ti.UI.createLabel(_obj.style.lblMedicare);
	_obj.lblMedicare.text = 'Medicare Number';
	
	_obj.tabSelIconView = Ti.UI.createView(_obj.style.tabSelIconView);
	_obj.tabSelIconView.width = '100%';
	_obj.imgTabSel = Ti.UI.createImageView(_obj.style.imgTabSel);
		
	_obj.scrollableView = Ti.UI.createScrollableView(_obj.style.scrollableView);
	
	// Passport 
	_obj.passportView = Ti.UI.createView(_obj.style.stepView);
	_obj.lblNote = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNote.text = 'Note';
	
	_obj.lblNoteTxt = Ti.UI.createLabel(_obj.style.lblKYCNote);
	_obj.lblNoteTxt.text = '\u00B7 If you are entering your passport details, please ensure that the name (First Name, First Middle Name, Last Name) mentioned for registration is as per the details mentioned on your passport';
	_obj.lblNoteTxt.top = 5; 
	
	_obj.txtPassportNo = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtPassportNo.maxLength = 9;
	_obj.txtPassportNo.hintText = 'Passport No*';
	_obj.borderViewS32 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.countryView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblCountry = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblCountry.text = 'Passport Country*';
	_obj.imgCountry = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewS33 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.countryView.addEventListener('click',function(){
		country();
	});
	
	_obj.txtPassportIssuePlace = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtPassportIssuePlace.hintText = 'Passport Issue Place*';
	_obj.borderViewSPIP = Ti.UI.createView(_obj.style.borderView);
	
	_obj.COBView = Ti.UI.createView(_obj.style.countryView);
	_obj.lblCOB = Ti.UI.createLabel(_obj.style.lblCountry);
	_obj.lblCOB.text = 'Country of Birth*';
	_obj.imgCOB = Ti.UI.createImageView(_obj.style.imgCountry);
	_obj.borderViewS34 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.COBView.addEventListener('click',function(){
		nationality();
	});
	
	_obj.txtSurname = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtSurname.hintText = 'Surname at Citizenship*';
	_obj.txtSurname.maxLength = 25;
	_obj.borderViewS35 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtSurnameBirth = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtSurnameBirth.hintText = 'Surname at Birth*';
	_obj.txtSurnameBirth.maxLength = 25;
	_obj.borderViewS36 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtPOB = Ti.UI.createTextField(_obj.style.txtPassportPersonalNo);
	_obj.txtPOB.hintText = 'Passport Place of Birth*';
	_obj.txtPOB.maxLength = 25;
	_obj.borderViewS37 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.termsAgreeView = Ti.UI.createView(_obj.style.termsView);
	_obj.termsAgreeView.backgroundColor = '#e9e9e9';
	_obj.lblAgree = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblAgree.text = 'I agree to the following';
	_obj.lblAgreeTxt = Ti.UI.createLabel(_obj.style.lblTermsTxt);
	_obj.lblAgreeTxt.text = 'We may disclose your personal information to an organisation providing verification of your identity, including on-line verification of your identity where we are required to do so by law, such as under the Anti-Money Laundering and Counter Terrorism Financing Act 2006. We may verify your identity using information held by a Credit Reporting Body (CRB). To do this we may disclose personal information such as your name, date of birth and address to the CRB to obtain an assessment of whether that personal information matches information held by the CRB. The CRB may give us a report on that assessment and to do so may use personal information about you and other individuals in their files. Alternative means of verifying your identity are available on request. If we are unable to verify your identity using information held by a CRB we will provide you with a notice to this effect and give you the opportunity to contact the CRB to update your information held by them';
	_obj.imgAgree = Ti.UI.createImageView(_obj.style.imgTerms);
	
	_obj.termsDonotAgreeView = Ti.UI.createView(_obj.style.termsView);
	_obj.termsDonotAgreeView.backgroundColor = '#fff';
	_obj.termsDonotAgreeView.top = 0;
	_obj.lblDonotAgree = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblDonotAgree.text = 'I do not agree';
	_obj.lblDonotAgreeTxt = Ti.UI.createLabel(_obj.style.lblTermsTxt);
	_obj.lblDonotAgreeTxt.text = 'To get my identity verified through the stated means and would be willing to share any identity documents that you may want such as Passport copy, Driving licence, etc.';
	_obj.imgDonotAgree = Ti.UI.createImageView(_obj.style.imgTerms);
	
	_obj.termsAgreeView.addEventListener('click',function(){
		if(_obj.imgAgree.image === '/images/checkbox_unsel.png')
		{
			_obj.term = 1;
			_obj.imgAgree.image = '/images/checkbox_sel.png';
			_obj.imgDonotAgree.image = '/images/checkbox_unsel.png';
		}
		else
		{
			_obj.term = 0;
			_obj.imgAgree.image = '/images/checkbox_unsel.png';
		}
		
		_obj.imgUploadView.height = 0;
		_obj.imgUploadView.visible = false;
	});
	
	var docType = null;
	var docRequestId = null;
	var docTypeCode = null;
	var docUploadPath = null;
	
	_obj.termsDonotAgreeView.addEventListener('click',function(){
		if(_obj.imgDonotAgree.image === '/images/checkbox_unsel.png')
		{
			_obj.term = 0;
			_obj.imgDonotAgree.image = '/images/checkbox_sel.png';
			_obj.imgAgree.image = '/images/checkbox_unsel.png';
			
			
			activityIndicator.showIndicator();
			var xhr = require('/utils/XHR');
	
			xhr.call({
				url : TiGlobals.appURLTOML,
				get : '',
				post : '{' +
					'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
					'"requestName":"GETDOCUMENTREQUESTID",'+
					'"partnerId":"'+TiGlobals.partnerId+'",'+
					'"channelId":"'+TiGlobals.channelId+'",'+
					'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
					'"ipAddress":"'+TiGlobals.ipAddress+'"'+
					'}',
				success : xhrSuccess,
				error : xhrError,
				contentType : 'application/json',
				timeout : 10000
			});
	
			function xhrSuccess(e) {
				require('/utils/Console').info('Result ======== ' + e.result);
				activityIndicator.hideIndicator();
				docRequestId = e.result.docRequestId;
				_obj.imgUploadView.height = 50;
				_obj.imgUploadView.visible = true;
			}
	
			function xhrError(e) {
				activityIndicator.hideIndicator();
				require('/utils/Network').Network();
				xhr = null;
			}
		}
		else
		{
			_obj.term = 0;
			_obj.imgDonotAgree.image = '/images/checkbox_unsel.png';
			
			_obj.imgUploadView.height = 0;
			_obj.imgUploadView.visible = false;
		}
	});
	
	_obj.btnSubmitPassport = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmitPassport.title = 'SUBMIT';
	
	// Medicare
	/*_obj.medicareView = Ti.UI.createView(_obj.style.stepView);
	
	_obj.txtMedicareNo = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtMedicareNo.hintText = 'Medicare No*';
	_obj.borderViewS40 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.txtMedicareRefNo = Ti.UI.createTextField(_obj.style.txtPassportNo);
	_obj.txtMedicareRefNo.hintText = 'Medicare Reference No*';
	_obj.borderViewS41 = Ti.UI.createView(_obj.style.borderView);
	
	_obj.termsAgreeView1 = Ti.UI.createView(_obj.style.termsView);
	_obj.termsAgreeView1.backgroundColor = '#e9e9e9';
	_obj.lblAgree1 = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblAgree1.text = 'I agree to the following';
	_obj.lblAgreeTxt1 = Ti.UI.createLabel(_obj.style.lblTermsTxt);
	_obj.lblAgreeTxt1.text = 'We may disclose your personal information to an organisation providing verification of your identity, including on-line verification of your identity where we are required to do so by law, such as under the Anti-Money Laundering and Counter Terrorism Financing Act 2006. We may verify your identity using information held by a Credit Reporting Body (CRB). To do this we may disclose personal information such as your name, date of birth and address to the CRB to obtain an assessment of whether that personal information matches information held by the CRB. The CRB may give us a report on that assessment and to do so may use personal information about you and other individuals in their files. Alternative means of verifying your identity are available on request. If we are unable to verify your identity using information held by a CRB we will provide you with a notice to this effect and give you the opportunity to contact the CRB to update your information held by them';
	_obj.imgAgree1 = Ti.UI.createImageView(_obj.style.imgTerms);
	
	_obj.termsDonotAgreeView1 = Ti.UI.createView(_obj.style.termsView);
	_obj.termsDonotAgreeView1.backgroundColor = '#fff';
	_obj.termsDonotAgreeView1.top = 0;
	_obj.lblDonotAgree1 = Ti.UI.createLabel(_obj.style.lblTerms);
	_obj.lblDonotAgree1.text = 'I do not agree';
	_obj.lblDonotAgreeTxt1 = Ti.UI.createLabel(_obj.style.lblTermsTxt);
	_obj.lblDonotAgreeTxt1.text = 'To get my identity verified through the stated means and would be willing to share any identity documents that you may want such as Passport copy, Driving licence, etc.';
	_obj.imgDonotAgree1 = Ti.UI.createImageView(_obj.style.imgTerms);
	
	_obj.termsAgreeView1.addEventListener('click',function(){
		if(_obj.imgAgree1.image === '/images/checkbox_unsel.png')
		{
			_obj.term1 = 1;
			_obj.imgAgree1.image = '/images/checkbox_sel.png';
			_obj.imgDonotAgree1.image = '/images/checkbox_unsel.png';
		}
		else
		{
			_obj.term1 = 0;
			_obj.imgAgree1.image = '/images/checkbox_unsel.png';
		}
	});
	
	_obj.termsDonotAgreeView1.addEventListener('click',function(){
		if(_obj.imgDonotAgree1.image === '/images/checkbox_unsel.png')
		{
			_obj.term1 = 0;
			_obj.imgDonotAgree1.image = '/images/checkbox_sel.png';
			_obj.imgAgree1.image = '/images/checkbox_unsel.png';
		}
		else
		{
			_obj.term1 = 0;
			_obj.imgDonotAgree1.image = '/images/checkbox_unsel.png';
		}
	});*/
	
	_obj.imgUploadView = Ti.UI.createView(_obj.style.imgUploadView);
	_obj.imgUploadView.height = 0;
	_obj.imgUploadView.visible = false;
	
	_obj.btnPassport = Ti.UI.createButton(_obj.style.btnPassport);
	_obj.btnPassport.title = 'UPLOAD PASSPORT';
	
	_obj.imgView = Ti.UI.createImageView(_obj.style.imgView);
	_obj.lblDocSize = Ti.UI.createLabel(_obj.style.lblDocSize);
	_obj.btnUpload = Ti.UI.createButton(_obj.style.btnUpload);
	
	_obj.btnSubmitDriving = Ti.UI.createButton(_obj.style.btnSubmit);
	_obj.btnSubmitDriving.title = 'SUBMIT';
	
	_obj.passportView.add(_obj.lblNote);
	_obj.passportView.add(_obj.lblNoteTxt);
	_obj.passportView.add(_obj.txtPassportNo);
	_obj.passportView.add(_obj.borderViewS32);
	_obj.countryView.add(_obj.lblCountry);
	_obj.countryView.add(_obj.imgCountry);
	_obj.passportView.add(_obj.countryView);
	_obj.passportView.add(_obj.borderViewS33);
	
	_obj.passportView.add(_obj.txtPassportIssuePlace);
	_obj.passportView.add(_obj.borderViewSPIP);
	
	_obj.COBView.add(_obj.lblCOB);
	_obj.COBView.add(_obj.imgCOB);
	_obj.passportView.add(_obj.COBView);
	_obj.passportView.add(_obj.borderViewS34);
	
	_obj.passportView.add(_obj.txtSurname);
	_obj.passportView.add(_obj.borderViewS35);
	
	_obj.passportView.add(_obj.txtSurnameBirth);
	_obj.passportView.add(_obj.borderViewS36);
	
	_obj.passportView.add(_obj.txtPOB);
	_obj.passportView.add(_obj.borderViewS37);
	
	_obj.termsAgreeView.add(_obj.lblAgree);
	_obj.termsAgreeView.add(_obj.lblAgreeTxt);
	_obj.termsAgreeView.add(_obj.imgAgree);
	_obj.passportView.add(_obj.termsAgreeView);
	
	_obj.termsDonotAgreeView.add(_obj.lblDonotAgree);
	_obj.termsDonotAgreeView.add(_obj.lblDonotAgreeTxt);
	_obj.termsDonotAgreeView.add(_obj.imgDonotAgree);
	_obj.passportView.add(_obj.termsDonotAgreeView);
	
	_obj.imgUploadView.add(_obj.btnPassport);
	_obj.imgUploadView.add(_obj.imgView);
	_obj.imgUploadView.add(_obj.lblDocSize);
	_obj.imgUploadView.add(_obj.btnUpload);
	_obj.passportView.add(_obj.imgUploadView);
	
	_obj.passportView.add(_obj.btnSubmitPassport);
	
	// Medicare
	
	/*_obj.medicareView.add(_obj.txtMedicareNo);
	_obj.medicareView.add(_obj.borderViewS40);
	_obj.medicareView.add(_obj.txtMedicareRefNo);
	_obj.medicareView.add(_obj.borderViewS41);
	
	_obj.termsAgreeView1.add(_obj.lblAgree1);
	_obj.termsAgreeView1.add(_obj.lblAgreeTxt1);
	_obj.termsAgreeView1.add(_obj.imgAgree1);
	_obj.medicareView.add(_obj.termsAgreeView1);
	
	_obj.termsDonotAgreeView1.add(_obj.lblDonotAgree1);
	_obj.termsDonotAgreeView1.add(_obj.lblDonotAgreeTxt1);
	_obj.termsDonotAgreeView1.add(_obj.imgDonotAgree1);
	_obj.medicareView.add(_obj.termsDonotAgreeView1);
	
	_obj.medicareView.add(_obj.btnSubmitDriving);*/
	
	//_obj.scrollableView.views = [_obj.passportView,_obj.medicareView];
	//_obj.scrollableView.views = [_obj.passportView];
	
	_obj.kycView.add(_obj.txtStreet1);
	_obj.kycView.add(_obj.borderViewS23);
	
	_obj.kycView.add(_obj.txtStreetName);
	_obj.kycView.add(_obj.borderViewSName);
	
	_obj.streetTypeView.add(_obj.lblStreetType);
	_obj.streetTypeView.add(_obj.imgStreetType);
	_obj.kycView.add(_obj.streetTypeView);
	_obj.kycView.add(_obj.borderViewSST);
	
	_obj.kycView.add(_obj.txtCity);
	_obj.kycView.add(_obj.borderViewS27);
	
	if(countryName[0] !== 'Singapore')
	{
		if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
		{
			_obj.stateView.add(_obj.lblState);
			_obj.stateView.add(_obj.imgState);
			_obj.kycView.add(_obj.stateView);
			_obj.kycView.add(_obj.borderViewS28);
        }
        else
        {
			_obj.kycView.add(_obj.lblState);
			_obj.kycView.add(_obj.borderViewS28);
        }
	}
	
	_obj.kycView.add(_obj.txtZip);
	_obj.kycView.add(_obj.borderViewS29);
	
	_obj.dayView.add(_obj.lblDay);
	_obj.dayView.add(_obj.txtDay);
	_obj.kycView.add(_obj.dayView);
	_obj.kycView.add(_obj.borderViewS30);
	
	_obj.mobileView.add(_obj.lblMobile);
	_obj.mobileView.add(_obj.txtMobile);
	_obj.kycView.add(_obj.mobileView);
	_obj.kycView.add(_obj.borderViewS31);
	
	_obj.kycView.add(_obj.tabView);
	_obj.tabView.add(_obj.lblPassport);
	//_obj.tabView.add(_obj.lblMedicare);
	_obj.tabView.add(_obj.tabSelView);
	_obj.tabSelIconView.add(_obj.imgTabSel);
	_obj.tabView.add(_obj.tabSelIconView);
	_obj.kycView.add(_obj.passportView);
	
	_obj.mainView.add(_obj.kycView);
	_obj.globalView.add(_obj.mainView);
	_obj.winKYC.add(_obj.globalView);
	_obj.winKYC.open();
	
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	};
	
	function uploadImage()
	{
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR_BCM');

		xhr.call({
			url : TiGlobals.appURLBCM,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"'+TiGlobals.bcmConfig+'",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"source":"'+TiGlobals.bcmSource+'",'+
				'"type":"document",'+
				'"docType":"Passport",'+
				'"platform":"'+TiGlobals.osname+'",'+
				'"corridor":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(evt) {
			activityIndicator.hideIndicator();
			if(evt.result.status === "S")
			{
				require('/utils/Console').info('Result ======== ' + evt.result);
				docType = evt.result.response.docType;
				docTypeCode = evt.result.response.docType;
				docUploadPath = evt.result.response.uploadpath;
				
				_obj.f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,docType+'.jpg');
				_obj.imgUpload = 0;
				
				var optionDialogPhoto = require('/utils/OptionDialog').showOptionDialog('',[L('takePicture'), L('selectPicture')],-1);
				optionDialogPhoto.show();
				
				optionDialogPhoto.addEventListener('click',function(evt){
					if(optionDialogPhoto.options[evt.index] !== L('btn_cancel'))
					{
						if(evt.index === 0)
						{
							// Take Picture
							
							Ti.Media.showCamera({
						        showControls:true,
						        mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
						        autohide:true,
						        allowEditing:false,
						        autorotate:true,
						        success:function(event) {
						        	
									_obj.image = event.media;
						        	_obj.f.write(_obj.image);
						        	
						        	if(_obj.f.size < 5000000)
						        	{
						        		if(TiGlobals.osname === 'android')
						        		{
						        			_obj.imgView.image = _obj.f.nativePath;	
						        		}
						        		else
						        		{
						        			_obj.imgView.image = _obj.f.resolve();
						        		}
							        	
							        	_obj.lblDocSize.text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
							        	_obj.imgView.show();
							        	_obj.btnUpload.show();
							        	_obj.btnPassport.hide();
							        }
							        else
							        {
							        	require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
							        }	
						        }
							});
						}
						
						if(evt.index === 1)
						{
							// Obtain an image from the gallery
							
					        Titanium.Media.openPhotoGallery({
					            success:function(event)
					            { 
					            	if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO)
					                {
					                	_obj.image = event.media;				            
									    _obj.f.write(_obj.image);
									    
									    if(_obj.f.size < 5000000)
							        	{
								        	_obj.imgView.image = _obj.f.read();
								        	_obj.lblDocSize.text = '(File Size : '+ bytesToSize(_obj.f.size) +')';
								        	_obj.imgView.show();
								        	_obj.btnUpload.show();
								        	_obj.btnPassport.hide();
								        }
								        else
								        {
								        	require('/utils/AlertDialog').toast('The file you upload should not exceed the file size of 5 MB.');
								        }
					                }      
					            },
					            cancel:function()
					            {
					            }
					        });
						}
						
						optionDialogPhoto = null;
					}
				});
				
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}
	
		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.btnPassport.addEventListener('click',function(e){
		
		if(TiGlobals.osname === 'android')
		{
			if (Ti.Media.hasCameraPermissions()) {
				uploadImage();
			} else { 
			    Ti.Media.requestCameraPermissions(function(e) {
		             if (e.success === true) {
		             	uploadImage();
		             } else {
		                 /*alert("Access denied, error: " + e.error);*/
		             }
			    });
			}	
		}
		else
		{
			uploadImage();
		}
	});
	
	_obj.btnUpload.addEventListener('click',function(e){
		var targetFile = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/" + Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + docRequestId + "_" + require('/utils/Date').today('-','dmy') + ".jpg";
		var filename = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + docRequestId + "_" + require('/utils/Date').today('-','dmy') + ".jpg";
		var docname = Ti.App.Properties.getString('ownerId') + "_" + docTypeCode + "_" + docRequestId + "_" + require('/utils/Date').today('-','dmy');
		var relativepath = docUploadPath + "/" + TiGlobals.partnerId + "/" + docTypeCode + "/";
		
		var xmlHttp = Ti.Network.createHTTPClient();
      	activityIndicator.showIndicator();
  		xmlHttp.onload = function(e)
		{
			if(xmlHttp.responseText === '1' || xmlHttp.responseText === 1)
			{
				var xhr = require('/utils/XHR');
				xhr.call({
					url : TiGlobals.appURLTOML,
					get : '',
					post : '{' +
						'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
						'"requestName":"KYCVERIFICATIONDOCUPLOAD",'+
						'"partnerId":"'+TiGlobals.partnerId+'",'+
						'"channelId":"'+TiGlobals.channelId+'",'+
						'"ipAddress":"'+TiGlobals.ipAddress+'",'+
						'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
						'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
						'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
						'"countryCode":"'+countryCode[0]+'",'+
						'"docType":"'+docType+'"'+
						'"docRequestId":"'+docRequestId+'"'+
						'"docPath":"'+targetFile+'"'+
						'}',
					success : xhrSuccess,
					error : xhrError,
					contentType : 'application/json',
					timeout : 10000
				});
					
				function xhrSuccess(evt) {
					activityIndicator.hideIndicator();
					if(evt.result.responseFlag === "S")
					{
						if(TiGlobals.osname === 'android')
						{	
							require('/utils/AlertDialog').toast('Document uploaded successfully');
						}
						else
						{
							require('/utils/AlertDialog').iOSToast('Document uploaded successfully');
						}
						
						myDocuments();
					}
					else
					{
						if(evt.result.message === L('invalid_session') || evt.result.message === 'Invalid Session')
						{
							require('/lib/session').session();
							destroy_kyc();
						}
						else
						{
							require('/utils/AlertDialog').showAlert('',evt.result.message,[L('btn_ok')]).show();
						}
					}
					xhr = null;
				}
		
				function xhrError(evt) {
					activityIndicator.hideIndicator();
					require('/utils/Network').Network();
					xhr = null;
				}
				
			}
			else
			{
				activityIndicator.hideIndicator();
				require('/utils/AlertDialog').showAlert('','Error uploading document. Please try again later.',[L('btn_ok')]).show();	
			}
		};
		
		xmlHttp.onerror = function(e)
		{
			activityIndicator.hideIndicator();
		};
		
		require('/utils/Console').info(JSON.stringify({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadPath:relativepath}));
		
		xmlHttp.open('POST', TiGlobals.appUploadBCM + 'upload_doc.php',true);
		xmlHttp.send({doc:_obj.f.read(),source:'APP',name:filename,docname:docname,uploadpath:relativepath});
	});
	
	function changeTabs(selected)
	{
		if(selected === 'passport')
		{
			_obj.tabSelView.left = 0;
			_obj.tabSelView.right = null;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = 0;
			_obj.tabSelIconView.right = null;
			_obj.tabSelIconView.width = '50%';
			_obj.lblPassport.color = TiFonts.FontStyle('whiteFont');
			_obj.lblPassport.backgroundColor = '#6F6F6F';
			_obj.lblMedicare.color = TiFonts.FontStyle('blackFont');
			_obj.lblMedicare.backgroundColor = '#E9E9E9';
		}
		else
		{
			_obj.tabSelView.left = null;
			_obj.tabSelView.right = 0;
			_obj.tabSelView.width = '50%';
			_obj.tabSelIconView.left = null;
			_obj.tabSelIconView.right = 0;
			_obj.tabSelIconView.width = '50%';
			_obj.lblMedicare.color = TiFonts.FontStyle('whiteFont');
			_obj.lblMedicare.backgroundColor = '#6F6F6F';
			_obj.lblPassport.color = TiFonts.FontStyle('blackFont');
			_obj.lblPassport.backgroundColor = '#E9E9E9';
		}
	}
	
	_obj.tabView.addEventListener('click',function(e){
		if(e.source.sel === 'passport' && _obj.tab !== 'passport')
		{
			_obj.scrollableView.movePrevious();
			changeTabs('passport');
			_obj.tab = 'passport';
		}
		
		if(e.source.sel === 'medicare' && _obj.tab !== 'medicare')
		{
			_obj.scrollableView.moveNext();
			changeTabs('medicare');
			_obj.tab = 'medicare';
		}
	});
	
	function submitKYC()
	{
		// Apartment/House No.
		/*_obj.flat = _obj.txtAptNo.value; 
		if(_obj.txtAptNo.value == '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your ' + _obj.txtAptNo.hintText,[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of ' + _obj.txtAptNo.hintText + ' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validateTextField(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('',_obj.txtAptNo.hintText + ' cannot contain  |  or  \'  or  ` or  \" or #',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtAptNo.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtAptNo.value.charAt(i)+' entered for '+_obj.txtAptNo.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtAptNo.value = '';
    		_obj.txtAptNo.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}*/
		
		// StreetNo
		
		_obj.street1 = _obj.txtStreet1.value;
		if(_obj.txtStreet1.value === "")
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Street No',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street No',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(_obj.txtStreet1.value.search("[^0-9]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only numbers are allowed in Street No',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtStreet1.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtStreet1.value.charAt(i)+' entered for '+_obj.txtStreet1.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtStreet1.value = '';
    		_obj.txtStreet1.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		
		// Street Name
		_obj.street2 = _obj.txtStreetName.value;
		if(_obj.txtStreetName.value === "")
		{
			require('/utils/AlertDialog').showAlert('','Please enter your Street Name',[L('btn_ok')]).show();
    		_obj.txtStreetName.value = '';
    		_obj.txtStreetName.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtStreetName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Street Name',[L('btn_ok')]).show();
    		_obj.txtStreetName.value = '';
    		_obj.txtStreetName.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(_obj.txtStreetName.value.search('[^a-zA-Z]') >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in Street Name',[L('btn_ok')]).show();
    		_obj.txtStreetName.value = '';
    		_obj.txtStreetName.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').validate_allow_special_char(_obj.txtStreetName.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+_obj.txtStreetName.value.charAt(i)+' entered for '+_obj.txtStreetName.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtStreetName.value = '';
    		_obj.txtStreetName.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		
		// Street Type
		if(_obj.lblStreetType.text === 'Select Street Type*')
		{
			require('/utils/AlertDialog').showAlert('','Please select a street type',[L('btn_ok')]).show();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
			
		}
		
		// City
		_obj.city = _obj.txtCity.value;
			
		if(_obj.txtCity.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your city',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtCity.value) == false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of city',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(_obj.txtCity.value.search("[^a-zA-Z ]") >= 0)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in city',[L('btn_ok')]).show();
    		_obj.txtCity.value = '';
    		_obj.txtCity.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else
		{
		}	
		
		//State
		if(countryName[0] !== 'Singapore')
		{
			if(countryName[0] === 'United States' || countryName[0] === 'United Kingdom' || countryName[0] === 'Australia' || countryName[0] === 'Canada' || countryName[0] === 'United Arab Emirates')
			{
				
				_obj.state = _obj.lblState.text;
				
				if(countryName[0] === 'United Arab Emirates')
				{
					if(_obj.lblState.text === 'Select Emirates*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your Emirate',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select Emirates*';
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else
					{
						
					}	
				}
				else
				{
					if(_obj.lblState.text === 'Select State*')
					{
						require('/utils/AlertDialog').showAlert('','Please select your state',[L('btn_ok')]).show();
			    		_obj.lblState.text = 'Select State*';
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else
					{
					}
				}
	
	        }
	        else
	        {
				_obj.state = _obj.lblState.value;
				
				if(_obj.lblState.value === '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your state',[L('btn_ok')]).show();
		    		_obj.lblState.value = '';
		    		_obj.lblState.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
				else if(require('/lib/toml_validations').checkBeginningSpace(_obj.lblState.value) === false)
				{
					require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of state',[L('btn_ok')]).show();
		    		_obj.lblState.value = '';
		    		_obj.lblState.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
				else if(_obj.lblState.value.search("[^a-zA-Z ]") >= 0)
				{
					require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in state',[L('btn_ok')]).show();
		    		_obj.lblState.value = '';
		    		_obj.lblState.focus();
		    		_obj.kycView.scrollTo(0,0);
					return;
				}
				else
				{
				}
				
	        }
		}
		
		// Zipcode
		
		if(_obj.txtZip.value === '')
		{
			require('/utils/AlertDialog').showAlert('','Please enter your '+_obj.txtZip.hintText,[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtZip.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of '+_obj.txtZip.hintText+' field',[L('btn_ok')]).show();
    		_obj.txtZip.value = '';
    		_obj.txtZip.focus();
    		_obj.kycView.scrollTo(0,0);
			return;
		}
		
		if(_obj.txtZip.value.length > 0)
		{
	 		var len = _obj.txtZip.value.length;
			var validchars = '';	
			validchars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
			var currChar, prevChar, nextChar;
			var validAlph = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var validNum = "0123456789";
			var validAlphFlag = false;
			var validNumFlag = false;
	
			for(i = 0; i <= len - 1; i++)
			{
				currChar = _obj.txtZip.value.charAt(i);
				prevChar = _obj.txtZip.value.charAt(i-1);
				nextChar = _obj.txtZip.value.charAt(i+1);
	
				if(!(validAlph.indexOf(currChar) == -1) && validAlphFlag == false)
				{
					validAlphFlag = true;
				}
	
				if(!(validNum.indexOf(currChar) == -1) && validNumFlag == false)
				{
					validNumFlag = true;
				}
							
				if(validchars.indexOf(currChar) == -1)
				{
					if(countryName[0] != 'Canada')
					{
						require('/utils/AlertDialog').showAlert('','Invalid character "' + currChar + '" entered in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
				}
	
				if(currChar == ' ' && prevChar == ' ' || prevChar == ' ' && currChar == ' ' && nextChar == ' ' || prevChar == ' ' && currChar == ' ' && i == len-1 || currChar == ' ' && i == len-1)
				{
					if(countryName[0] == 'United Kingdom')
					{
						if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText + '. Only one space is allowed in middle',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else
					{
						if(i == len-1)
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the End of ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else
						{
							require('/utils/AlertDialog').showAlert('','Remove Unnecessary Space in the ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					break;
				}
			}
						
			if((countryName[0] != 'United Arab Emirates') && (countryName[0] != 'UAE') && (countryName[0] != 'Hong Kong')) //Country check for HK & UAE
			{		
				if((countryName[0] == 'Singapore') || (countryName[0] == 'Australia') || (countryName[0] == 'United States') || (countryName[0] == 'Portugal'))
				{
					if(countryName[0] != 'Portugal')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
	
					if((countryName[0] == 'Singapore') && (len != 6))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 6 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if((countryName[0] == 'Australia') && (len != 4))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if((countryName[0] == 'United States') && (len != 5))
					{
						require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
			    		_obj.txtZip.value = '';
			    		_obj.txtZip.focus();
			    		_obj.kycView.scrollTo(0,0);
						return;
					}
					else if((countryName[0] == 'Portugal'))
					{			
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else if( len < 3 || len > 25)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 25 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
				}
				else
				{
					if(countryName[0] == 'Canada')
					{
						var pcode = _obj.txtZip.value;
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						else if(pcode.indexOf(" ") != 3)
						{
							require('/utils/AlertDialog').showAlert('','There must be a space after 3rd character in '+_obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'United Kingdom')
					{			
						if( len < 5 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 5 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else
					{
						if( len < 3 || len > 8)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of minimum 3 and maximum 8 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
	
					if((countryName[0] == 'France') || (countryName[0] == 'Germany') || (countryName[0] == 'Italy') || (countryName[0] == 'Euroland') || (countryName[0] == 'New Zealand'))
					{
						var pncode = _obj.txtZip.value;
						if(pncode.split(" ").length > 2)
						{
							require('/utils/AlertDialog').showAlert('','Only one single space is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
							
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'Belgium')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
	
						if(len != 4)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 4 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'Spain')
					{
						if(!validNumFlag == true)
						{
							require('/utils/AlertDialog').showAlert('','Only Alphanumeric or Numeric is allowed in ' + _obj.txtZip.hintText,[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'United States')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						if(len != 5)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 5 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] == 'Japan')
					{
						if(_obj.txtZip.value.search("[^0-9]") >= 0)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be a Numeric Value',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
						if(len != 7)
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' should be of 7 characters',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
					else if(countryName[0] != 'Spain')
					{ 
						if(!(validAlphFlag == true && validNumFlag == true))
						{
							require('/utils/AlertDialog').showAlert('',_obj.txtZip.hintText + ' must be Alpha Numeric',[L('btn_ok')]).show();
				    		_obj.txtZip.value = '';
				    		_obj.txtZip.focus();
				    		_obj.kycView.scrollTo(0,0);
							return;
						}
					}
				}
			} 
		 }
		 
		////////////////////// Mobile No ////////////////////
		if(_obj.txtMobile.value === '' || _obj.txtMobile.value === 'Mobile Number*')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtMobile.value) === false)
		{
			require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(countryName[0] === 'United Kingdom')
		{ 						
			if(_obj.txtMobile.value.charAt(0) === 0 && _obj.txtMobile.value != '')
			{
				require('/utils/AlertDialog').showAlert('','Please enter your Mobile Number without 0 in the beginning',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
		|| countryName[0] === 'Finland' || countryName[0] === 'France' 
		|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
		|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
		|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
		 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 10))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal' || countryName[0] === 'Belgium' || countryName[0] === 'Netherlands')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(countryName[0] === 'Germany')
		{ 									
			if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 11))
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
	    		_obj.txtMobile.value = '';
	    		_obj.txtMobile.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 8 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Cyprus')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if(_obj.txtMobile.value.length != 0 && (_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 11) && countryName[0] === 'New Zealand')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if((_obj.txtMobile.value.length < 9 || _obj.txtMobile.value.length > 10) && countryName[0] === 'Spain')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if((_obj.txtMobile.value.length < 10 || _obj.txtMobile.value.length > 11) && countryName[0] === 'Austria')
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else if((_obj.txtMobile.value.length < 7 || _obj.txtMobile.value.length > 9) && ((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')))
		{
			require('/utils/AlertDialog').showAlert('','Please enter a valid Mobile Number',[L('btn_ok')]).show();
    		_obj.txtMobile.value = '';
    		_obj.txtMobile.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		else
		{
			
		}
		
		////////////////////// Daytime No ////////////////////
		//if UAE than it is not cumpusory
		if(countryName[0] != 'United Arab Emirates' && countryName[0] != 'UAE') 
		{		
			if(_obj.txtDay.value === '' || _obj.txtDay.value === 'Day Time Number*')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtDay.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(require('/lib/toml_validations').checkinLastSpace(_obj.txtDay.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the end of Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if((_obj.txtDay.value.length < 9 || _obj.txtDay.value.length > 10) && countryName[0] === 'Spain')
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(_obj.txtDay.value.length != 0 && _obj.txtDay.value.length < 7 && countryName[0] === 'New Zealand')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if((_obj.txtDay.value.length < 3 || _obj.txtDay.value.length > 9) && countryName[0] === 'Germany')
			{
				require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
			else if(countryName[0] === 'United Kingdom')
			{
				if(_obj.txtDay.value.charAt(0) === 0 && _obj.txtDay.value != '')
				{
					require('/utils/AlertDialog').showAlert('','Please enter your Day Time Number without 0 in the beginning',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
				else if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
			else if(countryName[0] === 'United States' || countryName[0] === 'Canada' 
			|| countryName[0] === 'Finland' || countryName[0] === 'France' 
			|| countryName[0] === 'Greece' || countryName[0] === 'Ireland' 
			|| countryName[0] === 'Italy' || countryName[0] === 'Japan' 
			|| countryName[0] === 'Luxembourg' || countryName[0] === 'Malta' ||
			 countryName[0] === 'Slovakia' || countryName[0] === 'Slovenia')
			{ 									
				if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 10))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
			else if(countryName[0] === 'Australia' || countryName[0] === 'Portugal')
			{ 									
				if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 9))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
			else if(countryName[0] === 'Belgium' || countryName[0] === 'Netherlands' || countryName[0] === 'Singapore' || countryName[0] === 'Hong Kong')
			{ 									
				if(_obj.txtDay.value.length != 0 && (_obj.txtDay.value.length < 8))
				{
					require('/utils/AlertDialog').showAlert('','Please enter a valid Day Time Number',[L('btn_ok')]).show();
		    		_obj.txtDay.value = '';
		    		_obj.txtDay.focus();
		    		_obj.kycView.scrollTo(0,150);
					return;
				}
			}
		}
		if(((countryName[0] === 'United Arab Emirates') || (countryName[0] === 'UAE')) && _obj.txtDay.value.length > 0)
		{
			if(_obj.txtDay.value.length < 7 || _obj.txtDay.value.length > 9)
			{
				require('/utils/AlertDialog').showAlert('','Please enter valid residence phone Number',[L('btn_ok')]).show();
	    		_obj.txtDay.value = '';
	    		_obj.txtDay.focus();
	    		_obj.kycView.scrollTo(0,150);
				return;
			}
		}
		
		if(_obj.txtMobile.value === _obj.txtDay.value)
		{
			require('/utils/AlertDialog').showAlert('','Day Time No. and Mobile No. cannot be the same',[L('btn_ok')]).show();
    		_obj.txtDay.value = '';
    		_obj.txtDay.focus();
    		_obj.kycView.scrollTo(0,150);
			return;
		}
		
		// If Passport
		
		if(_obj.tab === 'passport')
		{
			// Passport No
			if(_obj.txtPassportNo.value.trim() === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a passport number',[L('btn_ok')]).show();
	    		_obj.txtPassportNo.value = '';
	    		_obj.txtPassportNo.focus();
	    		_obj.kycView.scrollTo(0,250);
				return;		
			}
			else
			{
				
			}
			
			// PassportCountry
			if(_obj.lblCountry.text === 'Passport Country*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a passport country',[L('btn_ok')]).show();
				_obj.kycView.scrollTo(0,250);
	    		return;		
			}
			else
			{
				
			}
			
			// Passport Issue Place
			
			if(_obj.txtPassportIssuePlace.value === "")
			{
				require('/utils/AlertDialog').showAlert('','Please provide a passport place of issue',[L('btn_ok')]).show();
	    		_obj.txtPassportIssuePlace.value = '';
	    		_obj.txtPassportIssuePlace.focus();
	    		_obj.kycView.scrollTo(0,250);
				return;
			}
			else if(require('/lib/toml_validations').checkBeginningSpace(_obj.txtPassportIssuePlace.value) === false)
			{
				require('/utils/AlertDialog').showAlert('','Space not allowed at the beginning of passport place of issue',[L('btn_ok')]).show();
	    		_obj.txtPassportIssuePlace.value = '';
	    		_obj.txtPassportIssuePlace.focus();
	    		_obj.kycView.scrollTo(0,250);
				return;
			}
			else if(_obj.txtPassportIssuePlace.value.search("[^a-zA-Z ]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Only alphabets are allowed in passport place of issue field',[L('btn_ok')]).show();
	    		_obj.txtPassportIssuePlace.value = '';
	    		_obj.txtPassportIssuePlace.focus();
	    		_obj.kycView.scrollTo(0,250);
				return;
			}
			else
			{
				
			}
				
			// Country of Birth
			if(_obj.lblCOB.text === 'Country of Birth*')
			{
				require('/utils/AlertDialog').showAlert('','Please select a country of birth',[L('btn_ok')]).show();
	    		return;		
			}
			else
			{
				
			}
			
			// Surname at Citizenship
			if(_obj.txtSurname.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a surname at citizenship',[L('btn_ok')]).show();
	    		_obj.txtSurname.value = '';
	    		_obj.txtSurname.focus();
	    		return;		
			}
			else
			{
				
			}
			
			// Surname at Birth
			if(_obj.txtSurnameBirth.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a surname at birth',[L('btn_ok')]).show();
	    		_obj.txtSurnameBirth.value = '';
	    		_obj.txtSurnameBirth.focus();
	    		_obj.kycView.scrollTo(0,250);
	    		return;		
			}
			else
			{
				
			}
			
			// POB
			if(_obj.txtPOB.value === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a passport place of birth',[L('btn_ok')]).show();
	    		_obj.txtPOB.value = '';
	    		_obj.txtPOB.focus();
	    		_obj.kycView.scrollTo(0,250);
	    		return;		
			}
			else
			{
				
			}
			
			if(_obj.term === null)
			{
				require('/utils/AlertDialog').showAlert('','Please select any one of the option',[L('btn_ok')]).show();
	    		return;
			}
			else
			{
				
			}
		}
		
		/*if(_obj.tab === 'medicare')
		{
			// Medicare No
			if(_obj.txtMedicareNo.value.trim() === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a medicare number',[L('btn_ok')]).show();
	    		_obj.txtMedicareNo.value = '';
	    		_obj.txtMedicareNo.focus();
	    		return;		
			}
			else if(_obj.txtMedicareNo.search("[^0-9-]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Medicare number should be numeric only',[L('btn_ok')]).show();
	    		_obj.txtMedicareNo.value = '';
	    		_obj.txtMedicareNo.focus();
	    		return;		
			}
			else
			{
				
			}
			
			// Medicare Ref. No
			if(_obj.txtMedicareRefNo.value.trim() === '')
			{
				require('/utils/AlertDialog').showAlert('','Please provide a medicare reference number',[L('btn_ok')]).show();
	    		_obj.txtMedicareRefNo.value = '';
	    		_obj.txtMedicareRefNo.focus();
	    		return;		
			}
			else if(_obj.txtMedicareNo.search("[^0-9-]") >= 0)
			{
				require('/utils/AlertDialog').showAlert('','Medicare reference number should be numeric only',[L('btn_ok')]).show();
	    		_obj.txtMedicareRefNo.value = '';
	    		_obj.txtMedicareRefNo.focus();
	    		return;		
			}
			else
			{
				
			}
			
			if(_obj.term1 === null)
			{
				require('/utils/AlertDialog').showAlert('','Please select any one of the option',[L('btn_ok')]).show();
	    		return;
			}
			else
			{
				
			}
		}*/
		
		// xhr call
		
		activityIndicator.showIndicator();
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"CUSTKYCVERIFICATIONREGISTRATION",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"loginId":"'+Ti.App.Properties.getString('loginId')+'",'+ 
				'"sessionId":"'+Ti.App.Properties.getString('sessionId')+'",'+
				'"ownerId":"'+Ti.App.Properties.getString('ownerId')+'",'+
				'"operatingmodeId":"INTL",'+
				'"roleId":"SEND",'+
				'"flatNo":"'+_obj.street1+'",'+
				'"buildingNo":"",'+
				'"street1":"'+_obj.street2+'",'+
				'"street2":"'+_obj.streetType+'",'+
				'"locality":"",'+
				'"subLocality":"",'+
				'"vendorId":"F",'+
				'"zipCode":"'+_obj.txtZip.value+'",'+
				'"city":"'+_obj.city+'",'+
				'"state":"'+_obj.state+'",'+
				'"country":"'+countryName[0]+'",'+
				'"ssn":"",'+
				'"mrzLine":"",'+
				'"drivingLicenseNo":"",'+
				'"passportNo":"'+_obj.txtPassportNo.value+'",'+
				'"passportIssuePlace":"'+_obj.txtPassportIssuePlace.value+'",'+
				'"passNationality":"'+_obj.lblCOB.text+'",'+
				'"passportPersonalNo":"",'+
				'"passportCountry":"'+_obj.lblCountry.text+'",'+
				'"passportBirthPlace":"'+_obj.txtPOB.value+'",'+
				'"medicareNo":"",'+
				'"medicareRefNo":"",'+
				'"surnameAtCitizenShip":"'+_obj.txtSurname.value+'",'+
				'"surnameAtBirth":"'+_obj.txtSurnameBirth.value+'",'+
				'"countryOfBirth":"'+_obj.lblCOB.text+'",'+
				'"passExpiryDate":"",'+
				'"countryCode":"'+countryCode[0]+'",'+
				'"dayTimeNumber":"'+_obj.txtDay.value+'",'+
				'"mobileNo":"'+_obj.txtMobile.value+'",'+
				'"privacyPolicyFlag":"Y"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});
		

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast(e.result.message);
				}
				else
				{
					require('/utils/AlertDialog').iOSToast(e.result.message);
				}
				
				try{
					Ti.App.fireEvent('changekycFlag');
				}catch(e){}
				
				destroy_kyc();
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
			require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	_obj.btnSubmitDriving.addEventListener('click',function(e){
		submitKYC();
	});
	
	_obj.btnSubmitPassport.addEventListener('click',function(e){
		submitKYC();
	});
	
	_obj.imgClose.addEventListener('click',function(e){
		destroy_kyc();
	});
	
	_obj.winKYC.addEventListener('androidback', function(){
		var alertDialog = Ti.UI.createAlertDialog({
			buttonNames:[L('btn_yes'), L('btn_no')],
			message:L('screen_exit')
		});
	
		alertDialog.show();
		
		alertDialog.addEventListener('click', function(e){
			alertDialog.hide();
			if(e.index === 0 || e.index === "0")
			{
				destroy_kyc();
				alertDialog = null;
			}
		});
	});
	
	function country()
	{
		activityIndicator.showIndicator();
		var countryOptions = [];
		
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
		
				for(var i=0;i<splitArr.length;i++)
				{
					countryOptions.push(splitArr[i]);
				}
				
				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Country',countryOptions,-1);
				optionDialogCountry.show();
				
				optionDialogCountry.addEventListener('click',function(evt){
					if(optionDialogCountry.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblCountry.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					}
					else
					{
						optionDialogCountry = null;
						_obj.lblCountry.text = 'Passport Country*';
					}
				});	
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
		    require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function nationality()
	{
		activityIndicator.showIndicator();
		var countryOptions = [];
		
		var xhr = require('/utils/XHR');
		
		xhr.call({
			url : TiGlobals.appURLTOML,
			get : '',
			post : '{' +
				'"requestId":"'+ Math.floor((Math.random() * 1000000000) + 10000) +'",'+
				'"requestName":"GETCOUNTRYRELATEDDETAILS",'+
				'"partnerId":"'+TiGlobals.partnerId+'",'+
				'"channelId":"'+TiGlobals.channelId+'",'+
				'"ipAddress":"'+TiGlobals.ipAddress+'",'+
				'"country":"'+countryName[0]+'"'+
				'}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			activityIndicator.hideIndicator();
			if(e.result.responseFlag === "S")
			{
				var splitArr = e.result.citizenship.split('~');
		
				for(var i=0;i<splitArr.length;i++)
				{
					countryOptions.push(splitArr[i]);
				}
				
				var optionDialogCountry = require('/utils/OptionDialog').showOptionDialog('Nationality',countryOptions,-1);
				optionDialogCountry.show();
				
				optionDialogCountry.addEventListener('click',function(evt){
					if(optionDialogCountry.options[evt.index] !== L('btn_cancel'))
					{
						_obj.lblCOB.text = optionDialogCountry.options[evt.index];
						optionDialogCountry = null;
					}
					else
					{
						optionDialogCountry = null;
						_obj.lblCOB.text = 'Passport Nationality*';
					}
				});
			}
			else
			{
				if(e.result.message === L('invalid_session') || e.result.message === 'Invalid Session')
				{
					require('/lib/session').session();
					destroy_kyc();
				}
				else
				{
					require('/utils/AlertDialog').showAlert('',e.result.message,[L('btn_ok')]).show();
				}
			}
			xhr = null;
		}

		function xhrError(e) {
			activityIndicator.hideIndicator();
		    require('/utils/Network').Network();
			xhr = null;
		}
	}
	
	function destroy_kyc()
	{
		try{
			if (_obj.globalView === null)
			{
				return;
			}
			
			require('/utils/Console').info('############## Remove registration start ##############');
			
			_obj.winKYC.close();
			require('/utils/RemoveViews').removeViews(_obj.winKYC,_obj.globalView);
			
			_obj = null;
			
			// Remove event listeners
			Ti.App.removeEventListener('destroy_kyc',destroy_kyc);
			require('/utils/Console').info('############## Remove registration end ##############');
		}
		catch(e)
		{require('/utils/Console').info(e);}
	}
	
	Ti.App.addEventListener('destroy_kyc', destroy_kyc);
}; // KYCModal()