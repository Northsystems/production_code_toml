///////// Global App Settings /////////

var TiGlobals = {
	platformHeight : 0,
	platformWidth : 0,
	osname : Ti.Platform.osname,

	// Custom
	/*appURLTOML : 'https://www.remit2india.com/json/services',
	//appURLBCM : 'https://172.16.0.28/mobileApp/config.php',
	//appURLBCM : 'http://172.16.0.28/mobileApp/config.php',
	appURLBCM : 'https://m.remit2india.com/mobileApp/config.php',
	//appURLBCM : 'http://www.remit2india.com/mobileApp/config.php',
	appUploadBCM : 'https://www.remit2india.com/',
	staticPagesURL : 'https://www.remit2india.com/mobileApp/',
	pushURL : 'https://m.remit2india.com/mobileApp/push.php', */
	
    //appURLTOML : 'http://testnew.remit2india.com/json/services',
    appURLTOML:'http://test.timesofmoney.com/json/services',
	appURLBCM : 'https://mstage.remit2india.com/mobileApp/config.php',
	appUploadBCM : 'https://mstage.remit2india.com/',
	staticPagesURL : 'https://m.remit2india.com/mobileApp/',
	pushURL : 'https://mstage.remit2india.com/mobileApp/push.php',

	
	googleAPI : '',
	menuSel : '0',
	menuOpen : '0',
	indicatorCheck : 0, // Check for android back
	sessionCheck : 0, // Check for session dialog

	// App specific

	partnerId : 'TOML',
	pushChannelId : 1,
	channelId : 'MobileApp',
	ipAddress : '127.0.0.1',
	bcmConfig : 'CONFIG',
	bcmSource : 'MobileApp',

	// App variables
	minExchangeValue : 0,
	maxExchangeValue : 0,
};

TiGlobals.platformHeight = TiGlobals.osname === 'android' ? ((Ti.Platform.displayCaps.platformHeight / Ti.Platform.displayCaps.logicalDensityFactor) - 25) : Ti.Platform.displayCaps.platformHeight;
TiGlobals.platformWidth = TiGlobals.osname === 'android' ? Ti.Platform.displayCaps.platformWidth / Ti.Platform.displayCaps.logicalDensityFactor : Ti.Platform.displayCaps.platformWidth;

var base64 = require('/lib/Crypt/Base64');
//mstage key
var key = base64.decode('irBvFMACV3956lHuu+y3jQ==');

//Production key
//var key = base64.decode('ynw1TalFGK+0FGaClrjHYA==');

// Global Container
var _scrollableView = null;

// Activity indicator assigned in main
var ActivityIndicator = null;
var activityIndicator = null;

var viewArr = [];

// Fonts
var TiFonts = require('/utils/Font');

require('/lib/config').loggedIn();

//////////// IP Address ////////////

function ipFinder() {
	var xmlhttp = Ti.Network.createHTTPClient();
	xmlhttp.onload = function()
	{
		require('/utils/Console').info('freegeoip.net' + xmlhttp.responseText);
		var hostipInfo = JSON.parse(xmlhttp.responseText);
		TiGlobals.ipAddress = hostipInfo.ip;
		require('/utils/Console').info(TiGlobals.ipAddress);
		xmlhttp = null;
	};
	
	xmlhttp.onerror = function(e)
	{
		TiGlobals.ipAddress = '127.0.0.1';
	};
	
	xmlhttp.open('GET', 'http://freegeoip.net/json/',true);
	xmlhttp.send();
}

ipFinder();

// Main
var Main = require('/js/Main');
var winMain = new Main();

if (TiGlobals.osname === 'iphone') {
	winMain.orientationModes = [Ti.UI.PORTRAIT];

	winMain.open({
		transition : Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT
	});

	// App in background

	Ti.App.addEventListener('resumed', function(e) {
		require('/utils/Console').info('Resume');
	});

	Ti.App.addEventListener('paused', function(e) {
		require('/utils/Console').info('Pause');
	});

} else if (TiGlobals.osname === 'ipad') {
	winMain.orientationModes = [Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT];
	winMain.open({
		transition : Ti.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT
	});

} else {
	winMain.exitOnClose = true;
	winMain.orientationModes = [Ti.UI.PORTRAIT];
	winMain.open();

	var clickCheck = 0;
	winMain.addEventListener('androidback', function() {

		if (clickCheck === 1) {
			return;
		}

		clickCheck = 1;
		// Reset click
		setTimeout(function() {
			clickCheck = 0;
		}, 350);

		if (TiGlobals.indicatorCheck === 0) {
			popView();
		}
	});
}

/////////// Social Apps ///////////

var fb = require('facebook');

if (TiGlobals.osname === 'android') {
	winMain.fbProxy = fb.createActivityWorker({
		lifecycleContainer : winMain
	});
}

var social = require('/lib/social');

var linkedIn = social.create({
	site : 'linkedin',
	consumerKey : '75itpvupwsy8lc',
	consumerSecret : 'sB9Zsugu3DPeOAgd'
});

var twitter = social.create({
	site : 'twitter',
	consumerKey : 'd00C5MvGsA5vfr6Q3GKKxliZG',
	consumerSecret : '4fMYDDG7JckMEEOLNS6smrWzoZnwC946wead9SgOkWC53Ebw5v'
});

var GoogleAuth = require('lib/googleAuth');
var googleAuth = new GoogleAuth({
	clientId : '61470950714-h4frlbvm5bidncl76o7r5pnojmtn6ql2.apps.googleusercontent.com',
	clientSecret : 'gD08L_yrr6WlbDU-Lil-XBCx',
	propertyName : 'googleToken',
	scope : ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email']
});

// Rate App
require('/lib/rate').plus(1, true);

// Back handler

function pushView(view) {
	if (viewArr.indexOf(view) === -1) {
		viewArr.push(view);
	}

	require('/utils/Console').info('Stacked Pages ====> ' + viewArr.length);
};

function popView() {

	if (viewArr.length > 1) {

		var currentPg = viewArr[viewArr.length - 1];
		var previousPg = viewArr[viewArr.length - 2];

		if (TiGlobals.menuOpen === '1') {
			Ti.App.fireEvent('menuAnimate');
			return;
		} else {

			if (Titanium.Network.online) {
				// Current page destroy
				require('/utils/PageController').pageBackController(currentPg);
			} else {
				require('/utils/Network').Network();
			}
		}
	} else {
		if (TiGlobals.menuOpen === '0') {
			var alertDialog = require('/utils/AlertDialog').showAlert('', L('exit_app'), [L('btn_yes'), L('btn_no')]);
			alertDialog.show();

			alertDialog.addEventListener('click', function(e) {
				alertDialog.hide();
				if (e.index === 0 || e.index === "0") {
					alertDialog = null;
					winMain.close();
				}
			});
		} else if (TiGlobals.menuOpen === '1') {
			// Close Menu
			Ti.App.fireEvent('menuAnimate');
			return;
		} else {
			// do nothing
		}
	}
};

function emptyView() {
	viewArr = [];
};

/////////////// URL SCHEME /////////////////

var resSplit = [];
function redirectScheme(res, schemeType) {
	if (schemeType === 'custom') {
		require('/utils/Console').info('RES === ' + res.validation);
		if (res.validation.search('%3D') == -1) {
			var res1 = (Ti.Utils.base64decode(Ti.Utils.base64decode(res.validation + '=='))).toString();
		} else {
			var res1 = (Ti.Utils.base64decode(Ti.Utils.base64decode(res.validation.replace('%3D', '=')))).toString();
		}
		resSplit = res1.split('#$$#');
		require('/utils/Console').info('length === ' + resSplit.length + '====Original === ' + res1 + ' ======= Split === ' + resSplit[resSplit.length - 1]);
		if (resSplit[resSplit.length - 1] === 'mor') {
			require('/utils/Console').info('mor === ' + resSplit[resSplit.length - 1]);
			setTimeout(function() {
				//require('/js/Detail').Detail(resSplit[0]);
			}, 500);
		}
	} else {
		resSplit = res.split('#$$#');
		require('/utils/Console').info('length === ' + resSplit.length + '====Original === ' + res + ' ======= Split === ' + resSplit[resSplit.length - 1]);
		if (resSplit[resSplit.length - 1] === 'mor') {
			require('/utils/Console').info('mor === ' + resSplit[resSplit.length - 1]);
			setTimeout(function() {
				//require('/js/Detail').Detail(resSplit[0]);
			}, 500);
		}
	}
}

// We don't want our URL to do anything before our main window is open

var type = '';

function urlToObject(url) {
	//require('/utils/Console').info(url);
	if (url.indexOf('remit2india:/?') > -1) {
		type = 'custom';
		//require('/utils/Console').info(url);
		var returnObj = {};
		url = url.replace('remit2india:/?', '');
		var params = url.split('&');
		params.forEach(function(param) {
			var keyAndValue = param.split('=');
			//returnObj[keyAndValue[0]] = decodeURI(keyAndValue[1]);
			returnObj[keyAndValue[0]] = keyAndValue[1];
		});
		return returnObj;

	} else {
		type = 'http';
		var urlSplit = url.split('/');
		return urlSplit[4] + '#$$#' + urlSplit[3];
	}
}

// Handle the URL in case it resumed the app

Ti.App.addEventListener('resumed', function() {
	ipFinder();
	try {
		if (Ti.App.getArguments().url !== '' || Ti.App.getArguments().url !== '' || Ti.App.getArguments().url !== 'undefined') {
			var res = urlToObject(Ti.App.getArguments().url);
			redirectScheme(res, type);
		}
	} catch(e) {
	}
});

winMain.addEventListener('open', function(e) {
	ipFinder();
	if (Ti.Platform.osname === 'iphone' || Ti.Platform.osname === 'ipad') {
		// Handle the URL in case it opened the app
		try {
			if (Ti.App.getArguments().url !== '' || Ti.App.getArguments().url !== '' || Ti.App.getArguments().url !== 'undefined') {

				var res = urlToObject(Ti.App.getArguments().url);
				redirectScheme(res, type);
			}
		} catch(e) {
		}
	} else {

		var intentURL = Ti.Android.currentActivity.intent.data;
		//require('/utils/Console').info('intentURL ===== '+intentURL);
		// On Android, somehow the app always opens as new
		try {
			if (intentURL !== null || intentURL !== '' || intentURL !== 'undefined') {
				var res = urlToObject(intentURL);
				redirectScheme(res, type);
			}
		} catch(e) {
		}

		// Android resume and pause events

		winMain.activity.addEventListener('resume', function() {
			require('/utils/Console').info('Resumed');
			ipFinder();
		});
		
		winMain.activity.addEventListener('pause', function() {
			require('/utils/Console').info('Pause');
			ipFinder();
			//if(Ti.App.Properties.getString('mPIN') === '1')
			//{
				//require('/js/Passcode').Passcode();
			//}
		});
	}
});

////////// PUSH NOTIFICATIONS /////////////

function redirectPush(res) {
	try {
		if (Ti.Platform.osname === 'android') {
			var resSplit = res.pl.split('||');
			var title = res.android.title;
			var message = res.android.alert;
		} else {
			var resSplit = res.data.pl.split('||');
			var title = res.data.title;
			var message = res.data.alert;
		}
		if (resSplit[0] === 'mor') {
			var alertDialog = Ti.UI.createAlertDialog({
				title : '',
				buttonNames : ['Yes', 'No'],
				message : title + ' Do you want to check it out?'
			});
			alertDialog.show();
			alertDialog.addEventListener('click', function(e) {
				alertDialog.hide();
				if (e.index === 0 || e.index === "0") {
					alertDialog = null;
					//require('/js/Detail').Detail(resSplit[1]);
				}
			});

		} else {

		}
	} catch(e) {
		//require('/utils/Console').info('Invalid push data');
	}
}

if (Ti.Platform.osname === 'android') {
	var deviceToken;
	var Cloud = require('ti.cloud');
	Cloud.debug = true;
	var CloudPush = require('ti.cloudpush');
	CloudPush.debug = true;
	//CloudPush.enabled = true;
	CloudPush.showTrayNotificationsWhenFocused = false;
	CloudPush.focusAppOnPush = false;
	CloudPush.singleCallback = true;

	function checkDeviceTokenAndroid() {
		var xhr = require('/utils/XHR_BCM');
		xhr.call({
			url : TiGlobals.pushURL,
			get : '',
			post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"CHECKPUSH",' + '"source":"mobile",' + '"platform":"' + TiGlobals.osname + '",' + '"deviceid":"' + Ti.App.Properties.getString('deviceToken') + '",' + '"uId":"' + Ti.App.Properties.getString('ownerId') + '",' + '"channelId":"' + TiGlobals.pushChannelId + '"' + '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Success:' + e.result);
			if (e.result.status === 'F') {
				// Not found in database
				try {
					Cloud.Users.create({
						username : deviceToken.substr(0, 90),
						first_name : deviceToken.substr(0, 90),
						last_name : deviceToken.substr(0, 90),
						password : deviceToken.substr(0, 90),
						password_confirmation : deviceToken.substr(0, 90)
					}, function(e) {
						if (e.success) {
							require('/utils/Console').info('Success:' + JSON.stringify(e));
							var user = e.users[0];

							require('/utils/Console').info('push/push.php?deviceid=' + deviceToken + '&acsuser=' + deviceToken.substr(0, 90) + '&acspwd=' + deviceToken.substr(0, 90) + '&platform=' + TiGlobals.osname + '&acsid=' + user.id);

							var xmlHttpACS = require('/utils/XHR_BCM');
							xmlHttpACS.call({
								url : TiGlobals.pushURL,
								get : '',
								post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"PUSH",' + '"source":"mobile",' + '"platform":"' + TiGlobals.osname + '",' + '"deviceid":"' + Ti.App.Properties.getString('deviceToken') + '",' + '"uId":"' + Ti.App.Properties.getString('ownerId') + '",' + '"acsuser":"' + deviceToken.substr(0, 90) + '",' + '"acspwd":"' + deviceToken.substr(0, 90) + '",' + '"acsid":"' + user.id + '",' + '"channelId":"' + TiGlobals.pushChannelId + '"' + '}',
								success : xmlHttpACSSuccess,
								error : xmlHttpACSError,
								contentType : 'application/json',
								timeout : 10000
							});

							function xmlHttpACSSuccess(eACS) {
								require('/utils/Console').info('Success:' + eACS.result);
								if (eACS.result.status === 'S') {
									loginAndroidCloudUser();
								}
								xmlHttpACS = null;
							}

							function xmlHttpACSError(eACS) {
								require('/utils/Network').Network();
								xmlHttpACS = null;
							}

						} else {
							//require('/utils/Console').info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
						}
					});
				} catch(e) {
				}
			} else {
				loginAndroidCloudUser();
			}
			xhr = null;
		}

		function xhrError(e) {
			require('/utils/Network').Network();
			xhr = null;
		}

	}

	// Fetch device token

	CloudPush.retrieveDeviceToken({
		success : function deviceTokenSuccess(e) {
			deviceToken = e.deviceToken;
			Ti.App.Properties.setString('deviceToken', deviceToken);
			require('/utils/Console').info('Device Token: ' + deviceToken);
			checkDeviceTokenAndroid();
		},
		error : function deviceTokenError(e) {
			//require('/utils/Console').info('Failed to register for push! ' + e.error);
		}
	});

	function loginAndroidCloudUser(e) {
		//Create a Default User in Cloud Console, and login with same credential
		Cloud.Users.login({
			login : deviceToken.substr(0, 90),
			password : deviceToken.substr(0, 90)
		}, function(e) {
			if (e.success) {
				require('/utils/Console').info("Android Login success");
				androidCloudUserSubscribe();
			} else {
				require('/utils/Console').info('Android Login error: ' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});
	}

	// Subscribe for push notification on cloud server

	function androidCloudUserSubscribe() {
		Cloud.PushNotifications.subscribe({
			channel : 'Remit2India',
			device_token : deviceToken,
			type : 'gcm'
		}, function(e) {
			if (e.success) {
				require('/utils/Console').info('Subscribed for Push Notification!');
			} else {
				require('/utils/Console').info('Subscribe error:' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});
	}

	function receivePush(evt) {
		try {
			require('/utils/Console').info(evt.payload);
			var result = JSON.parse(evt.payload);
			setTimeout(function() {
				redirectPush(result);
			}, 1000);
		} catch(e) {
			require('/utils/Console').info('Error in push');
		}
	}


	CloudPush.addEventListener('callback', function(evt) {
		receivePush(evt);
	});

	CloudPush.addEventListener('trayClickLaunchedApp', function(evt) {
		//require('/utils/Console').info('@@## Tray Click Launched App (app was not running)');
		//receivePush(evt);
	});

	CloudPush.addEventListener('trayClickFocusedApp', function(evt) {
		//require('/utils/Console').info('@@## Tray Click Focused App (app was already running)');
		//receivePush(evt);
	});
} else {

	//require('/utils/Console').info('Cloud Push Enter');
	var Cloud = require('ti.cloud');
	var deviceToken;

	function checkDeviceTokenIOS() {
		var xhr = require('/utils/XHR_BCM');
		xhr.call({
			url : TiGlobals.pushURL,
			get : '',
			post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"CHECKPUSH",' + '"source":"mobile",' + '"platform":"' + TiGlobals.osname + '",' + '"deviceid":"' + Ti.App.Properties.getString('deviceToken') + '",' + '"uId":"' + Ti.App.Properties.getString('ownerId') + '",' + '"channelId":"' + TiGlobals.pushChannelId + '"' + '}',
			success : xhrSuccess,
			error : xhrError,
			contentType : 'application/json',
			timeout : 10000
		});

		function xhrSuccess(e) {
			require('/utils/Console').info('Success:' + e.result);
			if (e.result.status === 'F') {
				// Not found in database
				try {
					Cloud.Users.create({
						username : deviceToken.substr(0, 90),
						first_name : deviceToken.substr(0, 90),
						last_name : deviceToken.substr(0, 90),
						password : deviceToken.substr(0, 90),
						password_confirmation : deviceToken.substr(0, 90)
					}, function(e) {
						if (e.success) {
							require('/utils/Console').info('Success:' + JSON.stringify(e));
							var user = e.users[0];

							var xmlHttpACS = require('/utils/XHR_BCM');
							xmlHttpACS.call({
								url : TiGlobals.pushURL,
								get : '',
								post : '{' + '"requestId":"' + Math.floor((Math.random() * 1000000000) + 10000) + '",' + '"requestName":"PUSH",' + '"source":"mobile",' + '"platform":"' + TiGlobals.osname + '",' + '"deviceid":"' + Ti.App.Properties.getString('deviceToken') + '",' + '"uId":"' + Ti.App.Properties.getString('ownerId') + '",' + '"acsuser":"' + deviceToken.substr(0, 90) + '",' + '"acspwd":"' + deviceToken.substr(0, 90) + '",' + '"acsid":"' + user.id + '",' + '"channelId":"' + TiGlobals.pushChannelId + '"' + '}',
								success : xmlHttpACSSuccess,
								error : xmlHttpACSError,
								contentType : 'application/json',
								timeout : 10000
							});

							function xmlHttpACSSuccess(eACS) {
								if (eACS.result.status === 'S') {
									loginIOSCloudUser();
								}
								xmlHttpACS = null;
							}

							function xmlHttpACSError(eACS) {
								require('/utils/Network').Network();
								xmlHttpACS = null;
							}

						} else {
							//require('/utils/Console').info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
						}
					});
				} catch(e) {
				}
			} else {
				loginIOSCloudUser();
			}
			xhr = null;
		}

		function xhrError(e) {
			require('/utils/Network').Network();
			xhr = null;
		}

	}

	//user login on cloud using default credential

	function loginIOSCloudUser() {
		Cloud.Users.login({
			login : deviceToken.substr(0, 90),
			password : deviceToken.substr(0, 90)
		}, function(e) {
			if (e.success) {
				var user = e.users[0];
				//require('/utils/Console').info("Loggin successfully");
				iosCloudUserSubscribe();
			} else {
				//require('/utils/Console').info("Error :" + e.message);
			}
		});
	}

	// Process incoming push notifications
	function receivePushiOS(evt) {
		require('/utils/Console').info('Received push: ' + JSON.stringify(evt));
		var result = JSON.parse(JSON.stringify(evt));

		setTimeout(function() {
			redirectPush(result);
		}, 1000);
	}

	// getting device token

	if (parseInt(Titanium.Platform.version) >= 8 && parseFloat(Ti.version) > 3.3) {

		//require('/utils/Console').info("=================== iOS8 detected ===================");

		Ti.App.iOS.addEventListener('usernotificationsettings', e = function() {

			Ti.Network.registerForPushNotifications({
				success : deviceTokenSuccess,
				error : deviceTokenError,
				callback : receivePushiOS
			});

			Ti.App.iOS.removeEventListener('usernotificationsettings', e);
		});

		Ti.App.iOS.registerUserNotificationSettings({
			types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
		});

	} else {

		//require('/utils/Console').info("=================== iOS7 detected ===================");

		Titanium.Network.registerForPushNotifications({
			types : [Titanium.Network.NOTIFICATION_TYPE_BADGE, Titanium.Network.NOTIFICATION_TYPE_ALERT, Titanium.Network.NOTIFICATION_TYPE_SOUND],
			success : deviceTokenSuccess,
			error : deviceTokenError,
			callback : receivePushiOS
		});
	}

	// Save the device token for subsequent API calls

	function deviceTokenSuccess(e) {
		deviceToken = e.deviceToken;
		Ti.App.Properties.setString('deviceToken', deviceToken);
		checkDeviceTokenIOS();
	}

	function deviceTokenError(e) {
		//require('/utils/Console').info('Failed to register for push notifications! ' + e.error);
	}

	// Subscribe for push notification on cloud server

	function iosCloudUserSubscribe() {
		Cloud.PushNotifications.subscribe({
			channel : 'Remit2India',
			type : 'ios',
			device_token : deviceToken
		}, function(e) {
			if (e.success) {
				//require('/utils/Console').info('Success:' + ((e.error && e.message) || JSON.stringify(e)));
			} else {
				//require('/utils/Console').info('Error:' + ((e.error && e.message) || JSON.stringify(e)));
			}
		});
	}

	// reset badge

	Cloud.PushNotifications.resetBadge({
		device_token : Ti.App.Properties.getString('deviceToken')
	}, function(e) {
		try {
			if (e.success) {
				//require('/utils/Console').info('Badge Reset!');
				Ti.UI.iPhone.setAppBadge(null);
			} else {
				//require('/utils/Console').info(e);
			}
		} catch(e) {
		}
	});
}