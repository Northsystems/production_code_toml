exports.session = function()
{
	if(TiGlobals.sessionCheck === 0)
	{
		TiGlobals.sessionCheck = 1; // Handle multiple dialogs
		
		Ti.App.Properties.setString('loginStatus','0');
		
		//Ti.App.Properties.setString('ownerId','0');
		//Ti.App.Properties.setString('loginId','0');
		//Ti.App.Properties.setString('sessionId','0');
		
		Ti.App.fireEvent('loginData');
		
		var alertDialog = require('/utils/AlertDialog').showAlert('', 'To ensure security of your information, your session has been timed-out. We regret the inconvenience caused and request you to login again to avail the service.', [L('btn_ok')]);
		alertDialog.show();
	
		alertDialog.addEventListener('click', function(e) {
			alertDialog.hide();
			setTimeout(function(){
				if(Ti.App.Properties.getString('mPIN') === '1')
				{
					require('/js/Passcode').Passcode();
				}
				else
				{
					require('/js/LoginModal').LoginModal();	
				}
			},200);
			
			TiGlobals.sessionCheck = 0; // Reset to 0
			require('/utils/RemoveViews').removeAllScrollableViews(); // Move to dashboard
			alertDialog=null;
		});
	}
};