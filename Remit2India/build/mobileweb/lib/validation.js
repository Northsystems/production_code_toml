exports.isNumeric = function (sText)
{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++) 
   { 
	  Char = sText.charAt(i); 
	  if (ValidChars.indexOf(Char) == -1) 
      {
     	IsNumber = false;
      }
   }
   return IsNumber;   
};

exports.isSpecial = function (strString)
{
   //var strValidChars = "~`!@#$%^&*()_+-={}[]|\:;'<>?,./ ";
   var strValidChars = "~`!@#$%^&*()+={}[]|\:;'<>?,/ ";  // Allow only .-_
   var strChar;
   var blnResult = true;

   if (strString.length == 0) return false;

   //  test strString consists of valid characters listed above
   for (var y = 0; y < strString.length && blnResult == true; y++)
   {
     	strChar = strString.charAt(y);
     	if (!(strValidChars.indexOf(strChar) == -1))
         	{
         		blnResult = false;
         	}
   }
   return blnResult;
};

exports.isText = function (txt)
{
	var IsText = true;
	var re = /^[A-Za-z ]+$/;
	if(re.test(txt))
	{
		return IsText;
	}
	else
	{
		IsText = false;
		return IsText;
	}
};

exports.isAlphaNum = function (txt)
{
	var IsText = true;
	var re = /^[A-Za-z0-9 ]{3,20}$/;
	if(re.test(txt))
	{
		return IsText;
	}
	else
	{
		IsText = false;
		return IsText;
	}
};

exports.isEmail = function (str)
{
	var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	var IsEmail = true;
	
	if (filter.test(str))
	{
		return IsEmail;
	}
	else
	{
		IsEmail = false;
		return IsEmail;
	}
};

exports.isPwd = function (str)
{
	var filter = /^\w*(?=\w*\d)(?=\w*[a-z])\w*$/;
	var IsPwd = true;
	if(str.length < 6 || str.length >= 15)
	{
		IsPwd = false;
		return IsPwd;
	}
	else if (filter.test(str))
	{
		return IsPwd;
	}
	else
	{
		IsPwd = false;
		return IsPwd;
	}
};