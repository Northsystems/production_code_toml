//Common validations for TOML 

//This function is use for trim
exports.trim = function(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
};

exports.ltrim = function(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
};

exports.rtrim = function(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
};
			
String.prototype.trimLeft = function(charlist) {
	if (charlist === undefined)
    	charlist = "\s"; 
	return this.replace(new RegExp("^[" + charlist + "]+"), "");
};

String.prototype.trimRight = function(charlist) {
	if (charlist === undefined)
    	charlist = "\s";
	return this.replace(new RegExp("[" + charlist + "]+$"), "");
};

String.prototype.trim = function(charlist) {
	return this.trimLeft(charlist).trimRight(charlist);
};

//This function use for check valid email id or not
exports.validateEmail = function(email) 
{
	if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
	{
		return true;
	}
	else
	{
		return false;
	}
};

//This function use for character count
exports.charCounter = function(value, maxlimit, counter_id, id)
{
	var val = parseInt(value.length);
	if(maxlimit >= val)
	{
		document.getElementById(counter_id).innerHTML = parseInt(value.length);
	}
	else
	{
		document.getElementById(counter_id).innerHTML = maxlimit;
		document.getElementById(id).value = value.substr(0,maxlimit);
	}
};

exports.charCounterOther = function(value, maxlimit,counter_id, id)
{
	var val = parseInt(value.length);
	if(maxlimit >= val)
	{
		//document.getElementById(counter_id).innerHTML = parseInt(value.length);
	}
	else
	{
		document.getElementById(counter_id).innerHTML = maxlimit;
		document.getElementById(id).value = value.substr(0,maxlimit);
	}
};

//This function is use for character validation. for eaxample only alpha, numiric etc
var onlyalpha = "abcdefghijklmanopqrstuvwxyzABCDEFGHIJKLMANOPQRSTUVWXYZ";
var alphawithspace = "abcdefghijklmanopqrstuvwxyzABCDEFGHIJKLMANOPQRSTUVWXYZ ";
var number = "0123456789";

exports.charValidation = function(t,v)
{
	var w = "";
	for(i=0; i < t.length; i++)
	{
		x = t.charAt(i);
		if(v.indexOf(x,0) != -1)
		w += x;
	}
	t = w;
	return t;
};

//This is function for remove white spaces
var whitespace = " \t\n\r";
exports.stripWhitespace = function(s){
	return stripCharsInBag(s, whitespace);
};

// Removes all characters which appear in string bag from string s.
exports.stripCharsInBag = function(s, bag)
{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
};

exports.isWhitespace = function(s)
{
	var i;
    if (isEmpty(s)) return true;
    for(i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if(whitespace.indexOf(c) == -1) return false;
    }
    return true;
};

exports.isEmpty = function(s)
{
	return ((s == null) || (s.length == 0));
};

exports.trueRound = function(value, digits){
    return (Math.round((value*Math.pow(10,digits)).toFixed(digits-1))/Math.pow(10,digits)).toFixed(digits);
};

exports.checkBeginningSpace = function(textbox)
{
	chkString = escape(textbox);
	if(chkString.substring(0,3)=="%20")
	return false;
};

exports.checkinLastSpace = function(textbox)
{
	chkString = escape(textbox.value);
	if(chkString.substring(chkString.length-3,chkString.length+1)=="%20")
	return false;
};

exports.checkSpaceNumber = function(textbox,id,msg)
{
	chkString = escape(textbox.value);
	var v=0;
	for(i=3;i<=chkString.length-2;i++)
	{
		if(chkString.charAt(i)=="0")
		{
			v = v+1;
		}				
	}
	if(v > 1)
	{
		//printMsg("Only one space allowed in "+msg,textbox);
		document.getElementById(id+"_box").innerHTML = "Only one space allowed in "+msg;
		document.getElementById(id+"_box").className = 'error_text showDiv';
		return false;
	}
};

exports.validateTextField = function(textBox)
{
	var fieldValue = textBox;
	var ln = fieldValue.length;
	for(var i=0;i<ln;i++) 
	{
		if((fieldValue.charAt(i)=='|') || (fieldValue.charAt(i)=='\'') || (fieldValue.charAt(i)=='"') || (fieldValue.charAt(i)=='#') || fieldValue.charCodeAt(i)=='96') 
		{
			return false;
		}
	}
};

exports.validate_allow_special_char = function(textbox)
{
	var amtinv = textbox;
	var validStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,. ";
	if(amtinv.length > 0)
	{
		for(i=0;i<=amtinv.length-1;i++)
		{
			if(validStr.indexOf(amtinv.charAt(i))==-1)
			{
    			return false;
			}
		}
	}
};

exports.validate_special_char = function (textbox)
{
	var amtinv = textbox;
	var validStr = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.0123456789/-?(),\"+\n\r";
		  
	for(i=0;i<=textbox.length-1;i++)
	{
	  	if(validStr.indexOf(amtinv.charAt(i))==-1)
	  	{
	   		return false;
	   		break;
	  	}
	 }
};

exports.validateStringValueOnly = function(textbox)
{
	var amtinv = textbox;
	var validStr = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
	  
	for(i=0;i<=textbox.length-1;i++)
	{
		if(validStr.indexOf(amtinv.charAt(i))==-1)
		{
			return false;
		}
	}
};

exports.validateAlphaNumAtleastOneChar = function(textfield)
{
	var validStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ";
	var validCharStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	  
	for(i=0;i<=textfield.length-1;i++)
	{
		if(validStr.indexOf(textfield.charAt(i))===-1)
		{
			return false;
			break;
		}
	}
};

//function checks for Numeric Value Entry with Apropriate Field Msg
exports.validate_numeric_value_with_msg = function(textbox)
{
	if(isNaN(textbox))
	{
		return false;
	}
};

/***********************/
exports.validateSpacialCharacterOnly = function(textBox,msg)
{
	var fieldValue = textBox;
	var ln = fieldValue.length;
	for(var i=0;i<ln;i++) 
	{
		if((fieldValue.charAt(i)=='|') || (fieldValue.charAt(i)=='\'') || (fieldValue.charAt(i)=='"') || (fieldValue.charAt(i)=='#') || fieldValue.charCodeAt(i)=='96'){		
		}
	}
};


exports.validatePincode = function(pincode)
{
	var pin = pincode;
	if(isEmpty(stripWhitespace(pin))==false)
	{
		len = pin.length;
		for(x=0;x<len;x++) 
		{
			if( ((pin.charAt(x)<'0') || (pin.charAt(x)>'9'))&& ((pin.charAt(x)<'a') || (pin.charAt(x)>'z')) && ((pin.charAt(x)<'A') || (pin.charAt(x)>'Z'))&&(pin.charAt(x)!=' ') )
			{
				printMsg("Please enter a valid Pincode",pincode);
				break;
			}
		}
	}
};
			
exports.getnMonth = function(mm) 
{
	var nMonth;		
	if((mm=="JAN") || (mm=="January")) nMonth="1";
	else  if((mm=="FEB") || (mm=="February")) nMonth="2";
	else  if((mm=="MAR") || (mm=="March")) nMonth="3";
	else  if((mm=="APR") || (mm=="April")) nMonth="4";
	else  if((mm=="MAY") || (mm=="May")) nMonth="5";
	else  if((mm=="JUN") || (mm=="June")) nMonth="6";
	else  if((mm=="JUL") || (mm=="July")) nMonth="7";
	else  if((mm=="AUG") || (mm=="August")) nMonth="8";
	else  if((mm=="SEP") || (mm=="September")) nMonth="9";
	else  if((mm=="OCT") || (mm=="October")) nMonth="10";
	else  if((mm=="NOV") || (mm=="November")) nMonth="11";
	else  if((mm=="DEC") || (mm=="December")) nMonth="12";
	
	return nMonth;
};

exports.getMonthName = function(mm) 
{	
	var MonthName;
	
	if((mm=="1") || (mm=="01")) MonthName="January";
	else  if((mm=="2") || (mm=="02")) MonthName="February";
	else  if((mm=="3") || (mm=="03")) MonthName="March";
	else  if((mm=="4") || (mm=="04")) MonthName="April";
	else  if((mm=="5") || (mm=="05")) MonthName="May";
	else  if((mm=="6") || (mm=="06")) MonthName="June";
	else  if((mm=="7") || (mm=="07")) MonthName="July";
	else  if((mm=="8") || (mm=="08")) MonthName="August";
	else  if((mm=="9") || (mm=="09")) MonthName="September";
	else  if((mm=="10")) MonthName="October";
	else  if((mm=="11")) MonthName="November";
	else  if((mm=="12")) MonthName="December";

	return MonthName;
};

exports.isEmail = function(email) { 
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
};

exports.validate_integer_value = function(textbox)
{
 	var fldlen = textbox.length;
 	for(i=0;i<=fldlen-1;i++)
 	{
  		if(textbox.charAt(i) == ".")
  		{
  			return false;
  		}
 	}
};

exports.validate_ispositive = function(textbox)
{
	if(parseFloat(textbox) <= parseFloat("0"))
	{
		return false;
	}
};

exports.validateAlpaNumericSpecialChars = function(textbox,msg)
{
	var thisStr = textbox;
	var validStr = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-()_,:./& ";
	  
	for(i=0;i<=thisStr.length-1;i++)
	{
		if(validStr.indexOf(thisStr.charAt(i))==-1)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+thisStr.charAt(i)+' entered in '+msg,[L('btn_ok')]).show();
			return false;
			break;
		}
	}
};

exports.validateAlpaNumeric = function(textbox,msg)
{
	var amtinv = textbox;
	var validStr = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890- ";
	  
	for(i=0;i<=textbox.value.length-1;i++)
	{
		if(validStr.indexOf(amtinv.charAt(i))==-1)
		{
			require('/utils/AlertDialog').showAlert('','Only alphabets and numbers are allowed in '+msg,[L('btn_ok')]).show();
			return false;
			break;
		}
	}
};

exports.customValidation = function(textbox,msg,regEx)
{
	var thisStr = textbox;
	var validStr = regEx;
	  
	for(i=0;i<=thisStr.length-1;i++)
	{
		if(validStr.indexOf(thisStr.charAt(i))==-1)
		{
			require('/utils/AlertDialog').showAlert('','Invalid character '+thisStr.charAt(i)+' entered in '+msg,[L('btn_ok')]).show();
			return false;
			break;
		}
	}
};

exports.removeHTMLTags = function(input){
	var strInputCode = input;
	strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1){
		return (p1 == "lt")? "<" : ">";
	});
	var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
	return strTagStrippedText;		
};

exports.splitEmail = function(val)
{
	var maillist = val;
	var splittedmaillist = new Array();
	splittedmaillist = maillist.split(",");
	
	for(i=0;i<splittedmaillist.length;i++)
	{
		useremail = splittedmaillist[i].trim();
		
		var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    	if(re.test(useremail) === false)
		{
			return false;
			break;
		}
	}
	return true;
};


//Twitter Short URL
exports.twitterURL = function(twitterMessage,socialLink)
{	
	var generic_access_token = '0d218c9133628cf75c9d2b5fb3dce5788e29e011';
	var url_to_shorten = socialLink;
	var url_encoded = encodeURI(url_to_shorten);
	var text = twitterMessage;
	
	activityIndicator.showIndicator();
	
	var xmlhttp = Ti.Network.createHTTPClient();
			
	xmlhttp.onload = function()
	{
		activityIndicator.hideIndicator();
		
		Ti.API.info(xmlhttp.responseText);
		
		var resp = JSON.parse(xmlhttp.responseText);
		
		twitter.share({
	        message: twitterMessage + '(' + resp.data.url + ')',
	        url : resp.data.url,
	        success: function() {
	            if(TiGlobals.osname === 'android')
				{
					require('/utils/AlertDialog').toast('Tweet posted');
				}
				else
				{
					require('/utils/AlertDialog').iOSToast('Tweet posted');
				}
	        },
	        error: function(error) {
	        	var error = JSON.parse(error);
	        	require('/utils/AlertDialog').showAlert('Twitter',error.errors[0].message,['OK']).show();
	        }
	    });
		
		xmlhttp = null;
	};

	xmlhttp.onerror = function(e)
	{
		activityIndicator.hideIndicator();
		require('/utils/Network').Network();
		xmlhttp = null;
	};
	
	xmlhttp.open('GET', "https://api-ssl.bitly.com/v3/shorten?access_token="+generic_access_token+"&longUrl="+url_encoded,true);
	xmlhttp.send();

};