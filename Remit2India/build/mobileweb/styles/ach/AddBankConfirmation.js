/*global exports,require*/
var _ = require('/utils/Underscore');

var stepView = {
	top:40,
	bottom:0,
	height:Ti.UI.SIZE,
	left:0,
	right:0,
	layout:'vertical',
	scrollType:'vertical',
	showVerticalScrollIndicator:true
};

var button = {
	top:15,
	left:20,
	right:20,
	height:40,
	bottom:10,
	textAlign:'center',
	font:TiFonts.FontStyle('lblBold14'),
	color:TiFonts.FontStyle('whiteFont'),
	backgroundColor:'#ed1b24',
	borderRadius:15
};

var imgRadio = {
	image:'/images/radio_unsel.png',
    width:40,
    height:40
};

exports.AddBankConfirmation = {
	winAddBankConfirmation : {
		top:TiGlobals.osname === 'android' ? 0 : 20,
		backgroundColor:'#FFF',
		fullscreen:false,
		navBarHidden:true
	},
	globalView : {
		top:0,
		bottom:0,
		left:0,
		right:0,
		backgroundColor:'#FFF',
		zIndex:100
	},
	mainView : {
		top:0,
		bottom:0,
		height:Ti.UI.SIZE,
		left:0,
		right:0
	},
	headerView : {
		top:0,
		left:0,
		right:0,
		height:40
	},
	lblHeader : {
		height:40,
    	width:Ti.UI.SIZE,
    	textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    	font:TiFonts.FontStyle('lblSwissBold16'),
		color:TiFonts.FontStyle('greyFont')
	},
	imgClose : {
		right:0,
		image:'/images/close.png',
	    width:40,
	    height:40
	},
	headerBorder : {
		top:39,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#eee'
	},
	
	addBankConfirmationView : _.defaults({}, stepView),
	lbl1 : {
		top:10,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	lbl2 : {
		top:10,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	lbl3 : {
		top:10,
		left:20,
		right:20,
		height:Ti.UI.SIZE,
		textAlign:'left',
		font:TiFonts.FontStyle('lblNormal12'),
		color:TiFonts.FontStyle('greyFont'),
	},
	tableView : {
		top:0,
		left:0,
		height:336,
		right:0,
		scrollable:false,
		showVerticalScrollIndicator:false,
		tableSeparatorInsets:{left:0,right:0},
		separatorColor:'#eee',
		backgroundColor:'transparent'
	},
	btnSubmit : _.defaults({}, button),
	addBankConfirmationFinalView : _.defaults({}, stepView),
	
	webView : {
		top:10,
		left:0,
		right:0,
		height:Ti.UI.SIZE,
		enableZoomControls:false,
		scalesPageToFit:true,
		disableBounce:true
	},
	
	borderView : {
		top:0,
		left:0,
		right:0,
		height:1,
		backgroundColor:'#acacac'
	},
	done : {
		title:'Done',
		height:35,
		width:50
	}
};